groups['projects_runtime'] =
{
	liveemu = 
	{
		name = "liveemu",
		kind = c_projectKindWindowedApp,
		srcPath = 
		{
			c_src_root.."frontend/application/**.hpp",
			c_src_root.."frontend/application/**.cpp",
		},
		includedirs = { c_src_root.."frontend/application" },
		targetname_Debug32 = "liveemu_x86D",
		targetname_Release32 = "liveemu_x86",
		targetname_Debug64 = "liveemu_x64D",
		targetname_Release64 = "liveemu_x64",
		flags { "WinMain" },
		defines = nil,
		dependencies = 
		{
			{"projects_modules", "emulator"},
			{"projects_modules", "frontend"},
			{"projects_modules", "renderer"},
			{"projects_modules", "webviewer"},
			{"projects_modules", "ui"},
			{"projects_modules", "scraping"},
			{"projects_modules", "common"},
		},
	},
	testEmulator = 
	{
		name = "testEmulator",
		kind = c_projectKindWindowedApp,
		srcPath = 
		{
			c_src_root.."emulator/application/**.hpp",
			c_src_root.."emulator/application/**.cpp",
		},
		includedirs = { c_src_root.."emulator/application" },
		targetname_Debug32 = "testEmulator_x86D",
		targetname_Release32 = "testEmulator_x86",
		targetname_Debug64 = "testEmulator_x64D",
		targetname_Release64 = "testEmulator_x64",
		flags { "WinMain" },
		defines = nil,
		dependencies = 
		{
			{"projects_modules", "emulator"},
			{"projects_modules", "frontend"},
			{"projects_modules", "renderer"},
			{"projects_modules", "ui"},
			{"projects_modules", "scraping"},
			{"projects_modules", "common"},
		},
	}
}