newoption 
{
	trigger		= "vs_express",
	description	= "Build solution with Express limitation (no folders)"
}

newoption
{
	trigger		= "target_platform",
	description = "Build solution for a specified platform.",
	allowed = 
	{
		{	"windows",	"Windows platform"	},
		{	"linux",	"Linux platform" },
		{	"mac",		"Apple Mac Os platform" }
	}
}

newoption
{
	trigger		= "projects",
	description = "Choose which project to generate.",
	allowed = 
	{
		{	"all",				""	},
		{	"internal",			"Runtime program and dependencies"	},
		{	"externals",		"External libraries"	},
		
		{	"emulator_mame",	"Emulator Mame (Arcade)" },
		{	"emulator_demul",	"Emulator Demul (Sega Dreamcast, Naomi, Atomiswave)" },
		{	"emulator_dolphin",	"Emulator Dolphin (Nintendo Gamecube, Wii)" },
		{	"emulator_yabause",	"Emulator Yabause (Sega Saturn)" }
	}
}

c_src_root = "../../src/"
c_externals_root = "../../../liveemu_externals/"
c_workspaceName = "Liveemu"

dofile('../premake/common.lua')
dofile('../premake/projects_externals.lua')
dofile('../premake/projects_libraries.lua')
dofile('../premake/projects_modules.lua')
dofile('../premake/projects_runtime.lua')
dofile('../premake/projects_editor.lua')
dofile('../premake/projects_emulator_mame.lua')

function GenerateProjects(config)
	if _OPTIONS["projects"]=="internal" then
		-- startproject "frontend"
		
		GenerateGroupProjects(config, "projects_editor")
		GenerateGroupProjects(config, "projects_runtime")
		GenerateGroupProjects(config, "projects_modules")
		
	elseif _OPTIONS["projects"]=="all" then
		-- startproject "frontend"
		
		GenerateGroupProjects(config, "projects_externals")
		GenerateGroupProjects(config, "projects_libraries")
		GenerateGroupProjects(config, "projects_modules")
		GenerateGroupProjects(config, "projects_runtime")
		GenerateGroupProjects(config, "projects_editor")
		
	elseif _OPTIONS["projects"]=="externals" then
		
		GenerateGroupProjects(config, "projects_libraries")
		GenerateGroupProjects(config, "projects_externals")
		
	elseif _OPTIONS["projects"]=="emulator_mame" then
		startproject "mame"
		GenerateGroupProjects(config, "projects_emulator_mame")
	end
end

configurations { "Debug32", "Release32", "Debug64", "Release64", "EmulatorDebug32", "EmulatorRelease32" }
c_workspaceName = "Liveemu_".._OPTIONS["projects"]

function make()
	solution(c_workspaceName)
	IsEmulatorBuild = false
	
	GenerateProjects("Debug32")
	GenerateProjects("Release32")
	GenerateProjects("Debug64")
	GenerateProjects("Release64")
	
	IsEmulatorBuild = true
	GenerateProjects("EmulatorDebug32")
	GenerateProjects("EmulatorRelease32")
end