groups['projects_editor'] =
{
	editor = 
	{
		name = "editor",
		kind = c_projectKindWindowedApp,
		srcPath = 
		{
			c_src_root.."editor/application/**.hpp",
			c_src_root.."editor/application/**.cpp",
		},
		includedirs = { c_src_root.."editor/application" },
		targetname_Debug32 = "editor_x86D",
		targetname_Release32 = "editor_x86",
		targetname_Debug64 = "editor_x64D",
		targetname_Release64 = "editor_x64",
		flags { "WinMain" },
		defines = nil,
		dependencies = 
		{
			{"projects_modules", "emulator"},
			{"projects_modules", "frontend"},
			{"projects_modules", "renderer"},
			{"projects_modules", "webviewer"},
			{"projects_modules", "ui"},
			{"projects_modules", "scraping"},
			{"projects_modules", "common"},
		},
	}
}