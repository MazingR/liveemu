groups['projects_modules'] =
{
	backend = 
	{
		name = "module_backend",
		kind = c_projectKindStaticLib,
		dependencyInclude = { c_src_root.."frontend/modules" },
		srcPath = 
		{
			c_src_root.."frontend/modules/backend/**.h",
			c_src_root.."frontend/modules/backend/**.c",
		},
		includedirs = { c_src_root.."frontend/modules/backend" },
		defines = {"FE_D3D9x"},
		targetname = "backend",
		dependencies = 
		{
			{"projects_externals", "directx"},
			{"projects_externals", "pthreads"},
		},
	},
	common = 
	{
		name = "module_common",
		kind = c_projectKindStaticLib,
		dependencyInclude = { c_src_root.."frontend/modules" },
		srcPath = 
		{
			c_src_root.."frontend/modules/common/**.hpp",
			c_src_root.."frontend/modules/common/**.cpp",
		},
		includedirs = { 
			c_src_root.."frontend/modules/common",
			c_externals_root.."libraries/common/gmtl/include",
			c_externals_root.."libraries/common/rapidjson/include"},
		targetname = "common",
		defines = { "CURL_STATICLIB", "HTTP_ONLY", "USE_MBEDTLS" },
		dependencies = 
		{
			{"projects_libraries", "sdl"},
			{"projects_libraries", "freetype"},
			{"projects_libraries", "freeimage"},
			{"projects_libraries", "sqllite"},
			{"projects_libraries", "curl"},
		},
	},
	renderer = 
	{
		name = "module_renderer",
		kind = c_projectKindStaticLib,
		dependencyInclude = { c_src_root.."frontend/modules" },
		srcPath = 
		{
			c_src_root.."frontend/modules/rendering/**.hpp",
			c_src_root.."frontend/modules/rendering/**.cpp",
			c_src_root.."../build/data/themes/common/**.fx",
		},
		links = { "d3d11.lib", "d3dx11.lib", "d3dx11d.lib", "d3dx9.lib", "d3dx9d.lib", "dxguid.lib" },
		includedirs = { c_src_root.."frontend/modules/rendering", c_externals_root.."libraries/windows/directx/include" },
		defines = nil,
		targetname = "renderer",
		dependencies = 
		{	
			{"projects_modules", "common"},
			{"projects_externals", "directx"},
			{"projects_externals", "fontwrapper"},
			{"projects_libraries", "imgui"},
		},
	},
	ui = 
	{
		name = "module_ui",
		kind = c_projectKindStaticLib,
		dependencyInclude = { c_src_root.."frontend/modules" },
		srcPath = 
		{
			c_src_root.."frontend/modules/ui/**.hpp",
			c_src_root.."frontend/modules/ui/**.cpp",
		},
		includedirs = { c_src_root.."frontend/modules/ui" },
		defines = nil,
		targetname = "ui",
		dependencies = 
		{
			{"projects_modules", "renderer"},
		},
	},
	webviewer = 
	{
		name = "module_webviewer",
		kind = c_projectKindStaticLib,
		dependencyInclude = { c_src_root.."frontend/modules" },
		srcPath = 
		{
			c_src_root.."frontend/modules/webviewer/**.hpp",
			c_src_root.."frontend/modules/webviewer/**.cpp",
		},
		includedirs = { c_src_root.."frontend/modules/webviewer" },
		defines = nil,
		targetname = "webviewer",
		dependencies = 
		{
			{"projects_externals", "awesomium"},
			{"projects_modules", "renderer"},
		},
	},
	emulator = 
	{
		name = "module_emulator",
		kind = c_projectKindStaticLib,
		dependencyInclude = { c_src_root.."frontend/modules" },
		srcPath = 
		{
			c_src_root.."frontend/modules/emulator/**.hpp",
			c_src_root.."frontend/modules/emulator/**.cpp",
		},
		includedirs = { c_src_root.."frontend/modules/emulator" },
		defines = nil,
		targetname = "emulator",
		dependencies = 
		{
			{"projects_externals", "directx"},
			{"projects_modules", "common"},
		},
	},
	frontend = 
	{
		name = "module_frontend",
		kind = c_projectKindStaticLib,
		dependencyInclude = { c_src_root.."frontend/modules" },
		srcPath = 
		{
			c_src_root.."frontend/modules/frontend/**.hpp",
			c_src_root.."frontend/modules/frontend/**.cpp",
		},
		includedirs = { c_src_root.."frontend/modules/frontend" },
		defines = nil,
		targetname = "frontend",
		dependencies = 
		{
			{"projects_modules", "emulator"},
			{"projects_modules", "renderer"},
			{"projects_modules", "webviewer"},
			{"projects_modules", "ui"},
			{"projects_modules", "scraping"},
			{"projects_modules", "common"},
		},
	},
	scraping = 
	{
		name = "module_scraping",
		kind = c_projectKindStaticLib,
		dependencyInclude = { c_src_root.."frontend/modules" },
		srcPath = 
		{
			c_src_root.."frontend/modules/scraping/**.hpp",
			c_src_root.."frontend/modules/scraping/**.cpp",
		},
		includedirs = { c_src_root.."frontend/modules/scraping" },
		defines = nil,
		targetname = "scraping",
		dependencies = 
		{
			{"projects_modules", "common"},
		},
	}
}