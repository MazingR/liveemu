groups['projects_externals'] =
{
	fontwrapper = 
	{
		kind = c_projectKindExternal,
		dependencyInclude = { c_externals_root.."libraries/windows/fw1fontwrapper/include" },
		dependencyLibDir_Debug32   = { c_externals_root.."libraries/windows/fw1fontwrapper/lib/x86" },
		dependencyLibDir_Release32 = { c_externals_root.."libraries/windows/fw1fontwrapper/lib/x86" },
		dependencyLibDir_Debug64   = { c_externals_root.."libraries/windows/fw1fontwrapper/lib/x64" },
		dependencyLibDir_Release64 = { c_externals_root.."libraries/windows/fw1fontwrapper/lib/x64" },
		targetname = "FW1FontWrapper",
	},
	directx = 
	{
		kind = c_projectKindExternal,
		dependencyInclude = { c_externals_root.."libraries/windows/directx/include" },
		dependencyLibDir_Debug32   = { c_externals_root.."libraries/windows/directx/lib/x86" },
		dependencyLibDir_Release32 = { c_externals_root.."libraries/windows/directx/lib/x86" },
		dependencyLibDir_Debug64   = { c_externals_root.."libraries/windows/directx/lib/x64" },
		dependencyLibDir_Release64 = { c_externals_root.."libraries/windows/directx/lib/x64" },
		targetname = "d3d11",
	},
	awesomium = 
	{
		kind = c_projectKindExternal,
		dependencyInclude = 
		{
			c_externals_root.."libraries/common/awesomium/include",
		},
		dependencyLibDir = { c_externals_root.."libraries/common/awesomium/build/lib" },
		targetname = "awesomium",
	},
	pthreads = 
	{
		kind = c_projectKindExternal,
		dependencyInclude = { c_externals_root.."libraries/common/pthreads/include" },
		dependencyLibDir = { c_externals_root.."libraries/common/pthreads/lib/x86" },
		targetname = "pthreads",
	}
}