c_projectKindConsoleApp	="ConsoleApp"
c_projectKindWindowedApp="WindowedApp"
c_projectKindSharedLib	="SharedLib"
c_projectKindStaticLib	="StaticLib"
c_projectKindExternal	="External"
c_generateDir = "projects"

groups = {}

function ProjectHas(_project, tag)
	if	_project[tag]  ~= nil then
		return true
	else
		return false
	end
end

function GetProjectTargetDir(config, prj)
	if prj["kind"] == c_projectKindConsoleApp or prj["kind"] == c_projectKindWindowedApp then
		return "../../build/bin/"..config.."/"
	elseif prj["kind"] == c_projectKindExternal then
		if ProjectHas(prj, "dependencyLibDir_"..config) then
			return prj["dependencyLibDir_"..config]
		else
			return prj["dependencyLibDir"]
		end
	else
		return "../../build/bin/"..config.."/libraries/"
	end
end

function GetProjectTargetFullName(config, prj)
	if prj["language"]=="C#" then
		return prj["targetname"]
	end
	
	local targetConfig = "targetname_"..config
	local extension = ""
	
	if prj["kind"] == c_projectKindConsoleApp or prj["kind"] == c_projectKindWindowedApp then
		extension = ".exe"
	elseif prj["kind"] == c_projectKindStaticLib then
		extension = ".lib"
	elseif prj["kind"] == c_projectKindSharedLib then
		extension = ".dll"
	end
	
	
	if ProjectHas(prj, targetConfig) then
		return prj[targetConfig]..extension
	else
		return prj["targetname"]..extension
	end
end

function GetProjectTargetName(config, prj)
	if prj["language"]=="C#" then
		return prj["targetname"]
	end
	
	local targetConfig = "targetname_"..config
	
	if ProjectHas(prj, targetConfig) then
		return prj[targetConfig]
	else
		return prj["targetname"]
	end
end

function ProcessDependencies(config, prj)
	if ProjectHas(prj, "dependencies") then 
		local dependencies = prj["dependencies"]
		for i = 1, #dependencies do
			local dependency = groups[ dependencies[i][1] ] [ dependencies[i][2] ]
			
			dependson { dependency["name"] }
			
			if ProjectHas(dependency, "dependencyInclude") then includedirs { dependency["dependencyInclude"] }	end
			if ProjectHas(dependency, "includedirs") then 		includedirs  { dependency["includedirs"] }		end
			if ProjectHas(dependency, "links") then 			links  { dependency["links"] }					end
			if ProjectHas(dependency, "libdirs") then 			libdirs  { dependency["libdirs"] }				end
			
			if dependency["kind"]~=c_projectKindExternalDotNet then
				links  {dependency["targetname"], GetProjectTargetFullName(config, dependency) }
				libdirs { GetProjectTargetDir(config, dependency) }
			end
			
			ProcessDependencies(config, dependency)
		end
	end
end

function GenerateProject(config, groupName, projectName)
	local prj = groups[groupName][projectName]
	
	if prj["kind"]~=c_projectKindExternal and prj["kind"]~=c_projectKindExternalDotNet then
		configuration "Debug32 or Release32 or EmulatorDebug32 or EmulatorRelease32"
			architecture "x32"
		configuration "Debug64 or Release64 or EmulatorDebug64 or EmulatorRelease64"
			architecture "x64"
		configuration "Debug32 or Debug64 or EmulatorDebug32 or EmulatorDebug64"
			defines {"DEBUG", "_DEBUG"}
		configuration "Release32 or Release64 or EmulatorRelease32 or EmulatorRelease64"
			defines {"NDEBUG"}
			flags {"OptimizeSpeed", "FloatFast", "EnableSSE2"}
				
		configuration ""
			if ProjectHas(prj, "name")			then project (prj["name"]) 				end
			location(c_generateDir)
			
		if ProjectHas(prj, "language") then 
			language(prj["language"])
		else
			language "C++"
		end
		-- characterset("MBCS")
		
		configuration { config }
			flags("Symbols")
			
			if ProjectHas(prj, "kind")			then kind (prj["kind"])					end
			if ProjectHas(prj, "srcPath")		then files { prj["srcPath"] }			end
			if ProjectHas(prj, "includedirs")	then includedirs { prj["includedirs"] }	end
			if ProjectHas(prj, "defines")		then defines{ prj["defines"] }			end
			if ProjectHas(prj, "links")			then links(prj["links"])				end
			if ProjectHas(prj, "excludes")		then excludes(prj["excludes"])			end
			if ProjectHas(prj, "libdirs")		then libdirs(prj["libdirs"])			end
			if ProjectHas(prj, "linkoptions")	then linkoptions(prj["linkoptions"])	end
			
			if prj["kind"] == c_projectKindStaticLib then
				linkoptions {"/NODEFAULTLIB"}
			end
			
			-- buildoptions {"/MTd"}
			flags { "StaticRuntime" }
			targetname (GetProjectTargetName(config, prj))
			ProcessDependencies(config, prj)		
			targetdir(GetProjectTargetDir(config, prj))
			
			if IsEmulatorBuild then
				defines { "FE_NO_CUSTOM_ALLOCATOR", "HAVE_LIBC" }
			end
	end
end

function GenerateGroupProjects(config, groupName)
	local groupData = groups[groupName]
	for projectName,projectData in pairs(groupData) do
		print ("["..config.."] : "..projectName)
		GenerateProject(config, groupName, projectName)
	end
end