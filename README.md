# Overview

* **Media Center like user interface with intuitive, fluid, and classy design to browse through games** (displays artowrks, boxArts,...)
* **Native gamepad and arcade sticks support** (user interface is designed to be usable in a TV use case, uses Xbox guide buttons for special commands)
* **Meta Configurator**
 * Intelligent config files override (detects config files, saves most of the settings and is able to fetch back the settings values after emulator run)
 * Enables centralised gamepad configuration
 * Meta settings applied for all emulators (fullscreen, resolution, ...)
 * Intelligent game configuration (provides simple way to save game specific configuration)
 * Complex emulator launch process (for instance : mounting an iso on drive for sega saturn emulator)
 * Enables centralised runtime commands (for instance game state save & load when emulator supports it, in order to create auto saves, or saves on quit)
 * etc...
* **Several ways to browse the games**
 * By platform
 * By Genres
 * By maximum local players
 * Through user created playlists (all the menus and filters are easily customisable through the user interface script)
* **Internal user interface script engine**
 * provides easy way to customize ui (menus, list games, animations,...)
 * provides easy to customize ui commands (display filtered list, display menu, display options, etc...)
 * script is editable at runtime with instance update
 * the script engine is designed to enable theme like content with specific user experience for each not limited to ui colors
