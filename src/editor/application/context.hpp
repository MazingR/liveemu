#pragma once

#include <commoneditor.hpp>
#include <ui/widget.hpp>
#include <ui/uiscriptfile.hpp>

struct ImFont;
struct FeRenderBatch;
class FeEditorViewportController;
struct FeRenderGeometryInstance;
class FeUiUserMouseInputHandler;
class FeEditorContext;
class FeEditorComponent;

typedef std::function<void(FeEditorContext& context)> FePopupDrawCallback;
typedef std::function<void(bool bCanceled)> FePopupCloseCallback;

#define FeEContentView_Values(_d)	\
		_d(FeEContentView, List)	\
		_d(FeEContentView, Thumb)	\

FE_DECLARE_ENUM(FeEContentView, FeEContentView_Values)

namespace FeEEditorMode
{
	enum Type
	{
		Edit		= 1 << 0,
		Play		= 1 << 1,
		Sequencer	= 1 << 2,
	};
}

#define FeEEditorToggle_Values(_d)		 \
_d(FeEEditorToggle, Grid				)\
_d(FeEEditorToggle, GridLock			)\
_d(FeEEditorToggle, NormalizedValues	)\
_d(FeEEditorToggle, SchematicView		)\
_d(FeEEditorToggle, None		)\

FE_DECLARE_ENUM(FeEEditorToggle, FeEEditorToggle_Values)

class FeEditorToggleEntry : public FeSerializable
{
public:
#define FeEditorToggleEntry_Properties(_d)	\
			_d(bool,					bState)		\
			_d(FeEEditorToggle::Type,	Type)		\

	FeEditorToggleEntry(bool bDefaultState) : bState(bDefaultState)
	{}
	FeEditorToggleEntry() : FeEditorToggleEntry(false)
	{}

	void Switch()
	{
		bState = !bState;
	}

	FE_DECLARE_CLASS_BODY(FeEditorToggleEntry_Properties, FeEditorToggleEntry, FeSerializable)
};

struct FeEditorWidgetDeletion
{
	FeWidgetPath ParentPath;
	FeWidget* Widget;

	FeEditorWidgetDeletion() : Widget(nullptr){}
};

struct FeEditorDragSubContext
{
	FeEditorComponent*	HoveredComponent;
	FeVector2			Position;

	void Reset()
	{
		HoveredComponent = nullptr;
	}
};

struct FeEDragContextState
{
	enum Type
	{
		Idle,
		Start,
		Drag,
		Drop,
	};
};
struct FeEditorDragContext
{
	FeEditorDragSubContext		Start;
	FeEditorDragSubContext		End;
	FeAssetFile*				Data;
	FeEDragContextState::Type	State;

	void Reset()
	{
		Data = nullptr;
		State = FeEDragContextState::Idle;
		Start.Reset();
		End.Reset();
	}
};

class FeEditorSelectionEntry : public FeSerializable
{
public:
	#define FeEditorSelectionEntry_Properties(_d)	\
	_d(FeTArray<FeNamedGUID>			,Path			)\
	_d(FeString						,Type			)\
	
	#define FeEditorSelectionEntry_PropertiesN(_d)	\
	_d(FeAsset*						,Pointer		)\

	FE_DECLARE_CLASS_BODY_EX(FeEditorSelectionEntry_Properties, FeEditorSelectionEntry_PropertiesN, FeEditorSelectionEntry, FeSerializable)

	FeEditorSelectionEntry()
	{
		Reset();
	}
	void Reset()
	{
	}
	bool operator ==(const FeEditorSelectionEntry& other) const
	{
		return Pointer == other.Pointer && Type == other.Type && Path == other.Path;
	}
	bool operator !=(const FeEditorSelectionEntry& other) const
	{
		return !(*this == other);
	}
};
class FeEditorDataContext : public FeSerializable
{
public:
	#define FeEditorDataContext_Properties(_d)					\
	_d(FeTArray<FeEditorSelectionEntry>	,Selection				)\
	
	#define FeEditorDataContext_PropertiesN(_d)					\
	_d(FeAssetFile*					,AssetFile					)\
	_d(FeWidget*					,SelectionRootWidget		)\
	_d(FeWidget*					,ViewportDraggedItem		)\
	_d(FeTArray<FeWidget*>			,ViewportHoverStack			)\
	_d(FeEditorViewportController*	,ViewportActiveController	)\
	_d(FeEditorWidgetDeletion		,LastWidgetDeletion			)\
	_d(FeEditorDragContext			,Drag						)\

	FE_DECLARE_CLASS_BODY_EX(FeEditorDataContext_Properties, FeEditorDataContext_PropertiesN, FeEditorDataContext, FeSerializable)

public:
	void ClearSelection();
	void AddToSelection(FeAssetFile* assetFile);
	void AddToSelection(FeWidget* asset, bool bClear = true);
	void AddToSelection(FeAsset* asset, bool bClear = true);
	
	FeEditorDataContext() 
	{
		Reset();
	}
	template<typename T>
	T* GetSelectedAsset()
	{
		if (Selection.GetSize() > 0)
		{
			FeAsset* ptr = Selection[0].Pointer;

			if (ptr && ptr->Type == FeAssetType::AssetFile())
				return static_cast<FeAssetFile*>(ptr)->GetAsset<T>();
		}
		return nullptr;
	}
	FeWidget* GetSelectedWidget()
	{
		if (Selection.GetSize() > 0 && Selection[0].Pointer)
		{
			if (Selection[0].Pointer->Type == FeAssetType::Widget())
				return static_cast<FeWidget*>(Selection[0].Pointer);
			else if (Selection[0].Pointer->Type == FeAssetType::AssetFile())
				return static_cast<FeAssetFile*>(Selection[0].Pointer)->GetAsset<FeWidget>();
		}
		return nullptr;
	}
	const FeWidgetPath& GetSelectionPath() const
	{
		FE_ASSERT(Selection.GetSize() == 1, "incoherent selection size");
		return Selection[0].Path;
	}
	bool IsSelectionEmpty()
	{
		return Selection.GetSize() == 0;
	}
	void Reset()
	{
		ClearSelection();
		AssetFile = nullptr;
		SelectionRootWidget = nullptr;
		ViewportDraggedItem = nullptr;
		ViewportHoverStack.Clear();
		ViewportActiveController = nullptr;
	}
	void FetchPointers();
private:
	FeEditorSelectionEntry& DoAddToSelection(FeAsset* asset, bool bClear = true);
};

class FeEditorVisualLayout : public FeSerializable
{
public:
	#define FeEditorVisualLayout_Properties(_d)	\

	FE_DECLARE_CLASS_BODY(FeEditorVisualLayout_Properties, FeEditorVisualLayout, FeSerializable)

	FeRect	Menu;
	FeRect	Toolbar;
	FeRect	StatusBar;
	FeRect	Viewport;

	FeRect	TabContainerBottom;
	FeRect	TabContainerRight;
	FeRect	TabContainerBottomRight;
};

class FeEditorViewportVisualContext : public FeSerializable
{
public:
	#define FeEditorViewportVisualContext_Properties(_d)\
	_d(FeVector2, NativeResolution)						\

	FE_DECLARE_CLASS_BODY(FeEditorViewportVisualContext_Properties, FeEditorViewportVisualContext, FeSerializable)

	FeRect ScreenDimensions;
	FeTArray<FeRenderGeometryInstance*> Geometries;
	FeEditorViewportController*	DraggedController;
	
	FeTransform DragInitialTransform;
	FeWidget* DraggedWidget;
	FeTransform DragTransform;
	FeVector2 GridCell;
	
	void ClearGeometry();
	FeRenderGeometryInstance* PushGeometry(const FeAssetRef& effectRef);
	FeRenderGeometryInstance* PushGeometry();
	int32 GeometryCount;

	uint32 LoadGeometryAndEffects(FeRenderBatch& renderBatch);
	void ComputeZOrder();

	void Reset()
	{
		DraggedWidget = nullptr;
		DraggedController = nullptr;
	}

	FeEditorViewportVisualContext()
	{
		NativeResolution = FeVector2(1280, 720);
	}
	float GetAspectRatio()
	{
		static float ViewportRatio = NativeResolution[1] / NativeResolution[0];
		return ViewportRatio;
	}
};
class FeEditorVisualContext : public FeSerializable
{
public:
	#define FeEditorVisualContext_Properties(_d)	\
	_d(FeTArray<FeEditorToggleEntry>,	Toggles	)	\
	_d(FeEditorVisualLayout,			Layout	)	\
	_d(FeEditorViewportVisualContext,	Viewport)	\
	_d(FeEContentView::Type,			ContentBrowserViewType)\
	_d(int32,							ContentBrowserThumbSize)\

	ImFont* IconsFont;

	float GetViewportAspectRatio()
	{
		const FeRect& rectViewport = Layout.Viewport;
		return rectViewport.GetHeight() / (float)rectViewport.GetWidth();
	}
	void Reset()
	{
		IconsFont = nullptr;
		Viewport.Reset();
	}
	void Create()
	{
		for (int32 i = 0; i < FeEEditorToggle::None; ++i)
		{
			auto& toggle = Toggles.Add();
			toggle.bState = false;
			toggle.Type = (FeEEditorToggle::Type)i;
		}
	}
	bool IsToggled(FeEEditorToggle::Type toggleType) const
	{
		uint32 idx = Toggles.IndexOf([&](const FeEditorToggleEntry& entry) -> bool {return entry.Type == toggleType; });
		return idx != FE_INVALID_ID ? Toggles[idx].bState : false;
	}

	FeEditorVisualContext()
	{
		ContentBrowserViewType = FeEContentView::List;
		ContentBrowserThumbSize = 32;
	}
	FE_DECLARE_CLASS_BODY(FeEditorVisualContext_Properties, FeEditorVisualContext, FeSerializable)
};

class FeEditorInputContext : public FeSerializable
{
public:
	#define FeEditorInputContext_Properties(_d)	\

	static const int32 MaxPressedKeys = 16;
	static const int32 MouseKeysCount = 3;
	
	FeVector2 CusrorPosition;
	FeVector2 CusrorPositionDelta;

	int32	WasPressedKeys[MaxPressedKeys];
	int32	PressedKeys[MaxPressedKeys];
	bool	MouseKeysDown[MouseKeysCount];
	bool	MouseKeysChanged[MouseKeysCount];
	int32	MouseWheel;
	
	void Reset()
	{
		MouseWheel = 0;

		for (int32 i = 0; i < MaxPressedKeys; ++i)
		{
			WasPressedKeys[i] = -1;
			PressedKeys[i] = -1;
		}

		for (int32 i = 0; i < MouseKeysCount; ++i)
		{
			MouseKeysDown[i] = false;
			MouseKeysChanged[i] = false;
		}
	}
	void Update(FeEditorContext& PreviousContext, FeUiUserMouseInputHandler* UserInput);
	void OnKeyReleased(int32 key);
	void OnKeyPressed(int32 key);
	void OnMouseWheel(int32 y);
	bool IsKeyDown(char key);
	bool WasKeyDown(char key);
	bool IsKeyPressed(char key);
	bool IsKeyReleased(char key);
	
	FE_DECLARE_CLASS_BODY(FeEditorInputContext_Properties, FeEditorInputContext, FeSerializable)
};

class FeEditorContext : public FeSerializable
{
public:
	static FePath GetSerializePath()
	{
		FePath file = "editor/context.json";
		return file;
	}

#define FeEditorContext_Properties(_d)	\
	_d(FeEditorInputContext,	Input)	\
	_d(FeEditorDataContext,		Data)	\
	_d(FeEditorVisualContext,	Visual)	\

	uint32	Mode;
	FeDt	Dt;

	FeEditorContext()
	{
		Reset();
	}

	void Reset()
	{
		Mode = FeEEditorMode::Edit;
		Input.Reset();
		Data.Reset();
		Visual.Reset();
	}

	FE_DECLARE_CLASS_BODY(FeEditorContext_Properties, FeEditorContext, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeEditorContext)