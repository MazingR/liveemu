#pragma once

#include <common/common.hpp>
#include <common/tarray.hpp>
#include <common/maths.hpp>
#include <common/serializable.hpp>

#define FE_HEAPID_EDITOR 1

template<typename T>
void TransportArrayDataElement(T& to, T& from)
{
	to = from;
}
template<typename T>
void TransportArrayDataElement(FeTPtr<T>& to, FeTPtr<T>& from)
{
	// transfer ownership
	if (from.Owner)
	{
		to.Assign(from.GetNull(), from.HeapId, true);
		from.Owner = false;
	}
	else
	{
		to = from;
	}
}
template<typename TA, typename TB>
void TransportArrayDataElement(TA& to, TB& from)
{
}

class FeIChangeArrayContainer
{
public:
	virtual void Do() = 0;
	virtual void ReDo() = 0;
	virtual void UnDo() = 0;
	virtual void SelfDelete(uint32 iHeapId) = 0;

	virtual FeSerializable* GetOwner() = 0;
	
	FeString PropertyName;
};
class FeChangeAbstractArrayContainer : public FeIChangeArrayContainer, public FeISelfDeletable
{
public:
	FeSerializable* Owner;

	FeIArray* Array;
	uint32 Index;
	bool bAdd;

	FeSerializable* GetOwner()
	{
		return Owner;
	}

	static FeChangeAbstractArrayContainer* Create(FeIArray* pArray, uint32 index, bool _bAdd, FeSerializable* owner, const FeString& name)
	{
		auto result = FE_NEW(FeChangeAbstractArrayContainer, FE_HEAPID_EDITOR);

		result->Array = pArray;
		result->Index = index;
		result->bAdd = _bAdd;
		result->Owner = owner;
		result->PropertyName = name;

		return result;
	}

	FeChangeAbstractArrayContainer() { Array = nullptr; }

	void ReDo()
	{
		if (bAdd)
		{
			Index = Array->GetSize();
			Array->AddEntry();
		}
		else
		{
			Array->RemoveEntry(Index);
		}
	}
	void Do()
	{
		if (bAdd)
		{
			Index = Array->GetSize();
			Array->AddEntry();
		}
		else
		{
			Array->RemoveEntry(Index);
		}
	}
	void UnDo()
	{
		if (bAdd)
		{
			Array->RemoveEntry(Index);
		}
		else
		{
			Array->AddEntry();
		}
	}
	void SelfDelete(uint32 iHeapId)
	{
		FE_DELETE(FeChangeAbstractArrayContainer, this, FE_HEAPID_EDITOR);
	}
};


template<typename T>
class FeChangeArrayContainer : public FeIChangeArrayContainer, public FeISelfDeletable
{
public:
	FeSerializable* Owner;

	FeTArray<T>* Array;
	T Entry;
	uint32 Index;
	bool bAdd;
	
	FeSerializable* GetOwner()
	{
		return Owner;
	}

	static FeChangeArrayContainer<T>* Create(FeTArray<T>* pArray, uint32 index, bool _bAdd, FeSerializable* owner, const FeString& name)
	{
		auto result = FE_NEW(FeChangeArrayContainer<T>, FE_HEAPID_EDITOR);

		result->Array = pArray;
		result->Index = index;
		result->bAdd = _bAdd;
		result->Owner = owner;
		result->PropertyName = name;

		return result;
	}

	FeChangeArrayContainer() { Array = nullptr; }

	void ReDo()
	{
		if (bAdd)
		{
			Index = Array->GetSize();
			auto& addedEntry = Array->Add();
			TransportArrayDataElement(addedEntry, Entry);
		}
		else
		{
			TransportArrayDataElement(Entry, Array[Index]);
			Array->RemoveAt(Index);
		}
	}
	void Do()
	{
		if (bAdd)
		{
			Index = Array->GetSize();
			auto& addedEntry = Array->Add();
		}
		else
		{
			TransportArrayDataElement(Entry, Array[Index]);
			Array->RemoveAt(Index);
		}
	}
	void UnDo()
	{
		if (bAdd)
		{
			TransportArrayDataElement(Entry, Array[Index]);
			Array->RemoveAt(Index);
		}
		else
		{
			auto& addedEntry = Array->Add();
			TransportArrayDataElement(addedEntry, Entry);
		}
	}
	void SelfDelete(uint32 iHeapId)
	{
		FE_DELETE(FeChangeArrayContainer<T>, this, FE_HEAPID_EDITOR);
	}
};

class FeIChangePropertyContainer
{
public:
	virtual void Do() = 0;
	virtual void UnDo() = 0;
	virtual void SelfDelete(uint32 iHeapId) = 0;
	virtual bool IsChanged() = 0;
	virtual void OnChanged() = 0;
	virtual void* GetPtr() = 0;
	virtual FeSerializable* GetOwner() = 0;

	FeString PropertyName;
};

template<typename T>
class FeChangePropertyContainer : public FeIChangePropertyContainer, public FeISelfDeletable
{
public:
	FeSerializable* Owner;
	T* Ptr;
	T OldValue;
	T NewValue;
	bool bChanged;

	FeSerializable* GetOwner()
	{
		return Owner;
	}

	static FeChangePropertyContainer<T>* Create(T* value, FeSerializable* owner, const FeString& name)
	{
		auto result = FE_NEW(FeChangePropertyContainer<T>, FE_HEAPID_EDITOR);
		result->Ptr = value;
		result->OldValue = *value;
		result->NewValue = *value;
		result->bChanged = false;
		result->Owner = owner;
		result->PropertyName = name;

		return result;
	}

	FeChangePropertyContainer() { Ptr = nullptr; }

	void Do()
	{
		*Ptr = NewValue;
	}
	void UnDo()
	{
		*Ptr = OldValue;
	}
	void SelfDelete(uint32 iHeapId)
	{
		FE_DELETE(FeChangePropertyContainer<T>, this, FE_HEAPID_EDITOR);
	}
	bool IsChanged()
	{
		return bChanged;
	}
	void OnChanged()
	{
		NewValue = *Ptr;
		bChanged = true;
	}
	void* GetPtr()
	{
		return Ptr;
	}
};