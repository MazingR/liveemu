#include <actions/EditorActionShowPopup.hpp>

#include <moduleeditor.hpp>

void FeEditorActionShowPopup::Do(FeEditorContext& context)
{
	FeModuleEditor::Get()->ShowPopup(ComponentName, OnCloseCallback);
}
