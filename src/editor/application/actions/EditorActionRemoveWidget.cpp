#include <actions/EditorActionRemoveWidget.hpp>

void FeEditorActionRemoveWidget::Do(FeEditorContext& context)
{
	auto pSelection = context.Data.GetSelectedWidget();

	if (pSelection)
	{
		auto pParent = pSelection->GetParent();

		uint32 iFound = pParent->Children.IndexOf([&](const FeTPtr<FeWidget>& e) -> bool { return e.Get() == pSelection; });
		FE_ASSERT(iFound != FE_INVALID_ID, "incohrent widget content");

		// save deletion in context (goes in action log)
		context.Data.LastWidgetDeletion.Widget = pSelection;
		pParent->ComputePath(context.Data.LastWidgetDeletion.ParentPath);
		
		// delete from parent's children array
		auto& child = pParent->Children[iFound];
		child.Owner = false;
		pParent->Children.RemoveAt(iFound);

		context.Data.AddToSelection(pParent);
	}
}
void FeEditorActionRemoveWidget::UnDo(FeEditorContext& context)
{
	auto pParent = FeWidget::GetWidgetFromPath(context.Data.SelectionRootWidget, context.Data.LastWidgetDeletion.ParentPath);
	auto pWidget = context.Data.LastWidgetDeletion.Widget;

	auto& childPtr = pParent->Children.Add();
	childPtr.Assign(pWidget, FE_HEAPID_UI, true);

	pWidget->SetParent(pParent);
	pWidget->PostInitialize();

	context.Data.AddToSelection(pWidget);
}
bool FeEditorActionRemoveWidget::CanExecute(FeEditorContext& context)
{
	auto pSelection = context.Data.GetSelectedWidget();
	return pSelection && !pSelection->IsRoot();
}
