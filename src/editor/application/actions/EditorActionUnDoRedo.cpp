#include <actions/EditorActionUnDoRedo.hpp>

#include <moduleeditor.hpp>

void FeEditorActionUnDo::Do(FeEditorContext& context)
{
	FeModuleEditor::Get()->ActionUndo();
}
void FeEditorActionReDo::Do(FeEditorContext& context)
{
	FeModuleEditor::Get()->ActionRedo();
}
