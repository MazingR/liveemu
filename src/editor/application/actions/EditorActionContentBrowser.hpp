#pragma once

#include <action.hpp>

class FeEditorActionContentBrowserRemove : public FeEditorAction
{
public:
#define FeEditorActionContentBrowserRemove_Properties(_d)

	FeEditorActionContentBrowserRemove() : FeEditorAction()
	{
		Name = GetName();
	}
	static const FeString& GetName()
	{
		static const FeString s_Name = "Remove";
		return s_Name;
	}

	virtual void Do(FeEditorContext& context) override;
	FE_DECLARE_CLASS_BODY(FeEditorActionContentBrowserRemove_Properties, FeEditorActionContentBrowserRemove, FeEditorAction)
};

class FeEditorActionContentBrowserRename : public FeEditorAction
{
public:
#define FeEditorActionContentBrowserRename_Properties(_d)

	FeEditorActionContentBrowserRename() : FeEditorAction()
	{
		Name = GetName();
	}
	static const FeString& GetName()
	{
		static const FeString s_Name = "Rename";
		return s_Name;
	}

	virtual void Do(FeEditorContext& context) override;
	FE_DECLARE_CLASS_BODY(FeEditorActionContentBrowserRename_Properties, FeEditorActionContentBrowserRename, FeEditorAction)
};
