#pragma once

#include <action.hpp>

class FeEditorActionRemoveWidget : public FeEditorAction
{
public:
#define FeEditorActionRemoveWidget_Properties(_d)

	FeEditorActionRemoveWidget() : FeEditorAction()
	{
		IsUndoable = true;
		Name = "Remove Ui Widget";
	}
	virtual bool CanExecute(FeEditorContext& context) override;
	virtual void Do(FeEditorContext& context) override;
	virtual void UnDo(FeEditorContext& context) override;

	FE_DECLARE_CLASS_BODY(FeEditorActionRemoveWidget_Properties, FeEditorActionRemoveWidget, FeEditorAction)
};
