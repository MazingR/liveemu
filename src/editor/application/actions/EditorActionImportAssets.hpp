#pragma once

#include <action.hpp>

class FeEditorActionImportAssets : public FeEditorAction
{
public:
#define FeEditorActionImportAssets_Properties(_d)

	FeEditorActionImportAssets() : FeEditorAction()
	{
	}

	FE_DECLARE_CLASS_BODY(FeEditorActionImportAssets_Properties, FeEditorActionImportAssets, FeEditorAction)
};

class FeEditorActionImportImages : public FeEditorActionImportAssets
{
public:
#define FeEditorActionImportImages_Properties(_d)

	static const FeString& GetName()
	{
		static const FeString s_Name = "Import images";
		return s_Name;
	}
	FeEditorActionImportImages() : FeEditorActionImportAssets()
	{
		Name = GetName();
	}

	virtual void Do(FeEditorContext& context) override;
	FE_DECLARE_CLASS_BODY(FeEditorActionImportImages_Properties, FeEditorActionImportImages, FeEditorActionImportAssets)
};
