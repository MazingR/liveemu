#include <actions/EditorActionImportAssets.hpp>

#include <ui/moduleui.hpp>
#include <moduleeditor.hpp>

void FeEditorActionImportImages::Do(FeEditorContext& context)
{
	auto fileBrowser = FeModuleEditor::Get()->GetComponents().GetFileBrowser();

	auto openPopupAction = FeModuleEditor::Get()->CreateAction<FeEditorActionShowPopup>(false, "Edit", FeFileBrowserComponent::ClassName(), [](bool bCancel) -> void
	{
		if (false == bCancel)
		{
			auto fileBrowser = FeModuleEditor::Get()->GetComponents().GetFileBrowser();
			bool bImportedSomething = false;
			const auto& files = fileBrowser->SelectedFiles;

			for (auto& file : files)
			{
				const FePath& filePath = *file.File;
				bool bAlreadyExists = false;
				
				FeTArray<FeImage*> images;
				FeModuleUi::Get()->AssetsRegistry.GetAssets<FeImage>(images);

				for (auto& asset : images)
				{
					if (asset->File == filePath)
					{
						bAlreadyExists = true;
						break;
					}
				}

				if (!bAlreadyExists)
				{
					bImportedSomething = true;
					auto newImg = FeModuleUi::Get()->AssetsRegistry.CreateAsset<FeImage>(FeModuleEditor::Get()->GetComponents().GetContentBrowser()->CurrentDirectoryPath);
					
					newImg->ID.Name = filePath.File.CstrNotNull();
					newImg->File = filePath;
				}
			}
			if (bImportedSomething)
			{
				FeModuleEditor::Get()->GetComponents().GetContentBrowser()->ComputeContentList();
			}
		}
	});
	fileBrowser->Reset(); // reset file browser
	fileBrowser->RefreshContent(".*.png");
	fileBrowser->bAllowMultiselect = true;

	FeModuleEditor::Get()->ActionDo(openPopupAction);
}
