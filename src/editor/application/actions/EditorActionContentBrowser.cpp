#include <actions/EditorActionContentBrowser.hpp>

#include <moduleeditor.hpp>

void FeEditorActionContentBrowserRemove::Do(FeEditorContext& context)
{
	//auto contentBrowser = FeModuleEditor::Get()->GetComponents().GetContentBrowser();
	//auto selection = contentBrowser->GetSelection();

	//if (selection.AssetPtr && contentBrowser->SelectedSctiptFile)
	//{
	//	bool bClearWidgetSelection = false;
	//	if (context.Data.SelectionRootWidget && selection.AssetPtr->Type == FeAssetType::Widget() && selection.AssetPtr->Name == context.Data.SelectionRootWidget->Name)
	//		bClearWidgetSelection = true;

	//	FeModuleUi::Get()->RemoveAsset(*contentBrowser->SelectedSctiptFile, selection.AssetPtr->Name, selection.AssetPtr->Type);
	//	
	//	if (bClearWidgetSelection)
	//		FeModuleEditor::Get()->ClearSelection();
	//}
}
void FeEditorActionContentBrowserRename::Do(FeEditorContext& context)
{
	FeModuleEditor::Get()->ShowPopup("", nullptr, [this](FeEditorContext& _context) -> void
	{
		// draw popup content callback
		auto selection = FeModuleEditor::Get()->GetComponents().GetContentBrowser()->ClickedAsset;
		if (selection)
		{
			auto& stringName = selection->Asset->ID.Name;
			bool bChanged = DrawString(&stringName, false, ImVec2(), true);
		}
	});
}