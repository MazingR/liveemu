#include <actions/EditorActionCreateAsset.hpp>

#include <ui/moduleui.hpp>
#include <moduleeditor.hpp>

void FeEditorActionCreateAsset::Do(FeEditorContext& context)
{
	const FePath& dirPath = FeModuleEditor::Get()->GetComponents().GetContentBrowser()->CurrentDirectoryPath;

		 if (AssetType == FeAssetType::Image())		FeModuleUi::Get()->AssetsRegistry.CreateAsset<FeImage>(dirPath);
	else if (AssetType == FeAssetType::Font())		FeModuleUi::Get()->AssetsRegistry.CreateAsset<FeFont>(dirPath);
	else if (AssetType == FeAssetType::Effect())	FeModuleUi::Get()->AssetsRegistry.CreateAsset<FeRenderEffect>(dirPath);
	else if (AssetType == FeAssetType::Widget())	FeModuleUi::Get()->AssetsRegistry.CreateAsset<FeWidget>(dirPath);
	else if (AssetType == FeAssetType::Sequence())	FeModuleUi::Get()->AssetsRegistry.CreateAsset<FeUiSequence>(dirPath);
	else
	{
		FE_ASSERT(false, "invalid asset type name : '%s'", AssetType.CstrNotNull());
	}
}
void FeEditorActionCreateAsset::UnDo(FeEditorContext& context)
{
	FE_ASSERT_NOT_IMPLEMENTED;
}
