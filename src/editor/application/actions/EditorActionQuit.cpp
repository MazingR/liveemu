#include <actions/EditorActionQuit.hpp>

#include <common/application.hpp>

void FeEditorActionQuit::Do(FeEditorContext& context) 
{
	FeApplication::GetStaticInstance()->Quit(); 
}
