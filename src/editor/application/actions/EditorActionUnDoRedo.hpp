#pragma once

#include <action.hpp>

class FeEditorActionUnDo : public FeEditorAction
{
public:
#define FeEditorActionUnDo_Properties(_d)
	FeEditorActionUnDo() { Name = "UnDo"; }
	virtual void Do(FeEditorContext& context) override;
	FE_DECLARE_CLASS_BODY(FeEditorActionUnDo_Properties, FeEditorActionUnDo, FeEditorAction)
};

class FeEditorActionReDo : public FeEditorAction
{
public:
#define FeEditorActionReDo_Properties(_d)
	FeEditorActionReDo() { Name = "ReDo"; }
	virtual void Do(FeEditorContext& context) override;
	FE_DECLARE_CLASS_BODY(FeEditorActionReDo_Properties, FeEditorActionReDo, FeEditorAction)
};
