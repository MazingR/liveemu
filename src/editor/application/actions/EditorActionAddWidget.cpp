#include <actions/EditorActionAddWidget.hpp>

#include <ui/moduleui.hpp>
#include <moduleeditor.hpp>


void FeEditorActionAddWidget::AddWidgetToParent(FeEditorContext& context, FeWidget* pParent, FeWidget* pWidget)
{
	pWidget->SetParent(pParent);
	pWidget->PostInitialize();

	context.Data.AddToSelection(pWidget); // set selection to new widget
}
FeWidget* FeEditorActionAddWidget::CreateNewWidget(FeEditorContext& context, FeWidget* pParent, const FeString& name)
{
	auto& childPtr = pParent->Children.Add();
	
	auto pChild = static_cast<FeWidget*>(GetObjectsFactory().CreateObjectFromFactory(WidgetType, FE_HEAPID_UI));
	FE_ASSERT(pChild, "couldn't create widget of type %s", WidgetType.Cstr());

	if (pChild)
	{
		childPtr.Assign(pChild, FE_HEAPID_UI, true);

		pChild->GetTransform().Scale.mData[0] = 0.5f;
		pChild->GetTransform().Scale.mData[1] = 0.5f;
		pChild->ID.Name = name;
		pChild->Effect = FeModuleUi::Get()->GetTheme().DefaultEffect;

		auto pSelection = context.Data.GetSelectedWidget();

		if (pSelection && pSelection->GetParent() == pParent)
		{
			pChild->Transform = pSelection->Transform;
		}
	}
	return pChild;
}
void FeEditorActionAddWidget::ReDo(FeEditorContext& context, const FeEditorDataContext& dataContextAfter)
{
	if (context.Data.SelectionRootWidget)
	{
		FeWidget* pNewWidget = nullptr;

		pNewWidget = CreateNewWidget(context, context.Data.SelectionRootWidget, dataContextAfter.GetSelectionPath().Back().Name);
		AddWidgetToParent(context, context.Data.SelectionRootWidget, pNewWidget);
	}
}
void FeEditorActionAddWidget::Do(FeEditorContext& context)
{
	auto pParent = context.Data.SelectionRootWidget;

	if (pParent)
	{
		int32 NameCounter = 1;
		FeString newWidgetName;
		do
		{
			FeStringTools::Format(newWidgetName, "Widget_%d", NameCounter++);
		} while (pParent->Children.Contains([&](const FeTPtr<FeWidget>& child) -> bool { return child.Get()->ID.Name == newWidgetName; }));
		
		auto pNewWidget = CreateNewWidget(context, pParent, newWidgetName);
		AddWidgetToParent(context, pParent, pNewWidget);

		if (OnCreatedCallback)
			OnCreatedCallback(pNewWidget);
	}

	FeModuleEditor::Get()->HidePopup(true); // hide create widget popup
}
void FeEditorActionAddWidget::UnDo(FeEditorContext& context)
{
	const auto& pathToChild = context.Data.GetSelectionPath();

	auto pParent = context.Data.SelectionRootWidget;
	auto pChild = FeWidget::GetWidgetFromPath(context.Data.SelectionRootWidget, pathToChild);

	FE_ASSERT(pParent && pChild && pChild->GetParent() == pParent, "incoherent data context");

	{
		uint32 iFound = pParent->Children.IndexOf([&](const FeTPtr<FeWidget>& e) -> bool { return e.Get() == pChild; });
		FE_ASSERT(iFound != FE_INVALID_ID, "incohrent widget content");

		auto& child = pParent->Children[iFound];
		pParent->Children.RemoveAt(iFound);
		child.Delete();

		context.Data.AddToSelection(pParent);
	}
}
