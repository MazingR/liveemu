#include <actions/EditorActionSaveAll.hpp>

#include <moduleeditor.hpp>

void FeEditorActionSaveAll::Do(FeEditorContext& context)
{
	FeModuleEditor::Get()->SaveAll();
}
bool FeEditorActionSaveAll::CanExecute(FeEditorContext& context)
{
	return FeModuleEditor::Get()->IsSaveNeeded();
}
