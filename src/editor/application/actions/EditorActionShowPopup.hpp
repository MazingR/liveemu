#pragma once

#include <action.hpp>

class FeEditorActionShowPopup : public FeEditorAction
{
public:
#define FeEditorActionShowPopup_Properties(_d)

	FeEditorActionShowPopup(const FeString& componentName, FePopupCloseCallback callback) : FeEditorAction()
	{
		ComponentName = componentName;
		OnCloseCallback = callback;
		FeStringTools::Format(Name, "Show Popup : %s", ComponentName.Cstr());
	}
	FeEditorActionShowPopup() : FeEditorActionShowPopup("", nullptr) { FE_ASSERT(false, "we shouldn't get here"); }

	FeString ComponentName;
	FePopupCloseCallback OnCloseCallback;

	virtual void Do(FeEditorContext& context) override;
	FE_DECLARE_CLASS_BODY(FeEditorActionShowPopup_Properties, FeEditorActionShowPopup, FeEditorAction)
};
