#pragma once

#include <action.hpp>

class FeEditorActionToggleOption : public FeEditorAction
{
public:
#define FeEditorActionToggleOption_Properties(_d)	\
			_d(FeEEditorToggle::Type,	ToggleType)	\

	FeEditorActionToggleOption(FeEEditorToggle::Type toggleType) : ToggleType(toggleType)
	{
		if (toggleType != FeEEditorToggle::None)
			Name = FeStringTools::Concat("Toggle ", FeEEditorToggle::ToString(ToggleType));
	}
	FeEditorActionToggleOption() : FeEditorActionToggleOption(FeEEditorToggle::None) {}

	virtual void Do(FeEditorContext& context) override;
	bool IsToggleed(FeEditorContext& context);

	FE_DECLARE_CLASS_BODY(FeEditorActionToggleOption_Properties, FeEditorActionToggleOption, FeEditorAction)
};
