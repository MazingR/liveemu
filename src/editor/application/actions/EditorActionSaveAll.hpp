#pragma once

#include <action.hpp>

class FeEditorActionSaveAll : public FeEditorAction
{
public:
#define FeEditorActionSaveAll_Properties(_d)	\

	FeEditorActionSaveAll() { Name = "Save All"; }

	virtual void Do(FeEditorContext& context) override;
	virtual bool CanExecute(FeEditorContext& context) override;

	FE_DECLARE_CLASS_BODY(FeEditorActionSaveAll_Properties, FeEditorActionSaveAll, FeEditorAction)
};
