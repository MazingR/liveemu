#pragma once

#include <action.hpp>

class FeEditorActionQuit : public FeEditorAction
{
public:
#define FeEditorActionQuit_Properties(_d)	\

	FeEditorActionQuit() { Name = "Quit"; }
	virtual void Do(FeEditorContext& context) override;
	FE_DECLARE_CLASS_BODY(FeEditorActionQuit_Properties, FeEditorActionQuit, FeEditorAction)
};
