#pragma once

#include <action.hpp>

class FeEditorActionCreateAsset : public FeEditorAction
{
public:
#define FeEditorActionCreateAsset_Properties(_d)

	FeString AssetType;

	FeEditorActionCreateAsset() : FeEditorAction() { FE_ASSERT(false, "we shouldn't get here"); }
	FeEditorActionCreateAsset(const FeString& assetType) : FeEditorAction()
	{
		IsUndoable = true;
		AssetType = assetType;
		FeStringTools::Format(Name, "Create asset : %s", assetType.Cstr());
	}
	virtual void Do(FeEditorContext& context) override;
	virtual void UnDo(FeEditorContext& context) override;

	FE_DECLARE_CLASS_BODY(FeEditorActionCreateAsset_Properties, FeEditorActionCreateAsset, FeEditorAction)
};