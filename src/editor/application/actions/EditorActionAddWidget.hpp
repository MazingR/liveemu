#pragma once

#include <action.hpp>

class FeEditorActionAddWidget : public FeEditorAction
{
public:
#define FeEditorActionAddWidget_Properties(_d)

	FeEditorActionAddWidget(const FeString& widgetType) : FeEditorAction()
	{
		OnCreatedCallback = nullptr;
		IsUndoable = true;
		WidgetType = widgetType;
		FeStringTools::Format(Name, "Create : %s", WidgetType.Cstr());
	}
	FeEditorActionAddWidget() : FeEditorActionAddWidget("") { FE_ASSERT(false, "we shouldn't get here"); }

	virtual void ReDo(FeEditorContext& context, const FeEditorDataContext& dataContextAfter) override;
	virtual void Do(FeEditorContext& context) override;
	virtual void UnDo(FeEditorContext& context) override;

	FeString WidgetType;
	std::function<void(FeWidget*)> OnCreatedCallback;

	FE_DECLARE_CLASS_BODY(FeEditorActionAddWidget_Properties, FeEditorActionAddWidget, FeEditorAction)

private:
	FeWidget* CreateNewWidget(FeEditorContext& context, FeWidget* pParent, const FeString& name);
	void AddWidgetToParent(FeEditorContext& context, FeWidget* pParent, FeWidget* pWidget);
};
