#include <actions/EditorActionToggleOption.hpp>

bool FeEditorActionToggleOption::IsToggleed(FeEditorContext& context)
{
	return context.Visual.IsToggled(ToggleType);
}
void FeEditorActionToggleOption::Do(FeEditorContext& context)
{
	uint32 iFound = context.Visual.Toggles.IndexOf([this](const FeEditorToggleEntry& entry) ->  bool { return entry.Type == ToggleType; });
	FE_ASSERT(iFound != FE_INVALID_ID, "FeEditorActionToggleOption : invalid toggle type");

	if (iFound != FE_INVALID_ID)
	{
		context.Visual.Toggles[iFound].Switch();
	}
}
