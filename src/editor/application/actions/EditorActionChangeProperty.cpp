#include <actions/EditorActionChangeProperty.hpp>

#include <rendering/modulerenderer.hpp>
#include <ui/moduleui.hpp>

void FeEditorActionChangeProperty::Do(FeEditorContext& context)
{
	if (Container)
	{
		Container->Do();
		OnAppliedChange();
	}
}
void FeEditorActionChangeProperty::UnDo(FeEditorContext& conetext)
{
	if (Container)
	{
		Container->UnDo();
		OnAppliedChange();
	}
}
void FeEditorActionChangeProperty::OnAppliedChange()
{
	if (Container->GetOwner())
	{
		if (Container->GetOwner()->IsChildOf(FeWidget::ClassName()))
		{
			auto widget = static_cast<FeWidget*>(Container->GetOwner());
			widget->OnPropertyChanged(Container->GetPtr());

			if (widget->IsRoot() && Container->GetPtr() == &widget->GetTransform())
			{
				FeModuleUi::Get()->OnViewportChanged();
			}
		}
		else if (Container->GetOwner()->IsChildOf(FeRenderEffect::ClassName()))
		{
			auto assetFile = FeModuleUi::Get()->AssetsRegistry.Files.Find([&](const FeTPtr<FeAssetFile> file) -> bool { return file.Get()->Asset.Get() == Container->GetOwner(); });
			if (assetFile)
				FeModuleRendering::Get()->LoadEffect(assetFile->Get());
		}
	}
}
