#pragma once

#include <action.hpp>

class FeEditorActionChangeProperty : public FeEditorAction
{
public:
#define FeEditorActionChangeProperty_Properties(_d)	\

	FeEditorActionChangeProperty() { Container = nullptr; IsUndoable = true; }
	~FeEditorActionChangeProperty()
	{
		if (Container)
		{
			Container->SelfDelete(FE_HEAPID_EDITOR);
			Container = nullptr;
		}
	}
	FeIChangePropertyContainer* Container;

	virtual void Do(FeEditorContext& context) override;
	virtual void UnDo(FeEditorContext& context) override;

	FE_DECLARE_CLASS_BODY(FeEditorActionChangeProperty_Properties, FeEditorActionChangeProperty, FeEditorAction)

private:
	void OnAppliedChange();
};
