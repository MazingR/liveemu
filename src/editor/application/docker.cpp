#include <docker.hpp>


FeEditorDocker::FeEditorDocker(const FeRect& rect)
{
	CurrentOffset = rect;
}

void FeEditorDocker::Dock(FeRect& rect, uint32 dockType, int32 desiredW, int32 desiredH)
{
	int32 width = ComputeWidth(desiredW);
	int32 height = ComputeHeight(desiredH);
	DoDock(rect, dockType, width, height);
}
FeRect FeEditorDocker::Dock(uint32 dockType, int32 width, int32 height)
{
	FeRect rect;
	Dock(rect, dockType, width, height);
	return rect;
}
void FeEditorDocker::Dock(FeRect& rect, uint32 dockType, float desiredW, float desiredH)
{
	int32 width = ComputeWidth(desiredW);
	int32 height = ComputeHeight(desiredH);
	DoDock(rect, dockType, width, height);
}
FeRect FeEditorDocker::Dock(uint32 dockType, float percentW, float percentH)
{
	FeRect rect;
	Dock(rect, dockType, percentW, percentH);
	return rect;
}

void FeEditorDocker::DoDock(FeRect& rect, uint32 dockType, int32 width, int32 height)
{
	rect = CurrentOffset;

	if (dockType & FeEDock::Top)
	{
		rect.bottom = rect.top + height;
		CurrentOffset.top += height;
	}
	if (dockType & FeEDock::Left)
	{
		rect.right = rect.left + width;
		CurrentOffset.left += width;
	}
	if (dockType & FeEDock::Right)
	{
		rect.left = rect.right - width;
		CurrentOffset.right -= width;
	}
	if (dockType & FeEDock::Bottom)
	{
		rect.top = rect.bottom - height;
		CurrentOffset.bottom -= height;
	}
}
int32 FeEditorDocker::ComputeWidth(int32 desired)
{
	int32 max = CurrentOffset.right - CurrentOffset.left;
	if (desired == 0 || desired > max)
		return max;
	else
		return desired;
}
int32 FeEditorDocker::ComputeHeight(int32 desired)
{
	int32 max = CurrentOffset.bottom - CurrentOffset.top;
	if (desired == 0 || desired > max)
		return max;
	else
		return desired;
}
int32 FeEditorDocker::ComputeWidth(float desired)
{
	int32 max = CurrentOffset.right - CurrentOffset.left;
	return (int32)(max*desired);
}
int32 FeEditorDocker::ComputeHeight(float desired)
{
	int32 max = CurrentOffset.bottom - CurrentOffset.top;
	return (int32)(max*desired);
}
