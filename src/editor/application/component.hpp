#pragma once

#include <commoneditor.hpp>
#include <actions.hpp>
#include <filesystem.hpp>

class FeEditorContext;
class FeWidget;
struct ImFont;
class FeEditorMenu;
struct ImVec4;

struct FeWidgetList
{
	FeTArray<FeWidget*> Widgets;
};

class FeEditorComponent : public FeSerializable
{
public:
	virtual void Draw(FeEditorContext& context) {}

	virtual void BeginWindow();
	virtual void EndWindow();
	
	bool IsVisible;
	bool IsOverlay;
	FeRect Rect;
	FeString Title;

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeEditorComponent, FeSerializable)

	FeEditorComponent() : IsVisible(true), IsOverlay(false) {}

protected:
	bool Begin(const char* strTitle);
};
class FeWidgetListComponent : public FeEditorComponent
{
public:
	FeWidgetListComponent() : FeEditorComponent(), PreviousSelected(nullptr), Selected(nullptr) 
	{
		Title = "Outline";
	}
	void Draw(FeEditorContext& context);

	void DrawWidget(FeWidget* widget);
	bool IsSelectionChanged() { return PreviousSelected != Selected; }

	FeWidgetList Target;
	FeWidget* Selected;

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeWidgetListComponent, FeEditorComponent)

private:
	FeWidget* PreviousSelected;
};

class FeStyleComponent : public FeEditorComponent
{
public:
	FeStyleComponent() : FeEditorComponent() {}
	void Draw(FeEditorContext& context);

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeStyleComponent, FeEditorComponent)
};

class FeMenuComponent : public FeEditorComponent
{
public:
	FeMenuComponent() : FeEditorComponent() {}
	void Draw(FeEditorContext& context);

	FeTArray<FeEditorMenu*> Entries;

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeMenuComponent, FeEditorComponent)
};

struct FeEditorButton
{
	FeTSharedPtr<FeEditorAction> Action;

	FeEditorButton(){}
	FeEditorButton(FeTSharedPtr<FeEditorAction> action) : Action(action) {}
};
class FeToolbarComponent : public FeEditorComponent
{
public:

	FeToolbarComponent() : FeEditorComponent(), ButtonPressed(-1) {}
	void Draw(FeEditorContext& context);

	FeTArray<FeEditorButton> Buttons;
	int32 ButtonPressed;

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeToolbarComponent, FeEditorComponent)
};

struct FeStatusBarMessage
{
	static const int32 MessageLength = 512;

	FeColor Color;
	float Timer;
	char Message[MessageLength];

	FeStatusBarMessage()
	{
		Timer = 0;
		FeColorHelper::GetColor(FeEColor::White, Color);
		sprintf_s(Message, "");
	}
	void Set(const char* str, FeEColor::Type eColor, int32 timer = 0)
	{

		Timer = (float)timer;
		FeColorHelper::GetColor(eColor, Color);
		sprintf_s(Message, str);
	}
};
class FeStatusBarComponent : public FeEditorComponent
{
public:
	FeStatusBarComponent() : FeEditorComponent() {}
	void Draw(FeEditorContext& context);
	void AddNotification(const char* str, FeEColor::Type color, float time = 2.f);
	FeStatusBarMessage Status;
	FeTArray<FeStatusBarMessage> Notifications;

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeStatusBarComponent, FeEditorComponent)
};
class FeOverlayComponent : public FeEditorComponent
{
public:
	FeOverlayComponent() : FeEditorComponent(), Content(nullptr), OnCloseCallback(nullptr), OnDrawCallback(nullptr) {}
	void Draw(FeEditorContext& context);

	FeEditorComponent* Content;
	FePopupCloseCallback OnCloseCallback;
	FePopupDrawCallback OnDrawCallback;

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeOverlayComponent, FeEditorComponent)
};
class FeTabComponent : public FeEditorComponent
{
public:
	FeTabComponent() : FeEditorComponent(), Current(0) {}
	void Draw(FeEditorContext& context);

	FeTArray<FeEditorComponent*> Tabs;
	int32 Current;

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeTabComponent, FeEditorComponent)
};
class FeAddWidgetComponent : public FeEditorComponent
{
public:
	FeAddWidgetComponent() : FeEditorComponent()
	{
		Title = "Add Ui Widget";
	}
	void Draw(FeEditorContext& context);
	
	FeTArray<FeEditorButton> Buttons;

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeAddWidgetComponent, FeEditorComponent)
};

struct FeEditorContentFilter
{
	static const int32 FilterRegexSize = 512;
	FeString Type;

	char Name[FilterRegexSize];

	FeEditorContentFilter()
	{
		Type = FeAssetType::None();
		Name[0] = '\0';
	}
};

struct FeContentBrowserEntry
{
	FeAssetFile* AssetFile;

	const ImVec4& GetColor(bool bInvert) const;
	static const ImVec4& GetSelectionColor();

	FeContentBrowserEntry() : AssetFile(nullptr) {}
};
class FeContentBrowserComponent : public FeEditorComponent
{
public:
	FeContentBrowserComponent() : FeEditorComponent()
	{
		ClearSelection();
		Title = "Content Browser";
	}
	void Draw(FeEditorContext& context);

	FePath CurrentDirectoryPath;

	FeAssetFile* ClickedAsset;
	FeAssetFile* DoubleClickedAsset;
	FeAssetFile* PreviousDoubleClickedAsset;

	FeEditorContentFilter Filter;
	FeTArray<FeContentBrowserEntry> ContentList;

	void ClearSelection()
	{
		PreviousDoubleClickedAsset = nullptr;
		ClickedAsset = nullptr;
		DoubleClickedAsset = nullptr;
		SelectedDir = nullptr;
	}
	void ComputeContentList();

	bool IsSelectionChanged() { return PreviousDoubleClickedAsset != DoubleClickedAsset; }
	bool IsSelectionValid() { return DoubleClickedAsset != nullptr; }
	FeAssetFile* GetSelection() { return DoubleClickedAsset; }
	void RefreshTree();

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeContentBrowserComponent, FeEditorComponent)

private:
	FeDirectory RootDir;
	FeDirectory* SelectedDir;

	void DrawItemContextMenu();
	void DrawContentList(FeEditorContext& context);
	void DrawContentThumb(FeEditorContext& context);
};
struct FeFileBrowserEntry
{
	const FePath* File;
	const FeDirectory* Directory;
};
class FeFileBrowserComponent : public FeEditorComponent
{
public:
	FeFileBrowserComponent() : FeEditorComponent(), SelectedDir(nullptr)
	{
		Title = "File browser";
		bAllowMultiselect = false;
	}

	void Draw(FeEditorContext& context);
	void RefreshContent(const char* szFilter);
	void Reset();

	FeTArray<FeFileBrowserEntry> SelectedFiles;
	bool bAllowMultiselect;
	
	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeFileBrowserComponent, FeEditorComponent)
private:
	FeDirectory RootDir;
	FeDirectory* SelectedDir;
};

class FeEmulatorComponent : public FeEditorComponent
{
public:
	FeEmulatorComponent() : FeEditorComponent()
	{
		Title = "Emulator";
	}
	void Draw(FeEditorContext& context);
	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeEmulatorComponent, FeEditorComponent)

private:
	FeWidget* PreviousSelected;
};