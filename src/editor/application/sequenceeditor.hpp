#pragma once

#include <component.hpp>
#include <imgui.hpp>

struct FeTrackCreator
{
	FeString PropertyName;
	FeString ActionName;
	FeString TypeName;

	template<typename T>
	static FeTrackCreator Create(const FeString& proertyName)
	{
		FeTrackCreator result = { proertyName, FeStringTools::Format("Add track : %s", proertyName.Cstr()), T::ClassName() };
		return result;
	}
};

class FeSequencerComponent : public FeEditorComponent
{
public:
	FeSequencerComponent() : FeEditorComponent()
	{
		SelectedInterpolIndex = FE_INVALID_ID;
		Title = "Sequencer";
		PlaybackTime = 0.0f;
		PlaybackFrame = 0;
		MaxTime = 1.0f;
		TimeStep = 1.0f;
		FramesCount = 0;
		IsSequenceDirty = false;
		IsWidgetDirty = false;
		KeySize = FeVector2(13, 20);
		FramesPerSec = 60;
		Sequence = nullptr;
		Widget = nullptr;
		WidgetDefault = nullptr;
		WidgetRoot = nullptr;
		SequenceAssetFile = nullptr;

		WidgetAssetFile.AssetRef.Path = "__SequencerEditorWidget__";
		WidgetAssetFile.AssetRef.Type = FeAssetType::Widget();

		ImguiConvert(ColorFrame, FeColorHelper::GetColor(FeEColor::Gray));
		ImguiConvert(ColorFrameInterval, FeColorHelper::GetColor(FeEColor::DimGray));
		ImguiConvert(ColorFrameSelected, FeColorHelper::GetColor(FeEColor::Orange));
		ImguiConvert(ColorFrameDragKey, FeColorHelper::GetColor(FeEColor::LightPink));
		ImguiConvert(ColorFramePlayback, FeColorHelper::GetColor(FeEColor::Cyan));
		ImguiConvert(ColorKeyFrame, FeColorHelper::GetColor(FeEColor::IndianRed));
		ImguiConvert(ColorKeyFrameInterpolation, FeColorHelper::GetColor(FeEColor::RosyBrown));
		ImguiConvert(ColorKeyContent, FeColorHelper::GetColor(FeEColor::White));
		ColorFramePlaybackEmpty = ImVec4(0, 0, 0, 0);
	}
	void Draw(FeEditorContext& context);
	void SetSequence(FeAssetFile* assetFile);
	FeWidget* GetWidget() { return WidgetRoot; }
	FeUiSequence* GetSequence() { return Sequence; }
	FeAssetFile* GetSequenceAssetFile() { return SequenceAssetFile; }
	
	void SetKeyFrame(int32 frame, void* value, FeUiPropertyInterpolation* track, FeWidget* widget=nullptr);
	void OnPropertyChanged(FeIChangePropertyContainer* changedPropertyContainer);

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeSequencerComponent, FeEditorComponent)
private:
	struct FeKeyFrameRef
	{
		FeUiPropertyInterpolation* Track;
		uint32 KeyFrameIndex;
		FeKeyFrame* KeyFrame;
		uint32 FrameIndex;
		uint32 DraggedFrameIdx;

		void Reset()
		{
			KeyFrame = nullptr;
			Track = nullptr;
			KeyFrameIndex = FE_INVALID_ID;
			DraggedFrameIdx = -1;
		}
		FeKeyFrameRef()
		{
			Reset();
		}
	};

	ImVec4	ColorFrame,
			ColorFrameInterval,
			ColorFrameSelected,
			ColorFrameDragKey,
			ColorFramePlayback,
			ColorFramePlaybackEmpty,
			ColorKeyFrame,
			ColorKeyFrameInterpolation,
			ColorKeyContent;

	void Refresh(FeEditorContext& context);
	void DrawProperties(FeEditorContext& context);
	uint32 SelectedInterpolIndex;

	float PlaybackTime;
	int32 PlaybackFrame;

	float MaxTime;
	float TimeStep;
	int32 FramesCount;
	bool IsSequenceDirty;
	bool IsWidgetDirty;

	FeKeyFrameRef DraggedKeyFrame;
	FeKeyFrameRef ClickedKeyFrame;

	FeVector2 KeySize;
	int32 FramesPerSec;

	FeUiSequence* Sequence;
	FeWidget* WidgetRoot;
	FeWidget* Widget;
	FeWidget* WidgetDefault;
	FeAssetFile* SequenceAssetFile;
	FeAssetFile WidgetAssetFile;

	FeTArray<FeTrackCreator> CommonTracks;
	FeUiSequenceInstance SequenceInstance;
};