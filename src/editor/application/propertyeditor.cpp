#include "propertyeditor.hpp"

#include <moduleeditor.hpp>
#include <ui/uiuserinputhandler.hpp>
#include <ui/moduleui.hpp>
#include <rendering/renderresourceshandler.hpp>
#include <rendering/geometry.hpp>
#include "imgui.hpp"
#include <imgui.h>

template<typename T>
bool UseAutoCreatePropertyContainaer(T*) { return true; }

bool UseAutoCreatePropertyContainaer(FeTransform*) { return false; }
bool UseAutoCreatePropertyContainaer(FeUiBinding*) { return false; }

FeIChangePropertyContainer* changedPropertyContainer = nullptr;
FeIChangeArrayContainer* changedArrayContainer = nullptr;

template<typename T>
void CreateChangedPropertyContainer(FeSerializer* serializer, FeSerializable* owner, T* value, const T& previousValue)
{
	FeChangePropertyContainer<T>* container = FeChangePropertyContainer<T>::Create(value, owner, serializer->GetCurrentPropertyName());
	container->bChanged = true;
	container->OldValue = previousValue;

	FE_ASSERT(!changedPropertyContainer, "changed multiple propreties in 1 frame ?!");
	changedPropertyContainer = container;
}
template<typename T>
void CreateChangedArrayContainer(FeSerializer* serializer, FeIArray* array, uint32 index, bool bAdd, FeSerializable* owner)
{
	auto pArray = dynamic_cast<FeTArray<T>*>(array);
	FE_ASSERT(pArray, "incoherent types of array");

	FE_ASSERT(!changedArrayContainer, "changed multiple propreties in 1 frame ?!");
	changedArrayContainer = FeChangeArrayContainer<T>::Create(pArray, index, bAdd, owner, serializer->GetCurrentPropertyName());
}
void CreateChangedAbstractArrayContainer(FeSerializer* serializer, FeIArray* array, uint32 index, bool bAdd, FeSerializable* owner)
{
	FE_ASSERT(!changedArrayContainer, "changed multiple propreties in 1 frame ?!");
	changedArrayContainer = FeChangeAbstractArrayContainer::Create(array, index, bAdd, owner, serializer->GetCurrentPropertyName());
}

PropertyGridScopeWidget::PropertyGridScopeWidget(const char* header)
{
	ImGui::AlignFirstTextHeightToWidgets();
	ImGui::Text(header);
	ImGui::NextColumn();
	ImGui::PushItemWidth(-1);
}
PropertyGridScopeWidget::~PropertyGridScopeWidget()
{
	ImGui::PopItemWidth();
	ImGui::NextColumn();
}
PropertyGridScopeID::PropertyGridScopeID(void* ID)
{
	ImGui::PushID(ID);
}
PropertyGridScopeID::~PropertyGridScopeID()
{
	ImGui::PopID();
}
bool CanShowTargetProprety(FeSerializable* container, FeSerializer* serializer, void* value);
bool DrawEnum(int32* value, const FeString* values, int32 valuesCount, bool bPushId);

void SkipColumns(int32 count)
{
	for (int32 i = 0; i < count;++i)
		ImGui::NextColumn();
}

template<typename T>
void PropertyDrawHeader(FeSerializable* target, FeSerializer* serializer, void* value)
{
	{ /* draw header (property name)*/
		ImGui::AlignFirstTextHeightToWidgets();
		const auto& path = serializer->GetPath();

		if (path.CurrentDepth > 0)
		{
			const auto& indirection = path.Indirections[path.CurrentDepth];
			
			if (indirection.Index != FE_INVALID_ID) // if current element is an element of an array
			{
				{
					SCOPE_ID(removeEntry, value);
					auto iconsFont = FeModuleEditor::Get()->GetContext().Visual.IconsFont;
					ImGui::PushFont(iconsFont);

					if (ImGui::SmallButton("l"))
					{
						CreateChangedArrayContainer<T>(serializer, serializer->GetArraysPath().Back(), indirection.Index, false, target);
					}
					ImGui::PopFont();
				}
				ImGui::SameLine();
			}
		}

		ImGui::Text(serializer->GetCurrentPropertyName().CstrNotNull());
		ImGui::NextColumn();
	}
}

void PropertyDrawHeader(const char* text)
{
	{ /* draw header (property name)*/
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text(text);
		ImGui::NextColumn();
	}
}
template<typename T>
bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, T* value)
{
	ImGui::Text("??");
	return false;
}

template<typename T>
uint32 FePropertyEditorDeseserialize(FeSerializable* target, FeSerializer* serializer, T* value)
{
	int32 iRes = FeEReturnCode::Success;
	bool bShow = CanShowTargetProprety(target, serializer, value);

	if (bShow)
	{
		ImGui::PushID(value);
		{
			PropertyDrawHeader<T>(target, serializer, value);

			{ /* draw property edit widget*/
				ImGui::AlignFirstTextHeightToWidgets();
				ImGui::PushItemWidth(-1);
				T previousValue = *value;

				if (FePropertyEditorDeseserializeValue(target, serializer, value))
				{
					CreateChangedPropertyContainer(serializer, target, value, previousValue);
				}
				ImGui::PopItemWidth();
				ImGui::NextColumn();
			}
		}
		ImGui::PopID();
	}
	return iRes;
}

bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, bool* value)
{
	return ImGui::Checkbox("", value);
}

bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, FeString* value)
{
	return DrawString(value, false, ImVec2(), false);
}
bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, FeGuid* value)
{
	return false;
}

bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, FePath* value)
{
	struct FileDialogResult
	{
		struct State
		{
			enum Type
			{
				Idle,
				Opened,
				Canceled,
				Validated,
			};
		};

		FeSerializable* Target;
		FeSerializer* Serializer;
		FePath Value;
		FePath PreviousValue;
		
		State::Type State;

		void Reset()
		{
			State = State::Idle;
			Target = nullptr;
			Serializer = nullptr;
			Value.Clear();
			PreviousValue.Clear();
		}
		FileDialogResult()
		{
			Reset();
		}
	};

	static FileDialogResult fileDialogResult;

	bool bChanged = false;
	if (fileDialogResult.State > FileDialogResult::State::Opened)
	{
		if (fileDialogResult.Target == target && fileDialogResult.Serializer == serializer && fileDialogResult.State == FileDialogResult::State::Validated)
		{
			*value = fileDialogResult.Value;
			bChanged = true;
		}
		fileDialogResult.Reset();
	}

	auto iconsFont = FeModuleEditor::Get()->GetContext().Visual.IconsFont;
	ImGui::PushFont(iconsFont);
	
	if (ImGui::Button("z", ImVec2(20, 20)))
	{
		fileDialogResult.Target = target;
		fileDialogResult.Serializer = serializer;
		fileDialogResult.PreviousValue = *value;
		fileDialogResult.State = FileDialogResult::State::Opened;

		auto fileBrowser = FeModuleEditor::Get()->GetComponents().GetFileBrowser();
		auto openPopupAction = FeModuleEditor::Get()->CreateAction<FeEditorActionShowPopup>(false, "Edit", FeFileBrowserComponent::ClassName(), [](bool bCancel) -> void
		{
			if (false == bCancel)
			{
				fileDialogResult.State = FileDialogResult::State::Canceled;

				auto fileBrowser = FeModuleEditor::Get()->GetComponents().GetFileBrowser();
				bool bImportedSomething = false;
				const auto& files = fileBrowser->SelectedFiles;

				for (auto& file : files)
				{
					fileDialogResult.Value = *file.File;
					fileDialogResult.State = FileDialogResult::State::Validated;
					break;
				}
			}
		});
		fileBrowser->Reset(); // reset file browser
		fileBrowser->RefreshContent(".*");
		fileBrowser->bAllowMultiselect = false;

		FeModuleEditor::Get()->ActionDo(openPopupAction);
	}
	ImGui::PopFont();
	ImGui::SameLine();

	const uint32 BufSize = 2048;
	static FeStaticString<BufSize> strDrawString;
	static const void* strDrawStringPtr = nullptr;

	if (strDrawStringPtr != value)
	{
		FeFileTools::FormatFullPath(*value, strDrawString);
		strDrawStringPtr = value;
	}
	static char FileName[FE_STRING_SIZE_128];
	sprintf_s(FileName, "%s/%s%s", value->Dir.CstrNotNull(), value->File.CstrNotNull(), value->Ext.CstrNotNull());
	ImGui::InputText("", FileName, COMMON_PATH_SIZE - 1, ImGuiInputTextFlags_ReadOnly);
	
	return bChanged;
}
bool ComboBoxItemFillCallback(void* data, int idx, const char** out_text)
{
	static const char sEmpty[] = "";

	if (idx == 0)
	{
		*out_text = sEmpty;
	}
	else
	{
		idx--;

		FeTArray<FeAssetFile*>& assetFiles = *(FeTArray<FeAssetFile*>*)data;

		if (assetFiles.GetSize() <= idx)
			return false;

		*out_text = assetFiles[idx]->Asset->ID.Name.CstrNotNull();
	}
	
	return true;
}
bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, FeAssetRef* value)
{
	bool bChanged = false;

	uint32 selected = 0;

	FeTArray<FeAssetFile*> assetFiles;
	FeModuleUi::Get()->AssetsRegistry.GetAssetFiles(value->Type, assetFiles);
	FeAssetFile* currentAssetFile = nullptr;

	{ // Compute all asset names of type

		selected = assetFiles.IndexOf([&](const FeAssetFile* p) -> bool { return p->AssetRef == *value; });

		if (selected == FE_INVALID_ID)
		{
			selected = 0;
		}
		else
		{
			currentAssetFile = assetFiles[selected];
			selected += 1;
		}
	}
	{
		int32 newSelected = (int32)selected;
		{
			auto& context = FeModuleEditor::Get()->GetContext();
			auto iconsFont = context.Visual.IconsFont;
			ImGui::PushFont(iconsFont);

			SCOPE_ID(buttonAsset, &value->Path);

			if (ImGui::Button("G", ImVec2(20,20))) // apply value from content browser selection
			{
				auto selectedAsset = FeModuleEditor::Get()->GetComponents().GetContentBrowser()->ClickedAsset;
				if (selectedAsset && selectedAsset->IsOfType<FeImage>())
				{
					*value = selectedAsset->AssetRef;
					bChanged = true;
				}
			}

			ImGui::SameLine();

			if (ImGui::Button("z", ImVec2(20, 20))) // select referenced asset for editing
			{
				if (currentAssetFile)
					context.Data.AddToSelection(currentAssetFile);
			}

			ImGui::PopFont();
		}
		ImGui::SameLine();

		ImGui::Combo("", &newSelected, ComboBoxItemFillCallback, &assetFiles, assetFiles.GetSize()+1);
		
		if (newSelected != selected)
		{
			if (newSelected == 0)
				value->Path = "";
			else
				*value = assetFiles[newSelected-1]->AssetRef;

			bChanged = true;
		}
	}

	if (value->Type == FeAssetType::Image())
	{
		void* textureId = nullptr;

		auto pImg = FeModuleUi::Get()->AssetsRegistry.GetAsset<FeImage>(*value);

		if (pImg)
		{
			auto pResourcesHandler = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();
			const FeRenderResource*  pResource = pResourcesHandler->GetResource(pImg->GetResourceId());
			if (pResource)
			{
				textureId = pResource->GetD3DSRV(0);
			}
			else
			{
				FeModuleUi::Get()->LoadImage(pImg);
			}
		}
		if (ImGui::ImageButton(textureId, ImVec2(40, 40)))
		{}
	}

	return bChanged;
}
bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, float* value)
{
	return ImGui::InputFloat("", value, 0.1f);
}
bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, int32* value)
{
	return ImGui::InputInt("", value);
}
bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, FeVector2* value)
{
	return ImGui::InputFloat2("", value->mData, 3);
}
bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, FeVector3* value)
{
	return ImGui::InputFloat3("", value->mData, 3);
}
bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, FeVector4* value)
{
	return ImGui::ColorEdit4("", value->mData);
}
bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, FeSerializable* value);


bool DrawUiBinding(FeSerializable* root, FeSerializer* serializer, FeUiBinding* value, bool bIsTarget, const char* name, bool* bRemove = nullptr)
{
	bool bChanged = false;
	FeEUiBindingType::Type& eBindType = value->GetType();

	SCOPE_ID(binding, value);

	int32 Height = 80;
	if (bIsTarget)
		Height = 130;
	else if (eBindType == FeEUiBindingType::Database || eBindType == FeEUiBindingType::Asset_Image)
		Height = 150;

	ImGui::PushStyleVar(ImGuiStyleVar_ChildWindowRounding, 1.0f);
	ImGui::BeginChild(name, ImVec2(-1, Height), true);
	ImGui::Text(name);
	if (bRemove)
	{
		ImGui::SameLine();
		*bRemove = ImGui::Button("Remove");
	}
	ImGui::Columns(2);
	ImGui::SetColumnOffset(1, 60);

	if (bIsTarget)
	{
		{	SCOPE_PROPERTYGRID_ELEMENT(Property);
			bChanged = DrawString(&value->GetProperty(), false, ImVec2(), true) || bChanged;
		}
		{	SCOPE_PROPERTYGRID_ELEMENT(Index);
			bChanged = ImGui::InputInt("", &value->GetIndex()) || bChanged;
		}
		{
			SCOPE_PROPERTYGRID_ELEMENT(Path);
			{
				SCOPE_ID(path, &value->GetPath());
				ImGui::Columns(2);
				ImGui::SetColumnOffset(1, 30);
				
				uint32 indexEntry = 0;
				uint32 indexRemove = FE_INVALID_ID;
				for (auto& entry : value->GetPath())
				{
					ImGui::AlignFirstTextHeightToWidgets();
					{
						SCOPE_ID(remove, &entry);
						if (ImGui::SmallButton("-"))
							indexRemove = indexEntry;
					}

					ImGui::NextColumn();
					ImGui::PushItemWidth(-1);
					// todo: refactor this!
					//bChanged = DrawString(&entry.Name, false, ImVec2(), true) || bChanged;
					ImGui::PopItemWidth();
					SkipColumns(1);
					indexEntry++;
				}

				if (indexRemove != FE_INVALID_ID)
					value->GetPath().RemoveAt(indexRemove);

				SkipColumns(1);
			}
		}
		ImGui::Columns(1);
		{
			SCOPE_ID(buttonAdd, (&value->GetPath())+1);
			if (ImGui::SmallButton("+"))
				value->GetPath().Add();
		}
		SkipColumns(2);
	}
	else
	{
		{
			SCOPE_PROPERTYGRID_ELEMENT(Type);
			DrawEnum((int32*)(&eBindType), FeEUiBindingType::GetValues(), FeEUiBindingType::Count, true);
		}

		switch (eBindType)
		{
		case FeEUiBindingType::Database:
		{
			SCOPE_PROPERTYGRID_ELEMENT(SQL);
			bChanged = DrawString(&value->GetValue(), true, ImVec2(0, 100), true) || bChanged;
		} break;
		case FeEUiBindingType::Text:
		{
			SCOPE_PROPERTYGRID_ELEMENT(Value);
			bChanged = DrawString(&value->GetValue(), false, ImVec2(0, 0), true) || bChanged;
		} break;
		case FeEUiBindingType::Variable:
		{
			SCOPE_PROPERTYGRID_ELEMENT(Variable);
			bChanged = DrawString(&value->GetValue(), false, ImVec2(0, 0), true) || bChanged;
		} break;
		case FeEUiBindingType::Asset_Image:
		{
			SCOPE_PROPERTYGRID_ELEMENT(Variable);
			FeAssetRef previousValue = value->Asset;
			value->Asset.Type = FeAssetType::Image();
			bChanged = FePropertyEditorDeseserializeValue(root, serializer, &value->Asset) || bChanged;

		} break;
		}
	}
	ImGui::EndChild();
	ImGui::PopStyleVar();

	return bChanged;
}
bool FePropertyEditorDeseserializeValue(FeSerializable* root, FeSerializer* serializer, FeUiDataBinding* value)
{
	bool bChanged = false;
	
	ImGui::Separator();

	{ // draw right side of proprety grid
		ImGui::AlignFirstTextHeightToWidgets();
		
		{	SCOPE_ID(Multiple, &value->GetMultiple());
			ImGui::Text("Multiple");
			ImGui::SameLine();
			if (ImGui::Checkbox("", &value->GetMultiple()))
				bChanged = true;
		}
		ImGui::SameLine();
		{	SCOPE_ID(Multiple, &value->GetApplyVisibility());
			ImGui::Text("Apply Visibility");
			ImGui::SameLine();
			if (ImGui::Checkbox("", &value->GetApplyVisibility()))
				bChanged = true;
		}

		SkipColumns(2);
	}
	SkipColumns(2);

	bChanged |= DrawUiBinding(root, serializer, &value->GetSource(), false, "Source");
	auto& targetBinds = value->GetTargets();

	uint32 iRemoveIdx = FE_INVALID_ID;

	for (int32 targetBindIdx = 0; targetBindIdx < targetBinds.GetSize(); ++targetBindIdx)
	{
		SkipColumns(2);

		auto& targetBind = targetBinds[targetBindIdx];
		bool bRemove = false;
		char strName[32];
		sprintf_s(strName, "Target [%d]", targetBindIdx);
		DrawUiBinding(root, serializer, &targetBind, true, strName, &bRemove);
		if (bRemove)
			iRemoveIdx = targetBindIdx;
	}
	
	if (iRemoveIdx != FE_INVALID_ID)
		targetBinds.RemoveAt(iRemoveIdx);

	SkipColumns(2);
	
	if (ImGui::Button("AddTarget"))
		targetBinds.Add();

	SkipColumns(2);
	ImGui::Separator();

	return bChanged;
}

uint32 FePropertyEditorDeseserialize(FeSerializable* target, FeSerializer* serializer, FeTransform* value)
{
	int32 iRes = FeEReturnCode::Success;
	
	bool bShowProperty = true;
	
	// special cases when we want to show/hide property  <== todo : make this object oriented ?
	FeWidget* widget = nullptr;

	if (target->IsChildOf(FeWidget::ClassName()))
	{
		widget = (FeWidget*)target;

		static FeString strDesignerTransform	= "DesignerTransform";
		static FeString strTransform			= "Transform";

		const FeString& strPropertyName = serializer->GetCurrentPropertyName();

		if (strPropertyName == strDesignerTransform && !widget->IsRoot())
			bShowProperty = false;
		//else if (strPropertyName == strTransform && bIsRoot)
		//	bShowProperty = false;
	}

	if (value && bShowProperty)
	{
		ImGui::PushID(value);
		{
			ImGui::AlignFirstTextHeightToWidgets();
			const auto& propertyName = serializer->GetCurrentPropertyName();

			uint32 nodeFlags = ImGuiTreeNodeFlags_DefaultOpen;

			bool node_open = ImGui::TreeNodeEx("Object", nodeFlags, propertyName.CstrNotNull());

			static FeModuleEditor* moduleEditor = nullptr;
			if (moduleEditor == nullptr)
				moduleEditor = FeApplication::GetStaticInstance()->GetModule<FeModuleEditor>();

			bool bUsePixel = false==moduleEditor->GetContext().Visual.IsToggled(FeEEditorToggle::NormalizedValues);
			const auto& nativeResolution = moduleEditor->GetContext().Visual.Viewport.NativeResolution;

			ImGui::NextColumn();
			struct TransformAxisLocks
			{
				bool T[3], R[3], S[3];
				TransformAxisLocks()
				{
					memset(this, 0, sizeof(TransformAxisLocks));
				}
			};
			static TransformAxisLocks axisLocks;

			char strTransform[64];
			sprintf(strTransform, "T(%.2f,%.2f,%.2f) S(%.2f,%.2f,%.2f)"
				, value->Translation.getData()[0], value->Translation.getData()[1], value->Translation.getData()[2]
				, value->Scale.getData()[0], value->Scale.getData()[1], value->Scale.getData()[2]);
			PropertyDrawHeader(strTransform);

			static FeVector3 previousValue;

			auto drawVec3Input = [&](FeVector3& vec3, bool bInvertY, bool* axisLocks, bool bScaling) -> bool
			{
				static FeVector3 tmp;
				tmp = vec3;
				previousValue = vec3;

				bool bVecChanged = false;

				if (bUsePixel)
				{
					float yFactor = (bInvertY ? -1.f : 1.f);
					
					float factors[3];
					ComputePixelConvertFactors(widget, factors, yFactor);

					int32 value[3] = { vec3[0] * factors[0], vec3[1] * factors[1], vec3[2] * factors[2]};

					if (ImGui::InputInt3("", value))
					{
						tmp[0] = value[0] / factors[0];
						tmp[1] = value[1] / factors[1];
						tmp[2] = value[2] / factors[2];

						bVecChanged =true;
					}
				}
				else
				{
					bVecChanged = ImGui::InputFloat3("", tmp.mData, 3);
				}

				if (bVecChanged)
				{
					bool bChangedAxes[3];
					int32 iChangedLockAxis = -1;
					for (int32 i = 0; i < 3; ++i)
					{
						bChangedAxes[i] = vec3[i] != tmp[i];
						if (bChangedAxes[i] && axisLocks[i])
							iChangedLockAxis = i;
					}
					if (iChangedLockAxis >= 0)
					{
						float scaling = tmp[iChangedLockAxis] / vec3[iChangedLockAxis];
						for (int32 i = 0; i < 3; ++i)
						{
							if (axisLocks[i])
								vec3[i] *= scaling;
						}
					}
					else
					{
						vec3 = tmp;
					}

					CreateChangedPropertyContainer(serializer, target, &vec3, previousValue);
				}

				return bVecChanged;
			};

			auto drawAxisLockButtons = [&](bool* locks) -> void
			{
				const char axisNames[][4] = { " x ", " y ", " z " };
				for (int32 i = 0; i < 3; ++i)
				{
					ImGui::SameLine();
					ImGui::PushID(&locks[i]);
					ImGui::PushItemWidth(20);
					if (DrawPushButton(locks[i], true, axisNames[i])) 
						locks[i] = !locks[i];

					ImGui::PopItemWidth();
					ImGui::PopID();
				}
			};

			if (node_open)
			{
				ImGui::PushID(&value->Translation);
				PropertyDrawHeader("Translation");
				{ /* draw property edit widget*/
					ImGui::AlignFirstTextHeightToWidgets();
					drawVec3Input(value->Translation, true, axisLocks.T, false);
				}
				ImGui::PopID();
				//drawAxisLockButtons(axisLocks.T);   no axis lock for translation
				ImGui::NextColumn();

				ImGui::PushID(&value->Scale);
				PropertyDrawHeader("Scale");
				{ /* draw property edit widget*/
					ImGui::AlignFirstTextHeightToWidgets();
					drawVec3Input(value->Scale, false, axisLocks.S, true);
				}
				ImGui::PopID();
				drawAxisLockButtons(axisLocks.S);
				ImGui::NextColumn();

				//ImGui::PushID(&value->Rotation);
				//PropertyDrawHeader("Rotation");
				//{ /* draw property edit widget*/
				//	ImGui::AlignFirstTextHeightToWidgets();
				//	if (ImGui::InputFloat3("", &value->Rotation[0], 0.01f))
				//		bChanged = true;
				//}
				//ImGui::PopID();
				//ImGui::NextColumn();

				ImGui::TreePop();
			}
		}
		ImGui::PopID();
	}

	return false;
}
uint32 FePropertyEditorDeseserialize(FeSerializable* target, FeSerializer* serializer, FeSerializable* value)
{
	int32 iRes = FeEReturnCode::Success;

	bool bShow = CanShowTargetProprety(target, serializer, value);

	if (value && bShow)
	{
		ImGui::PushID(value);
		{
			ImGui::AlignFirstTextHeightToWidgets();

			const auto& path = serializer->GetPath();

			if (path.CurrentDepth > 0)
			{
				const auto& indirection = path.Indirections[path.CurrentDepth];

				if (indirection.Index != FE_INVALID_ID) // if current element is an element of an array
				{
					{
						SCOPE_ID(removeEntry, value);
						auto iconsFont = FeModuleEditor::Get()->GetContext().Visual.IconsFont;
						ImGui::PushFont(iconsFont);

						if (ImGui::SmallButton("l"))
						{
							//CreateChangedArrayContainer<FeSerializable>(serializer, serializer->GetArraysPath().Back(), indirection.Index, false, target);
							CreateChangedAbstractArrayContainer(serializer, serializer->GetArraysPath().Back(), indirection.Index, false, target);
						}
						ImGui::PopFont();
					}
					ImGui::SameLine();
				}
			}

			bool bDefaultOpen = false;
			
			if (value->IsChildOf(FeImageAspect::ClassName()))
				bDefaultOpen = true;

			uint32 nodeFlags = bDefaultOpen ? ImGuiTreeNodeFlags_DefaultOpen : 0;
			bool node_open = ImGui::TreeNodeEx("Object", nodeFlags, value->GetThisClassName().CstrNotNull());

			ImGui::NextColumn();

			if (node_open)
			{
				if (value->IsChildOf(FeUiDataBinding::ClassName()))
				{
					if (FePropertyEditorDeseserializeValue(target, serializer, (FeUiDataBinding*)value))
					{
						if (target->IsChildOf(FeWidget::ClassName()))
							static_cast<FeWidget*>(target)->OnDataChanged();
					}
				}
				else
				{
					FePropertyEditorDeseserializeValue(target, serializer, value);
				}
				ImGui::TreePop();
			}

			ImGui::NextColumn();
		}
		ImGui::PopID();
	}
	return iRes;
}

bool ComboBoxItemFillEnumCallback(void* data, int idx, const char** out_text)
{
	const FeString* values = (const FeString*)data;
	*out_text = values[idx].CstrNotNull();
	return true;
}

bool DrawEnum(int32* value, const FeString* values, int32 valuesCount, bool bPushId)
{
	if (bPushId)
		ImGui::PushID(value);

	int32 selected = *value >= 0 && *value<valuesCount ? *value : 0;

	int32 selectedChanged = selected;
	bool bChanged = ImGui::Combo("", &selectedChanged, ComboBoxItemFillEnumCallback, (void*)values, valuesCount);
	
	if (bPushId)
		ImGui::PopID();

	if (selectedChanged != selected)
	{
		*value = selectedChanged;
		return true;
	}
	return false;
}
uint32 FePropertyEditorDeseserializeEnum(FeSerializable* target, FeSerializer* serializer, const FeString* values, int32 valuesCount, int32* value)
{
	int32 iRes = FeEReturnCode::Success;

	ImGui::PushID(value);
	{
		PropertyDrawHeader(serializer->GetCurrentPropertyName().CstrNotNull());
		{ /* draw property edit widget*/
			ImGui::AlignFirstTextHeightToWidgets();

			if (value)
			{
				int32 previousValue = *value;
				if (DrawEnum(value, values, valuesCount, false))
					CreateChangedPropertyContainer(serializer, target, value, previousValue);
			}

			ImGui::NextColumn();
		}
	}
	ImGui::PopID();
	return iRes;
}

class FePropertyEditorSerializer : public FeSerializer
{
public:
	FePropertyEditorSerializer(FeSerializable* target, uint32 iHeapId) : FeSerializer(iHeapId), Target(target)
	{
		bReadOnly = true;
		bIsSerializingArray = false;
		bSkipArrayEntries = false;
	}
	
	#define DECLARE_SERIALIZER_FUNCTIONS(type)										\
	uint32 Serialize(const type* value) { FE_ASSERT_NOT_IMPLEMENTED_RETURN(0); }	\
	uint32 Deserialize(type* value)													\
	{																				\
		if (bIsSerializingArray && bSkipArrayEntries)								\
			return FeEReturnCode::Success;											\
																					\
		return FePropertyEditorDeseserialize(Target, this, value); 					\
	}


	virtual uint32 DeserializeEnum(const FeString* values, int32 valuesCount, fromStringFunc fromString, int32* output)
	{
		FePropertyEditorDeseserializeEnum(Target, this, values, valuesCount, output);
		return FeEReturnCode::Success;
	}

	DECLARE_SERIALIZER_FUNCTIONS(bool)
	DECLARE_SERIALIZER_FUNCTIONS(int)
	DECLARE_SERIALIZER_FUNCTIONS(float)
	DECLARE_SERIALIZER_FUNCTIONS(uint32)
	DECLARE_SERIALIZER_FUNCTIONS(uint64)
	DECLARE_SERIALIZER_FUNCTIONS(FePath)
	DECLARE_SERIALIZER_FUNCTIONS(FeVector2)
	DECLARE_SERIALIZER_FUNCTIONS(FeVector3)
	DECLARE_SERIALIZER_FUNCTIONS(FeColor)
	DECLARE_SERIALIZER_FUNCTIONS(FeTransform)
	DECLARE_SERIALIZER_FUNCTIONS(FeString)
	DECLARE_SERIALIZER_FUNCTIONS(FeSerializable)
	DECLARE_SERIALIZER_FUNCTIONS(FeAssetRef)
	DECLARE_SERIALIZER_FUNCTIONS(FeGuid)

	// ------------------------------------------------------------------------------------------------------------------------------------------------

	uint32 SerializeArray(const FeIArray* value)
	{
		FE_ASSERT_NOT_IMPLEMENTED_RETURN(0);
	}
	// ------------------------------------------------------------------------------------------------------------------------------------------------

	// Array
	uint32 DeserializeArrayBegin(FeIArray* value)
	{
		int32 iRes = FeEReturnCode::Success;
		
		if (value && CanShowTargetProprety(Target, this, value))
		{
			bIsSerializingArray = true;
			ImGui::PushID(value);
			{
				ImGui::AlignFirstTextHeightToWidgets();
				bool node_open = ImGui::TreeNode("Object", GetCurrentPropertyName().CstrNotNull());

				ImGui::NextColumn();
				ImGui::AlignFirstTextHeightToWidgets();
				char label[32];
				sprintf(label, "%u", value->GetSize());
				ImGui::Text(label);
				ImGui::SameLine();
				{
					SCOPE_ID(addEntry, value);
					auto iconsFont = FeModuleEditor::Get()->GetContext().Visual.IconsFont;
					ImGui::PushFont(iconsFont);
					if (ImGui::SmallButton("i"))
					{
						FE_ASSERT(!changedArrayContainer, "changed multiple propreties in 1 frame ?!");
						changedArrayContainer = FeChangeAbstractArrayContainer::Create(GetArraysPath().Back(), 
							-1, true, Target, GetCurrentPropertyName());
					}
					ImGui::PopFont();
				}
				ImGui::NextColumn();
				
				bSkipArrayEntries = !node_open;
			}
		}
		return FeEReturnCode::Success;
	}
	uint32 DeserializeArrayEnd(FeIArray* value)
	{
		if (value && CanShowTargetProprety(Target, this, value))
		{
			if (!bSkipArrayEntries)
				ImGui::TreePop();

			ImGui::PopID();

			bIsSerializingArray = false;
			bSkipArrayEntries = false;
		}

		return FeEReturnCode::Success;
	}

	FeSerializable* Target;
	bool bIsSerializingArray;
	bool bSkipArrayEntries;
};

bool CanShowTargetProprety(FeSerializable* container, FeSerializer* serializer, void* value)
{
	auto propSerializer = static_cast<FePropertyEditorSerializer*>(serializer);
	bool bShow = true;
	// skip property if we are editing a template reference
	if (propSerializer->Target && propSerializer->Target->IsChildOf(FeWidget::ClassName()))
	{
		auto widget = static_cast<FeWidget*>(propSerializer->Target);
		bShow = widget->Template.Path.IsEmpty() 
			|| (void*)value == (void*)&widget->Template
			|| (void*)value == (void*)&widget->ID.Name
			|| (void*)container == (void*)&widget->Transform;
	}
	return bShow;
}

bool FePropertyEditorDeseserializeValue(FeSerializable* target, FeSerializer* serializer, FeSerializable* value)
{
	int32 iRes = FeEReturnCode::Success;
	
	// draw right side of proprety grid
	ImGui::AlignFirstTextHeightToWidgets();
	ImGui::Text("");

	ImGui::NextColumn();

	if (value)
	{
		FePropertyEditorSerializer serializer(value, FE_HEAPID_UI);
		iRes = value->Deserialize(&serializer);
		ImGui::NextColumn();
	}
	return FeEReturnCode::Success;
}

void FePropertyEditorComponent::Draw(FeEditorContext& context)
{
	BeginWindow();

	if (!Begin("Property editor"))
		return;

	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2, 2));
	ImGui::Columns(2);
	ImGui::Separator();

	if (Target)
	{
		FeSerializable* actualTarget = Target;
		
		bool bIsSequenceWidgetContainer =
			context.Mode & FeEEditorMode::Sequencer &&
			Target->IsChildOf(FeWidget::ClassName()) &&
			static_cast<FeWidget*>(Target)->IsRoot();

		if (Target->IsChildOf(FeAssetFile::ClassName()))
			actualTarget = static_cast<FeAssetFile*>(Target)->Asset.Get();

		if (false == bIsSequenceWidgetContainer)
		{
			FePropertyEditorSerializer serializer(actualTarget, FE_HEAPID_UI);
			actualTarget->Deserialize(&serializer);
		}

		if (context.Mode & FeEEditorMode::Sequencer)
		{
			if (changedPropertyContainer)
			{
				FeModuleEditor::Get()->GetComponents().GetSequencer()->OnPropertyChanged(changedPropertyContainer);
				
				changedPropertyContainer->SelfDelete(FE_HEAPID_EDITOR);
				changedPropertyContainer = nullptr;
			}
		}
		else
		{
			if (changedPropertyContainer)
			{
				auto editAction = FE_NEW(FeEditorActionChangeProperty, DEFAULT_HEAP);
				editAction->Container = changedPropertyContainer;
				char tmpName[128];
				sprintf_s(tmpName, "Change [%s] : %s", actualTarget->GetThisClassName().Cstr(), changedPropertyContainer->PropertyName);
				editAction->Name = tmpName;
				FeModuleEditor::Get()->ActionDo(editAction);

				changedPropertyContainer->SelfDelete(FE_HEAPID_EDITOR);
				changedPropertyContainer = nullptr;
			}
			if (changedArrayContainer)
			{
				changedArrayContainer->Do();
				changedArrayContainer->SelfDelete(FE_HEAPID_EDITOR);
				changedArrayContainer = nullptr;
			}
		}
	}

	//ImGui::Columns(1);
	//ImGui::Separator();
	ImGui::PopStyleVar();
	
	EndWindow();
}