#include <moduleeditor.hpp>

#include "filesystem.hpp"
#include <d3dx11include.hpp>
#include <rendering/modulerenderer.hpp>
#include <rendering/renderresourceshandler.hpp>

#include <ui/uiuserinputhandler.hpp>
#include <ui/moduleui.hpp>

#include "imgui.hpp"
#include <imgui.h>

#include <SDL.h>

static ID3D11ShaderResourceView* g_pFontTextureView = NULL;
uint32 FeEditorConfig::GetActionIconId(const FeString& ActionGroupName, const FeString& ActionName)
{
	uint32 groupIdx = ActionGroups.IndexOf([&](const FeEditorActionGroupData& entry) -> bool { return entry.Name == ActionGroupName; });

	if (groupIdx != FE_INVALID_ID)
	{
		uint32 iconIdx = ActionGroups[groupIdx].Actions.IndexOf([&](const FeEditorActionData& entry) -> bool { return entry.ActionName == ActionName; });
		if (iconIdx != FE_INVALID_ID)
			return ActionGroups[groupIdx].Actions[iconIdx].Icon;
	}
	return 0;
}
void FeEditorMenu::FetchActionRef()
{
	ActionPtr = FeModuleEditor::Get()->GetAction(Action);
	for (auto& entry : Entries)
		entry.FetchActionRef();
}
void FeModuleEditor::SelectStartWidget()
{
	ClearSelection();
	
	// go to start widget
	for (auto& widget : FeModuleUi::Get()->GetRootWidgets())
	{
		widget.IsVisible = widget.AssetFile->AssetRef == FeModuleUi::Get()->GetTheme().RootWidgets.GetStart();
		if (widget.IsVisible)
		{
			Context.Data.AddToSelection(widget.Root);
		}
	}
}
void FeModuleEditor::SwitchMode()
{
	if (Context.Mode & FeEEditorMode::Edit)
		Context.Mode = FeEEditorMode::Play;
	else
		Context.Mode = FeEEditorMode::Edit;

	FeModuleUi::Get()->RefreshWidgets(false);
	SelectStartWidget();
}
uint32 FeModuleEditor::Reload()
{
	Unload();

	ActionsHistoryIndex = 0;
	LastSaveActionHistorySize = 0;
	LastSaveActionHistoryIndex = 0;
	
	if (Components.Map.size() == 0)
	{ // Create components
		Components.Add<FePropertyEditorComponent>(true, FeEDock::Right);
		Components.Add<FeThemeConfigEditorComponent>(true, FeEDock::Right);
		Components.Add<FeEmulatorComponent>(true, FeEDock::Right);
		Components.Add<FeWidgetListComponent	>(true, FeEDock::Right | FeEDock::Bottom);
		Components.Add<FeContentBrowserComponent>(true, FeEDock::Bottom);
		Components.Add<FeSequencerComponent		>(true, FeEDock::Bottom);

		Components.Add<FeStyleComponent			>(true);
		Components.Add<FeMenuComponent			>(true);
		Components.Add<FeToolbarComponent		>(true);
		Components.Add<FeStatusBarComponent		>(true);
		Components.Add<FeOverlayComponent		>(false);
		Components.Add<FeFileBrowserComponent	>(false);
		Components.Add<FeAddWidgetComponent		>(false);
	}

	Components.GetThemeEditor()->Target = &FeModuleUi::Get()->GetTheme();
	Components.GetContentBrowser()->RefreshTree();

	FeJson::Deserialize(Config, FeEditorConfig::GetSerializePath(), FE_HEAPID_FILESYSTEM);

	auto UiModule = FeApplication::GetStaticInstance()->GetModule<FeModuleUi>();
	
	Context.Reset();
	FeJson::Deserialize(Context, FeEditorContext::GetSerializePath(), FE_HEAPID_FILESYSTEM);
	Context.Mode = FeEEditorMode::Edit;

	UserInput = dynamic_cast<FeUiUserMouseInputHandler*>(UiModule->GetNavigationHandler().GetUser(0).GetNull());

	{ // Create actions
		CreateAction<FeEditorActionSaveAll>(true, "File");
		CreateAction<FeEditorActionQuit>(true, "File");
		CreateAction<FeEditorActionImportImages>(true, "File");

		FeTArray<const FeString*> assetTypes;
		FeAssetType::GetCreatableTypes(assetTypes);
		for (auto& assetType : assetTypes)
			CreateAction<FeEditorActionCreateAsset>(true, "File", *assetType);

		CreateAction<FeEditorActionUnDo>(true, "Edit");
		CreateAction<FeEditorActionReDo>(true, "Edit");

		CreateAction<FeEditorActionContentBrowserRemove>(true, "ContentBrowser");
		CreateAction<FeEditorActionContentBrowserRename>(true, "ContentBrowser");
		
		CreateAction<FeEditorActionAddWidget>(true, "Edit", FeWidget::ClassName());
		CreateAction<FeEditorActionAddWidget>(true, "Edit", FeUiImage::ClassName());
		CreateAction<FeEditorActionAddWidget>(true, "Edit", FeUiText::ClassName());
		CreateAction<FeEditorActionAddWidget>(true, "Edit", FeUiEmulatorViewport::ClassName());

		CreateAction<FeEditorActionShowPopup>(true, "Edit", FeAddWidgetComponent::ClassName(), nullptr); // show popup : add ui widget
		
		CreateAction<FeEditorActionRemoveWidget>(true, "Edit");
		
		for (int32 i = 0; i<FeEEditorToggle::None;++i)
			CreateAction<FeEditorActionToggleOption>(true, "Toggles", (FeEEditorToggle::Type)i);
	}
	{ // Init toolbar
		ImGuiIO& io = ImGui::GetIO();

		FeStaticString<FE_STRING_SIZE_128> fullPath;
		FePath ttfPath = "editor/fonts/default.ttf";
		
		FeFileTools::FormatFullPath(ttfPath, fullPath);
		io.Fonts->AddFontFromFileTTF(fullPath.Value, 14);
		
		ttfPath = "editor/fonts/icons-1.ttf";
		FeFileTools::FormatFullPath(ttfPath, fullPath);
		Context.Visual.IconsFont = io.Fonts->AddFontFromFileTTF(fullPath.Value, 18);

		Components.GetToolbar()->Buttons.Clear();

		if (Context.Visual.Toggles.GetSize() == 0)
			Context.Visual.Create();
		
		for (auto& action : Actions)
		{
			if (action->Icon != 0)
			{
				auto& button = Components.GetToolbar()->Buttons.Add();
				button.Action = action.Get();
			}
		}
	}
	{ // Init : Add ui widget
		Components.GetAddWidget()->Buttons.Clear();
		Components.GetAddWidget()->Buttons.Add(FeEditorButton(GetAction("Edit", FeStringTools::Format("Create : %s", FeWidget::ClassName().Cstr()))));
		Components.GetAddWidget()->Buttons.Add(FeEditorButton(GetAction("Edit", FeStringTools::Format("Create : %s", FeUiImage::ClassName().Cstr()))));
		Components.GetAddWidget()->Buttons.Add(FeEditorButton(GetAction("Edit", FeStringTools::Format("Create : %s", FeUiText::ClassName().Cstr()))));
		Components.GetAddWidget()->Buttons.Add(FeEditorButton(GetAction("Edit", FeStringTools::Format("Create : %s", FeUiEmulatorViewport::ClassName().Cstr()))));
	}
	{ // Init menus
		Components.GetMenu()->Entries.Free();

		for (auto& menu : Config.Menus) // register in imgui component
		{
			menu.FetchActionRef();
			Components.GetMenu()->Entries.Add(&menu);
		}
	}
	{ // Init imgui renderer
		auto Renderer = FeModuleRendering::Get();
		ImGui_ImplDX11_Init(Renderer->GetDevice().GetD3DDevice(), Renderer->GetDevice().GetImmediateContext());
		ImGui_ImplDX11_CreateDeviceObjects();
		Renderer->RegisterRenderCallback(RenderImgui);
		SetImguiDirty(false);
	}

	UiModule->GetSequencer().SetPaused(true);

	Viewport.Initialize();
	Context.Visual.Viewport.LoadGeometryAndEffects(Viewport.RenderBatch);
	Components.GetStatusBar()->Status.Set("Idle", FeEColor::White);

	SelectStartWidget();

	return FeEReturnCode::Success;
}
uint32 FeModuleEditor::Load(const FeModuleInit* initBase)
{
	Components.CreateTabContainers();

	Init = *(FeModuleEditorInit*)initBase;
	auto RenderingModule = FeModuleRendering::Get();
	
	return Reload();
}
uint32 FeModuleEditor::Unload()
{
	ActionsHistory.Free();
	Actions.Free();

	ImGui_ImplDX11_Shutdown();

	return FeEReturnCode::Success;
}
void FeModuleEditor::ClearSelection()
{
	Context.Data.ClearSelection();

	Components.GetPropertyEditor()->Target = nullptr;
	Components.GetOutline()->Target.Widgets.Clear();
	Components.GetOutline()->Selected = nullptr;

	Components.GetContentBrowser()->ClearSelection();
	Components.GetContentBrowser()->ComputeContentList();
}
void FeModuleEditor::OnKeyReleased(int32 key)
{
	Context.Input.OnKeyReleased(key);
}
void FeModuleEditor::OnKeyPressed(int32 key)
{
	Context.Input.OnKeyPressed(key);
}
void FeModuleEditor::OnMouseWheel(int32 y)
{
	Context.Input.OnMouseWheel(y);
}
bool FeModuleEditor::IsKeyDown(char key)
{
	return Context.Input.IsKeyDown(key);
}
uint32 FeModuleEditor::Update(const FeDt& fDt)
{
	{ // Todo : data driven shortcuts binding
		const Uint8 *state = SDL_GetKeyboardState(NULL);
		if (state[SDL_SCANCODE_LCTRL])
		{
			if (Context.Input.IsKeyReleased(SDL_SCANCODE_W)) ActionUndo();
			if (Context.Input.IsKeyReleased(SDL_SCANCODE_Y)) ActionRedo();
			if (Context.Input.IsKeyReleased(SDL_SCANCODE_S)) SaveAll();
		}
		if (Context.Input.IsKeyReleased(SDL_SCANCODE_ESCAPE)) HidePopup(true);
		
		if (Viewport.IsCursorOver())
		{
			if (Context.Input.IsKeyReleased(SDL_SCANCODE_DELETE))
			{
				static FeEditorActionRef actionRef("Edit", "Remove Ui Widget");
				ActionDo(GetAction(actionRef));
			}
			if (Context.Input.IsKeyReleased(SDL_SCANCODE_RETURN))
			{
				static FeEditorActionRef actionRef("Edit", "Show Popup : FeAddWidgetComponent");
				ActionDo(GetAction(actionRef));
			}
		}
	}

	auto resolution = FeModuleRendering::Get()->GetScreenDimensions();

	if (!FeModuleRendering::Get()->GetDevice().IsValid())
		return FeEReturnCode::Success;

	
	bool bIsInEditMode = Context.Mode & FeEEditorMode::Edit;
	
	FeModuleUi::Get()->SetIsRenderEnabled(!bIsInEditMode || !Context.Visual.IsToggled(FeEEditorToggle::SchematicView));
	FeModuleUi::Get()->GetSequencer().SetPaused(bIsInEditMode);
	FeModuleUi::Get()->GetNavigationHandler().SetIsEnabled(!bIsInEditMode);

	Context.Dt = fDt;
	bool bIsOverlayVisible = Components.GetOverlay()->IsVisible;

	{ // Compute dragging context
		// Start
		if (Context.Input.MouseKeysChanged[0] && Context.Input.MouseKeysDown[0])
		{
			Context.Data.Drag.Reset();
			Context.Data.Drag.Start.Position = Context.Input.CusrorPosition;
			Context.Data.Drag.State = FeEDragContextState::Start;
		}
		// Drag
		else if (!Context.Input.MouseKeysChanged[0] && Context.Input.MouseKeysDown[0])
		{
			FeVector2 cursorDelta = Context.Input.CusrorPositionDelta;
			if (FeMath::Length(cursorDelta) > 0.0001f)
			{
				Context.Data.Drag.State = FeEDragContextState::Drag;
			}
		}
		else if (Context.Input.MouseKeysChanged[0])
		{
			// Drop
			if (Context.Data.Drag.State == FeEDragContextState::Drag)
			{
				Context.Data.Drag.End.Position = Context.Input.CusrorPosition;
				Context.Data.Drag.State = FeEDragContextState::Drop;

				auto draggedData = Context.Data.Drag.Data;

				if (Viewport.IsCursorOver() && draggedData)
				{
					if (draggedData->Asset->IsChildOf(FeWidget::ClassName()))
					{
						if (Context.Data.AssetFile != draggedData)
						{
							auto actionPtr = CreateAction<FeEditorActionAddWidget>(false, "Edit", FeWidget::ClassName());

							actionPtr.GetT<FeEditorActionAddWidget>()->OnCreatedCallback = [&](FeWidget* newWidget) -> void
							{
								newWidget->Template = draggedData->AssetRef;
								newWidget->OnTemplateChanged(false);
								newWidget->OnParentChanged();
								newWidget->PostInitialize();

								//const auto& parentT = newWidget->GetParent()->TraversalTransform;
								//const auto& globalScale = FeModuleUi::Get()->GetGlobalTransform().Scale;

								//newWidget->Transform.Scale[0] = (1.0f / FeGeometryHelper::GetMatrixScaleX(parentT)) * globalScale[0];
								//newWidget->Transform.Scale[1] = (1.0f / FeGeometryHelper::GetMatrixScaleY(parentT)) * globalScale[1];
								//newWidget->SetTransientTransform(newWidget->Transform);

								FeVector3 pixelScale = newWidget->Transform.Scale;
								const auto& nativeRes = Context.Visual.Viewport.NativeResolution;
								pixelScale[0] *= nativeRes[0];
								pixelScale[1] *= nativeRes[1];

								newWidget->SetPixelScale(pixelScale);
								newWidget->OnPropertyChangedTransform();

								FeViewportHelper::SnapWidgetToCursor(newWidget, GetViewport());

							};
							ActionDo(actionPtr);
						}
					}
					else if (draggedData->Asset->IsChildOf(FeImage::ClassName()))
					{
						auto image = static_cast<FeImage*>(draggedData->Asset.Get());
						auto actionPtr = CreateAction<FeEditorActionAddWidget>(false, "Edit", FeUiImage::ClassName());

						actionPtr.GetT<FeEditorActionAddWidget>()->OnCreatedCallback = [&](FeWidget* widget) -> void
						{
							FeViewportHelper::SnapWidgetToCursor(widget, GetViewport());

							auto widgetImage = static_cast<FeUiImage*>(widget);
							widgetImage->ImageAspect.Resize = FeEImageResize::LetterBox;

							auto& transform = widgetImage->Transform;
							widgetImage->Image = draggedData->AssetRef;
							widgetImage->OnPropertyChanged(&widgetImage->Image);

							const FeRenderResource*  pResource = FeModuleRenderResourcesHandler::Get()->GetResource(image->GetResourceId());
							if (pResource)
							{
								auto pTexture = static_cast<const FeRenderTexture*>(pResource);
								FeVector3 pixelScale(pTexture->Width, pTexture->Height, 1);
								widgetImage->SetPixelScale(pixelScale);
							}
							
							FeString newWidgetName;
							int32 NameCounter = 1;
							do
							{
								FeStringTools::Format(newWidgetName, "%s %d", image->ID.Name.CstrNotNull(), NameCounter++);
							} while (widget->GetParent()->Children.Contains([&](const FeTPtr<FeWidget>& child) -> bool { return child.Get()->ID.Name == newWidgetName; }));

							widget->ID.Name = newWidgetName;

							widget->SetTransientTransform(transform);
							widget->OnPropertyChangedTransform();
						};
						ActionDo(actionPtr);
					}
				}
			}
			// Cancel
			else
			{
				Context.Data.Drag.State = FeEDragContextState::Idle;
			}
		}
	}
	
	{
		if (Components.GetToolbar()->ButtonPressed != -1)
		{
			auto& button = Components.GetToolbar()->Buttons[Components.GetToolbar()->ButtonPressed];
			
			if (button.Action.IsValid())
				ActionDo(button.Action);

			FeJson::Serialize(&Context, FeEditorContext::GetSerializePath());
		}
		ImGuiIO& io = ImGui::GetIO();
		
		if (UserInput)
			UserInput->Update();

		io.DisplaySize = ImVec2((float)resolution.GetWidth(), (float)resolution.GetHeight());
		io.DeltaTime = fDt.TotalSeconds;

		{ // Compute visual layout
			int32 rightWidgetW = (float)resolution.GetWidth() * 0.33f;
			if (rightWidgetW < 600)
				rightWidgetW = 600;
			
			{
				FeEditorDocker docker(resolution);

				Context.Visual.Layout.Menu = docker.Dock(FeEDock::Top, 0, 20);
				Context.Visual.Layout.StatusBar = docker.Dock(FeEDock::Bottom, 0, 20);
				Context.Visual.Layout.Toolbar = docker.Dock(FeEDock::Top, 0, 35);

				float aspectRatio = Context.Visual.Viewport.GetAspectRatio();
				FeRect rectRight = docker.Dock(FeEDock::Right, rightWidgetW, 0);
				Context.Visual.Layout.Viewport = docker.Dock(FeEDock::Left | FeEDock::Top, 0, (int32)(docker.CurrentOffset.GetWidth() * aspectRatio));

				docker.CurrentOffset.left = 0;
				Context.Visual.Layout.TabContainerBottom = docker.Dock(FeEDock::Bottom, 0, 0);
				FeEditorDocker dockerR(rectRight);

				Context.Visual.Layout.TabContainerRight = dockerR.Dock(FeEDock::Top, 1.f, 0.5f);
				Context.Visual.Layout.TabContainerBottomRight = dockerR.Dock(FeEDock::Bottom, 1.f, 1.f);
				
				Components.TabContainers[FeEDock::Right]->Rect					= Context.Visual.Layout.TabContainerRight;
				Components.TabContainers[FeEDock::Right | FeEDock::Bottom]->Rect = Context.Visual.Layout.TabContainerBottomRight;
				Components.TabContainers[FeEDock::Bottom]->Rect					= Context.Visual.Layout.TabContainerBottom;

				Components.GetMenu()->Rect				= Context.Visual.Layout.Menu;
				Components.GetToolbar()->Rect			= Context.Visual.Layout.Toolbar;
				Components.GetStatusBar()->Rect			= Context.Visual.Layout.StatusBar;
			}
			{
				FeEditorDocker docker(resolution);
				Components.GetOverlay()->Rect = docker.Dock(FeEDock::Top, 0.5f, 0.5f);
			}
		}

		{ // Compute selection

			{ 
				// select widget from viewport picking
				if (UserInput && UserInput->ButtonStateChanged(0) && UserInput->IsButtonDown(0) && false == Context.Data.ViewportHoverStack.IsEmpty())
				{
					Context.Data.AddToSelection(Context.Data.ViewportHoverStack.Back()); 
				}// select widget from outline componenent
				else if (Components.GetOutline()->IsSelectionChanged())
				{
					Context.Data.AddToSelection(Components.GetOutline()->Selected);
				}// select content browser selection
				else if (Components.GetContentBrowser()->IsSelectionChanged() && Components.GetContentBrowser()->IsSelectionValid())
				{
					Context.Data.AddToSelection(Components.GetContentBrowser()->GetSelection());
				}
			}

			if (false==Context.Data.IsSelectionEmpty())
			{
				Components.GetOutline()->Selected = Context.Data.GetSelectedWidget();
				Components.GetPropertyEditor()->Target = Context.Data.Selection[0].Pointer;
				
				for (auto& rootWidget : FeModuleUi::Get()->GetRootWidgets())
					rootWidget.IsVisible = rootWidget.Root == Context.Data.SelectionRootWidget;
			}
		}
		
		if (!bIsOverlayVisible)
			Viewport.Update(Context);

		// Compute data context
		{
			FeWidgetList widgetsList;
			for (auto& rootWidget : FeModuleUi::Get()->GetRootWidgets())
				widgetsList.Widgets.Add(rootWidget.Root);

			Components.GetOutline()->Target = widgetsList;
		}
		
		if (bIsInEditMode)
		{
			Context.Input.Update(PreviousContext, UserInput); // Register inputs
			Viewport.Draw(Context); // Update viewport
		}
	}

	// Start the frame
	ImGui::NewFrame();

	for (auto entry : Components.Map)
	{
		if (entry.second->IsVisible && !entry.second->IsOverlay)
			entry.second->Draw(Context);
	}
	for (auto entry : Components.TabContainers)
	{
		entry.second->Draw(Context);
	}

	if (bIsInEditMode)
	{
		if (Context.Data.Drag.State == FeEDragContextState::Drag && Context.Data.Drag.Data)
		{
			auto dragged = Context.Data.Drag.Data->Asset.Get();

			ImGui::BeginTooltip();
			ImGui::Text("Type : %s", dragged->GetThisClassName().Cstr());
			if (dragged->IsChildOf(FeImage::ClassName()))
			{
				auto image = static_cast<FeImage*>(dragged);
				const FeRenderResource*  pResource = FeModuleRenderResourcesHandler::Get()->GetResource(image->GetResourceId());
				if (pResource)
				{
					auto pTexture = static_cast<const FeRenderTexture*>(pResource);
					ImGui::Image(pTexture->D3DSRV, ImVec2(120, 120));
				}
			}
			ImGui::EndTooltip();
		}
		{
			if (Viewport.IsCursorOver() && false == Context.Data.ViewportHoverStack.IsEmpty())
			{
				ImGui::BeginTooltip();
				for (auto& widget : Context.Data.ViewportHoverStack)
				{
					float z = FeGeometryHelper::GetMatrixTranslationZ(widget->GetTraversalTransform());
					ImGui::Text("(%4.2f\t%s", z, widget->ID.Name.Cstr());
				}
				ImGui::EndTooltip();
			}

			//ImGui::SetNextWindowPos(ImVec2(10, 80));

			//bool bOpen = true;
			//if (ImGui::Begin("Debug", &bOpen, ImVec2(0, 0),0.3f, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings))
			//{
			//	ImGui::Text("Viewport %d x %d", Context.Visual.Layout.Viewport.GetWidth(), Context.Visual.Layout.Viewport.GetHeight());
			//	FeVector2 grid;
			//	FeViewportHelper::ComputeNormalizedGridSize(Context, grid);
			//	ImGui::Text("Grid %4.2f x %4.2f", grid[0], grid[1]);

			//	//ImGui::Separator();
			//	//ImGui::Text("Mouse Position: (%.1f,%.1f)", ImGui::GetIO().MousePos.x, ImGui::GetIO().MousePos.y);
			//}
			//ImGui::End();
		}
		// Compute editor mode
		{
			auto sequenceEditor = Components.GetSequencer();
			bool bSelectionChanged = SelectionChanged();
			
			if (bSelectionChanged)
			{
				auto selectedSequence = Context.Data.GetSelectedAsset<FeUiSequence>();

				if (selectedSequence)
				{
					sequenceEditor->SetSequence(Context.Data.AssetFile);
					Components.ShowComponent(sequenceEditor);
				}
			}
			
			if (Components.IsVisible(sequenceEditor))
			{
				Context.Mode = FeEEditorMode::Edit | FeEEditorMode::Sequencer;

				if (bSelectionChanged && Context.Data.SelectionRootWidget != sequenceEditor->GetWidget()->GetRootParent())
					Context.Data.AddToSelection(sequenceEditor->GetWidget());
			}
			else
			{
				if (Context.Mode & FeEEditorMode::Sequencer)
					Context.Data.AddToSelection(sequenceEditor->GetSequenceAssetFile());

				Context.Mode = FeEEditorMode::Edit;
			}
		}
	}
	SetImguiDirty(true);
	
	PreviousContext = Context;
	memcpy_s(&PreviousContext.Input, sizeof(FeEditorInputContext), &Context.Input, sizeof(FeEditorInputContext));

	return FeEReturnCode::Success;
}
void FeModuleEditor::OnShutingDown()
{
	FeJson::Serialize(&Context, FeEditorContext::GetSerializePath());
}
void FeModuleEditor::SaveAll()
{
	LastSaveActionHistorySize = ActionsHistory.GetSize();
	LastSaveActionHistoryIndex = ActionsHistoryIndex;
	FeModuleUi::Get()->SaveScripts();
	Components.GetStatusBar()->AddNotification("Saved scripts.", FeEColor::PaleGreen);
}
bool FeModuleEditor::IsSaveNeeded()
{
	return	LastSaveActionHistorySize != ActionsHistory.GetSize() || 
			LastSaveActionHistoryIndex != ActionsHistoryIndex;
}
void FeModuleEditor::HidePopup(bool bCanceled)
{
	Components.GetOverlay()->IsVisible = false;
	
	if (Components.GetOverlay()->OnCloseCallback) // call user callback
	{
		Components.GetOverlay()->OnCloseCallback(bCanceled);
		Components.GetOverlay()->OnCloseCallback = nullptr;
	}
}
void FeModuleEditor::ShowPopup(const FeString& componentName, FePopupCloseCallback onCloseCallback /*= nullptr*/, FePopupDrawCallback onDrawCallback /*= nullptr*/)
{
	Components.GetOverlay()->Content = Components.GetByName(componentName);
	Components.GetOverlay()->IsVisible = true;
	Components.GetOverlay()->OnCloseCallback = onCloseCallback;
	Components.GetOverlay()->OnDrawCallback = onDrawCallback;
}
void FeModuleEditor::ActionUnDoRedo(bool bUndo)
{
	if (bUndo && ActionsHistoryIndex == 0)
	{
		Components.GetStatusBar()->AddNotification("Nothing to undo", FeEColor::LightGray, 0.5f);
		return;
	}
	else if (!bUndo && ActionsHistoryIndex == ActionsHistory.GetSize())
	{
		Components.GetStatusBar()->AddNotification("Nothing to re do", FeEColor::LightGray, 0.5f);
		return;
	}
	FeEditorActionLogContext* pActionLog = bUndo ? &ActionsHistory[--ActionsHistoryIndex] : &ActionsHistory[ActionsHistoryIndex++];
	Context.Data = bUndo ? pActionLog->DataContextAfter :pActionLog->DataContextBefore;
	Context.Data.FetchPointers();

	if (bUndo)	pActionLog->Action->UnDo(Context);
	else		pActionLog->Action->ReDo(Context, pActionLog->DataContextAfter);

	Components.GetStatusBar()->AddNotification(pActionLog->Action->Name.CstrNotNull(), bUndo ? FeEColor::PaleGreen : FeEColor::Orange, 0.5f);
}
void FeModuleEditor::ActionUndo()
{
	ActionUnDoRedo(true);

}
void FeModuleEditor::ActionRedo()
{
	ActionUnDoRedo(false);
}
void FeModuleEditor::ActionDo(FeEditorAction* action)
{
	FeTSharedPtr<FeEditorAction> actionPtr(action);
	ActionDo(actionPtr);
}
void FeModuleEditor::ActionDo(FeTSharedPtr<FeEditorAction>& action)
{
	if (!action->CanExecute(Context))
		return;

	if (action->IsUndoable)
	{
		while (ActionsHistory.GetSize() > ActionsHistoryIndex)
		{
			ActionsHistory.PopBack();
		}

		FeEditorActionLogContext& actionLog = ActionsHistory.Add();
		actionLog.Action = action;
		actionLog.DataContextBefore = Context.Data;
		
		ActionsHistoryIndex++;
		FE_ASSERT(ActionsHistoryIndex == ActionsHistory.GetSize(), "incoherent state");
	}

	action->Do(Context);
	
	if (action->IsUndoable)
	{
		ActionsHistory.Back().DataContextAfter = Context.Data;
	}
}