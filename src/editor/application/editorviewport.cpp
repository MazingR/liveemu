#include <editorviewport.hpp>

#include <rendering/modulerenderer.hpp>
#include <rendering/commonrenderer.hpp>
#include <ui/moduleui.hpp>
#include <moduleeditor.hpp>

#include <SDL.h>

namespace FeViewportHelper
{
	void SnapWidgetToCursor(FeWidget* widget, const FeEditorViewport& viewport)
	{
		const auto& cursorPos = viewport.CursorNormalized.Position;

		auto& transform = widget->Transform;
		transform.Translation[0] = cursorPos[0];
		transform.Translation[1] = cursorPos[1];

		widget->SetTransientTransform(transform);
		widget->OnPropertyChangedTransform();
	}
	void ComputeDirection(FeVector2& vDir, FeEViewportDirection::Type dir)
	{
		switch (dir)
		{
		case FeEViewportDirection::N:	vDir[0] = 0.0f;		vDir[1] = 0.5f; break;
		case FeEViewportDirection::NE:	vDir[0] = 0.5f;		vDir[1] = 0.5f; break;
		case FeEViewportDirection::E:	vDir[0] = 0.5f;		vDir[1] = 0.0f; break;
		case FeEViewportDirection::SE:	vDir[0] = 0.5f;		vDir[1] = -0.5f; break;
		case FeEViewportDirection::S:	vDir[0] = 0.0f;		vDir[1] = -0.5f; break;
		case FeEViewportDirection::SW:	vDir[0] = -0.5f;	vDir[1] = -0.5f; break;
		case FeEViewportDirection::W:	vDir[0] = -0.5f;	vDir[1] = 0.0f; break;
		case FeEViewportDirection::NW:	vDir[0] = -0.5f;	vDir[1] = 0.5f; break;
		}
	}
	void Scale(FeViewportRect& rect, const FeVector2& scale, const FeVector2& pivot)
	{
		float& cTx = rect.GetTranslateX();
		float& cTy = rect.GetTranslateY();
		float& cSx = rect.GetScaleX();
		float& cSy = rect.GetScaleY();

		cTx -= (pivot[0]) * scale[0];
		cTy += (pivot[1]) * scale[0];

		cSx += scale[0];
		cSy += scale[1];
	}
	void Dock(FeViewportRect& rect, FeViewportRect& container, FeEViewportDirection::Type dir)
	{
		FeVector2 vDir;
		ComputeDirection(vDir, dir);

		vDir[0] += 0.5f;
		vDir[1] -= 0.5f;

		float cSx = container.GetScaleX();
		float cSy = container.GetScaleY();

		float eSx = rect.GetScaleX();
		float eSy = rect.GetScaleY();

		float tX = container.GetTranslateX() + vDir[0] * cSx - vDir[0] * eSx;
		float tY = container.GetTranslateY() + vDir[1] * cSy - vDir[1] * eSy;

		rect.SetTranslateX(tX);
		rect.SetTranslateY(tY);
	}
	void Align(FeViewportRect& rect, FeViewportRect& container, FeEViewportAlignement::Type horizontal, FeEViewportAlignement::Type vertical)
	{
		float cTx = container.GetTranslateX();
		float cTy = container.GetTranslateY();
		float cSx = container.GetScaleX();
		float cSy = container.GetScaleY();

		float& eTx = rect.GetTranslateX();
		float& eTy = rect.GetTranslateY();

		float& eSx = rect.GetScaleX();
		float& eSy = rect.GetScaleY();

		switch (horizontal)
		{
		case FeEViewportAlignement::Min: eTx = cTx; break;
		case FeEViewportAlignement::Max: eTx = cTx + cSx - eSx; break;
		case FeEViewportAlignement::Stretch:
			eSx = cSx;
			eTx = cTx;
			break;
		}
		switch (vertical)
		{
		case FeEViewportAlignement::Min: eTy = cTy; break;
		case FeEViewportAlignement::Max: eTy = cTy - cSy + eSy; break;
		case FeEViewportAlignement::Stretch:
			eSy = cSy;
			eTy = cTy;
			break;
		}
	}
	void ComputeAlignementFromDirection(FeEViewportDirection::Type dir, FeEViewportAlignement::Type& horizontal, FeEViewportAlignement::Type& vertical)
	{
		switch (dir)
		{
		case FeEViewportDirection::N: horizontal = FeEViewportAlignement::Stretch;	vertical = FeEViewportAlignement::Min;	break;
		case FeEViewportDirection::E: horizontal = FeEViewportAlignement::Max;		vertical = FeEViewportAlignement::Stretch;	break;
		case FeEViewportDirection::S: horizontal = FeEViewportAlignement::Stretch;	vertical = FeEViewportAlignement::Max;	break;
		case FeEViewportDirection::W: horizontal = FeEViewportAlignement::Min;		vertical = FeEViewportAlignement::Stretch;	break;
		default:
			horizontal = FeEViewportAlignement::None;
			vertical = FeEViewportAlignement::None;
		}
	}
	void UniformScaleAdd(FeViewportRect& rect, float value)
	{
		float& eTx = rect.GetTranslateX();
		float& eTy = rect.GetTranslateY();
		float& eSx = rect.GetScaleX();
		float& eSy = rect.GetScaleY();

		eTx -= value*0.5f;
		eTy += value*0.5f;
		eSx += value;
		eSy += value;
	}
	void ApplyOffset(FeViewportRect& rect, FeEViewportDirection::Type dir, float value)
	{
		FeVector2 vDir;
		ComputeDirection(vDir, dir);

		float& eTx = rect.GetTranslateX();
		float& eTy = rect.GetTranslateY();

		eTx += vDir[0] * value;
		eTy += vDir[1] * value;
	}
	bool IsDirectionDiagonal(FeEViewportDirection::Type dir)
	{
		switch (dir)
		{
		case FeEViewportDirection::N:
		case FeEViewportDirection::E:
		case FeEViewportDirection::S:
		case FeEViewportDirection::W:
			return false;
		default:
			return true;
		}
	}
	void ComputeNormalizedGridSize(FeEditorContext& context, FeVector2& GridSizeN)
	{
		if (context.Data.SelectionRootWidget)
		{
			int32 iGridSize = context.Data.SelectionRootWidget->GetDesignerGrid();
			const auto& rootScale = context.Data.SelectionRootWidget->DesignerTransform.Scale;
			const auto& nativeRes = context.Visual.Viewport.NativeResolution;

			FeVector2 GridSize(iGridSize / nativeRes[0], iGridSize / nativeRes[1]);

			FeRect rectViewport = context.Visual.Layout.Viewport;

			float viewportSx = rectViewport.GetWidth() / (float)context.Visual.Viewport.ScreenDimensions.GetWidth();
			float viewportSy = rectViewport.GetHeight() / (float)context.Visual.Viewport.ScreenDimensions.GetHeight();
			//FeVector3& designerScale = context.Data.SelectionRootWidget->GetDesignerTransform().Scale;

			//FeVector2 ViewportSize((1.0f / rectViewport.GetWidth())*viewportSx, (1.0f / rectViewport.GetHeight())*viewportSy);
			FeVector2 ViewportSize(viewportSx, viewportSy);

			INLINE_OP_VEC2(GridSizeN, GridSize, rootScale, *);
			INLINE_OP_VEC2(GridSizeN, GridSizeN, ViewportSize, *);
		}
	}
	void SnapToGrid(FeEditorContext& context, float& outputX, float& outputY)
	{
		if (context.Data.SelectionRootWidget)
		{
			int32 iGridSize = context.Data.SelectionRootWidget->GetDesignerGrid();
			const auto& rootScale = context.Data.SelectionRootWidget->Transform.Scale;
			const auto& nativeRes = context.Visual.Viewport.NativeResolution;

			FeVector2 GridSize(iGridSize / nativeRes[0], iGridSize / nativeRes[1]);
			
			GridSize[0] *= 1.0f / rootScale[0];
			GridSize[1] *= 1.0f / rootScale[1];

			outputX -= FeMath::FMod(outputX, GridSize[0]);
			outputY -= FeMath::FMod(outputY, GridSize[1]);
		}
	}
	void SnapToGrid(FeEditorContext& context, FeVector3& output)
	{
		SnapToGrid(context, output[0], output[1]);
	}
	void ComputeBoundingBox(FeWidget* widget, FeMatrix4& output)
	{
		float& outputTx = FeGeometryHelper::GetMatrixTranslationX(output);
		float& outputTy = FeGeometryHelper::GetMatrixTranslationY(output);
		float& outputSx = FeGeometryHelper::GetMatrixScaleX(output);
		float& outputSy = FeGeometryHelper::GetMatrixScaleY(output);
		
		outputSx = 0.0f;
		outputSy = 0.0f;
		outputTx = 100.f;
		outputTy = -100.f;

		widget->Traverse([&](FeTraversalState& traversal) -> void
		{
			const FeMatrix4& mat = traversal.Current->GetTraversalTransform();

			float Sx = FeGeometryHelper::GetMatrixScaleX(mat);
			float Sy = FeGeometryHelper::GetMatrixScaleY(mat);

			float Tx = FeGeometryHelper::GetMatrixTranslationX(mat);
			float Ty = FeGeometryHelper::GetMatrixTranslationY(mat);

			outputTx = FeMath::Min(outputTx, Tx);
			outputTy = FeMath::Max(outputTy, Ty);

			outputSx = FeMath::Max(outputSx, Sx + Tx - outputTx);
			outputSy = FeMath::Max(outputSy, Sy - Ty + outputTy);
		});
	}
}

const float FeEditorViewport::HoverPrevisionOffset = 0.005f;

void FeEditorViewport::Initialize()
{
	RenderBatch.GeometryInstances.Clear();
	RenderBatch.Viewport.Unload();
	RenderBatch.Viewport.UseBackBuffer = true;

	Transform.SetIdentity();
	DesignerTransform.SetIdentity();

	for (int32 i = 0; i < FeEditorViewportController::Count; ++i)
	{
		Overlays.Controllers[i].Type = (FeEViewportController::Type) (i / FeEViewportDirection::Count);
		Overlays.Controllers[i].Direction = (FeEViewportDirection::Type) (i % FeEViewportDirection::Count);
	}
}
bool FeEditorViewport::IsCursorOverController(FeEditorContext& context)
{
	return GetHoveredController(context) != nullptr;
}
FeEditorViewportController* FeEditorViewport::GetHoveredController(FeEditorContext& context)
{
	if (context.Data.GetSelectedWidget())
	{
		for (auto& ctrler : Overlays.Controllers)
		{
			if (FeViewportHelper::IsDirectionDiagonal(ctrler.Direction))
				if (FeMathHelper::IntersectPointWithRect(context.Input.CusrorPosition, ctrler.Rect.Matrix, HoverPrevisionOffset))
					return &ctrler;
		}
		for (auto& ctrler : Overlays.Controllers)
		{
			if (false == FeViewportHelper::IsDirectionDiagonal(ctrler.Direction))
				if (FeMathHelper::IntersectPointWithRect(context.Input.CusrorPosition, ctrler.Rect.Matrix, HoverPrevisionOffset))
					return &ctrler;
		}
	}
	return nullptr;
}
bool FeEditorViewport::IsCursorOver()
{
	return	CursorNormalized.Position.getData()[0] >= 0.f && CursorNormalized.Position.getData()[0] <= 1.f &&
			CursorNormalized.Position.getData()[1] <= 0.f && CursorNormalized.Position.getData()[1] >= -1.f;
}
void FeEditorViewport::ComputeHovered(FeEditorContext& context)
{
	auto ModuleUi = FeApplication::GetStaticInstance()->GetModule<FeModuleUi>();
	FeWidget* pHoveredWidget = nullptr;
	context.Data.ViewportHoverStack.Clear();
	
	// Compute hovered widget
	if (IsCursorOver() && !IsCursorOverController(context))
	{
		bool button1Drag = context.Data.Drag.State == FeEDragContextState::Drag && context.Data.Drag.Start.HoveredComponent == nullptr;
		int32 depth = (context.Mode & FeEEditorMode::Sequencer) ? 2 : 1;

		if (!button1Drag) // skip if we are dragging
		{
			float fHoveredZ = 999.0f;

			for (auto& widget : ModuleUi->GetRootWidgets())
			{
				if (widget.IsVisible)
				{
					widget.Root->Traverse([&, this](FeTraversalState& traversal) -> void
					{
						const FeMatrix4& mat = traversal.Current->GetTraversalTransform();
						//float fTz = FeGeometryHelper::GetMatrixTranslationZ(mat);

						bool bIntersects = FeMathHelper::IntersectPointWithRect(Cursor.Position, mat);
						if (bIntersects)
							context.Data.ViewportHoverStack.Add(traversal.Current);
					}, depth);
				}
			}
		}
	}
	{ // Sort instances along Z axis
		auto compareZ = [](const FeWidget* a, const FeWidget* b) -> int
		{
			float aZ = FeGeometryHelper::GetMatrixTranslationZ(a->GetTraversalTransform());
			float bZ = FeGeometryHelper::GetMatrixTranslationZ(b->GetTraversalTransform());

			aZ -= a->GetDepth();
			bZ -= b->GetDepth();

			if (aZ > bZ)		return 1;
			else if (aZ < bZ)	return 2;
			else				return 0;
		};

		FeArrayHelper::QuickSort<FeWidget*>::Execute(context.Data.ViewportHoverStack, compareZ);
	}
}
void FeEditorViewport::Update(FeEditorContext& context)
{
	auto resolution = FeModuleRendering::Get()->GetScreenDimensions();
	bool bIsInEditMode = (context.Mode & FeEEditorMode::Edit);

	context.Visual.Viewport.ScreenDimensions = resolution;
	FeViewportHelper::ComputeNormalizedGridSize(context, context.Visual.Viewport.GridCell);
	
	FeVector2 cursorDelta = context.Input.CusrorPositionDelta;
	FeVector2 cursorPos = context.Input.CusrorPosition;

	FeVector3 rootScale(1, 1, 1);

	if (context.Data.SelectionRootWidget)
	{
		rootScale[0] = context.Data.SelectionRootWidget->Transform.Scale[0];
		rootScale[1] = context.Data.SelectionRootWidget->Transform.Scale[1];
	}
	FeRect rectViewport = context.Visual.Layout.Viewport;

	// Resize viewport
	{
		Transform.SetIdentity();

		float Tx, Ty, Sx, Sy;
		float ratioViewport = rectViewport.GetHeight() / (float)rectViewport.GetWidth();
		float aspectRatio = context.Visual.Viewport.GetAspectRatio();
		if (ratioViewport > aspectRatio)
			rectViewport.bottom = (int32)(rectViewport.GetWidth() * aspectRatio) + rectViewport.top;

		Sx = rectViewport.GetWidth() / (float)resolution.GetWidth();
		Sy = rectViewport.GetHeight() / (float)resolution.GetHeight();

		Tx = rectViewport.left / (float)resolution.GetWidth();
		Ty = -rectViewport.top / (float)resolution.GetHeight();

		Transform.Scale[0] = Sx;
		Transform.Scale[1] = Sy;
		Transform.Translation[0] = Tx;
		Transform.Translation[1] = Ty;

		FeMath::TransformInvert(cursorPos, Transform);

		if (context.Data.SelectionRootWidget && bIsInEditMode)
		{
			FeVector3& designerScale = context.Data.SelectionRootWidget->GetDesignerTransform().Scale;
			FeVector3& designerOffset = context.Data.SelectionRootWidget->GetDesignerTransform().Translation;

			if (context.Input.MouseWheel != 0 && IsCursorOver())
			{

				float sign = FeMath::Sign(context.Input.MouseWheel);
				float speed = 0.1f;
				float scale = 1.f + sign*speed;

				auto cursorRelativePos = FeVector2(cursorPos[0] - designerOffset[0], cursorPos[1] - designerOffset[1]);

				auto zoomOffset = FeVector3( designerScale[0] * 0.5f - cursorRelativePos[0],
											-designerScale[1] * 0.5f - cursorRelativePos[1],
											0);

				zoomOffset *= speed*sign;
				designerOffset += zoomOffset;

				designerScale[0] *= scale;
				designerScale[1] *= scale;
			}
			bool button3Drag = context.Input.MouseKeysDown[2] && !context.Input.MouseKeysChanged[2] && (FeMath::Length(cursorDelta) > 0);

			if (button3Drag)
			{
				designerOffset[0] += cursorDelta.getData()[0];
				designerOffset[1] += cursorDelta.getData()[1];
			}

			float DesignerTx = designerOffset.getData()[0] * Sx;
			float DesignerTy = designerOffset.getData()[1] * Sy;

			float DesignerSx = designerScale.getData()[0] * Sx;
			float DesignerSy = designerScale.getData()[1] * Sy;

			DesignerTx += (Sx - DesignerSx)*0.5f;
			DesignerTy += -(Sy - DesignerSy)*0.5f;

			// align to grid
			DesignerTx -= FeMath::FMod(DesignerTx, context.Visual.Viewport.GridCell[0]);
			DesignerTy -= FeMath::FMod(DesignerTy, context.Visual.Viewport.GridCell[1]);

			Sx = DesignerSx;
			Sy = DesignerSy;
			Tx += DesignerTx;
			Ty += DesignerTy;
		}

		DesignerTransform.Scale[0] = Sx;
		DesignerTransform.Scale[1] = Sy;
		DesignerTransform.Translation[0] = Tx;
		DesignerTransform.Translation[1] = Ty;

		RenderBatch.Viewport.Rect = rectViewport; // render scissor
		Dimensions = rectViewport;

		FeModuleUi::Get()->SetViewport(DesignerTransform, Dimensions);
	}

	{ // compute cursor postition
		auto ModuleUi = FeApplication::GetStaticInstance()->GetModule<FeModuleUi>();
		FeTPtr<FeUiUserInputHandler>& userInput = ModuleUi->GetNavigationHandler().GetUser(0);
		FeUiUserInputHandler* pUserInput = userInput.Get();

		Cursor.Position				= pUserInput->GetPosition();
		Cursor.Delta				= pUserInput->GetPositionDelta();
		CursorNormalized.Position	= pUserInput->GetPosition();
		CursorNormalized.Delta		= pUserInput->GetPositionDelta();

		{
			CursorNormalized.Position[0] -= Transform.Translation[0];
			CursorNormalized.Position[1] -= Transform.Translation[1];
			CursorNormalized.Position[0] *= 1.0f / Transform.Scale[0];
			CursorNormalized.Position[1] *= 1.0f / Transform.Scale[1];
		}
		{
			CursorNormalized.Delta[0] -= Transform.Translation[0];
			CursorNormalized.Delta[1] -= Transform.Translation[1];
			CursorNormalized.Delta[0] *= 1.0f / Transform.Scale[0];
			CursorNormalized.Delta[1] *= 1.0f / Transform.Scale[1];
		}
	}

	ComputeHovered(context);
	
	if (bIsInEditMode && IsCursorOver()) // Compute mouse dragging
	{
		bool button1Drag = context.Data.Drag.State == FeEDragContextState::Drag && context.Data.Drag.Start.HoveredComponent == nullptr;

		auto selection = context.Data.GetSelectedWidget();
		//bool bCanTransform =  /*&& selection != context.Data.SelectionRootWidget*/;

		if (nullptr != selection)
		{
			bool bCanEdit = !(context.Mode & FeEEditorMode::Sequencer && selection->IsRoot());

			if (bCanEdit)
			{
				float absSx = 1.0f;
				float absSy = 1.0f;

				if (!selection->IsRoot())
				{
					const auto& matrix = selection->GetParent()->GetTraversalTransform();

					absSx /= FeGeometryHelper::GetMatrixScaleX(matrix);
					absSy /= FeGeometryHelper::GetMatrixScaleY(matrix);

					cursorDelta[0] *= absSx;
					cursorDelta[1] *= absSy;
				}

				FeVector2 vMoveDir(0, 0);

				if (context.Input.IsKeyReleased(SDL_SCANCODE_RIGHT)) vMoveDir[0] = 1.f;
				if (context.Input.IsKeyReleased(SDL_SCANCODE_LEFT))	 vMoveDir[0] = -1.f;
				if (context.Input.IsKeyReleased(SDL_SCANCODE_UP))	 vMoveDir[1] = 1.f;
				if (context.Input.IsKeyReleased(SDL_SCANCODE_DOWN))	 vMoveDir[1] = -1.f;

				const auto& gridCell = context.Visual.Viewport.GridCell;

				if (FeMath::Length(vMoveDir) > 0.0f)
				{
					auto& transform = selection->GetTransform();

					transform.Translation[0] += vMoveDir[0] * gridCell[0] * absSx;
					transform.Translation[1] += vMoveDir[1] * gridCell[1] * absSy;

					selection->SetTransientTransform(transform);
					selection->OnPropertyChangedTransform();
				}
				else
				{
					if (button1Drag)
					{
						if (!context.Visual.Viewport.DraggedWidget) // starting to drag
							context.Visual.Viewport.DragTransform = selection->GetTransform();

						auto& dragTransform = context.Visual.Viewport.DragTransform;
						auto widget = context.Visual.Viewport.DraggedWidget ? context.Visual.Viewport.DraggedWidget : selection;

						bool bDragStart = context.Visual.Viewport.DraggedWidget != widget;

						auto controller = bDragStart ? GetHoveredController(context) : context.Visual.Viewport.DraggedController;
						bool bApplyTransform = controller || !widget->IsRoot();

						if (bApplyTransform)
						{
							if (bDragStart) // save up initial transform
								context.Visual.Viewport.DragInitialTransform = widget->GetTransform();

							context.Visual.Viewport.DraggedWidget = widget;

							if (controller)
							{
								context.Visual.Viewport.DraggedController = controller;
								controller->ComputeDragTransformation(dragTransform, cursorDelta);
							}
							else
							{
								dragTransform.Translation[0] += cursorDelta.getData()[0];
								dragTransform.Translation[1] += cursorDelta.getData()[1];
							}

							auto& widgetTransform = widget->GetTransform();
							auto& widgetTranslate = widgetTransform.Translation;
							widgetTransform = dragTransform;

							if (!controller && context.Visual.IsToggled(FeEEditorToggle::GridLock))
							{
								const auto& matrix = widget->GetParent()->GetTraversalTransform();

								float absSx = 1.0f / FeGeometryHelper::GetMatrixScaleX(matrix);
								float absSy = 1.0f / FeGeometryHelper::GetMatrixScaleY(matrix);

								// align to grid
								FeViewportHelper::SnapToGrid(context, widgetTranslate);
								//widgetTranslate[0] -= FeMath::FMod(widgetTranslate[0], gridCell[0] );//* absSx
								//widgetTranslate[1] -= FeMath::FMod(widgetTranslate[1], gridCell[1] );//* absSy
							}

							widget->SetTransientTransform(widget->GetTransform());
							selection->OnPropertyChangedTransform();

							if (selection->IsRoot())
								FeModuleUi::Get()->SetViewport(DesignerTransform, rectViewport, true);
						}
					}
					else
					{
						auto draggedWidget = context.Visual.Viewport.DraggedWidget;
						if (draggedWidget)
						{
							auto container = FeChangePropertyContainer<FeTransform>::Create(&(draggedWidget->GetTransform()), draggedWidget, "Transform");
							container->bChanged = true;
							container->OldValue = context.Visual.Viewport.DragInitialTransform;

							if (context.Mode & FeEEditorMode::Sequencer)
							{
								FeModuleEditor::Get()->GetComponents().GetSequencer()->OnPropertyChanged(container);
								container->SelfDelete(FE_HEAPID_EDITOR);
								container = nullptr;
							}
							else
							{
								auto editAction = FE_NEW(FeEditorActionChangeProperty, DEFAULT_HEAP);
								editAction->Container = container;
								char tmpName[128];
								sprintf_s(tmpName, "Change [%s] : %s", draggedWidget->GetThisClassName().Cstr(), container->PropertyName);
								editAction->Name = tmpName;
								FeModuleEditor::Get()->ActionDo(editAction);
							}
						}
						context.Visual.Viewport.DraggedController = nullptr;
						context.Visual.Viewport.DraggedWidget = nullptr;
					}
				}
			}
		}
	}
}
void FeEditorViewport::Draw(FeEditorContext& context)
{
	context.Visual.Viewport.ClearGeometry();

	Overlays.Grid.Draw(context, *this);
	Overlays.RootWidget.Draw(context, *this);
	Overlays.Widgets.Draw(context, *this);
	Overlays.Selection.Draw(context, *this);
	//Overlays.Cursor.Draw(context, *this);

	if (context.Data.GetSelectedWidget())
	{
		for (auto& Controller : Overlays.Controllers)
			Controller.Draw(context, *this);
	}

	context.Visual.Viewport.ComputeZOrder();

	FeModuleRendering::Get()->RegisterRenderBatch(&RenderBatch);
}
void FeEditorViewportController::ComputeDragTransformation(FeTransform& transform, FeVector2& dragDelta)
{
	if (Type == FeEViewportController::Scale)
	{
		FeVector2 vDir;
		FeViewportHelper::ComputeDirection(vDir, Direction);
		vDir *= 2.f;

		FeVector2 actualDragDelta = dragDelta;

		if (FeViewportHelper::IsDirectionDiagonal(Direction))
		{
			float len = FeMath::Length(actualDragDelta);
			float ratio = transform.Scale[1] / transform.Scale[0];
			actualDragDelta[0] = dragDelta[0];
			actualDragDelta[1] = dragDelta[0] * ratio;

			if (Direction == FeEViewportDirection::NW || Direction == FeEViewportDirection::SE)
				actualDragDelta[1] *= -1.f;


		}

		float deltaSx = vDir[0] * actualDragDelta[0];
		float deltaSy = vDir[1] * actualDragDelta[1];

		transform.Scale[0] += deltaSx;
		transform.Scale[1] += deltaSy;

		transform.Translation[0] += vDir[0] < 0.f ? vDir[0] * deltaSx : 0.f;
		transform.Translation[1] += vDir[1] > 0.f ? vDir[1] * deltaSy : 0.f;
	}
}

void FeEditorViewportController::Draw(FeEditorContext& context, FeEditorViewport& viewport)
{
	static FeAssetRef Effect("editor/EditorEffect_Controller.fes", "Effect");

	if (context.Data.GetSelectedWidget())
	{
		bool bIsHovered = FeMathHelper::IntersectPointWithRect(context.Input.CusrorPosition, Rect.Matrix, FeEditorViewport::HoverPrevisionOffset);

		float viewportRatio = context.Visual.Viewport.GetAspectRatio();
		float thick = bIsHovered ? 0.006f : 0.0025f;
		float offset = 0.025f;
		FeColor colorA(0.8f, 0.8f, 0, 0.5f);
		FeColor colorB(0.8f, 0.8f, 0, 0.5f);

		if (context.Data.GetSelectedWidget()->IsRoot())
			FeViewportHelper::ComputeBoundingBox(context.Data.GetSelectedWidget(), ContainerRect.Matrix);
		else
			ContainerRect.Matrix = context.Data.GetSelectedWidget()->GetTraversalTransform();
		
		switch (Type)
		{
			case FeEViewportController::Scale:
			{
				Rect.SetScaleX(thick*viewportRatio);
				Rect.SetScaleY(thick);

				FeEViewportAlignement::Type horizontalAlign, verticalAlign;
				FeViewportHelper::ComputeAlignementFromDirection(Direction, horizontalAlign, verticalAlign);
				if (FeViewportHelper::IsDirectionDiagonal(Direction))
				{
					FeViewportHelper::UniformScaleAdd(ContainerRect, offset);
					Color = colorA;
					FeViewportHelper::Dock(Rect, ContainerRect, Direction);
					Rect.SetTranslateZ(-0.01f);
				}
				else
				{
					FeViewportHelper::UniformScaleAdd(ContainerRect, offset);
					Color = colorB;
					FeViewportHelper::Align(Rect, ContainerRect, horizontalAlign, verticalAlign);
				}
				
				//FeViewportHelper::ApplyOffset(Rect, Direction, offset);
				
			} break;
			default:
				return;
		}
		if (bIsHovered)
			Color *= 1.7f;

		auto pGeom = context.Visual.Viewport.PushGeometry(Effect);
		pGeom->Transform = Rect.Matrix;
		pGeom->Owner = this;
	}
}
FeColor& FeEditorViewportController::GetShaderPropertyColor(const FeString& PropertyName)
{
	static FeColor val;

	static FeString Property_Color = "Color";
	if (PropertyName == Property_Color)
		return Color;

	return val;
}
void FeEditorViewportCursorOverlay::Draw(FeEditorContext& context, FeEditorViewport& viewport)
{
	static FeAssetRef Effect("editor/EditorEffect_Cursor.fes", "Effect");

	FeTransform t;
	t.SetIdentity();

	t.Scale[0] = 0.01f;
	t.Scale[1] = 0.01f;
	t.Translation[0] = viewport.Cursor.Position[0];
	t.Translation[1] = viewport.Cursor.Position[1];

	auto pGeom = context.Visual.Viewport.PushGeometry(Effect);
	FeGeometryHelper::ComputeMatrix(pGeom->Transform, t);
}
void FeEditorViewportSelection::Draw(FeEditorContext& context, FeEditorViewport& viewport)
{
	static FeAssetRef Effect("editor/EditorEffect_Selection.fes", "Effect");
	
	if (context.Data.GetSelectedWidget())
	{
		auto pGeom = context.Visual.Viewport.PushGeometry(Effect);
		pGeom->Transform = context.Data.GetSelectedWidget()->GetTraversalTransform();
	}
}
void FeEditorViewportRootWidget::Draw(FeEditorContext& context, FeEditorViewport& viewport)
{
	static FeAssetRef Effect("editor/EditorEffect_Widget.fes", "Effect");

	auto root = context.Data.SelectionRootWidget;

	if (root && root->IsVisible())
	{
		auto pGeom = context.Visual.Viewport.PushGeometry(Effect);
		pGeom->Transform = root->GetTraversalTransform();
		pGeom->Owner = this;
	}
}
void FeEditorViewportWidgets::Draw(FeEditorContext& context, FeEditorViewport& viewport)
{
	static FeAssetRef Effect("editor/EditorEffect_Widget.fes", "Effect");

	auto root = context.Data.SelectionRootWidget;
	if (root)// compute all widgets display
	{
		root->Traverse([&, this](FeTraversalState& traversal) -> void
		{
			auto current = traversal.Current;
			if (root != current 
				&& current && current->IsVisible() 
				&& current != context.Data.GetSelectedWidget()
				&& current->GetThisClassName() != FeUiTextChar::ClassName())
			{
				auto pGeom = context.Visual.Viewport.PushGeometry(Effect);
				pGeom->Transform = current->GetTraversalTransform();
				pGeom->Owner = this;
			}
		}, 2);
	}
}
void FeEditorViewportGrid::Draw(FeEditorContext& context, FeEditorViewport& viewport)
{
	static FeAssetRef Effect("editor/EditorEffect_Grid.fes", "Effect");
	
	auto rootWidget = context.Data.SelectionRootWidget;

	if (rootWidget)
	{
		int32 iGridSize = rootWidget->GetDesignerGrid();
		const auto& rootScale = rootWidget->DesignerTransform.Scale;
		const auto& nativeRes = context.Visual.Viewport.NativeResolution;

		FeVector2 GridSize(iGridSize, iGridSize);
		INLINE_OP_VEC2(Dimensions, GridSize, rootScale, *);
	}

	if (context.Visual.IsToggled(FeEEditorToggle::Grid)) // compute grid
	{
		auto pGeom = context.Visual.Viewport.PushGeometry(Effect);
		pGeom->Owner = this;
		FeGeometryHelper::ComputeMatrix(pGeom->Transform, viewport.Transform);
	}
}

FeVector4& FeEditorViewportGrid::GetShaderPropertyVector(const FeString& PropertyName)
{
	static FeString Property_GridDimensions = "GridDimensions";

	if (PropertyName == Property_GridDimensions)
		return Dimensions;

	return FeEditorViewportWidget::GetShaderPropertyVector(PropertyName);
}
float FeEditorViewportWidget::GetShaderPropertyScalar(const FeString& PropertyName)
{
	static float val;
	return val; 
}
FeVector4& FeEditorViewportWidget::GetShaderPropertyVector(const FeString& PropertyName)
{
	static FeVector4 val; 
	return val;
}
FeColor& FeEditorViewportWidget::GetShaderPropertyColor(const FeString& PropertyName)
{
	static FeColor val;

	static FeString Property_BorderColor = "BorderColor";
	static FeString Property_BackgroundColor = "BackgroundColor";

	if (PropertyName == Property_BorderColor)		
		return BorderColor;
	if (PropertyName == Property_BackgroundColor)	
		return BackgroundColor;

	return val;
}

float& FeViewportRect::GetTranslateX() { return FeGeometryHelper::GetMatrixTranslationX(Matrix); }
float& FeViewportRect::GetTranslateY() { return FeGeometryHelper::GetMatrixTranslationY(Matrix); }
float& FeViewportRect::GetTranslateZ() { return FeGeometryHelper::GetMatrixTranslationZ(Matrix); }

float& FeViewportRect::GetScaleX() { return FeGeometryHelper::GetMatrixScaleX(Matrix); }
float& FeViewportRect::GetScaleY() { return FeGeometryHelper::GetMatrixScaleY(Matrix); }
float& FeViewportRect::GetScaleZ() { return FeGeometryHelper::GetMatrixScaleZ(Matrix); }

void FeViewportRect::SetTranslateX(float value) { float& output = GetTranslateX(); output = value; }
void FeViewportRect::SetTranslateY(float value) { float& output = GetTranslateY(); output = value; }
void FeViewportRect::SetTranslateZ(float value) { float& output = GetTranslateZ(); output = value; }

void FeViewportRect::SetScaleX(float value) { float& output = GetScaleX(); output = value; }
void FeViewportRect::SetScaleY(float value) { float& output = GetScaleY(); output = value; }
void FeViewportRect::SetScaleZ(float value) { float& output = GetScaleZ(); output = value; }