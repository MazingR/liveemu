#include <context.hpp>
#include <rendering/modulerenderer.hpp>
#include <ui/moduleui.hpp>
#include <moduleeditor.hpp>

#include <SDL.h>

#include "imgui.hpp"
#include <imgui.h>


bool IsKeyText (int key)
{
	return	(SDL_SCANCODE_A <= key && key <= SDL_SCANCODE_0) ||
		(SDL_SCANCODE_SPACE <= key && key <= SDL_SCANCODE_SLASH) ||
		(SDL_SCANCODE_KP_DIVIDE <= key && key <= SDL_SCANCODE_KP_PERIOD) ||
		key == SDL_SCANCODE_NONUSBACKSLASH;
}
bool IsKeyModifier(int key)
{
	return SDL_SCANCODE_LCTRL <= key && key <= SDL_SCANCODE_RGUI;
}
char MapKeyToChar(int key)
{
	static FeTMap<int32, char> mapKeyToChar;
	static bool bIsMapInit = false;

	if (!bIsMapInit)
	{
		bIsMapInit = true;

		for (int32 key = SDL_SCANCODE_KP_1; key <= SDL_SCANCODE_KP_9; ++key)
			mapKeyToChar.Add(key, '1' + (key - SDL_SCANCODE_KP_1));

		mapKeyToChar.Add(SDL_SCANCODE_KP_0, '0');
		mapKeyToChar.Add(SDL_SCANCODE_KP_PERIOD, '.');
		mapKeyToChar.Add(SDL_SCANCODE_KP_MINUS, '-');

		mapKeyToChar.Add(SDL_SCANCODE_1, '&');
		mapKeyToChar.Add(SDL_SCANCODE_2, '�');
		mapKeyToChar.Add(SDL_SCANCODE_3, '"');
		mapKeyToChar.Add(SDL_SCANCODE_4, '\'');
		mapKeyToChar.Add(SDL_SCANCODE_5, '(');
		mapKeyToChar.Add(SDL_SCANCODE_6, '-');
		mapKeyToChar.Add(SDL_SCANCODE_7, '�');
		mapKeyToChar.Add(SDL_SCANCODE_8, '_');
		mapKeyToChar.Add(SDL_SCANCODE_9, '�');
		mapKeyToChar.Add(SDL_SCANCODE_0, '�');

		mapKeyToChar.Add(SDL_SCANCODE_NONUSBACKSLASH, '<');
	}
	char keyChar = 0;

	uint32 iFind = mapKeyToChar.Find(key);
	if (iFind != FE_INVALID_ID)
		keyChar = mapKeyToChar.GetValueAt(iFind);
	else
		keyChar = SDL_GetKeyFromScancode((SDL_Scancode)key);

	return keyChar;
}
char AltChar(char input, SDL_Scancode key)
{
	static FeTMap<char, char> mapChar;
	static FeTMap<SDL_Scancode, char> mapKey;
	static bool bIsMapInit = false;

	if (!bIsMapInit)
	{
		bIsMapInit = true;

		mapChar.Add(')', ']');
		mapChar.Add('=', '}');

		mapKey.Add(SDL_SCANCODE_2, '~');
		mapKey.Add(SDL_SCANCODE_3, '#');
		mapKey.Add(SDL_SCANCODE_4, '{');
		mapKey.Add(SDL_SCANCODE_5, '[');
		mapKey.Add(SDL_SCANCODE_6, '|');
		mapKey.Add(SDL_SCANCODE_7, '`');
		mapKey.Add(SDL_SCANCODE_8, '\\');
		mapKey.Add(SDL_SCANCODE_9, '^');
		mapKey.Add(SDL_SCANCODE_0, '@');
	}

	char output = input;
	uint32 iValIdx = mapChar.Find(input);
	if (iValIdx != FE_INVALID_ID)
	{
		output = mapChar.GetValueAt(iValIdx);
	}
	else
	{
		uint32 iValIdx = mapKey.Find(key);
		if (iValIdx != FE_INVALID_ID)
			output = mapKey.GetValueAt(iValIdx);
	}

	return output;
};
char ShiftChar(char input)
{
	static FeTMap<char, char> map;
	static bool bIsMapInit = false;

	if (!bIsMapInit)
	{
		bIsMapInit = true;

		for (char c = 'a'; c <= 'z'; ++c)
			map.Add(c, c - ('a' - 'A'));
		for (char c = 'A'; c <= 'Z'; ++c)
			map.Add(c, c + ('a' - 'A'));

		map.Add('�', '%');
		map.Add('^', '�');
		map.Add('$', '�');
		map.Add('*', '�');
		map.Add(',', '?');
		map.Add(';', '.');
		map.Add(':', '/');
		map.Add('!', '�');

		map.Add('&', '1');
		map.Add('�', '2');
		map.Add('"', '3');
		map.Add('\'', '4');
		map.Add('(', '5');
		map.Add('-', '6');
		map.Add('�', '7');
		map.Add('_', '8');
		map.Add('�', '9');
		map.Add('�', '0');
		
		map.Add(')', '�');
		map.Add('=', '+');

		map.Add('<', '>');
	}

	char output = input;

	uint32 iValIdx = map.Find(input);
	if (iValIdx != FE_INVALID_ID)
		output = map.GetValueAt(iValIdx);

	return output;
};

bool FeEditorInputContext::IsKeyDown(char key)
{
	for (int32 i = 0; i < 16; ++i)
	{
		if (PressedKeys[i] == key)
			return true;
	}
	return false;
}
bool FeEditorInputContext::WasKeyDown(char key)
{
	for (int32 i = 0; i < 16; ++i)
	{
		if (WasPressedKeys[i] == key)
			return true;
	}
	return false;
}
bool FeEditorInputContext::IsKeyPressed(char key)
{
	return IsKeyDown(key) && !WasKeyDown(key);
}
bool FeEditorInputContext::IsKeyReleased(char key)
{
	return !IsKeyDown(key) && WasKeyDown(key);
}
void FeEditorInputContext::OnKeyReleased(int32 key)
{
	for (int32 i = 0; i < 16; ++i)
	{
		if (PressedKeys[i] == key)
		{
			PressedKeys[i] = -1;
		}
	}
}
void FeEditorInputContext::OnKeyPressed(int32 key)
{
	for (int32 i = 0; i < 16; ++i)
	{
		if (PressedKeys[i] == key)
			break;

		if (PressedKeys[i] == -1)
		{
			PressedKeys[i] = key;
			break;
		}
	}
}
void FeEditorInputContext::OnMouseWheel(int32 y)
{
	MouseWheel = y;
}

void FeEditorInputContext::Update(FeEditorContext& PreviousContext, FeUiUserMouseInputHandler* UserInput)
{
	auto resolution = FeModuleRendering::Get()->GetScreenDimensions();
	ImGuiIO& io = ImGui::GetIO();

	io.MouseWheel = (float)MouseWheel;
	MouseWheel = 0;

	for (int32 i = 0; i < MaxPressedKeys; ++i)
		WasPressedKeys[i] = PressedKeys[i];

	if (UserInput)
	{
		CusrorPositionDelta = UserInput->GetPositionDelta();
		CusrorPosition = UserInput->GetPosition();

		for (int32 i = 0; i < FeEditorInputContext::MouseKeysCount; ++i)
		{
			MouseKeysChanged[i] = UserInput->ButtonStateChanged(i);
			MouseKeysDown[i] = UserInput->IsButtonDown(i);
		}

		for (int32 i = 0; i < 3; ++i)
			io.MouseDown[i] = UserInput->IsButtonDown(i);

		io.MousePos.x = (signed short)(CusrorPosition.getData()[0] * resolution.GetWidth());
		io.MousePos.y = (signed short)(CusrorPosition.getData()[1] * -resolution.GetHeight());
		io.MouseDoubleClickTime = 0.3f;
	}

	const Uint8 *state = SDL_GetKeyboardState(NULL);

	if (state[SDL_SCANCODE_RETURN])
	{
		printf("<RETURN> is pressed.\n");
	}
	if (state[SDL_SCANCODE_RIGHT] && state[SDL_SCANCODE_UP])
	{
		printf("Right and Up Keys Pressed.\n");
	}

	static bool bCapsLock = false;
	if (state[SDL_SCANCODE_CAPSLOCK])
		bCapsLock = !bCapsLock;

	io.KeyMap[ImGuiKey_Tab] = SDL_SCANCODE_TAB;
	io.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
	io.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
	io.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
	io.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
	io.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
	io.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
	io.KeyMap[ImGuiKey_Delete] = SDL_SCANCODE_DELETE;
	io.KeyMap[ImGuiKey_Backspace] = SDL_SCANCODE_BACKSPACE;
	io.KeyMap[ImGuiKey_Enter] = SDL_SCANCODE_RETURN;
	io.KeyMap[ImGuiKey_Escape] = SDL_SCANCODE_ESCAPE;

	io.KeyMap[ImGuiKey_A] = SDL_SCANCODE_A;
	io.KeyMap[ImGuiKey_C] = SDL_SCANCODE_C;
	io.KeyMap[ImGuiKey_V] = SDL_SCANCODE_V;
	io.KeyMap[ImGuiKey_X] = SDL_SCANCODE_X;
	io.KeyMap[ImGuiKey_Y] = SDL_SCANCODE_Y;
	io.KeyMap[ImGuiKey_Z] = SDL_SCANCODE_Z;

	io.KeyShift = state[SDL_SCANCODE_LSHIFT] || state[SDL_SCANCODE_RSHIFT];
	io.KeyCtrl = state[SDL_SCANCODE_LCTRL] || state[SDL_SCANCODE_RCTRL];
	io.KeyAlt = state[SDL_SCANCODE_LALT] || state[SDL_SCANCODE_RALT];
	io.KeySuper = bCapsLock;

	for (int32 i = 0; i < SDL_NUM_SCANCODES; ++i)
	{
		io.KeysDown[i] = 1 == state[i];
	}

	io.InputCharacters[0] = '\0';

	for (int32 i = 0; i < 16; ++i)
		io.InputCharacters[i] = '\0';

	for (int32 i = 0; i < 16; ++i)
	{
		auto key = PressedKeys[i];

		if (key != -1 && !PreviousContext.Input.IsKeyDown(key) && IsKeyText(key))
		{
			char keyChar = MapKeyToChar(key);

			if (keyChar)
			{
				bool bShiftChar = io.KeySuper;
				bShiftChar = io.KeySuper ? !io.KeyShift : io.KeyShift;

				if (io.KeyAlt)
					keyChar = AltChar(keyChar, (SDL_Scancode)key);
				else if (bShiftChar)
					keyChar = ShiftChar(keyChar);
			}
				io.InputCharacters[0] = keyChar;
		}
	}
	// Hide OS mouse cursor if ImGui is drawing it
	//if (io.MouseDrawCursor)
	//	SetCursor(NULL);
}

void FeEditorViewportVisualContext::ClearGeometry()
{
	GeometryCount = 0;
	for (auto& pGeom : Geometries)
		pGeom->IsCulled = true;

}
uint32 FeEditorViewportVisualContext::LoadGeometryAndEffects(FeRenderBatch& renderBatch)
{
	auto RenderingModule = FeModuleRendering::Get();

	FeAssetRegistry assets;
	assets.Files.SetHeapId(FE_HEAPID_UI);
	assets.LoadFiles("editor", ".*\\.fes");

	for (auto& file : assets.Files)
	{
		if (file->IsOfType<FeRenderEffect>())
		{
			RenderingModule->LoadEffect(file.Get());
		}
	}

	for (auto& pGeom : Geometries)
	{
		FE_DELETE(FeRenderGeometryInstance, pGeom, FE_HEAPID_RENDERER);
	}
	Geometries.Free();
	Geometries.Resize(1024);

	for (uint32 i = 0; i < Geometries.GetSize(); ++i)
	{
		auto& geom = Geometries[i];
		auto pGeom = FE_NEW(FeRenderGeometryInstance, FE_HEAPID_RENDERER);
		geom = pGeom;

		pGeom->Reset();
		pGeom->Geometry = FeGeometryHelper::GetStaticGeometry(FeEGemetryDataType::Quad);
		pGeom->Transform = FeGeometryHelper::IdentityMatrix();
		pGeom->Viewport = &renderBatch.Viewport;

		renderBatch.GeometryInstances.Add(pGeom);
	}

	return FeEReturnCode::Success;
}
FeRenderGeometryInstance* FeEditorViewportVisualContext::PushGeometry(const FeAssetRef& effectRef)
{
	auto geom = PushGeometry();
	geom->Effect = effectRef.Path.GetId();
	return geom;
}
FeRenderGeometryInstance* FeEditorViewportVisualContext::PushGeometry()
{
	FE_ASSERT(GeometryCount < Geometries.GetSize(), "editor viewport : out of range");
	Geometries[GeometryCount]->IsCulled = false;
	return Geometries[GeometryCount++];
}
void FeEditorViewportVisualContext::ComputeZOrder()
{
	int32 iGeomIdx = 0;
	for (auto& pGeom : Geometries)
	{
		FeGeometryHelper::GetMatrixTranslationData(pGeom->Transform)[2] += -0.9f - (iGeomIdx++*0.0001f);
	}
}

FeEditorSelectionEntry& FeEditorDataContext::DoAddToSelection(FeAsset* asset, bool bClear /*= true*/)
{
	if (bClear)
		ClearSelection();

	auto& entry = Selection.Add();
	entry.Path.Clear();
	entry.Path.Add(asset->ID);
	entry.Pointer = asset;
	entry.Type = asset->Type;

	return entry;
}
void FeEditorDataContext::ClearSelection()
{
	Selection.Clear();
	SelectionRootWidget = nullptr;
	ViewportDraggedItem = nullptr;
	ViewportHoverStack.Clear();
}
void FeEditorDataContext::AddToSelection(FeAssetFile* asset)
{
	AssetFile = asset;
	
	if (asset)
		auto& entry = DoAddToSelection(asset, true);

	FetchPointers();
}
void FeEditorDataContext::AddToSelection(FeWidget* asset, bool bClear /*= true*/)
{
	if (!asset)
		return;

	auto rootWidget = FeModuleUi::Get()->GetRootWidget(asset->GetRootParent());
	if (rootWidget)
		AssetFile = rootWidget->AssetFile;

	auto& entry = DoAddToSelection(asset, bClear);
	asset->ComputePath(entry.Path, bClear);

	FetchPointers();
}
void FeEditorDataContext::AddToSelection(FeAsset* asset, bool bClear /*= true*/)
{
	if (!asset)
		return;

	if (asset->IsChildOf(FeWidget::ClassName()))
	{
		AddToSelection(static_cast<FeWidget*>(asset), bClear);
	}
	else
	{
		auto& entry = DoAddToSelection(asset, bClear);
	}
	FetchPointers();
}
void FeEditorDataContext::FetchPointers()
{
	auto selectedWidget = GetSelectedWidget();
	SelectionRootWidget = selectedWidget ? selectedWidget->GetRootParent() : nullptr;

	for (auto& entry : Selection)
	{
		uint32 pathLen = entry.Path.GetSize();
		entry.Pointer = nullptr;
		
		if (AssetFile)
		{
			if (pathLen == 1) // selected an asset file
			{
				entry.Pointer = AssetFile;
			}
			else if (entry.Type == FeAssetType::Widget())
			{
				FE_ASSERT(pathLen >= 2, "invalid selection path");

				SelectionRootWidget = AssetFile->GetAsset<FeWidget>();
				auto widget = FeWidget::GetWidgetFromPath(SelectionRootWidget, entry.Path, 1);
				entry.Pointer = widget;
			}
			else
			{
				FE_ASSERT_NOT_IMPLEMENTED;
			}
		}
	}
}
