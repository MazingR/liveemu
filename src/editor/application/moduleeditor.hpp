#pragma once

#include <commoneditor.hpp>

#include <common/application.hpp>
#include <rendering/commonrenderer.hpp>

#include "component.hpp"
#include "propertyeditor.hpp"
#include "sequenceeditor.hpp"
#include "docker.hpp"
#include "editorviewport.hpp"
#include "context.hpp"

class FeUiUserMouseInputHandler;
struct ImFont;

struct FeEEditorButton
{
	enum Type
	{
		Toggle_Grid,
		Toggle_GridLock,
		Toggle_NormalizedValues,
		Toggle_SchematicView,

		File_SaveAll,
	};
};
struct FeEditorComponents
{
	std::map<uint32, FeEditorComponent*> Map;
	std::map<uint32, FeTabComponent*> TabContainers;

	void CreateTabContainers()
	{
		TabContainers[FeEDock::Right]					= FE_NEW(FeTabComponent, FE_HEAPID_EDITOR);
		TabContainers[FeEDock::Bottom]					= FE_NEW(FeTabComponent, FE_HEAPID_EDITOR);
		TabContainers[FeEDock::Right | FeEDock::Bottom] = FE_NEW(FeTabComponent, FE_HEAPID_EDITOR);

		for (auto& tab : TabContainers)
		{
			tab.second->IsVisible = false;
			tab.second->Title = FeStringTools::Format("Tabs %u", tab.first);
		}
	}
	template<typename T>
	void Add(bool bVisible, uint32 dockType = FeEDock::None)
	{
		T* newComponent = FE_NEW(T, FE_HEAPID_EDITOR);
		newComponent->IsVisible = bVisible;
		Map[T::ClassName().Id()] = newComponent;

		if (dockType != FeEDock::None)
		{
			TabContainers[dockType]->Tabs.Add(newComponent);
			newComponent->IsOverlay = true;
		}
	}

	FeEditorComponent* GetByName(const FeString& name)
	{
		auto it = Map.find(name.Id());
		if (it == Map.end())
			return nullptr;

		return it->second;
	}

	FePropertyEditorComponent*		GetPropertyEditor()	{ return static_cast<FePropertyEditorComponent*		>(Map[FePropertyEditorComponent::ClassName().Id()]); }
	FeStyleComponent*				GetStyle()			{ return static_cast<FeStyleComponent*				>(Map[FeStyleComponent::ClassName().Id()]); }
	FeMenuComponent*				GetMenu()			{ return static_cast<FeMenuComponent*				>(Map[FeMenuComponent::ClassName().Id()]); }
	FeWidgetListComponent*			GetOutline()		{ return static_cast<FeWidgetListComponent*			>(Map[FeWidgetListComponent::ClassName().Id()]); }
	FeToolbarComponent*				GetToolbar()		{ return static_cast<FeToolbarComponent*			>(Map[FeToolbarComponent::ClassName().Id()]); }
	FeSequencerComponent*			GetSequencer()		{ return static_cast<FeSequencerComponent*			>(Map[FeSequencerComponent::ClassName().Id()]); }
	FeStatusBarComponent*			GetStatusBar()		{ return static_cast<FeStatusBarComponent*			>(Map[FeStatusBarComponent::ClassName().Id()]); }
	FeAddWidgetComponent*			GetAddWidget()		{ return static_cast<FeAddWidgetComponent*			>(Map[FeAddWidgetComponent::ClassName().Id()]); }
	FeOverlayComponent*				GetOverlay()		{ return static_cast<FeOverlayComponent*			>(Map[FeOverlayComponent::ClassName().Id()]); }
	FeThemeConfigEditorComponent*	GetThemeEditor()	{ return static_cast<FeThemeConfigEditorComponent*	>(Map[FeThemeConfigEditorComponent::ClassName().Id()]); }
	
	FeFileBrowserComponent*		GetFileBrowser()	{ return static_cast<FeFileBrowserComponent*	>(Map[FeFileBrowserComponent::ClassName().Id()]); }

	FeContentBrowserComponent*	GetContentBrowser()	{ return static_cast<FeContentBrowserComponent*	>(Map[FeContentBrowserComponent::ClassName().Id()]); }

	bool IsVisible(FeEditorComponent* component)
	{
		for (auto& tabContainer : TabContainers)
		{
			int32 index = 0;
			for (auto& tab : tabContainer.second->Tabs)
			{
				if (tab == component)
					return tabContainer.second->Current == index;

				index++;
			}
		}
		return false;
	}
	void ShowComponent(FeEditorComponent* component)
	{
		for (auto& tabContainer : TabContainers)
		{
			int32 index = 0;
			for (auto& tab : tabContainer.second->Tabs)
			{
				if (tab == component)
				{
					tabContainer.second->Current = index;
					break;
				}
				index++;
			}
		}
	}
};
namespace Awesomium
{
	class WebCore;
	class WebView;
};
class FeRenderLoadingResource;

struct FeModuleEditorInit : public FeModuleInit
{
public:
};

class FeEditorActionRef : public FeSerializable
{
public:
#define FeEditorActionRef_Properties(_d)	\
	_d(FeString,	Group)					\
	_d(FeString,	Name)					\

	FeEditorActionRef() {}
	FeEditorActionRef(const FeString& group, const FeString& name)
	{
		Group = group;
		Name = name;
	}

	FE_DECLARE_CLASS_BODY(FeEditorActionRef_Properties, FeEditorActionRef, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeEditorActionRef)

class FeEditorMenu : public FeSerializable
{
public:
	#define FeEditorMenu_Properties(_d)		\
	_d(FeString,				Title)		\
	_d(FeEditorActionRef,		Action)		\
	_d(FeTArray<FeEditorMenu>,	Entries)	\

	FeTSharedPtr<FeEditorAction> ActionPtr;

	FeEditorMenu() : ActionPtr(nullptr) {}

	void FetchActionRef();

	FE_DECLARE_CLASS_BODY(FeEditorMenu_Properties, FeEditorMenu, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeEditorMenu)

class FeEditorConfig : public FeSerializable
{
public:
	static FePath GetSerializePath()
	{
		FePath file = "editor/config.json";
		return file;
	}

	uint32 GetActionIconId(const FeString& ActionGroupName, const FeString& ActionName);

	#define FeEditorConfig_Properties(_d)					\
	_d(FeTArray<FeEditorActionGroupData>,	ActionGroups)	\
	_d(FeTArray<FeEditorMenu>,				Menus)			\

	FE_DECLARE_CLASS_BODY(FeEditorConfig_Properties, FeEditorConfig, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeEditorConfig)

struct FeEditorActionLogContext
{
	FeTSharedPtr<FeEditorAction>	Action;
	FeEditorDataContext				DataContextBefore;
	FeEditorDataContext				DataContextAfter;
};

class FeModuleEditor : public FeModule
{
public:
	static FeModuleEditor* Get()
	{
		static FeModuleEditor* instance = nullptr;

		if (!instance)
			instance = FeApplication::GetStaticInstance()->GetModule<FeModuleEditor>();

		return instance;
	}
	
	virtual uint32 Load(const FeModuleInit*) override;
	virtual uint32 Unload() override;
	virtual uint32 Update(const FeDt& fDt) override;
	void ClearSelection();

	uint32 Reload();
	void OnMouseWheel(int32 y);
	void OnKeyPressed(int32 y);
	void OnKeyReleased(int32 y);
	bool IsKeyDown(char key);
	void SwitchMode();
	void SaveAll();
	bool IsSaveNeeded();
	void ShowPopup(const FeString& componentName, FePopupCloseCallback onCloseCallback = nullptr, FePopupDrawCallback onDrawCallback=nullptr);
	void HidePopup(bool bCanceled=false);

	void ActionDo(FeEditorAction* action);
	void ActionDo(FeTSharedPtr<FeEditorAction>& action);
	void ActionUndo();
	void ActionRedo();

	virtual char* ComputeDebugOutput(const FeDt& fDt) { return ""; }

	virtual const char* GetName()
	{
		static const char* sName = "Editor";
		return sName;
	}
	FeEditorContext& GetContext() { return Context; }

	void OnShutingDown();

	FeTSharedPtr<FeEditorAction>& GetAction(const FeString& groupName, const FeString& name)
	{
		static FeTSharedPtr<FeEditorAction> nullResult;

		for (auto& action : Actions)
		{
			if (action->Name == name && action->GoupName == groupName)
				return action;
		}

		return nullResult;
	}
	FeTSharedPtr<FeEditorAction> GetAction(const FeEditorActionRef& ref)
	{
		return GetAction(ref.Group, ref.Name);
	}
	const FeEditorViewport& GetViewport() { return Viewport; }
	FeEditorComponents& GetComponents() { return Components; }

	template<typename T, typename... Args>
	FeTSharedPtr<FeEditorAction> CreateAction(bool bRegister, const FeString& actionsGroupName, Args... args)
	{
		FeTSharedPtr<FeEditorAction> newAction(FE_NEW(T, DEFAULT_HEAP, args...));
		
		if (bRegister)
		{
			FeTSharedPtr<FeEditorAction> oldAction = GetAction(actionsGroupName, newAction->Name);

			if (oldAction.IsValid())
			{
				FE_ASSERT(false, "action already exists");
				newAction = oldAction;
			}

			Actions.Add(newAction);// register
		}

		newAction->Icon = Config.GetActionIconId(actionsGroupName, newAction->Name);
		newAction->GoupName = actionsGroupName;

		return newAction;
	}
	template<typename T>
	void DeleteAction(T* action)
	{
		FE_DELETE(T, action, FE_HEAPID_EDITOR);
	}
	template<typename T, typename... Args>
	FeEditorButton& CreatToobarButton(const FeString& actionsGroupName, Args... args)
	{
		auto& button = Components.Toolbar.Buttons.Add();
		button.Action = CreateAction<T>(actionsGroupName, args...);
		return button;
	}
	bool SelectionChanged()
	{
		const auto& selection = Context.Data.GetSelection();
		const auto& previousSelection = PreviousContext.Data.GetSelection();

		return selection != previousSelection;
	}
private:
	void SelectStartWidget();
	void ActionUnDoRedo(bool bUndo);
	FeUiUserMouseInputHandler* UserInput;
	FeModuleEditorInit Init;

	FeEditorContext Context;
	FeEditorContext PreviousContext;

	FeEditorComponents Components;
	FeEditorViewport Viewport;
	
	FeTArray<FeTSharedPtr<FeEditorAction>> Actions;
	uint32 ActionsHistoryIndex;
	FeTArray<FeEditorActionLogContext> ActionsHistory;
	uint32 LastSaveActionHistorySize;
	uint32 LastSaveActionHistoryIndex;
	FeEditorConfig Config;
};