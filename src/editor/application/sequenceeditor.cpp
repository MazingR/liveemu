#include "sequenceeditor.hpp"

#include <moduleeditor.hpp>
#include <rendering/geometry.hpp>
#include <rendering/renderresourceshandler.hpp>
#include <ui/uiuserinputhandler.hpp>
#include <ui/moduleui.hpp>
#include "action.hpp"

#include "imgui.hpp"
#include <imgui.h>

void FeSequencerComponent::OnPropertyChanged(FeIChangePropertyContainer* changedPropertyContainer)
{
	if (Widget && Sequence)
	{
		FeUiPropertyInterpolation* changedInterpolation = nullptr;
		auto& propName = changedPropertyContainer->PropertyName;
		FeWidget* changedWidget = nullptr;
		FeWidgetPath targetPath;

		Widget->Traverse([&, this](FeTraversalState& traversal) -> void 
		{
			if (traversal.Current == changedPropertyContainer->GetOwner())
			{
				changedWidget = traversal.Current;
				
				traversal.Current->ComputePath(targetPath, true);
				targetPath.RemoveAt(0);
				targetPath.RemoveAt(0);

				for (auto& interpol : Sequence->Interpolations)
				{
					if (targetPath == interpol->TargetPath && propName == interpol->PropertyName)
					{
						changedInterpolation = interpol.Get();

						traversal.bCancelAll = true;
						traversal.bCancelBranch = true;
						break;
					}
				}
			}
		}, 1);

		if (changedWidget)
		{
			if (!changedInterpolation)
			{
				for (auto& commonTrack : CommonTracks)
				{
					if (commonTrack.PropertyName == propName)
					{
						auto& trackPtr = Sequence->Interpolations.Add();
						uint32 heapId = Sequence->Interpolations.GetHeapId();

						auto newTrack = static_cast<FeUiPropertyInterpolation*>(GetObjectsFactory().CreateObjectFromFactory(commonTrack.TypeName, heapId));
						newTrack->PropertyName = commonTrack.PropertyName;
						newTrack->TargetPath = targetPath;
						trackPtr.Assign(newTrack, heapId, true);

						changedInterpolation = newTrack;

						break;
					}
				}
			}

			if (changedInterpolation)
			{
				SetKeyFrame(PlaybackFrame, changedPropertyContainer->GetPtr(), changedInterpolation, changedWidget);
				IsSequenceDirty = true;
			}
		}
	}
}
void FeSequencerComponent::SetKeyFrame(int32 frame, void* value, FeUiPropertyInterpolation* track, FeWidget* widget /*= nullptr*/)
{
	FeWidget* pTargetWidget = FeWidget::GetWidgetFromPath(Widget, track->TargetPath);

	if (pTargetWidget)
	{
		void* propertyPtr = pTargetWidget->GetPropertyByName(track->PropertyName);
		if (propertyPtr)
		{
			FeKeyFrame* key = nullptr;

			float time = TimeStep*frame + 0.001f;

			for (int32 iKey = 0; iKey < track->GetKeyFramesCount(); ++iKey)
			{
				key = track->GetKeyFrame(iKey);
				if (key->Frame == frame)
					break;
				key = nullptr;
			}
			if (key == nullptr)
			{
				key = track->AddKeyFrame(time);
			}

			key->Frame = frame;

			if (track->GetThisClassName() == FeUiPropertyInterpolationTransform::ClassName())
			{
				auto trackTransfo = static_cast<FeUiPropertyInterpolationTransform*>(track);
				const FeTransform* valueTransform = nullptr;
				
				if (widget)
					valueTransform = &widget->Transform;
				else
					valueTransform = static_cast<const FeTransform*>(value ? value : propertyPtr);

				// recover default widget transform & compute additive key frame transform value
				if (trackTransfo->Additive)
				{
					FeWidget* pTargetWidgetDefault = FeWidget::GetWidgetFromPath(WidgetDefault, track->TargetPath);
					const auto propertyPtrDefault = static_cast<FeTransform*>(pTargetWidgetDefault->GetPropertyByName(track->PropertyName));
					auto keyTransfo = static_cast<FeKeyFrameTransform*>(key);

					keyTransfo->Value.Translation = valueTransform->Translation - propertyPtrDefault->Translation;
					INLINE_OP_VEC3(keyTransfo->Value.Scale, valueTransform->Scale, propertyPtrDefault->Scale, / );
				}
				else
				{
					key->SetValue(value ? value : propertyPtr);
				}
			}
			else
			{
				key->SetValue(value ? value : propertyPtr);
			}


			if (SequenceAssetFile)
			{
				if (FeUiSequence* sequence = SequenceAssetFile->GetAsset<FeUiSequence>())
				{
					sequence->OnKeyFramesChanged();
				}
			}
		}
	}
}
void FeSequencerComponent::DrawProperties(FeEditorContext& context)
{
	if (Sequence)
	{
		uint32 interpolIdx = 0;
		bool bBreak = false;

		for (auto& interpol : Sequence->Interpolations)
		{
			SCOPE_ID(interpol, &interpol);
			ImGui::AlignFirstTextHeightToWidgets();
			
			bool bWasSelected = interpolIdx == SelectedInterpolIndex;
			bool bSelected = ImGui::Selectable(interpol->PropertyName.CstrNotNull(), bWasSelected);

			if (bSelected)
			{
				SelectedInterpolIndex = interpolIdx;
			}

			interpolIdx++;
		}
	}

	if (ImGui::BeginPopupContextWindow())
	{
		if (SelectedInterpolIndex != FE_INVALID_ID && ImGui::Button("Remove"))
		{
			Sequence->Interpolations.RemoveAt(SelectedInterpolIndex);
			Refresh(context);
		}

		for (auto& commonTrack : CommonTracks)
		{
			if (ImGui::Button(commonTrack.ActionName.Cstr()))
			{
				bool bCanAddTrack = true;
				for (auto& interpol : Sequence->Interpolations)
				{
					if (interpol->PropertyName == commonTrack.PropertyName)
					{
						bCanAddTrack = false;
						break;
					}
				}

				if (bCanAddTrack)
				{
					// todo : make an action out of this
					auto& trackPtr = Sequence->Interpolations.Add();
					uint32 heapId = Sequence->Interpolations.GetHeapId();

					auto newTrack = static_cast<FeUiPropertyInterpolation*>(GetObjectsFactory().CreateObjectFromFactory(commonTrack.TypeName, heapId));
					
					newTrack->PropertyName = commonTrack.PropertyName;
					FeWidget* pTargetWidget = FeWidget::GetWidgetFromPath(Widget, newTrack->TargetPath);
					SetKeyFrame(0, nullptr, newTrack);

					trackPtr.Assign(newTrack, heapId, true);
					IsSequenceDirty = true;
				}
			}
		}
		ImGui::EndPopup();
	}
}
void FeSequencerComponent::Refresh(FeEditorContext& context)
{
	if (!Sequence)
		return;

	if (IsWidgetDirty)
	{
		IsWidgetDirty = false;

		auto widgetTemplate = FeModuleUi::Get()->AssetsRegistry.GetAsset<FeWidget>(Sequence->Widget);
		
		auto propertyEditor = FeModuleEditor::Get()->GetComponents().GetPropertyEditor();
		auto outlineComponent = FeModuleEditor::Get()->GetComponents().GetOutline();

		if (outlineComponent->Selected == WidgetRoot)
		{
			outlineComponent->Selected = nullptr;
			propertyEditor->Target = nullptr;
			context.Data.ClearSelection();
		}
		
		if (Widget)
		{
			Widget->SelfDelete(FE_HEAPID_UI);
			Widget = nullptr;
		}
		if (WidgetDefault)
		{
			WidgetDefault->SelfDelete(FE_HEAPID_UI);
			WidgetDefault = nullptr;
		}
		if (WidgetRoot)
		{
			auto& rootWidgets = FeModuleUi::Get()->GetRootWidgets();
			rootWidgets.Remove([&](const FeUiRootWidget& p) -> bool { return p.Root == WidgetRoot; });

			WidgetRoot->SelfDelete(FE_HEAPID_UI);
			WidgetRoot = nullptr;
		}
		if (widgetTemplate)
		{
			auto InitWidget = [&](FeWidget* widget) -> void
			{
				widget->PostInitialize();
				widget->Traverse([&, this](FeTraversalState& traversal) -> void { traversal.Current->OnTemplateChanged(); });
				widget->Traverse([&, this](FeTraversalState& traversal) -> void { traversal.Current->OnParentChanged(); });
				widget->Traverse([&, this](FeTraversalState& traversal) -> void { traversal.Current->PostInitialize(); });
				widget->Traverse([&, this](FeTraversalState& traversal) -> void { traversal.Current->OnDataChanged(); });
			};
			auto CreateWidgetCopy = [&]() -> FeWidget*
			{
				auto widget = static_cast<FeWidget*>(GetObjectsFactory().CreateObjectFromFactory(widgetTemplate->GetThisClassName(), FE_HEAPID_UI));
				widget->CopyFrom(widgetTemplate);
				InitWidget(widget);

				return widget;
			};

			Widget = CreateWidgetCopy(); // create editable widget
			WidgetDefault = CreateWidgetCopy(); // create widget copy (to backup default state)

			static FeString RootWidgetName = "_SequencerWidget_";

			// Create root widget (use as container of the editable widget)
			WidgetRoot = static_cast<FeWidget*>(GetObjectsFactory().CreateObjectFromFactory(widgetTemplate->GetThisClassName(), FE_HEAPID_UI));
			WidgetRoot->Transform.SetIdentity(); // revert widget transform change 
			InitWidget(WidgetRoot);
			WidgetRoot->ID.Name = RootWidgetName;

			auto& childPtr = WidgetRoot->Children.Add();
			childPtr.Assign(Widget); 
			InitWidget(WidgetRoot);

			WidgetAssetFile.Asset.Assign(WidgetRoot, FE_HEAPID_UI, false);
			auto& rootWidget = FeModuleUi::Get()->AddRootWidget(&WidgetAssetFile, WidgetRoot);
			WidgetRoot->Traverse([&, this](FeTraversalState& traversal) -> void
			{
				traversal.Current->GetGeometryInstance().Viewport = &rootWidget.RenderBatch.Viewport;
			});
		}
	}

	for (auto& interpol : Sequence->Interpolations)
	{
		for (int32 iKey = 0; iKey < interpol->GetKeyFramesCount(); ++iKey)
		{
			FeKeyFrame* key = interpol->GetKeyFrame(iKey);
			key->Frame = int32(key->Time / TimeStep);
		}
		interpol->SortKeyFrames();
	}

	SequenceInstance.Target = Widget;
	SequenceInstance.Interpolations.Clear();

	for (auto& interpolation : Sequence->GetInterpolations())
	{
		FeWidget* pTargetWidget = FeWidget::GetWidgetFromPath(WidgetDefault, interpolation->TargetPath);

		if (pTargetWidget)
		{
			FeInterpolatationInstance& interpolInstance = SequenceInstance.Interpolations.Add();
			interpolInstance.Clear();

			interpolInstance.Transient = false;
			interpolInstance.Interpolation = interpolation.Get();
			interpolation->ComputeTargetPropertyPtr(SequenceInstance, interpolInstance);
			
			if (interpolInstance.TargetProperty)
			{
				if (interpolation->GetThisClassName() == FeUiPropertyInterpolationTransform::ClassName())
				{
					static FeString DefaultPropertyName = "Transform";
					interpolInstance.TargetDefaultProperty = pTargetWidget->GetTransientPropertyByName(DefaultPropertyName);
				}
			}
			else
			{
				SequenceInstance.Interpolations.PopBack();
			}
		}
	}
}
void ComputePlaybackDrag(bool& bIsDragging, float frameTime, float& playbackTime)
{
	if (bIsDragging)
	{
		if (ImGui::IsMouseDown(0))
			playbackTime = frameTime + 0.001f;
		else
			bIsDragging = false;
	}
	else
	{
		if (ImGui::IsMouseClicked(0))
			bIsDragging = true;
	}
}
void FeSequencerComponent::SetSequence(FeAssetFile* assetFile	)
{
	auto sequence = assetFile->GetAsset<FeUiSequence>();

	if (Sequence == sequence)
		return;

	SequenceAssetFile = assetFile;
	Sequence = sequence;
	IsSequenceDirty = true;
	IsWidgetDirty = true;
}
void FeSequencerComponent::Draw(FeEditorContext& context)
{
	static bool bIsInit = false;
	
	if (false == bIsInit)
	{
		bIsInit = true;
		CommonTracks.Add(FeTrackCreator::Create<FeUiPropertyInterpolationTransform>("Transform"));
		CommonTracks.Add(FeTrackCreator::Create<FeUiPropertyInterpolationFloat>("Opacity"));
		CommonTracks.Add(FeTrackCreator::Create<FeUiPropertyInterpolationFloat>("Luminance"));
		CommonTracks.Add(FeTrackCreator::Create<FeUiPropertyInterpolationColor>("BorderColor"));
	}
	BeginWindow();

	if (Begin(Title.CstrNotNull()))
	{
		ImGuiStyle& style = ImGui::GetStyle();
		
		static FeUiSequence* PreviousSelectedSequence = nullptr;
		if (PreviousSelectedSequence != Sequence)
		{
			PreviousSelectedSequence = Sequence;
			Refresh(context);
		}

		if (Sequence)
		{
			bool button1Drag = context.Data.Drag.State == FeEDragContextState::Drag && context.Data.Drag.Start.HoveredComponent == nullptr;

			for (auto& interpolInstance : SequenceInstance.Interpolations)
			{
				interpolInstance.bEnded = false;
				interpolInstance.Interpolation->UpdatePropertyInterpolation(SequenceInstance, interpolInstance, PlaybackTime);
				if (false == button1Drag)
				{	// apply transient to serialized transform value (to synchronise track value with property editor)
					Widget->Traverse([&, this](FeTraversalState& traversal) -> void 
					{ 
						const FeMatrix4& matTraversal = traversal.Current->GetTraversalTransform();

						float fScaleX = FeGeometryHelper::GetMatrixScaleX(matTraversal);
						float fScaleY = FeGeometryHelper::GetMatrixScaleY(matTraversal);

						traversal.Current->Transform = traversal.Current->TransientTransform;
					});
				}
				//auto& viewport = FeModuleEditor::Get()->GetViewport();
				//FeModuleUi::Get()->SetViewport(viewport.DesignerTransform, viewport.Dimensions, true);
			}

			ImGui::Columns(2);
			ImGui::SetColumnOffset(1, 200);
			PlaybackFrame = int32 (PlaybackTime / TimeStep);
			TimeStep = 1.0f / FramesPerSec;
			static bool bIsDraggingPlayback = false;
			static int32 TimelineWidth = KeySize[0] * 500;
			ImVec2 imKeySize(KeySize[0], KeySize[1]);

			{ // Draw left side : proerties
				ImGui::BeginChild("Properties", ImVec2(200, 400), false, ImGuiWindowFlags_NoScrollbar);
				{
					ImGui::Columns(1);
					ImGui::Text(Sequence->ID.Name.CstrNotNull());
					ImGui::NextColumn();
					ImGui::Text("");
					ImGui::NextColumn();
					DrawProperties(context);
				}
				ImGui::EndChild();
			}
			ImGui::NextColumn();
			
			ImGui::SetNextWindowContentWidth(TimelineWidth);
			ImGui::BeginChild("Timeline", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);
			{ // Draw right size : timeline
				SCOPE_STYLEVAR_VEC2(ImGuiStyleVar_ItemSpacing, ImVec2(0, 2));
				SCOPE_STYLEVAR_VEC2(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
				SCOPE_STYLEVAR_VEC2(ImGuiStyleVar_FramePadding, ImVec2(0, 0));
				SCOPE_STYLECOLOR(ImGuiCol_Text, ColorKeyContent);
				
				ImGui::AlignFirstTextHeightToWidgets();

				float areaWidth = ImGui::GetContentRegionAvailWidth();
				FramesCount = (areaWidth / KeySize[0]);

				if (FramesCount > 0)
				{
					if (IsSequenceDirty)
					{
						IsSequenceDirty = false;
						Refresh(context);
					}

					static char strKey[] = "o";
					static char strKeyPlayback[] = "| ";
					static char strFrame[] = "";

					// Draw playback line
					{
						MaxTime = TimeStep*FramesCount;

						ImGui::Columns(FramesCount/10, 0, false);
						for (int32 frame = 0; frame < (FramesCount/10); ++frame)
						{
							float frameTime = (float)(frame * 10) / FramesPerSec;
							static char strTxt[12] = "";
							sprintf_s(strTxt, "%4.2f", frameTime);
							ImGui::Text(strTxt);
							ImGui::NextColumn();
						}

						ImGui::Columns(FramesCount, 0, false);

						for (int32 frame = 0; frame < FramesCount; ++frame)
						{
							float frameTime = (float)frame / FramesPerSec;

							ImVec4* frameColor = &ColorFrameInterval;

							if (frame == PlaybackFrame)
								frameColor = &ColorFramePlayback;
							else
								frameColor = &ColorFramePlaybackEmpty;

							if (frameColor)
								ImGui::PushStyleColor(ImGuiCol_Button, *frameColor);

							static char strTxt[12] = "";

							if (frame % 10 == 0)
								sprintf_s(strTxt, "|", frameTime);
							else
								strTxt[0] = '\0';

							ImGui::Button(strTxt, imKeySize);

							if (ImGui::IsItemHoveredRect())
								ComputePlaybackDrag(bIsDraggingPlayback, frameTime, PlaybackTime);

							if (frameColor)
								ImGui::PopStyleColor();

							ImGui::NextColumn();
						}
						ImGui::Columns(1);
						ImGui::Separator();
						ImGui::NextColumn();
					}


					for (auto& interpol : Sequence->Interpolations)
					{
						ImGui::Columns(FramesCount, 0, false);

						for (int32 frame = 0; frame < FramesCount; ++frame)
						{
							float frameTime = (float)frame / FramesPerSec;

							FeKeyFrame *keyCurrent, *keyPrevious, *keyNext;
							keyCurrent = keyPrevious = keyNext = nullptr;
							
							// Draw key if there is one on this frame
							for (int32 iKey = 0; iKey < interpol->GetKeyFramesCount(); ++iKey)
							{
								FeKeyFrame* key = interpol->GetKeyFrame(iKey);

								if (key->Frame < frame)
									keyPrevious = key;

								if (key->Frame > frame && keyNext == nullptr)
									keyNext = key;
								
								if (frame == key->Frame)
								{
									//FE_ASSERT(nullptr == keyCurrent, "two keys on same frame ?!");
									keyCurrent = key;

									ImVec4* frameColor = &ColorKeyFrame;

									if (ClickedKeyFrame.KeyFrame == key)
										frameColor = &ColorFrameSelected;

									ImGui::PushStyleColor(ImGuiCol_Button, *frameColor);
									{
										SCOPE_ID(keyClick, (void*)(interpol.Get() + iKey));
										ImGui::Button(frame == PlaybackFrame ? strKeyPlayback : strKey, imKeySize);
									}
									ImGui::PopStyleColor(); // keyColor

									if (ImGui::IsItemHovered())
									{
										if (ImGui::IsMouseClicked(0))
										{
											ClickedKeyFrame.Track = interpol.Get();
											ClickedKeyFrame.KeyFrameIndex = iKey;
											ClickedKeyFrame.KeyFrame = key;
											ClickedKeyFrame.FrameIndex = frame;
										}
										ComputePlaybackDrag(bIsDraggingPlayback, frameTime, PlaybackTime);

										ImGui::BeginTooltip();
										ImGui::Text("[%d] %4.3f (s)", key->Time, frame);

										if (interpol->GetThisClassName() == FeUiPropertyInterpolationFloat::ClassName())
										{
											ImGui::Text("Value = %4.2f", static_cast<FeKeyFrameFloat*>(key)->Value);
										}
										else if (interpol->GetThisClassName() == FeUiPropertyInterpolationColor::ClassName())
										{
											auto& value = static_cast<FeKeyFrameColor*>(key)->Value;
											ImGui::Text("Value = R=%2.2f G=%2.2f B=%2.2f A=%2.2f", value[0], value[1], value[2], value[3]);
										}
										else if (interpol->GetThisClassName() == FeUiPropertyInterpolationTransform::ClassName())
										{
											auto& value = static_cast<FeKeyFrameTransform*>(key)->Value;
											ImGui::Text("Translation (%2.2f, %2.2f, %2.2f)", value.Translation[0]	, value.Translation[1]	, value.Translation[2]);
											ImGui::Text("Scale       (%2.2f, %2.2f, %2.2f)", value.Scale[0]			, value.Scale[1]		, value.Scale[2]);
											ImGui::Text("Rotation    (%2.2f, %2.2f, %2.2f)", value.Rotation[0]		, value.Rotation[1]		, value.Rotation[2]);
										}

										ImGui::EndTooltip();
									}

									if (ImGui::IsItemHoveredRect())
									{
										if (context.Data.Drag.State == FeEDragContextState::Start)
										{
											DraggedKeyFrame.Track = interpol.Get();
											DraggedKeyFrame.KeyFrameIndex = iKey;
											DraggedKeyFrame.KeyFrame = key;
											DraggedKeyFrame.FrameIndex = frame;
										} 
									}
								}
							}

							// draw empty frame (no key)
							if (keyCurrent==nullptr)
							{
								ImVec4* frameColor = nullptr;

								if (DraggedKeyFrame.Track == interpol.Get() && DraggedKeyFrame.DraggedFrameIdx == frame)
									frameColor = &ColorFrameDragKey;
								else if (keyPrevious && keyNext)
									frameColor = &ColorKeyFrameInterpolation;
								else if (frame % 10 == 0)
									frameColor = &ColorFrameInterval;
								else
									frameColor = &ColorFrame;

								if (frameColor)
									ImGui::PushStyleColor(ImGuiCol_Button, *frameColor);

								ImGui::Button(frame == PlaybackFrame ? strKeyPlayback : strFrame, imKeySize); // empty frame

								if (frameColor)
									ImGui::PopStyleColor();

								if (DraggedKeyFrame.Track && ImGui::IsItemHoveredRect())
								{
									DraggedKeyFrame.DraggedFrameIdx = frame;

									if (context.Data.Drag.State == FeEDragContextState::Drop)
									{
										DraggedKeyFrame.KeyFrame->Time = frameTime + 0.001f;
										DraggedKeyFrame.Reset();
										IsSequenceDirty = true;
									}
								}

								if (ImGui::IsItemHoveredRect())
								{
									ComputePlaybackDrag(bIsDraggingPlayback, frameTime, PlaybackTime);

									ImGui::BeginTooltip();
									ImGui::Text("%4.3f (s) [%d] ", frameTime, frame);
									ImGui::EndTooltip();
								}
							}

							ImGui::NextColumn();
						}
					}
				}
			}
			ImGui::EndChild();
		}
		// Reset drag if we released click elsewhere
		if (context.Data.Drag.State == FeEDragContextState::Idle && DraggedKeyFrame.Track)
			DraggedKeyFrame.Reset();
	}
	EndWindow();
}
