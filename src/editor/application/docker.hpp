#pragma once
#include <commoneditor.hpp>

struct FeEDock
{
	enum Type
	{
		Top		= 1 << 0,
		Left	= 1 << 1,
		Right	= 1 << 2,
		Bottom	= 1 << 3,
		None	= 1 << 4,
	};
};
class FeEditorDocker
{
public:
	FeEditorDocker(const FeRect& rect);

	void Dock(FeRect& rect, uint32 dockType, int32 desiredW, int32 desiredH);
	FeRect Dock(uint32 dockType, int32 width, int32 height);
	void Dock(FeRect& rect, uint32 dockType, float desiredW, float desiredH);
	FeRect Dock(uint32 dockType, float percentW, float percentH);
	FeRect CurrentOffset;
private:
	void DoDock(FeRect& rect, uint32 dockType, int32 width, int32 height);
	int32 ComputeWidth(int32 desired);
	int32 ComputeHeight(int32 desired);
	int32 ComputeWidth(float desired);
	int32 ComputeHeight(float desired);
};
