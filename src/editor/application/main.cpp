#include <common/common.hpp>
#include <common/filesystem.hpp>

#include <rendering/modulerenderer.hpp>
#include <rendering/renderresourceshandler.hpp>

#include <common/memorymanager.hpp>

#include <ui/moduleui.hpp>
#include <scraping/modulescraping.hpp>
#include <webviewer/modulewebviewer.hpp>
#include <frontend/modulefrontend.hpp>

#include <moduleeditor.hpp>

#include <map>
#include <iostream>
#include <ctime>

#define SDL_MAIN_HANDLED
#include <SDL_syswm.h>
#include <SDL.h>

#include <frontend/modulefrontend.hpp>

class FeApplicationConfig : public FeSerializable
{
public:
#define FeApplicationConfig_Properties(_d)	\
	_d(FeString,			Theme)			\
	_d(FeScrapingConfig,	Scraping)		\

	FE_DECLARE_CLASS_BODY(FeApplicationConfig_Properties, FeApplicationConfig, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeApplicationConfig)

class FeEditorApplication : public FeApplication
{
public:

	virtual uint32 Load(const FeApplicationInit& appInit) override
	{
		uint32 iRes = FeApplication::Load(appInit);;
		FE_FAILEDRETURN(iRes);
		FeTArray<FePath> files;
		FeFileTools::List(files, "config", "config.json");

		FeApplicationConfig config;

		if (files.GetSize())
			iRes = FeJson::Deserialize(config, files[0], FE_HEAPID_EDITOR);

		FE_FAILEDRETURN(iRes);

		// Setup specific editor configuration
		config.GetScraping().GetStartupTasks().Clear();
		{
			FeModuleInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleFilesManager>(init));
		}
		{
			FeModuleRenderingInit init;

			FePath windowInfosPath;
			windowInfosPath = "editor/windowInfos.json";
			SetWindowInfosPath(windowInfosPath);

			init.WindowsCmdShow = appInit.WindowsCmdShow;
			init.WindowsInstance = (HINSTANCE)appInit.WindowsInstance;
			init.ScreenDimensions.right = WindowInfos.GetRect().getData()[2];
			init.ScreenDimensions.bottom = WindowInfos.GetRect().getData()[3];
			init.EnableFileWatcher = false;
			
			CreateAppWindow();
			
			init.WindowHandle = WindowHandle;

			auto pModule = CreateModule<FeModuleRendering>();
			FE_FAILEDRETURN(pModule->Load(&init));
		}
		{
			FeModuleScrapingInit init;
			init.Config = config.GetScraping();
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleScraping>(init));
		}
		{
			FeModuleInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleRenderResourcesHandler>(init));
		}
		{
			FeModuleUiInit init;
			init.EnableFileWatcher = false;
			init.ThemeToLoad = config.Theme;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleUi>(init));
		}
		{
			FeModuleWebViewerInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleWebViewer>(init));
		}
		{
			FeModuleEditorInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleEditor>(init));
		}
		{
			FeModuleFrontEndInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleFrontEnd>(init));
		}

		return iRes;
	}
	virtual void OnWindowSizeChanged(int32 width, int32 height) override
	{
		auto RenderingModule = GetModule<FeModuleRendering>();
		auto EditorModule = GetModule<FeModuleEditor>();
		auto UiModule = GetModule<FeModuleUi>();

		FeRect newDimensions = { 0 };
		newDimensions.right = width;
		newDimensions.bottom = height;

		if (RenderingModule->GetScreenDimensions() == newDimensions) // nothging changed
			return;

		EditorModule->ClearSelection();
		EditorModule->Unload();
		UiModule->Unload();
		
		RenderingModule->ResizeScreen(newDimensions);
		EditorModule->Reload();
		UiModule->Reload();
	}
	virtual void OnMouseWheel(int32 y) override
	{
		auto EditorModule = GetModule<FeModuleEditor>();
		EditorModule->OnMouseWheel(y);
	}
	void OnKeyPressedReleased(uint32 key,bool bPressed)
	{
		auto RenderingModule = GetModule<FeModuleRendering>();
		auto EditorModule = GetModule<FeModuleEditor>();
		auto UiModule = GetModule<FeModuleUi>();
		auto ResourcesHandler = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();

		if (bPressed)
		{
			EditorModule->OnKeyPressed(key);
		}
		else
		{
			EditorModule->OnKeyReleased(key);

			switch (key)
			{
			case SDL_SCANCODE_F2:
				EditorModule->ClearSelection();
				
				EditorModule->Unload();
				UiModule->Unload();
				ResourcesHandler->UnloadResources();
				RenderingModule->Unload();

				RenderingModule->Reload();
				EditorModule->Reload();
				UiModule->Reload();
				
				break;
			case SDL_SCANCODE_F3:
				ResourcesHandler->UnloadResources();
				RenderingModule->Reload();
				EditorModule->Reload();
				UiModule->Reload();
				break;
			case SDL_SCANCODE_F4:
				EditorModule->SwitchMode();
				break;
			case SDL_SCANCODE_F5:
				EditorModule->GetComponents().GetContentBrowser()->RefreshTree();
				UiModule->RefreshWidgets(true); // refresh visible widgets
				UiModule->LoadResources();
				EditorModule->GetContext().Data.FetchPointers();

				break;
			case SDL_SCANCODE_F6:
				EditorModule->ClearSelection();
				UiModule->RefreshWidgets(false); // refresh all widgets
				break;
			case SDL_SCANCODE_F8:
				//auto emuProcess = FeModuleFrontEnd::Get()->GetCurrentProcess();
				//static uint32 iCounter = 0;
				//iCounter = (iCounter + 1) % 4;
				//emuProcess->Start();

				//switch (iCounter)
				//{
				//case 0: emuProcess->Pause(); break;
				//case 1: emuProcess->Stop(); break;
				//case 2: emuProcess->SaveState(1); break;
				//case 3: emuProcess->LoadState(2); break;
				//}
				
				break;
			}
		}
	}
	virtual void OnKeyPressed(uint32 key) override
	{
		OnKeyPressedReleased(key, true);
	}
	virtual void OnKeyReleased(uint32 key) override
	{
		OnKeyPressedReleased(key, false);
	}
	virtual uint32 Unload() override
	{
		GetModule<FeModuleEditor>()->OnShutingDown();
		return FeApplication::Unload();
	}
};

//int _tmain(int argc, _TCHAR* argv[])
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "SDL2");		// 0
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "FileSystem");	// 1 FE_HEAPID_FILESYSTEM
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "JsonParser");	// 2 FE_HEAPID_JSONPARSER
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "Renderer");	// 3 FE_HEAPID_RENDERER
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "Ui");			// 4 FE_HEAPID_UI
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "StringPool");	// 5 FE_HEAPID_STRINGPOOL
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "Database");	// 6 

	FeMemoryManager::StaticInstance.Initialize();
	FeStringPool::Initialize();

	FePath::SetRoot("../../data");

	FeEditorApplication app;
	FeApplicationInit init;

	init.WindowsCmdLine = lpCmdLine;
	init.WindowsCmdShow = nCmdShow;
	init.WindowsInstance = hInstance;
	init.WindowsPrevInstance = hPrevInstance;

	FE_FAILEDRETURN(app.Load(init));
	FE_FAILEDRETURN(app.Run());
	FE_FAILEDRETURN(app.Unload());

	return 0;
}
