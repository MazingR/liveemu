#include "component.hpp"

#include <moduleeditor.hpp>
#include <rendering/renderresourceshandler.hpp>
#include <ui/uiuserinputhandler.hpp>
#include <ui/moduleui.hpp>
#include "action.hpp"
#include <frontend/modulefrontend.hpp>
#include <scraping/commonscraping.hpp>

#include "imgui.hpp"
#include <imgui.h>

bool DrawTreeEntry(FeDirectory& entry, FeDirectory*& selectedDir, bool bIsRoot = false)
{
	bool bChanged = false;

	uint32 uid = (uint32)&entry;

	ImGui::PushID(uid);
	ImGui::AlignFirstTextHeightToWidgets();

	ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;

	if (entry.Directories.IsEmpty())
		node_flags |= ImGuiTreeNodeFlags_Leaf;

	bool bWasSelected = selectedDir == &entry;
	if (bWasSelected)
		node_flags |= ImGuiTreeNodeFlags_Selected;

	if (bIsRoot)
		node_flags |= ImGuiTreeNodeFlags_DefaultOpen;

	bool node_open = ImGui::TreeNodeEx(&entry, node_flags, entry.Path.File.CstrNotNull());

	if (ImGui::IsItemClicked())
	{
		if (selectedDir != &entry)
		{
			bChanged = true;
			selectedDir = &entry;
		}
	}

	if (node_open)
	{
		for (auto& subDir : entry.Directories)
		{
			if (DrawTreeEntry(subDir, selectedDir, false))
				bChanged = true;
		}
		ImGui::TreePop();
	}
	ImGui::PopID();

	return bChanged;
}
bool DrawDirectories(FeDirectory& RootDir, FeDirectory*& selectedDir)
{
	return DrawTreeEntry(RootDir, selectedDir, true);
}

void FeEditorComponent::BeginWindow()
{
	if (!IsOverlay)
	{
		ImGui::SetNextWindowPos(ImVec2(Rect.left, Rect.top));
		ImGui::SetNextWindowSize(ImVec2(Rect.GetWidth(), Rect.GetHeight()), ImGuiSetCond_Always);
	}
}
void FeEditorComponent::EndWindow()
{
	if (!IsOverlay)
		ImGui::End();

	IsOverlay = false;
}
bool FeEditorComponent::Begin(const char* strTitle)
{
	bool bOpened = true;
	
	if (!IsOverlay)
	{
		if (!ImGui::Begin(strTitle, &bOpened, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize))
		{
			ImGui::End();
			return false;
		}
	}
	return true;
}
void FeStyleComponent::Draw(FeEditorContext& context)
{
	ImGuiStyle& style = ImGui::GetStyle();

	style.Colors[ImGuiCol_Text]						= ImVec4(0.90f, 0.90f, 0.90f, 1.00f);
	style.Colors[ImGuiCol_TextDisabled]				= ImVec4(0.60f, 0.60f, 0.60f, 1.00f);
	style.Colors[ImGuiCol_WindowBg]					= ImVec4(0.15f, 0.15f, 0.15f, 0.70f);
	style.Colors[ImGuiCol_ChildWindowBg]			= ImVec4(0.25f, 0.25f, 0.25f, 0.00f);
	style.Colors[ImGuiCol_PopupBg]					= ImVec4(0.10f, 0.10f, 0.10f, 0.90f);
	style.Colors[ImGuiCol_Border]					= ImVec4(0.70f, 0.70f, 0.70f, 0.65f);
	style.Colors[ImGuiCol_BorderShadow]				= ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
	style.Colors[ImGuiCol_FrameBg]					= ImVec4(0.80f, 0.80f, 0.80f, 0.30f);
	style.Colors[ImGuiCol_FrameBgHovered]			= ImVec4(0.90f, 0.80f, 0.80f, 0.40f);
	style.Colors[ImGuiCol_FrameBgActive]			= ImVec4(0.65f, 0.65f, 0.65f, 0.45f);
	style.Colors[ImGuiCol_TitleBg]					= ImVec4(0.27f, 0.27f, 0.27f, 1.00f);
	style.Colors[ImGuiCol_TitleBgCollapsed]			= ImVec4(0.40f, 0.40f, 0.40f, 1.00f);
	style.Colors[ImGuiCol_TitleBgActive]			= ImVec4(0.32f, 0.32f, 0.32f, 1.00f);
	style.Colors[ImGuiCol_MenuBarBg]				= ImVec4(0.40f, 0.40f, 0.40f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarBg]				= ImVec4(0.20f, 0.20f, 0.20f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarGrab]			= ImVec4(0.50f, 0.50f, 0.50f, 0.30f);
	style.Colors[ImGuiCol_ScrollbarGrabHovered]		= ImVec4(0.60f, 0.60f, 0.60f, 0.40f);
	style.Colors[ImGuiCol_ScrollbarGrabActive]		= ImVec4(0.80f, 0.60f, 0.40f, 0.40f);
	style.Colors[ImGuiCol_ComboBg]					= ImVec4(0.20f, 0.20f, 0.20f, 0.99f);
	style.Colors[ImGuiCol_CheckMark]				= ImVec4(0.90f, 0.90f, 0.90f, 0.50f);
	style.Colors[ImGuiCol_SliderGrab]				= ImVec4(1.00f, 1.00f, 1.00f, 0.30f);
	style.Colors[ImGuiCol_SliderGrabActive]			= ImVec4(0.80f, 0.50f, 0.50f, 1.00f);
	style.Colors[ImGuiCol_Button]					= ImVec4(0.67f, 0.40f, 0.40f, 0.60f);
	style.Colors[ImGuiCol_ButtonHovered]			= ImVec4(0.67f, 0.40f, 0.40f, 1.00f);
	style.Colors[ImGuiCol_ButtonActive]				= ImVec4(0.80f, 0.50f, 0.50f, 1.00f);
	style.Colors[ImGuiCol_Header]					= ImVec4(0.40f, 0.40f, 0.40f, 1.00f);
	style.Colors[ImGuiCol_HeaderHovered]			= ImVec4(0.45f, 0.45f, 0.45f, 1.00f);
	style.Colors[ImGuiCol_HeaderActive]				= ImVec4(0.53f, 0.53f, 0.53f, 1.00f);
	style.Colors[ImGuiCol_Column]					= ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
	style.Colors[ImGuiCol_ColumnHovered]			= ImVec4(0.70f, 0.70f, 0.70f, 1.00f);
	style.Colors[ImGuiCol_ColumnActive]				= ImVec4(0.90f, 0.90f, 0.90f, 1.00f);
	style.Colors[ImGuiCol_ResizeGrip]				= ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
	style.Colors[ImGuiCol_ResizeGripHovered]		= ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
	style.Colors[ImGuiCol_ResizeGripActive]			= ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
	style.Colors[ImGuiCol_CloseButton]				= ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
	style.Colors[ImGuiCol_CloseButtonHovered]		= ImVec4(0.70f, 0.70f, 0.70f, 1.00f);
	style.Colors[ImGuiCol_CloseButtonActive]		= ImVec4(0.70f, 0.70f, 0.70f, 1.00f);
	style.Colors[ImGuiCol_PlotLines]				= ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
	style.Colors[ImGuiCol_PlotLinesHovered]			= ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_PlotHistogram]			= ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_PlotHistogramHovered]		= ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_TextSelectedBg]			= ImVec4(0.00f, 0.00f, 1.00f, 0.35f);
	style.Colors[ImGuiCol_ModalWindowDarkening]		= ImVec4(0.20f, 0.20f, 0.20f, 0.35f);

	style.WindowRounding = 0.0f;
	style.ChildWindowRounding = 0.0f;
}

void DrawMenu(FeEditorMenu& menu, FeEditorContext& context)
{
	if (!menu.Title.IsEmpty())
	{
		if (ImGui::BeginMenu(menu.Title.CstrNotNull()))
		{
			for (auto& child : menu.Entries)
			{
				DrawMenu(child, context);
			}
			ImGui::EndMenu();
		}
	}
	else
	{
		if (menu.ActionPtr.IsValid())
		{	
			if (ImGui::MenuItem(menu.ActionPtr->Name.CstrNotNull()))
			{
				menu.ActionPtr->Do(context);
			}
		}
	}
}
void FeMenuComponent::Draw(FeEditorContext& context)
{
	if (ImGui::BeginMainMenuBar())
	{
		for (auto& rootEntry : Entries)
		{
			if (ImGui::BeginMenu(rootEntry->Title.CstrNotNull()))
			{
				for (auto& child : rootEntry->Entries)
				{
					DrawMenu(child, context);
				}
				ImGui::EndMenu();
			}
		}
		ImGui::EndMainMenuBar();
	}
}

void FeWidgetListComponent::DrawWidget(FeWidget* widget)
{
	if (!widget || !widget->IsVisible())
		return;

	uint32 uid = (uint32)widget;
	bool bChangedTransform = false;

	ImGui::PushID(uid);                      // Use object uid as identifier. Most commonly you could also use the object pointer as a base ID.
	ImGui::AlignFirstTextHeightToWidgets();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.

	int32 childrenCount = 0;
	widget->Traverse([&, this](FeTraversalState& traversal) -> void { childrenCount++; }, 1);

	static int selection_mask = (1 << 2);
	ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;
	if (childrenCount == 1)
		node_flags |= ImGuiTreeNodeFlags_Leaf;

	if (widget->IsRoot() || (Selected && widget->IsParentOf(Selected)))
		node_flags |= ImGuiTreeNodeFlags_DefaultOpen;
	

	bool bWasSelected = Selected == widget;
	if (bWasSelected)
		node_flags |= ImGuiTreeNodeFlags_Selected;

	bool node_open = ImGui::TreeNodeEx(widget, node_flags, "[%s] %s", widget->GetThisClassName().CstrNotNull(), widget->ID.Name.CstrNotNull());
	
	if (ImGui::IsItemClicked())
		Selected = widget;

	if (node_open)
	{
		widget->Traverse([&, this](FeTraversalState& traversal) -> void
		{
			if (widget != traversal.Current)
				DrawWidget(traversal.Current);
		}, 1);

		ImGui::TreePop();
	}
	ImGui::PopID();

	if (bChangedTransform)
	{
		widget->SetTransientTransform(widget->GetTransform());
		widget->OnPropertyChangedTransform();
	}
}
void FeWidgetListComponent::Draw(FeEditorContext& context)
{
	PreviousSelected = Selected;

	BeginWindow();
	{
		if (Begin(Title.CstrNotNull()))
		{
			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2, 2));
			ImGui::Columns(1);
			ImGui::Separator();

			DrawWidget(context.Data.SelectionRootWidget);

			ImGui::Columns(1);
			ImGui::Separator();
			ImGui::PopStyleVar();
		}
	}
	EndWindow();
}

void FeStatusBarComponent::Draw(FeEditorContext& context)
{
	ImGuiStyle& style = ImGui::GetStyle();
	ImVec4 imguiColorBg = style.Colors[ImGuiCol_WindowBg];

	ImguiConvert(style.Colors[ImGuiCol_WindowBg], FeColorHelper::GetColor(FeEColor::DimGray));
	style.Colors[ImGuiCol_WindowBg].w = 0.7f;

	BeginWindow();
	{
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(4, 2));
		bool bOpened = true;
		if (!ImGui::Begin("StatusBar", &bOpened, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove))
			return;

		if (Notifications.GetSize() > 0)
		{
			auto& notif = Notifications[0];

			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0, 0));
			{
				ImVec4 imguiColor;
				ImguiConvert(imguiColor, notif.Color);
				ImGui::TextColored(imguiColor, notif.Message);
			}

			notif.Timer -= context.Dt.TotalSeconds;
			if (notif.Timer <= 0)
				Notifications.RemoveAt(0);
		}
		else
		{
			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0, 0));
			{
				ImVec4 imguiColor;
				ImguiConvert(imguiColor, Status.Color);
				ImGui::TextColored(imguiColor, Status.Message);
			}
		}
		ImGui::PopStyleVar();
	}
	EndWindow();

	ImGui::PopStyleVar();
	style.Colors[ImGuiCol_WindowBg] = imguiColorBg;
}
void FeStatusBarComponent::AddNotification(const char* str, FeEColor::Type color, float time /*= 2.f*/)
{
	for (auto& oldNotif : Notifications)
		oldNotif.Timer = 0.01f;

	auto& notif = Notifications.Add();
	FeColorHelper::GetColor(color, notif.Color);
	sprintf_s(notif.Message, str);
	notif.Timer = time;
}

void NextToolbarItem(int32 width)
{
	ImGui::SameLine();
	ImGui::PushItemWidth(width);
}
void DrawToolbarSeparator()
{
	NextToolbarItem(20);
	ImGui::Spacing();
}
void FeToolbarComponent::Draw(FeEditorContext& context)
{
	ImVec4 pressedButtonColor;
	ImguiConvert(pressedButtonColor, FeColorHelper::GetColor(FeEColor::Yellow));
	ButtonPressed = -1;

	bool bOpened = true;
	if (!ImGui::Begin("Toolbar", &bOpened, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove))
	{
		ImGui::End();
		return;
	}
	
	auto iconsFont = context.Visual.IconsFont;

	if (iconsFont && iconsFont->IsLoaded()) // draw toolbar buttons
	{
		{
			ImGui::PushFont(iconsFont);
			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(1, 1));

			int32 buttonIdx = 0;
			for (auto& button : Buttons)
			{
				if (!button.Action.IsValid())
					continue;

				char strButtonImg[2] = "";
				strButtonImg[0] = (char)button.Action->GetIcon();
				strButtonImg[1] = '\0';

				bool bEnabled = button.Action->CanExecute(context);
				bool bPressed = false;
				if (button.Action->IsChildOf(FeEditorActionToggleOption::ClassName()))
					bPressed = button.Action.GetT<FeEditorActionToggleOption>()->IsToggleed(context);
				

				NextToolbarItem(30);
				if (DrawPushButton(bPressed, bEnabled, strButtonImg))
				{
					ButtonPressed = buttonIdx;
				}
				ImGui::PopFont();
				if (ImGui::IsItemHovered() && button.Action.IsValid())
				{
					ImGui::BeginTooltip();
					ImGui::Text(button.Action->Name.CstrNotNull());
					ImGui::EndTooltip();
				}
				ImGui::PushFont(iconsFont);

				buttonIdx++;
				ImGui::PopItemWidth();
			}

			ImGui::PopStyleVar();
			ImGui::PopFont();
		}
	}
	DrawToolbarSeparator();

	{ // draw grid size input
		NextToolbarItem(20);
		ImGui::Text("Grid");

		NextToolbarItem(80);
		int gridSize = 0;
		SCOPE_ID(GridSize, &context.Data.SelectionRootWidget->GetDesignerGrid());
		
		if (context.Data.SelectionRootWidget)
			gridSize = context.Data.SelectionRootWidget->GetDesignerGrid();

		if (ImGui::InputInt("", &gridSize) && context.Data.SelectionRootWidget)
		{
			context.Data.SelectionRootWidget->SetDesignerGrid(gridSize);
		}
	}
	{ // draw zoom input (designer scale)
		NextToolbarItem(20);
		ImGui::Text("Zoom");

		NextToolbarItem(80);
		int previousZoom, zoom = 100;
		float *pSx, *pSy, ratio;

		if (context.Data.SelectionRootWidget)
		{
			pSx = &context.Data.SelectionRootWidget->GetDesignerTransform().Scale[0];
			pSy = &context.Data.SelectionRootWidget->GetDesignerTransform().Scale[1];

			ratio = *pSy / *pSx;

			zoom = *pSx * 100;
			previousZoom = zoom;
		}
		SCOPE_ID(Zoom, &context.Data.SelectionRootWidget->GetDesignerTransform().Scale);
		if (ImGui::InputInt("", &zoom) && context.Data.SelectionRootWidget)
		{
			float epsilon = 0.00001f;
			*pSx = zoom/100.f;
			*pSx += zoom > previousZoom ? epsilon : -epsilon;
			*pSy = (*pSx) * ratio;
		}
	}
	{ // draw grid size input
		NextToolbarItem(20);
		ImGui::Text("Native");

		NextToolbarItem(80);
		FeVector2& nativeRes = context.Visual.Viewport.NativeResolution;
		int res[2] = { nativeRes[0], nativeRes[1] };
		SCOPE_ID(NativeRes, &context.Visual.Viewport.NativeResolution);
		
		if (ImGui::InputInt2("", res))
		{
			nativeRes[0] = (float)res[0];
			nativeRes[1] = (float)res[1];
		}
	}

	ImGui::End();

}

void FeAddWidgetComponent::Draw(FeEditorContext& context)
{
	BeginWindow();

	if (Begin(Title.CstrNotNull()))
	{
		for (auto& button : Buttons)
		{
			if (!button.Action.IsValid())
				continue;

			bool bEnabled = button.Action->CanExecute(context);
			bool bPressed = false;
			
			ImGui::Columns(1);
			SCOPE_ID(button, button.Action.Get());
			if (DrawPushButton(bPressed, bEnabled, button.Action->GetName().Cstr()))
			{
				FeModuleEditor::Get()->ActionDo(button.Action);
			}
		}
	}
	EndWindow();
}

void FeTabComponent::Draw(FeEditorContext& context)
{
	if (Tabs.IsEmpty())
		return;

	BeginWindow();
	bool bOpened = true;
	
	if (ImGui::Begin(Title.CstrNotNull(), &bOpened, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove))
	{
		ImGui::Columns(1);
		ImGui::Separator();

		for (int32 i = 0; i < Tabs.GetSize(); ++i)
		{
			bool bPressed = i == Current;
			if (DrawPushButton(bPressed, true, Tabs[i]->Title.CstrNotNull()))
			{
				Current = i;
			}
			if ((i + 1)<Tabs.GetSize())
				ImGui::SameLine();
		}
		ImGui::NextColumn();

		if (Current < Tabs.GetSize() && Current >= 0)
		{
			auto content = Tabs[Current];
			ImGui::BeginChild((ImGuiID)0, ImVec2(0, 0), false/*, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse*/);

			content->IsOverlay = true;
			content->Draw(context);
			ImGui::EndChild();
		}
	}
	EndWindow();
}
void FeOverlayComponent::Draw(FeEditorContext& context)
{
	if (Content || OnDrawCallback)
	{
		const char* title = Content ? Content->Title.CstrNotNull() : "";
		ImGui::OpenPopup(title);

		if (ImGui::BeginPopupModal(title))
		{
			ImGui::SetNextWindowPos(ImVec2(Rect.left, Rect.top));
			ImGui::SetNextWindowSize(ImVec2(Rect.GetWidth(), Rect.GetHeight()), ImGuiSetCond_Always);

			if (Content)
			{
				Content->IsOverlay = true;
				Content->Draw(context);
			}
			else if (OnDrawCallback)
			{
				OnDrawCallback(context);
			}

			ImGui::EndPopup();
		}
	}
}

struct ScopeColor
{
	uint32 Id;
	ImVec4 DefaultColor;

	ScopeColor(uint32 id, const ImVec4& color, bool bApply = true)
	{
		ImGuiStyle& style = ImGui::GetStyle();
		DefaultColor = style.Colors[id];
		if (bApply)
			style.Colors[id] = color;

		Id = id;
	}
	ScopeColor(uint32 id, FeEColor::Type color, bool bApply = true)
	{
		ImGuiStyle& style = ImGui::GetStyle();
		DefaultColor = style.Colors[id];
		if (bApply)
			ImguiConvert(style.Colors[id], FeColorHelper::GetColor(color));
		Id = id;
	}
	~ScopeColor()
	{
		ImGuiStyle& style = ImGui::GetStyle();
		style.Colors[Id] = DefaultColor;
	}
};
struct AssetThumbDrawHelper
{
	ImVec2 Dimensions;

	void Draw(const FeContentBrowserEntry& entry, bool bWasClicked, bool bWasDoubleClicked, bool& bClicked, bool& bDoubleClicked)
	{
		FeAsset* asset = entry.AssetFile->Asset.Get();

		ImGui::PushStyleVar(ImGuiStyleVar_ItemInnerSpacing, ImVec2(8, 8));
		static uint32 colorID = ImGuiCol_Button;

		ImGuiStyle& style = ImGui::GetStyle();

		const FeRenderResource*  pResource = nullptr;

		{
			ScopeColor colorA(ImGuiCol_Button, bWasClicked ? style.Colors[ImGuiCol_ButtonHovered] : entry.GetColor(false));
			ScopeColor colorB(ImGuiCol_Text, entry.GetColor(true));

			if (asset->Type == FeAssetType::Image())
			{
				// todo : load thumb version of texture image !
				void* textureID = 0;
				auto assetImage = static_cast<FeImage*>(asset);

				if (assetImage)
				{
					pResource = FeModuleRenderResourcesHandler::Get()->GetResource(assetImage->GetResourceId());
					if (pResource)
						textureID = pResource->GetD3DSRV(0);
					else
						FeModuleUi::Get()->LoadImage(assetImage);
				}

				ImGui::ImageButton(textureID, Dimensions);
			}
			else
			{
				ImGui::Button(asset->ID.Name.CstrNotNull(), Dimensions);
			}
		}
		if (ImGui::IsItemHovered())
		{
			if (ImGui::IsMouseClicked(0))
			{
				bClicked = true;
				//colorID += 1;
				if (ImGui::IsMouseDoubleClicked(0))
					bDoubleClicked = true;
			}

			ImGui::BeginTooltip();
			ImGui::Text("Type : %s", asset->Type.Cstr());
			ImGui::Text("Name : %s", asset->ID.Name.CstrNotNull());

			if (asset->Type == FeAssetType::Image() && pResource)
			{
				const auto pTexture = static_cast<const FeRenderTexture*>(pResource);
				ImGui::Text("Dimensions : %d x %d", pTexture->Width, pTexture->Height);
				ImGui::Text("Alpha channel : %s", pTexture->HasChannelAlpha ? "Yes" : "No");
			}
			ImGui::EndTooltip();
		}
		
		ImGui::PopStyleVar();
	}
};
void FeContentBrowserComponent::DrawContentThumb(FeEditorContext& context)
{
	int32 thumbSize = context.Visual.ContentBrowserThumbSize;
	ImGui::Columns(1);
	float areaWidth = ImGui::GetContentRegionAvailWidth();
	
	int32 thumbsPerLine = areaWidth / (thumbSize + thumbSize/10);
	int32 thumbIndex = 0;
	ImVec2 thumbSize2(thumbSize, thumbSize);
	ImVec2 thumbSelectSize(thumbSize, 10);

	ImGuiStyle& style = ImGui::GetStyle();
	ImVec4 defaultButtonColor = style.Colors[ImGuiCol_Button];
	ImVec4 defaultBgColor = style.Colors[ImGuiCol_FrameBg];
	ImVec4 defaultFgColor = style.Colors[ImGuiCol_Text];

	AssetThumbDrawHelper thumbDrawer;
	thumbDrawer.Dimensions = ImVec2(thumbSize, thumbSize);
	
	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0, 0));

	ImGui::PushItemWidth(thumbSize);

	for (auto& entry : ContentList)
	{
		if (thumbsPerLine == 0)
			break;

		if (thumbIndex++ % thumbsPerLine != 0) // not 'same line' at end of line only
			ImGui::SameLine();

		bool bWasClicked = ClickedAsset == entry.AssetFile;
		bool bWasDoubleClicked = DoubleClickedAsset == entry.AssetFile;

		SCOPE_ID(entry, &entry);
		bool bClicked = bWasClicked, bDoubleClicked = bWasDoubleClicked;

		thumbDrawer.Draw(entry, bWasClicked, bWasDoubleClicked, bClicked, bDoubleClicked);

		if (bDoubleClicked)
			DoubleClickedAsset = entry.AssetFile;
		if (bClicked)
			ClickedAsset = entry.AssetFile;

		if (ImGui::IsItemHoveredRect())
		{
			if (context.Data.Drag.State == FeEDragContextState::Start)
			{
				context.Data.Drag.Data = entry.AssetFile;
				context.Data.Drag.Start.HoveredComponent = this;
			}
		}
	}
	style.Colors[ImGuiCol_Button] = defaultButtonColor;
	ImGui::PopItemWidth();
	ImGui::PopStyleVar();
}
void FeContentBrowserComponent::DrawItemContextMenu()
{
	if (ImGui::BeginPopupContextWindow())
	{
		if (SelectedDir && SelectedDir != &RootDir)
		{
			static FeTArray<FeTSharedPtr<FeEditorAction>> contextActionsList;
			static FeTArray<FeTSharedPtr<FeEditorAction>> contextItemActionsList;

			if (contextActionsList.IsEmpty())
			{
				contextItemActionsList.Add(FeModuleEditor::Get()->GetAction("ContentBrowser", FeEditorActionContentBrowserRename::GetName()));

				FeTArray<const FeString*> assetTypes;
				FeAssetType::GetCreatableTypes(assetTypes);
				for (auto& assetType : assetTypes)
					contextActionsList.Add(FeModuleEditor::Get()->GetAction("File", FeStringTools::Format("Create asset : %s", assetType->Cstr())));

				contextActionsList.Add(FeModuleEditor::Get()->GetAction("File", FeEditorActionImportImages::GetName()));
			}

			if (ClickedAsset)
			{
				for (auto& action : contextItemActionsList)
				{
					if (action.IsValid() && ImGui::Button(action->Name.Cstr()))
					{
						FeModuleEditor::Get()->ActionDo(action);
						ComputeContentList();
					}
				}
			}

			for (auto& action : contextActionsList)
			{
				if (action.IsValid() && ImGui::Button(action->Name.Cstr()))
				{
					FeModuleEditor::Get()->ActionDo(action);
					ComputeContentList();
				}
			}
		}
		else
		{
			ImGui::Text("Select a directory");
		}
		ImGui::EndPopup();
	}
}
void FeContentBrowserComponent::DrawContentList(FeEditorContext& context)
{
	ImGuiStyle& style = ImGui::GetStyle();
	ImVec4 defaultBgColor = style.Colors[ImGuiCol_Button];

	for (auto& entry : ContentList)
	{
		bool bWasClicked = ClickedAsset == entry.AssetFile;
		bool bWasDoubleClicked = DoubleClickedAsset == entry.AssetFile;

		SCOPE_ID(entry, &entry);
		bool bClicked = bWasClicked, bDoubleClicked = bWasDoubleClicked;

		style.Colors[ImGuiCol_Button] = entry.GetColor(false);
		ImGui::SmallButton(" ");
		ImGui::SameLine();
		if (ImGui::Selectable(entry.AssetFile->Asset->ID.Name.CstrNotNull(), &bClicked, ImGuiSelectableFlags_AllowDoubleClick))
		{
			if (ImGui::IsMouseDoubleClicked(0))
				bDoubleClicked = true;
		}

		if (ImGui::IsItemHoveredRect())
		{
			if (context.Data.Drag.State == FeEDragContextState::Start)
			{
				context.Data.Drag.Data = entry.AssetFile;
				context.Data.Drag.Start.HoveredComponent = this;
			}
		}

		if (bDoubleClicked)
			DoubleClickedAsset = entry.AssetFile;
		if (bClicked)
			ClickedAsset = entry.AssetFile;
	}
	style.Colors[ImGuiCol_Button] = defaultBgColor;
}

const ImVec4& FeContentBrowserEntry::GetSelectionColor()
{
	static bool bInit = false;
	static ImVec4 colorSelected;
	if (!bInit)
	{
		bInit = true;
		ImguiConvert(colorSelected, FeColorHelper::GetColor(FeEColor::IndianRed));
	}
	return colorSelected;
}
const ImVec4& FeContentBrowserEntry::GetColor(bool bInvert) const
{
	static bool bColorInit = false;

	static ImVec4 color_Sequence;
	static ImVec4 color_Image	;
	static ImVec4 color_Font	;
	static ImVec4 color_Effect	;
	static ImVec4 color_Template;
	static ImVec4 color_Default	;

	static ImVec4 colorI_Sequence;
	static ImVec4 colorI_Image	;
	static ImVec4 colorI_Font	;
	static ImVec4 colorI_Effect	;
	static ImVec4 colorI_Template;
	static ImVec4 colorI_Default;

	if (!bColorInit)
	{
		bColorInit = true;
		ImguiConvert(color_Sequence	, FeColorHelper::GetColor(FeEColor::LightSalmon));
		ImguiConvert(color_Image	, FeColorHelper::GetColor(FeEColor::Snow3));
		ImguiConvert(color_Font		, FeColorHelper::GetColor(FeEColor::Goldenrod));
		ImguiConvert(color_Effect	, FeColorHelper::GetColor(FeEColor::DarkSlateBlue));
		ImguiConvert(color_Template	, FeColorHelper::GetColor(FeEColor::DarkSlateGray));
		ImguiConvert(color_Default	, FeColorHelper::GetColor(FeEColor::Snow4));

		#define INVERT_IMVEC4(output, input) {output.x=1.f-input.x; output.y=1.f-input.y; output.z=1.f-input.z; output.w=input.w; }
		
		INVERT_IMVEC4(colorI_Sequence	, color_Sequence);
		INVERT_IMVEC4(colorI_Image		, color_Image);
		INVERT_IMVEC4(colorI_Font		, color_Font);
		INVERT_IMVEC4(colorI_Effect		, color_Effect);
		INVERT_IMVEC4(colorI_Template	, color_Template);
		INVERT_IMVEC4(colorI_Default	, color_Default);
	}
	if (AssetFile->AssetRef.Type == FeAssetType::Sequence())return bInvert ? colorI_Sequence : color_Sequence;
	if (AssetFile->AssetRef.Type == FeAssetType::Image())	return bInvert ? colorI_Image : color_Image;
	if (AssetFile->AssetRef.Type == FeAssetType::Font())	return bInvert ? colorI_Font : color_Font;
	if (AssetFile->AssetRef.Type == FeAssetType::Effect())	return bInvert ? colorI_Effect : color_Effect;
	if (AssetFile->AssetRef.Type == FeAssetType::Widget())	return bInvert ? colorI_Template : color_Template;
	
	return color_Default;
}

void FeContentBrowserComponent::ComputeContentList()
{
	ContentList.Clear();

	FeString selectedDirPath;

	if (SelectedDir && SelectedDir != &RootDir)
	{
		FeStringTools::Format(selectedDirPath, "%s/%s", SelectedDir->Path.Dir.CstrNotNull(), SelectedDir->Path.File.CstrNotNull());
	}
	for (auto& assetFile : FeModuleUi::Get()->AssetsRegistry.Files)
	{
		bool bAdd = true;

		if (false==selectedDirPath.IsEmpty())
		{
			bAdd = assetFile->AssetRef.Path.Dir == selectedDirPath;
		}
		
		if (bAdd)
		{
			auto& entry = ContentList.Add();
			entry.AssetFile = assetFile.Get();
		}
	}
}
void FeContentBrowserComponent::Draw(FeEditorContext& context)
{
	BeginWindow();
	PreviousDoubleClickedAsset = DoubleClickedAsset;
	DoubleClickedAsset = nullptr;

	if (Begin(Title.CstrNotNull()))
	{
		{ // Draw buttons toolbar (view type + thumb size)
			ImGui::Columns(1);
			ImGui::PushItemWidth(100);
			{
				SCOPE_ID(viewtype, &context.Visual.ContentBrowserViewType);
				ImGui::Combo("View type", (int32*)&context.Visual.ContentBrowserViewType, "List\0Thumb\0");
			}
			{
				ImGui::SameLine();
				SCOPE_ID(thumbSize, &context.Visual.ContentBrowserThumbSize);
				ImGui::InputInt("Thumb size", &context.Visual.ContentBrowserThumbSize);
			}
			ImGui::PopItemWidth();
		}
		ImGui::Columns(2);
		ImGui::SetColumnOffset(1, 200);

		{ // Draw columns titles
			ImGui::TextDisabled("Directories");
			ImGui::NextColumn();
			ImGui::TextDisabled("Content");
			ImGui::NextColumn();
		}
		{ // Draw left side : dirs tree
			{
				ImGui::BeginChild("Browser", ImVec2(200, 400));
				{
					if (DrawDirectories(RootDir, SelectedDir))
					{
						if (SelectedDir)
							CurrentDirectoryPath = SelectedDir->Path;
						else
							CurrentDirectoryPath.Clear();

						ComputeContentList();
					}
				}
				ImGui::EndChild();
			}
		}
		ImGui::NextColumn();

		{ // Draw right size : content (list or thumbs)
			{
				ImGui::BeginChild("Content", ImVec2(0, 0));

				switch (context.Visual.ContentBrowserViewType)
				{
				case FeEContentView::List: DrawContentList(context); break;
				case FeEContentView::Thumb: DrawContentThumb(context); break;
				}
				DrawItemContextMenu();
				ImGui::EndChild();
			}
		}
	}	
	EndWindow();
}
void FeContentBrowserComponent::RefreshTree()
{
	FeFileTools::List(FeModuleUi::Get()->GetTheme().Directory, RootDir, ".*");
}

void AddDirFilesRecursive(const FeDirectory& dir, FeTArray<FeFileBrowserEntry>& output)
{
	for (auto& file : dir.Files)
	{
		auto& entry = output.Add();
		entry.File = &file;
		entry.Directory = &dir;
	}

	for (auto& subDir : dir.Directories)
		AddDirFilesRecursive(subDir, output);
}

void FeFileBrowserComponent::Draw(FeEditorContext& context)
{
	BeginWindow();

	if (Begin(Title.CstrNotNull()))
	{
		ImGui::Columns(2);
		ImGui::SetColumnOffset(1, 200);

		{ // Draw columns titles
			ImGui::TextDisabled("Browser");
			ImGui::NextColumn();
			ImGui::TextDisabled("Files");
			ImGui::NextColumn();
		}
		{ // Draw left side 
			{
				ImGui::BeginChild("Browser", ImVec2(200, 400));
				{
					DrawDirectories(RootDir, SelectedDir);
				}
				ImGui::EndChild();
			}
		}
		ImGui::NextColumn();

		{ // Draw right side
			{
				if (ImGui::BeginChild("Files", ImVec2(400, 400)))
				{
					if (SelectedDir)
					{
						for (auto& file : SelectedDir->Files)
						{
							uint32 fileIdx = SelectedFiles.IndexOf([&](const FeFileBrowserEntry& o) -> bool { return o.File == &file; });

							bool bWasSelected = fileIdx != FE_INVALID_ID;
							bool bSelected = bWasSelected;

							SCOPE_ID(file, &file);
							ImGui::Selectable(file.File.CstrNotNull(), &bSelected);

							if (bSelected && !bWasSelected)
							{
								if (!bAllowMultiselect)
									SelectedFiles.Clear();

								auto& entry = SelectedFiles.Add();
								entry.File = &file;
								entry.Directory = SelectedDir;
							}
							else if (!bSelected && bWasSelected)
							{
								SelectedFiles.RemoveAt(fileIdx);
							}
						}
					}
				}
				ImGui::EndChild();
			}
		}
		ImGui::Columns(1);

		{ // Draw buttons toolbar OK & CANCEL
			
			ImGui::PushItemWidth(100);
			{
				ImGui::PushID(0);
				if (ImGui::Button("Cancel"))
					FeModuleEditor::Get()->HidePopup(true);
				ImGui::PopID();
			}
			ImGui::SameLine();
			{
				ImGui::PushID(1);
				if (ImGui::Button("Validate"))
					FeModuleEditor::Get()->HidePopup(false);
				ImGui::PopID();
			}
			
			if (bAllowMultiselect)
			{
				ImGui::SameLine();
				ImGui::Spacing();
				ImGui::SameLine();
				{
					ImGui::PushID(3);
					if (ImGui::Button("UnSelect All"))
					{
						SelectedFiles.Clear();
					}
					ImGui::PopID();
				}
				ImGui::SameLine();
				{
					ImGui::PushID(2);
					if (ImGui::Button("Select All"))
					{
						if (SelectedDir)
						{
							SelectedFiles.Clear();
							for (auto& file : SelectedDir->Files)
							{
								auto& entry = SelectedFiles.Add();
								entry.File = &file;
								entry.Directory = SelectedDir;
							}
						}
					}
					ImGui::PopID();
				}
				ImGui::SameLine();
				{
					ImGui::PushID(3);
					if (ImGui::Button("Select All Recursive"))
					{
						if (SelectedDir)
						{
							SelectedFiles.Clear();
							AddDirFilesRecursive(*SelectedDir, SelectedFiles);
						}
					}
					ImGui::PopID();
				}
			}
			ImGui::PopItemWidth();
		}
	}
	EndWindow();
}
void FeFileBrowserComponent::Reset()
{
	SelectedFiles.Clear();
	SelectedDir = nullptr;
	RootDir.Directories.Clear();
	RootDir.Files.Clear();
}
void FeFileBrowserComponent::RefreshContent(const char* szFilter)
{
	FeFileTools::List(FeModuleUi::Get()->GetTheme().Directory, RootDir, szFilter);
}

void FeEmulatorComponent::Draw(FeEditorContext& context)
{
	BeginWindow();

	if (Begin(Title.CstrNotNull()))
	{
		ImGui::Columns(1);
		ImGui::NextColumn();

		ImGui::LabelText("Commands", "Commands");
		ImGui::NewLine();
		
		FeEmulatorProcess* emuProc = FeModuleFrontEnd::Get()->GetCurrentProcess();

		#define EmuButton(name) 		\
			if (ImGui::Button(#name))	\
			{							\
				emuProc->name();		\
			}							\
			ImGui::NextColumn();		\

		EmuButton(SendPause);
		EmuButton(SendResume);
		EmuButton(SendStop);
		EmuButton(SendHardReset);
		EmuButton(SendSoftReset);
		
		if (ImGui::Button("Focus"))		emuProc->SendSetFocus(true);
		if (ImGui::Button("UnFocus"))	emuProc->SendSetFocus(false);
		
		static FeString inputRomPath = "c:/Work/Internal/liveemu_externals/emulators/common/mame/roms/1944.zip";
		
		ImGui::Columns(2);
		DrawString(&inputRomPath, false, ImVec2(), true);
		ImGui::NextColumn();

		if (ImGui::Button("Launch"))
		{
			FeDataGameDump dump;
			dump.LocalPath = inputRomPath;
			emuProc->SendLoadGame(dump);
		}

		ImGui::Columns(1);
		ImGui::Text("Received messages : ");
		ImGui::NextColumn();

		const FeNetMessageCommunication& com = emuProc->GetCommunication();
		
		{
			SCOPELOCK((SDL_mutex*)com.InboxMutex); // <------ Lock Mutex

			for (uint32 i = com.Inbox.GetSize(); i > 0; --i)
			{
				auto msg = com.Inbox[i-1].Message.Get();

				FeEEmulatorNetMessage::Type eMsgType = msg->Type;
				FeString sMsgType = FeEEmulatorNetMessage::ToString(eMsgType);
				ImGui::Text("Msg [%u] : %s", i, sMsgType.Cstr());
				ImGui::NextColumn();
			}
		}  // <------ Unlock Mutex
	}
	EndWindow();
}
