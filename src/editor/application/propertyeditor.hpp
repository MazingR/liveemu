#pragma once

#include "component.hpp"

class FeSerializable;

class FePropertyEditorComponent : public FeEditorComponent
{
public:
	FePropertyEditorComponent() : Target(nullptr) 
	{
		Title = "Properties";
	}
	void Draw(FeEditorContext& context);

	FeSerializable* Target;

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FePropertyEditorComponent, FeEditorComponent)
};

class FeThemeConfigEditorComponent : public FePropertyEditorComponent
{
public:
	FeThemeConfigEditorComponent() : FePropertyEditorComponent()
	{
		Title = "Theme";
	}

	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, FeThemeConfigEditorComponent, FePropertyEditorComponent)
};
