#pragma once

#include <context.hpp>

class FeEditorActionData : public FeSerializable
{
public:
	#define FeEditorActionData_Properties(_d)	\
			_d(FeString,	ActionName)		\
			_d(uint32,		Icon)			\

	FE_DECLARE_CLASS_BODY(FeEditorActionData_Properties, FeEditorActionData, FeSerializable)
};

class FeEditorActionGroupData : public FeSerializable
{
public:
#define FeEditorActionGroupData_Properties(_d)				\
			_d(FeString,						Name)		\
			_d(FeTArray<FeEditorActionData>,	Actions)	\

	FE_DECLARE_CLASS_BODY(FeEditorActionGroupData_Properties, FeEditorActionGroupData, FeSerializable)
};

class FeEditorAction : public FeSerializable
{
public:
	#define FeEditorAction_Properties(_d)	\
			_d(bool,		IsUndoable)		\
			_d(FeString,	Name)			\
			_d(FeString,	GoupName)		\
			_d(uint32,		Icon)			\

	FeEditorAction() : IsUndoable(false), Name("Uknown")
	{
	}

	virtual void Do(FeEditorContext& context)
	{
	}
	virtual void UnDo(FeEditorContext& context)
	{}
	virtual void ReDo(FeEditorContext& context, const FeEditorDataContext& dataContextAfter)
	{
		if (IsUndoable)
			Do(context);
	}
	virtual bool CanExecute(FeEditorContext& context)
	{
		return true;
	}

	FE_DECLARE_CLASS_BODY(FeEditorAction_Properties, FeEditorAction, FeSerializable)
};