#pragma once

#include <action.hpp>

#include <actions/EditorActionAddWidget.hpp>
#include <actions/EditorActionChangeProperty.hpp>
#include <actions/EditorActionContentBrowser.hpp>
#include <actions/EditorActionCreateAsset.hpp>
#include <actions/EditorActionImportAssets.hpp>
#include <actions/EditorActionQuit.hpp>
#include <actions/EditorActionRemoveWidget.hpp>
#include <actions/EditorActionSaveAll.hpp>
#include <actions/EditorActionShowPopup.hpp>
#include <actions/EditorActionToggleOption.hpp>
#include <actions/EditorActionUnDoRedo.hpp>