#pragma once
#include <common/common.hpp>
#include <common/application.hpp>
#include <rendering/commonrenderer.hpp>

#include <commoneditor.hpp>
#include "context.hpp"

class FeEditorViewport;

struct FeEditorViewportCursor
{
	FeVector2	Position;
	FeVector2	Delta;
};

class FeEditorViewportWidget : public FeIShaderPropertyContainer
{
public:
	virtual void Draw(FeEditorContext& context, FeEditorViewport& viewport) = 0;

	// dummy functions
	virtual float GetShaderPropertyScalar(const FeString& PropertyName);
	virtual FeVector4& GetShaderPropertyVector(const FeString& PropertyName);
	virtual FeColor& GetShaderPropertyColor(const FeString& PropertyName);

	virtual FeEImageAlign::Type GetImageHorizontalAlign()	{ return FeEImageAlign::Min;  }
	virtual FeEImageAlign::Type GetImageVerticalAlign()		{ return FeEImageAlign::Min; }
	virtual FeEImageResize::Type GetImageResizeMode()		{ return FeEImageResize::Stretch; }
	virtual FeEImageSample::Type GetImageSampleMode()		{ return FeEImageSample::Normal; }

	FeColor BackgroundColor;
	FeColor BorderColor;
};

class FeEditorViewportSelection : public FeEditorViewportWidget
{
public:
	void Draw(FeEditorContext& context, FeEditorViewport& viewport);
};

class FeEditorViewportCursorOverlay : public FeEditorViewportWidget
{
public:
	void Draw(FeEditorContext& context, FeEditorViewport& viewport);
};

class FeEditorViewportRootWidget : public FeEditorViewportWidget
{
public:
	FeEditorViewportRootWidget()
	{
		BackgroundColor = FeColor(0.f, 0.f, 0.f, 0.0f);
		BorderColor = FeColor(0.6f, 0.0f, 0.5f, 0.5f);
	}
	void Draw(FeEditorContext& context, FeEditorViewport& viewport);
};

class FeEditorViewportWidgets : public FeEditorViewportWidget
{
public:
	FeEditorViewportWidgets()
	{
		BackgroundColor = FeColor(1.f, 1.f, 1.f, 0.03f);
		BorderColor = FeColor(1.f, 1.f, 1.f, 0.6f);
	}
	void Draw(FeEditorContext& context, FeEditorViewport& viewport);
};

class FeEditorViewportGrid : public FeEditorViewportWidget
{
public:
	void Draw(FeEditorContext& context, FeEditorViewport& viewport);
	virtual FeVector4& GetShaderPropertyVector(const FeString& PropertyName) override;

	FeVector4 Dimensions;
};

struct FeEViewportController
{
	enum Type
	{
		Scale,
		Count
	};
};
struct FeEViewportDirection
{
	enum Type
	{
		N,
		NE,
		E,
		SE,
		S,
		SW,
		W,
		NW,
		Count,
	};
};
struct FeEViewportAlignement
{
	enum Type
	{
		Stretch,
		Min,
		Max,
		None,
	};
};


struct FeViewportRect
{
	FeMatrix4 Matrix;

	float& GetTranslateX();
	float& GetTranslateY();
	float& GetTranslateZ();

	float& GetScaleX();
	float& GetScaleY();
	float& GetScaleZ();

	void SetTranslateX(float value);
	void SetTranslateY(float value);
	void SetTranslateZ(float value);

	void SetScaleX(float value);
	void SetScaleY(float value);
	void SetScaleZ(float value);
};

class FeEditorViewportController : public FeEditorViewportWidget
{
public:
	static const int32 Count = FeEViewportDirection::Count*FeEViewportController::Count;

	void ComputeDragTransformation(FeTransform& transform, FeVector2& dragDelta);
	void Draw(FeEditorContext& context, FeEditorViewport& viewport);
	virtual FeColor& GetShaderPropertyColor(const FeString& PropertyName) override;

	FeEViewportController::Type Type;
	FeEViewportDirection::Type Direction;
	FeViewportRect Rect;
	FeViewportRect ContainerRect;
	FeColor Color;
};

struct FeEditorViewportOverlays
{
	FeEditorViewportGrid			Grid;
	FeEditorViewportRootWidget		RootWidget;
	FeEditorViewportWidgets		Widgets;
	FeEditorViewportSelection		Selection;

	FeEditorViewportController		Controllers[FeEditorViewportController::Count];
	FeEditorViewportCursorOverlay	Cursor;
};
class FeEditorViewport
{
public:
	static const float HoverPrevisionOffset;

	void Initialize();
	void Update(FeEditorContext& context);
	void Draw(FeEditorContext& context);
	bool IsCursorOver();
	bool IsCursorOverController(FeEditorContext& context);
	FeEditorViewportController* GetHoveredController(FeEditorContext& context);
	
	FeEditorViewportOverlays Overlays;
	FeRenderBatch	RenderBatch;
	FeTransform		Transform;
	FeTransform		DesignerTransform;
	FeRect			Dimensions;

	FeEditorViewportCursor		Cursor;
	FeEditorViewportCursor		CursorNormalized;

private:
	void ComputeHovered(FeEditorContext& context);
};

namespace FeViewportHelper
{
	void SnapWidgetToCursor(FeWidget* widget, const FeEditorViewport& viewport);
	bool IntersectPointWithRect(const FeVector2& Point, const FeMatrix4& Rect, float offset = 0.0f);
	void ComputeDirection(FeVector2& vDir, FeEViewportDirection::Type dir);
	void Scale(FeViewportRect& rect, const FeVector2& scale, const FeVector2& pivot);
	void Dock(FeViewportRect& rect, FeViewportRect& container, FeEViewportDirection::Type dir);
	void Align(FeViewportRect& rect, FeViewportRect& container, FeEViewportAlignement::Type horizontal, FeEViewportAlignement::Type vertical);
	void ComputeAlignementFromDirection(FeEViewportDirection::Type dir, FeEViewportAlignement::Type& horizontal, FeEViewportAlignement::Type& vertical);
	void UniformScaleAdd(FeViewportRect& rect, float value);
	void ApplyOffset(FeViewportRect& rect, FeEViewportDirection::Type dir, float value);
	bool IsDirectionDiagonal(FeEViewportDirection::Type dir);
	void ComputeNormalizedGridSize(FeEditorContext& context, FeVector2& GridSizeN);
	void ComputeBoundingBox(FeWidget* widget, FeMatrix4& output);
}
