#include <common/common.hpp>
#include <common/filesystem.hpp>
#include <common/memorymanager.hpp>
#include <common/network.hpp>

#include <rendering/modulerenderer.hpp>
#include <rendering/renderresourceshandler.hpp>

#include <emulator/moduleemulator.hpp>
#include <frontend/modulefrontend.hpp>

#define SDL_MAIN_HANDLED
#include <SDL_syswm.h>
#include <SDL.h>

#include "imgui.hpp"
#include <imgui.h>

#define FE_HEAPID_EMULATOR 0

class FeApplicationConfig : public FeSerializable
{
public:
	#define FeApplicationConfig_Properties(_d)	\

	FE_DECLARE_CLASS_BODY(FeApplicationConfig_Properties, FeApplicationConfig, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeApplicationConfig)

class FeModuleEmulator : public FeModule
{
public:
	static FeModuleEmulator* Get()
	{
		static FeModuleEmulator* instance = nullptr;
		if (!instance)
			instance = FeApplication::GetStaticInstance()->GetModule<FeModuleEmulator>();
		return instance;
	}

	virtual uint32 Load(const FeModuleInit*) override
	{
		{	// Debug shader render
			FeAssetRegistry assets;
			assets.Files.SetHeapId(FE_HEAPID_FILESYSTEM);
			assets.LoadFiles("test/effect", ".*\\.fes");

			for (auto& file : assets.Files)
			{
				if (file->IsOfType<FeRenderEffect>())
				{
					uint32 iRes = FeModuleRendering::Get()->LoadEffect(file.Get());
					FE_FAILEDRETURN(iRes);
				}
			}
			RenderBatch.Viewport.Rect = FeModuleRendering::Get()->GetScreenDimensions();
			RenderBatch.Viewport.UseBackBuffer = true;
			Geometries.Resize(1);

			static FeAssetRef Effect("test/effect/EffectTest.fes", "Effect_Test");

			for (uint32 i = 0; i < Geometries.GetSize(); ++i)
			{
				auto pGeom = FE_NEW(FeRenderGeometryInstance, FE_HEAPID_RENDERER);
				Geometries[i] = pGeom;

				pGeom->Reset();
				pGeom->Geometry = FeGeometryHelper::GetStaticGeometry(FeEGemetryDataType::Quad);
				pGeom->Transform = FeGeometryHelper::IdentityMatrix();
				pGeom->Viewport = &RenderBatch.Viewport;
				pGeom->Effect = Effect.Path.GetId();

				RenderBatch.GeometryInstances.Add(pGeom);
			}
		}

		{ // Init imgui renderer
			auto Renderer = FeModuleRendering::Get();
			ImGui_ImplDX11_Init(Renderer->GetDevice().GetD3DDevice(), Renderer->GetDevice().GetImmediateContext());
			ImGui_ImplDX11_CreateDeviceObjects();
			Renderer->RegisterRenderCallback(RenderImgui);
			SetImguiDirty(false);
		}

		return FeEReturnCode::Success;
	}
	virtual uint32 Unload() override
	{
		int iReturned = 0;
		ImGui_ImplDX11_Shutdown();
		return FeEReturnCode::Success;
	}

	void SendNetMessage(FeEEmulatorNetMessage::Type eMsg)
	{
		FeEmulatorClient::Get()->CreateOuboxMessage([&](FeEmulatorNetMessageContainer& container) -> void
		{
			auto pMsg = FE_NEW(FeEmulatorNetMessage, FE_HEAPID_EMULATOR);
			container.Message.Assign(pMsg, FE_HEAPID_EMULATOR, true);
			pMsg->Type = eMsg;
		});
	}
	virtual uint32 Update(const FeDt& fDt) override
	{
		{ // Draw imgui
			ImGuiIO& io = ImGui::GetIO();
			auto resolution = FeModuleRendering::Get()->GetScreenDimensions();

			io.DisplaySize = ImVec2((float)resolution.GetWidth(), (float)resolution.GetHeight());
			io.DeltaTime = fDt.TotalSeconds;
			ImGui::NewFrame();
			bool bOpen = true;
			ImGui::Begin("Window", &bOpen, ImVec2(1000, 720));
			
			#define EmuButton(name) 								\
				if (ImGui::Button(#name))							\
				{													\
					SendNetMessage(FeEEmulatorNetMessage:: name);	\
				}													\
				ImGui::NextColumn();								\

			EmuButton(Pause);
			EmuButton(Stop);
			EmuButton(Resume);

			{
				int32 iMsg = 0;
				static FeTArray<FeString> msgs;
				FeEmulatorClient::Get()->ReadInboxMessages([&](const FeEmulatorNetMessageContainer& msg) -> void
				{
					FeEEmulatorNetMessage::Type eMsgType = msg.Message.Get()->Type;
					msgs.Add(FeEEmulatorNetMessage::ToString(eMsgType));
				});

				for (auto& msgType : msgs)
					ImGui::Text("Message %d : %s", ++iMsg, msgType.Cstr());
			}

			ImGui::End();

			SetImguiDirty(true);
		}
		FeModuleRendering::Get()->RegisterRenderBatch(&RenderBatch);

		return FeEReturnCode::Success;
	}
	virtual char* ComputeDebugOutput(const FeDt& fDt) { return ""; }
	virtual const char* GetName()
	{
		static const char* sName = "Emulator Test";
		return sName;
	}
	
	FeTArray<FeRenderGeometryInstance*> Geometries;
	FeRenderBatch RenderBatch;
};

class FeFrontendApplication : public FeApplication
{
public:

	virtual uint32 Load(const FeApplicationInit& appInit) override
	{
		uint32 iRes = FeApplication::Load(appInit);;
		FE_FAILEDRETURN(iRes);

		FeTArray<FePath> files;
		FeFileTools::List(files, "config", "config.json");
		FeApplicationConfig config;
		if (files.GetSize())
			iRes = FeJson::Deserialize(config, files[0], FE_HEAPID_RENDERER);

		FE_FAILEDRETURN(iRes);

#if 0 //def _DEBUG
		static uint32 bWait = 1;
		while (bWait)
		{ Sleep(10); }
#endif
		FeEmulatorClient::Get()->ParseProcessArgs(appInit);

		{
			FeModuleInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleFilesManager>(init));
		}
		{
			FeModuleRenderingInit init;

			init.WindowsCmdShow = appInit.WindowsCmdShow;
			init.WindowsInstance = (HINSTANCE)appInit.WindowsInstance;
			init.ScreenDimensions.right = 1280;
			init.ScreenDimensions.bottom = 720;
			init.EnableFileWatcher = true;
			
			CreateAppWindow()	;

			init.WindowHandle = WindowHandle;

			auto pModule = CreateModule<FeModuleRendering>();
			FE_FAILEDRETURN(pModule->Load(&init));
			
		}
		{
			FeModuleInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleRenderResourcesHandler>(init));
		}
		{
			FeModuleInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleEmulator>(init));
		}

		FeEmulatorClient::Get()->ParseProcessArgs(appInit);

		if (FeEmulatorClient::Get()->Config.SharedTargetHandle)
			FeModuleRendering::Get()->BindSharedTargetFromHandle(FeEmulatorClient::Get()->Config.SharedTargetHandle);

		FeEmulatorClient::Get()->ConnectToServer();

		return iRes;
	}

	virtual void OnKeyPressed(uint32 key) override
	{
		switch (key)
		{
		case SDL_SCANCODE_F1:
			//FeModuleRendering::Get()->DebugOutputNextModule();
			FeModuleEmulator::Get()->SendNetMessage(FeEEmulatorNetMessage::Pause);
			break;
		case SDL_SCANCODE_F2:
			FeModuleEmulator::Get()->SendNetMessage(FeEEmulatorNetMessage::Resume);
			break;
		case SDL_SCANCODE_F3:
			FeModuleEmulator::Get()->SendNetMessage(FeEEmulatorNetMessage::HardReset);
			break;
		case SDL_SCANCODE_F4:
			FeModuleEmulator::Get()->SendNetMessage(FeEEmulatorNetMessage::LoadState);
			break;
		}
	}
};

//int _tmain(int argc, _TCHAR* argv[])
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "SDL2");		// 0
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "FileSystem");	// 1 FE_HEAPID_FILESYSTEM
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "JsonParser");	// 2 FE_HEAPID_JSONPARSER
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "Renderer");	// 3 FE_HEAPID_RENDERER
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "Ui");			// 4 FE_HEAPID_UI
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "StringPool");	// 5 FE_HEAPID_STRINGPOOL
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "Database");	// 6 

	FeMemoryManager::StaticInstance.Initialize();
	FeStringPool::Initialize();
	FePath::SetRoot("../../data");

	FeFrontendApplication app;
	FeApplicationInit init;

	init.WindowsCmdLine = lpCmdLine;
	init.WindowsCmdShow = nCmdShow;
	init.WindowsInstance = hInstance;
	init.WindowsPrevInstance = hPrevInstance;

	FE_FAILEDRETURN(app.Load(init));
	FE_FAILEDRETURN(app.Run());
	FE_FAILEDRETURN(app.Unload());

	return 0;
}
