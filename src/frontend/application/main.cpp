#include <pch.hpp>

#include <frontend/modulefrontend.hpp>

class FeApplicationConfig : public FeSerializable
{
public:
#define FeApplicationConfig_Properties(_d)	\
	_d(FeString,			Theme)			\
	_d(FeScrapingConfig,	Scraping)		\

	FE_DECLARE_CLASS_BODY(FeApplicationConfig_Properties, FeApplicationConfig, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeApplicationConfig)

class FeFrontendApplication : public FeApplication
{
public:

	virtual uint32 Load(const FeApplicationInit& appInit) override
	{
		uint32 iRes = FeApplication::Load(appInit);;
		FE_FAILEDRETURN(iRes);

		FeTArray<FePath> files;
		FeFileTools::List(files, "config", "config.json");
		FeApplicationConfig config;
		if (files.GetSize())
			iRes = FeJson::Deserialize(config, files[0], FE_HEAPID_UI);

		FE_FAILEDRETURN(iRes);

		{
			FeModuleInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleFilesManager>(init));
		}
		{
			FeModuleRenderingInit init;

			FePath windowInfosPath;
			windowInfosPath = "editor/windowInfos.json";
			SetWindowInfosPath(windowInfosPath);

			init.WindowsCmdShow = appInit.WindowsCmdShow;
			init.WindowsInstance = (HINSTANCE)appInit.WindowsInstance;
			init.ScreenDimensions.right = WindowInfos.GetRect().getData()[2];
			init.ScreenDimensions.bottom = WindowInfos.GetRect().getData()[3];
			init.EnableFileWatcher = true;
			
			CreateAppWindow();

			init.WindowHandle = WindowHandle;

			auto pModule = CreateModule<FeModuleRendering>();
			FE_FAILEDRETURN(pModule->Load(&init));
		}
		{
			FeModuleScrapingInit init;
			init.Config = config.GetScraping();
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleScraping>(init));
		}
		{
			FeModuleInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleRenderResourcesHandler>(init));
		}
		{
			FeModuleUiInit init;
			init.EnableFileWatcher = true;
			init.ThemeToLoad = config.Theme;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleUi>(init));
		}
		{
			FeModuleWebViewerInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleWebViewer>(init));
		}
		{
			FeModuleFrontEndInit init;
			FE_FAILEDRETURN(CreateAndLoadModule<FeModuleFrontEnd>(init));
		}

		FeModuleFrontEnd::Get()->GetCurrentProcess()->Start();

		return iRes;
	}

	virtual void OnKeyPressed(uint32 key) override
	{
		switch (key)
		{
		case SDL_SCANCODE_F1:
			GetModule<FeModuleRendering>()->DebugOutputNextModule();
			break;
		case SDL_SCANCODE_F2:
			GetModule<FeModuleUi>()->Reload();
			break;
		}
	}
};

//int _tmain(int argc, _TCHAR* argv[])
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "SDL2");		// 0
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "FileSystem");	// 1 FE_HEAPID_FILESYSTEM
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "JsonParser");	// 2 FE_HEAPID_JSONPARSER
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "Renderer");	// 3 FE_HEAPID_RENDERER
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "Ui");			// 4 FE_HEAPID_UI
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "StringPool");	// 5 FE_HEAPID_STRINGPOOL
	FeMemoryManager::StaticInstance.CreateHeapMBytes(16, "Database");	// 6 

	FeMemoryManager::StaticInstance.Initialize();
	FeStringPool::Initialize();
	FePath::SetRoot("../../data");

	FeFrontendApplication app;
	FeApplicationInit init;

	init.WindowsCmdLine = lpCmdLine;
	init.WindowsCmdShow = nCmdShow;
	init.WindowsInstance = hInstance;
	init.WindowsPrevInstance = hPrevInstance;

	FE_FAILEDRETURN(app.Load(init));
	FE_FAILEDRETURN(app.Run());
	FE_FAILEDRETURN(app.Unload());

	return 0;
}
