#pragma once

#include <common/common.hpp>
#include <common/filesystem.hpp>

#include <rendering/modulerenderer.hpp>
#include <rendering/renderresourceshandler.hpp>

#include <common/memorymanager.hpp>

#include <ui/moduleui.hpp>
#include <scraping/modulescraping.hpp>
#include <webviewer/modulewebviewer.hpp>

#include <map>
#include <iostream>
#include <ctime>

#define SDL_MAIN_HANDLED
#include <SDL_syswm.h>
#include <SDL.h>


#if 0
void unitTest();
#endif