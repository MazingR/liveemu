#pragma once

#include <common/common.hpp>
#include <common/application.hpp>
#include <common/process.hpp>
#include <common/network.hpp>
#include <scraping/commonscraping.hpp>
#include <rendering/commonrenderer.hpp>
#include <emulator/moduleemulator.hpp>

class FeEmulatorProcess
{
public:
	FeEmulatorProcess()
	{
		ProcessInfo = { 0 };
		SharedTargetHandle = 0;
		State = FeEEmulatorProcessState::_0_NotRunning;
		TargetTextureId = 0;
		TargetTexture = nullptr;
		Connection.TcpPort = 4150;
		KeepAliveElapsedTime = 0;
	}

	uint32 Update(const FeDt& fDt);
	void Start();
	void Stop();
	uint32 ServerThreadRun(FeEConnectionCallbackType::Type eType);

	// interface : FeIEmulatorCommandHandler
	virtual void SendPause();
	virtual void SendStop();
	virtual void SendHardReset();
	virtual void SendSoftReset();
	virtual void SendResume();
	virtual void SendLoadGame(const FeDataGameDump& gameDump);
	virtual void SendSaveState(int32 iSlot);
	virtual void SendLoadState(int32 iSlot);
	virtual void SendSetFocus(bool bValue);
	virtual void SendHardStop();

	const FeNetMessageCommunication& GetCommunication() const { return Communication; }
protected:
	void SendNetMessage(FeEEmulatorNetMessage::Type eType);
	virtual const FePath& GetProcessPath() const = 0;
	virtual const FeString& GetProcessName() const = 0;
	virtual void ComputeCommandLine(FeStaticString<FE_STRING_SIZE_512>& strCmdLine);

	FeProcessInfo					ProcessInfo;
	uint64							SharedTargetHandle;
	FeResourceId					TargetTextureId;
	FeRenderTargetTexture*			TargetTexture;
	FeEEmulatorProcessState::Type	State;
	FeTcpClientServerConnection		Connection;
	FeNetMessageCommunication		Communication;
	float							KeepAliveElapsedTime;
	FeDataGameDump					PendingGameDumpToLoad;

	uint32 StartProcess();
	void LoadTargetTexture();
	void StartTcpServer();
};

class FeEmptyEmulatorProcess : public FeEmulatorProcess
{
protected:
	const FePath& GetProcessPath() const;
	virtual const FeString& GetProcessName() const;
};

class FeTestEmulatorProcess : public FeEmulatorProcess
{
protected:
	const FePath& GetProcessPath() const;
	virtual const FeString& GetProcessName() const;
};

class FeMameEmulatorProcess : public FeEmulatorProcess
{
protected:
	virtual const FeString& GetProcessName() const;
	const FePath& GetProcessPath() const;
};

struct FeModuleFrontEndInit : public FeModuleInit
{
public:
};

class FeModuleFrontEnd : public FeModule
{
public:
	static FeModuleFrontEnd* Get()
	{
		static FeModuleFrontEnd* instance = nullptr;

		if (!instance)
			instance = FeApplication::GetStaticInstance()->GetModule<FeModuleFrontEnd>();

		return instance;
	}
	virtual uint32 Load(const FeModuleInit*) override;
	virtual uint32 Unload() override;
	virtual uint32 Update(const FeDt& fDt) override;
	virtual char* ComputeDebugOutput(const FeDt& fDt) { return ""; }
	virtual const char* GetName()
	{
		static const char* sName = "FrontEnd";
		return sName;
	}
	FeEmulatorProcess* GetCurrentProcess()
	{
		return &EmuProfessMame;
	}
private:
	FeEmptyEmulatorProcess EmuProfessMame;
};