#include <modulefrontend.hpp>
#include <common/process.hpp>
#include <common/json.hpp>
#include <common/network.hpp>
#include <common/filesystem.hpp>

#include <rendering/modulerenderer.hpp>
#include <rendering/renderresourceshandler.hpp>
#include <SDL.h>

#define FE_HEAP_FRONTEND 0

template<typename T>
T* CreateMessage(FeTArray<FeEmulatorNetMessageContainer>& box)
{
	FeEmulatorNetMessageContainer& container = box.Add();
	auto pMsg = FE_NEW(T, FE_HEAP_FRONTEND);
	container.Message.Assign(pMsg, FE_HEAP_FRONTEND, true);

	return pMsg;
}
void FeEmulatorProcess::SendNetMessage(FeEEmulatorNetMessage::Type eType)
{
	SCOPELOCK((SDL_mutex*)Communication.OutboxMutex);
	auto msg = CreateMessage<FeEmulatorNetMessage>(Communication.Outbox);
	msg->Type = eType;
}
void FeEmulatorProcess::SendPause()
{
	SendNetMessage(FeEEmulatorNetMessage::Pause);
}
void FeEmulatorProcess::SendStop()
{
	SendNetMessage(FeEEmulatorNetMessage::Stop);
}
void FeEmulatorProcess::SendHardReset()
{
	SendNetMessage(FeEEmulatorNetMessage::HardReset);
}
void FeEmulatorProcess::SendSoftReset()
{
	SendNetMessage(FeEEmulatorNetMessage::SoftReset);
}
void FeEmulatorProcess::SendResume()
{
	SendNetMessage(FeEEmulatorNetMessage::Resume);
}
void FeEmulatorProcess::SendHardStop()
{
	FeProcessStop(ProcessInfo);
}
void FeEmulatorProcess::SendSetFocus(bool bFocus)
{
	SCOPELOCK((SDL_mutex*)Communication.OutboxMutex);
	auto msg = CreateMessage<FeEmulatorNetMsgSetFocus>(Communication.Outbox);
	msg->Type = FeEEmulatorNetMessage::SetFocus;
	msg->Value = bFocus;
}
void FeEmulatorProcess::SendLoadGame(const FeDataGameDump& gameDump)
{
	if (!FeProcessIsRunning(ProcessInfo))
	{
		Start();
		PendingGameDumpToLoad = gameDump;
	}
	else
	{
		SCOPELOCK((SDL_mutex*)Communication.OutboxMutex);
		auto msg = CreateMessage<FeEmulatorNetMsgLoadGame>(Communication.Outbox);
		msg->Type = FeEEmulatorNetMessage::LoadGame;
		msg->RomPath = gameDump.LocalPath;
	}
}
void FeEmulatorProcess::SendSaveState(int32 iSlot)
{
	SCOPELOCK((SDL_mutex*)Communication.OutboxMutex);
	auto msg = CreateMessage<FeEmulatorNetMsgLoadSaveState>(Communication.Outbox);
	msg->Type = FeEEmulatorNetMessage::SaveState;
	msg->Slot = iSlot;
}
void FeEmulatorProcess::SendLoadState(int32 iSlot)
{
	SCOPELOCK((SDL_mutex*)Communication.OutboxMutex);
	auto msg = CreateMessage<FeEmulatorNetMsgLoadSaveState>(Communication.Outbox);
	msg->Type = FeEEmulatorNetMessage::LoadState;
	msg->Slot = iSlot;
}

uint32 FeEmulatorProcess::StartProcess()
{
	const FePath& exePath = GetProcessPath();
	static FeStaticString<FE_STRING_SIZE_512> strCmdLine;
	ComputeCommandLine(strCmdLine);
	return FeProcessCreate(ProcessInfo, exePath, strCmdLine.Value, FeEProcessCreationFlags::NoWindow);
}
uint32 FeEmulatorProcess::ServerThreadRun(FeEConnectionCallbackType::Type eConnectionType)
{
	State = FeEEmulatorProcessState::_4_Running;

	while (true)
	{
		if (State != FeEEmulatorProcessState::_4_Running)
		{
			break;
		}

		if (eConnectionType == FeEConnectionCallbackType::Read)
		{
			FE_FAILEDRETURN(Communication.ReadToInbox(Connection));
		}
		else
		{
			_sleep(16);
			FE_FAILEDRETURN(Communication.SendOutbox(Connection));
		}
	}
}

void ConnectionCallback(FeTcpClientServerConnection* callbackParam, FeEConnectionCallbackType::Type eConnectionType)
{
	FeEmulatorProcess* pThis = (FeEmulatorProcess*)callbackParam->UserParam;
	pThis->ServerThreadRun(eConnectionType);
}
void FeEmulatorProcess::StartTcpServer()
{
	// Start tcp client listen thread
	Connection.ConnectionCallback = ConnectionCallback;
	Connection.UserParam = this;
	FeNetCreateServerConnection(Connection);
	State = FeEEmulatorProcessState::_3_WaitingTcpConnection;

}
void FeEmulatorProcess::ComputeCommandLine(FeStaticString<FE_STRING_SIZE_512>& strCmdLine)
{
	FeEmulatorProcessConfig config;
	FePath currentDir = FeFileTools::GetWorkingDir();

	FeStringBuffer configJson;
	config.TcpPort = Connection.TcpPort;
	config.SharedTargetHandle = SharedTargetHandle;
	config.SharedTargetWidth = TargetTexture->Width;
	config.SharedTargetHeight = TargetTexture->Height;
	config.FrontEndDirectory = currentDir.Dir;
	config.WindowHandle = (uint64)FeApplication::GetStaticInstance()->GetWindowHandle();
	config.ModuleHandle = (uint64)FeApplication::GetStaticInstance()->GetModuleHandle();

	FeJson::SerializeToMemory(&config, configJson, 0);

	strCmdLine.Clear();
	strCmdLine << GetProcessName().CstrNotNull() << " " << configJson.Cstr();

	for (auto& oneChar : strCmdLine.Value)
	{
		if (oneChar == '\"')
			oneChar = '$';
	}
}
void FeEmulatorProcess::LoadTargetTexture()
{
	FeRenderLoadingResource resource;
	resource.Type = FeEResourceType::RenderTargetTexture;
	resource.AllocateResource();

	static const char emuTargetPath[] = "[EMULATOR]"; // only one for now

	resource.Path.Source = emuTargetPath;
	resource.Path.Cooked = emuTargetPath;

	TargetTexture = resource.GetResourceT<FeRenderTargetTexture>();
	TargetTexture->Shared = true;
	TargetTexture->Width = 1280;
	TargetTexture->Height = 720;
	TargetTexture->Format = FeEWritableTextureFormat::R8G8B8X8;

	FeModuleRenderResourcesHandler::Get()->LoadResource(resource);
	TargetTextureId = resource.Id;
	
	// Check if texture is already loaded
	if (resource.Resource == nullptr)
	{
		FeRenderResource* loadedResource = FeModuleRenderResourcesHandler::Get()->GetResource(resource.Id);
		if (loadedResource)
			TargetTexture = static_cast<FeRenderTargetTexture*>(loadedResource);
	}
}
void FeEmulatorProcess::Start()
{
	if (State == FeEEmulatorProcessState::_0_NotRunning)
		State = FeEEmulatorProcessState::_1_Starting;
}
void FeEmulatorProcess::Stop()
{
	// nothing to do
}
uint32 FeEmulatorProcess::Update(const FeDt& fDt)
{
	switch (State)
	{
	case FeEEmulatorProcessState::_0_NotRunning:
	{
		// idle
	} break;
	case FeEEmulatorProcessState::_1_Starting:
	{
		LoadTargetTexture();

		State = FeEEmulatorProcessState::_2_LoadingRenderTargetTexture;
	} break;
	case FeEEmulatorProcessState::_2_LoadingRenderTargetTexture:
	{
		if (TargetTexture->LoadingState == FeEResourceLoadingState::Loaded)
		{
			SharedTargetHandle = TargetTexture->Texture.SharedHandle;
			StartTcpServer();

			if (FE_FAILED(StartProcess())) // Start processs
				State = FeEEmulatorProcessState::_0_NotRunning;
		}
	} break;
	case FeEEmulatorProcessState::_3_WaitingTcpConnection:
	{
		// let thread call change the state when client connects
	} break;
	case FeEEmulatorProcessState::_4_Running:
	{
		if (false == PendingGameDumpToLoad.LocalPath.IsEmpty())
		{
			SendLoadGame(PendingGameDumpToLoad); // ask running process to load game
			PendingGameDumpToLoad = FeDataGameDump(); // reset pending game
		}
		static const float KeepAliveInterval = 0.5f;
		
		// Check wether process is still running
		if ((KeepAliveElapsedTime += fDt.TotalSeconds) > KeepAliveInterval)
		{
			KeepAliveElapsedTime = 0.f;
			if (false == FeProcessIsRunning(ProcessInfo))
			{
				State = FeEEmulatorProcessState::_0_NotRunning;
			}
		}
	} break;
	default:
		FE_ASSERT(false, "Unknwon FeEEmulatorProcessState");
		return FeEReturnCode::Failed;
	}
	return FeEReturnCode::Success;
}

const FeString& FeEmptyEmulatorProcess::GetProcessName() const
{
	static const FeString name = "";
	return name;
}
const FePath& FeEmptyEmulatorProcess::GetProcessPath() const
{
	static const FePath exePath = "";
	return exePath;
}
const FeString& FeTestEmulatorProcess::GetProcessName() const
{
	static const FeString name = "testEmulator_x86D.exe";
	return name;
}
const FePath& FeTestEmulatorProcess::GetProcessPath() const
{
	static const FePath exePath = "../bin/Debug32/testEmulator_x86D.exe";
	return exePath;
}
const FeString& FeMameEmulatorProcess::GetProcessName() const
{
	static const FeString name = "mamed.exe";
	return name;
}
const FePath& FeMameEmulatorProcess::GetProcessPath() const
{
	static const FePath exePath = "../../../liveemu_externals/emulators/common/mame/mamed.exe";
	return exePath;
}

uint32 FeModuleFrontEnd::Load(const FeModuleInit* initBase)
{
	auto init = (FeModuleFrontEndInit*)initBase;
	EmuProfessMame.Start();

	return FeEReturnCode::Success;
}
uint32 FeModuleFrontEnd::Unload()
{
	EmuProfessMame.SendHardStop();
	return FeEReturnCode::Success;
}
uint32 FeModuleFrontEnd::Update(const FeDt& fDt)
{
	EmuProfessMame.Update(fDt);
	return FeEReturnCode::Success;
}
