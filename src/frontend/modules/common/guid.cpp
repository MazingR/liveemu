#include "guid.hpp"
#include "string.hpp"
#include <Windows.h>

FeGuid::FeGuid()
{
	memset(this, 0, sizeof(FeGuid));
}
bool FeGuid::IsValid() const
{
	return	Data1 != 0 ||
			Data2 != 0 ||
			Data3 != 0 ||
			Data4 != 0 ||
			Data5 != 0;
}
bool FeGuid::operator==(const FeGuid& other) const
{
	return
		Data1 == other.Data1 &&
		Data2 == other.Data2 &&
		Data3 == other.Data3 &&
		Data4 == other.Data4 &&
		Data5 == other.Data5;
}
bool FeGuid::operator!=(const FeGuid& other) const
{
	return false == (*this == other);
}
void FeGuid::ToString(FeString& output) const
{
	char strTmp[64];
	sprintf_s(strTmp, "%X-%X-%X-%X-%X", Data1, Data2, Data3, Data4, Data5);
	output = strTmp;
}
void FeGuid::FromString(const FeString& input)
{
	char strTmp[64];
	size_t length = input.Length();

	if (length < 64 && length > 0)
	{
		size_t dashIdx1 = FeStringTools::IndexOf(input.Cstr(), '-', 0, length);
		if (dashIdx1 + 1 >= length) return;

		size_t dashIdx2 = FeStringTools::IndexOf(input.Cstr(), '-', dashIdx1 + 1, length);
		if (dashIdx2 + 1 >= length) return;

		size_t dashIdx3 = FeStringTools::IndexOf(input.Cstr(), '-', dashIdx2 + 1, length);
		if (dashIdx3 + 1 >= length) return;

		size_t dashIdx4 = FeStringTools::IndexOf(input.Cstr(), '-', dashIdx3 + 1, length);
		if (dashIdx4 + 1 >= length) return;

		#define FETCH_VALUE(_offset, _length, _output, _type)	\
		{														\
			memcpy(strTmp, input.Cstr() + _offset, _length);	\
			strTmp[_length] = '\0';								\
			uint32 value;										\
			sscanf_s(strTmp, "%X", &value);						\
			_output = (_type)value;								\
		}

		FETCH_VALUE(0,				dashIdx1,					Data1,	uint32);
		FETCH_VALUE(dashIdx1 + 1,	dashIdx2 - dashIdx1 - 1,	Data2,	uint16);
		FETCH_VALUE(dashIdx2 + 1,	dashIdx3 - dashIdx2 - 1,	Data3,	uint16);
		FETCH_VALUE(dashIdx3 + 1,	dashIdx4 - dashIdx3 - 1,	Data4,	uint32);
		FETCH_VALUE(dashIdx4 + 1,	length - dashIdx4 - 1,		Data5,	uint32);
	}
	else
	{
		FE_LOG("invalid GUID serialized size %llu !", length);
	}
}
void FeGuid::Create(FeGuid& output)
{
	GUID guid;
	CoCreateGuid(&guid);
	memcpy_s(&output, sizeof(FeGuid), &guid, sizeof(GUID));
}
