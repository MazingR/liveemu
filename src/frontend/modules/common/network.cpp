
#include <curl/curl.h>

#include "network.hpp"
#include "filesystem.hpp"
#include "string.hpp"
#include <SDL.h>

void WsaLogError()
{
	DWORD err = GetLastError();
	LPTSTR Error = 0;

	if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		err,
		0,
		(LPTSTR)&Error,
		0,
		NULL) == 0)
	{
		// Failed in translating the error.
	}
	else
	{
		FE_LOG_ERROR("WsaLogError : %s", Error);
		LocalFree(Error);
	}
}
size_t url_write_none(void *buffer, size_t size, size_t nmemb, void *userp)
{
	return nmemb;
}
size_t url_write_string(void *buffer, size_t size, size_t nmemb, void *userp)
{
	FeStringBuffer* output = (FeStringBuffer*)userp;

	uint32 iWritten = output->Concat((char*)buffer, nmemb);
	FE_ASSERT(iWritten == nmemb, "buffer too small!");
	return iWritten;
}
size_t url_write_dynamic(void *buffer, size_t size, size_t nmemb, void *userp)
{
	FeDynamicBuffer* output = (FeDynamicBuffer*)userp;

	uint32 iWritten = output->Write(buffer, nmemb);

	FE_ASSERT(iWritten == nmemb, "buffer too small!");
	return iWritten;
}

#define CURL_SAFE_EXEC(func, params)																	\
	{																										\
		CURLcode __errorcode__ = func params;																\
		FE_ASSERT(__errorcode__ == CURLE_OK, "%s failed: %s\n", #func, curl_easy_strerror(__errorcode__));	\
		if (__errorcode__ != CURLE_OK)																		\
			return FeEReturnCode::Failed;																	\
	}

uint32 FeUrlTransferHandler::Initialize()
{
	CURL_SAFE_EXEC(curl_global_init, (CURL_GLOBAL_DEFAULT));

	CurlHandle = curl_easy_init();

	if (CurlHandle)
	{
		CURL_SAFE_EXEC(curl_easy_setopt, (CurlHandle, CURLOPT_HEADERFUNCTION, url_write_none));
		CURL_SAFE_EXEC(curl_easy_setopt, (CurlHandle, CURLOPT_WRITEFUNCTION, url_write_none));
		
		return FeEReturnCode::Success;
	}

	return FeEReturnCode::Failed;
}
uint32 FeUrlTransferHandler::Perform(const char* url, const char* userAgent /*= nullptr*/)
{
	if (userAgent)
	{
		CURL_SAFE_EXEC(curl_easy_setopt, (CurlHandle, CURLOPT_USERAGENT, userAgent));
	}
	CURL_SAFE_EXEC(curl_easy_setopt, (CurlHandle, CURLOPT_URL, url));

#ifdef SKIP_PEER_VERIFICATION
	/*
	* If you want to connect to a site who isn't using a certificate that is
	* signed by one of the certs in the CA bundle you have, you can skip the
	* verification of the server's certificate. This makes the connection
	* A LOT LESS SECURE.
	*
	* If you have a CA cert for the server stored someplace else than in the
	* default bundle, then the CURLOPT_CAPATH option might come handy for
	* you.
	*/
	curl_easy_setopt(CurlHandle, CURLOPT_SSL_VERIFYPEER, 0L);
#endif

#ifdef SKIP_HOSTNAME_VERIFICATION
	/*
	* If the site you're connecting to uses a different host name that what
	* they have mentioned in their server certificate's commonName (or
	* subjectAltName) fields, libcurl will refuse to connect. You can skip
	* this check, but this will make the connection less secure.
	*/
	curl_easy_setopt(CurlHandle, CURLOPT_SSL_VERIFYHOST, 0L);
#endif

	CURL_SAFE_EXEC(curl_easy_perform, (CurlHandle));

	CURL_SAFE_EXEC(curl_easy_setopt, (CurlHandle, CURLOPT_HEADERFUNCTION, url_write_none));

	return FeEReturnCode::Success;
}
uint32 FeUrlTransferHandler::Perform(FeStringBuffer* outputBuffer, const char* url, const char* userAgent /*= nullptr*/)
{
	if (!CurlHandle)
		return FeEReturnCode::Failed;

	if (outputBuffer)
	{
		CURL_SAFE_EXEC(curl_easy_setopt, (CurlHandle, CURLOPT_WRITEFUNCTION, url_write_string));
		CURL_SAFE_EXEC(curl_easy_setopt, (CurlHandle, CURLOPT_WRITEDATA, outputBuffer));
	}
	return Perform(url, userAgent);
}
uint32 FeUrlTransferHandler::Perform(FeDynamicBuffer* outputBuffer, const char* url, const char* userAgent /*= nullptr*/)
{
	if (!CurlHandle)
		return FeEReturnCode::Failed;

	if (outputBuffer)
	{
		CURL_SAFE_EXEC(curl_easy_setopt, (CurlHandle, CURLOPT_WRITEFUNCTION, url_write_dynamic));
		CURL_SAFE_EXEC(curl_easy_setopt, (CurlHandle, CURLOPT_WRITEDATA, outputBuffer));
	}
	return Perform(url, userAgent);
}
void FeUrlTransferHandler::Shutdown()
{
	if (CurlHandle)
	{
		curl_easy_cleanup(CurlHandle);
	}
	curl_global_cleanup();
}
FeUrlTransferHandler::~FeUrlTransferHandler()
{
	Shutdown();
}

// ------------------------------------------------------------------------------------------------------------------------
uint32 FeNetCreateSocketListener(struct FeTcpClientServerConnection& connection);
uint32 FeNetCloseSocketListener(uint64 socket);
uint32 FeNetWaitForClientConnection(struct FeTcpClientServerConnection& connection);
uint32 FeNetConnectToServer(struct FeTcpClientServerConnection& connection);


#define SOCKET_CHECK_RETURN(iValue) 								\
{ 																	\
	if (iValue == SOCKET_ERROR)										\
	{																\
		FE_LOG_ERROR("Socket Function Error %s (%d)", __FUNCTION__, \
			WSAGetLastError()); 									\
		WsaLogError();												\
		FE_ASSERT(iValue != SOCKET_ERROR, "");						\
		return FeEReturnCode::Failed;								\
	}																\
}																	\

#define FE_WSA_BUFF_SIZE 64
__declspec(thread) char Buffer[FE_WSA_BUFF_SIZE];

uint32 FeNetStartup()
{
	WSADATA wsd;
	if (WSAStartup(MAKEWORD(2, 2), &wsd) != 0)
	{
		FE_LOG_ERROR("Failed to load Winsock library!");
		return FeEReturnCode::Failed;
	}
	return FeEReturnCode::Success;
}
uint32 FeNetCreateSocketListener(struct FeTcpClientServerConnection& connection)
{
	struct sockaddr_in	local;
	connection.Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);

	if (connection.Socket == SOCKET_ERROR)
	{
		FE_LOG_ERROR("\tsocket() failed: %d", WSAGetLastError());
		return FeEReturnCode::Failed;
	}

	local.sin_addr.s_addr = inet_addr("127.0.0.1");
	local.sin_family = AF_INET;
	local.sin_port = htons(connection.TcpPort);

	if (bind((SOCKET)connection.Socket, (struct sockaddr *)&local, sizeof(local)) == SOCKET_ERROR)
	{
		FE_LOG_ERROR("bind() failed: %d", WSAGetLastError());
		WsaLogError();
		return FeEReturnCode::Failed;
	}
	
	if (listen((SOCKET)connection.Socket, 0) == SOCKET_ERROR)
	{
		FE_LOG_ERROR("listen() failed: %d", WSAGetLastError());
		WsaLogError();
		return FeEReturnCode::Failed;
	}
	return FeEReturnCode::Success;
}
uint32 FeNetCloseSocketListener(uint64 _socket)
{
	if (0 != closesocket((SOCKET)_socket))
		FeEReturnCode::Failed;

	if (0 != WSACleanup())
		FeEReturnCode::Failed;

	return FeEReturnCode::Success;
}
uint32 FeNetConnectToServer(struct FeTcpClientServerConnection& connection)
{
	WSADATA WSAData;
	SOCKADDR_IN sin;
	WSAStartup(MAKEWORD(2, 0), &WSAData);

	connection.Socket = socket(AF_INET, SOCK_STREAM, 0);
	sin.sin_addr.s_addr = inet_addr(connection.ServerName.Cstr());
	sin.sin_family = AF_INET;
	sin.sin_port = htons(connection.TcpPort);

	int iRes = connect((SOCKET)connection.Socket, (SOCKADDR *)&sin, sizeof(sin)); // syunchronous connect
	SOCKET_CHECK_RETURN(iRes);

	return FeEReturnCode::Success;
}

uint32 FeNetWaitForClientConnection(struct FeTcpClientServerConnection& connection)
{
	int iAddrSize;
	struct sockaddr_in client;
	iAddrSize = sizeof(client);

	connection.ClientSocket = accept((SOCKET)connection.Socket, (struct sockaddr *)&client, &iAddrSize);

	if (connection.ClientSocket == INVALID_SOCKET)
	{
		FE_LOG_ERROR("accept() failed: %d", WSAGetLastError());
		WsaLogError();

	}

	FE_LOG("accepted client: %s:%d", inet_ntoa(client.sin_addr), ntohs(client.sin_port));

	return FeEReturnCode::Success;
}
uint32 FeNetSelectSocket(uint64 _socket)
{
	static const timeval timeout = { 0, 1000 }; // 10 micro seconds timeout
	fd_set readfs;
	FD_ZERO(&readfs);
	FD_SET(_socket, &readfs);
	int32 iRet = 0;

	if (iRet = select(0, &readfs, NULL, NULL, &timeout) < 0)
	{
		SOCKET_CHECK_RETURN(iRet);
	}

	return iRet;
}
uint32 FeNetReadUint(uint64 _socket, uint32& output)
{
	char buffer[4];

	output = (uint32)-1;

	int32 iRes = recv((SOCKET)_socket, buffer, 4, 0);
	SOCKET_CHECK_RETURN(iRes);

	output = (uint32)
		((uint8)buffer[3] << 0) +
		((uint8)buffer[2] << 8) +
		((uint8)buffer[1] << 16) +
		((uint8)buffer[0] << 24);


	return FeEReturnCode::Success;
}
uint32 FeNetReadString(uint64 _socket, FeStringBuffer& output)
{
	// read string size from socket
	unsigned int iStringLength = -1;
	int32 iRes = FeNetReadUint((SOCKET)_socket, iStringLength);
	SOCKET_CHECK_RETURN(iRes);

	if (iStringLength)
	{
		// allocate needed memory if string buffer is too small
		if (output.GetAvailableSize() < iStringLength)
		{
			uint32 iNeeded = iStringLength - output.GetAvailableSize();
			output.Allocate(output.Size + iNeeded, output.Heap);
		}

		{	// Read from socket until we reached string length
			uint32 iTotalReadBytes = 0;
			char* pBuffer = ((char*)output.Data) + output.Length;

			do
			{
				uint32 iReadBytes = recv((SOCKET)_socket, pBuffer, iStringLength - iTotalReadBytes, 0);

				if (iReadBytes == SOCKET_ERROR)
					break;

				output.Length += iReadBytes;
				pBuffer += iReadBytes;
				iTotalReadBytes += iReadBytes;

			} while (iTotalReadBytes < iStringLength);

			if (iTotalReadBytes < iStringLength)
				FeEReturnCode::Failed;

			pBuffer[0] = '\0'; // add string end char
		}
	}
	return FeEReturnCode::Success;
}

uint32 FeNetWriteUint(uint64 _socket, uint32 input)
{
	char buffer[4];
	
	buffer[0] = input >> 24 & 0xFF;
	buffer[1] = input >> 16 & 0xFF;
	buffer[2] = input >> 8 & 0xFF;
	buffer[3] = input & 0xFF;

	int32 iRes = send((SOCKET)_socket, buffer, 4, 0);
	SOCKET_CHECK_RETURN(iRes);

	return FeEReturnCode::Success;
}
uint32 FeNetWriteString(uint64 _socket, const FeStringBuffer& input)
{
	uint32 iResult = FeNetWriteUint(_socket, input.Length);
	if (FE_SUCCEEDED(iResult))
	{
		int32 iRes = send((SOCKET)_socket, input.Cstr(), input.Length, 0);
		SOCKET_CHECK_RETURN(iRes);
	}
	return iResult;
}
uint32 FeNetWriteString(uint64 _socket, const char* input)
{
	uint32 iLen = strlen(input);
	uint32 iResult = FeNetWriteUint(_socket, iLen);

	if (FE_SUCCEEDED(iResult))
	{
		int32 iRes = send((SOCKET)_socket, input, iLen, 0);
		SOCKET_CHECK_RETURN(iRes);
	}
	return iResult;
}
uint32 FeNetWriteString(uint64 _socket, const FeString& input)
{
	uint32 iResult = FeNetWriteUint(_socket, input.Length());
	if (FE_SUCCEEDED(iResult))
	{
		int32 iRes = send((SOCKET)_socket, input.Cstr(), input.Length(), 0);
		SOCKET_CHECK_RETURN(iRes);
	}

	return iResult;
}

int32 FeNetConnectionWriteCallback(void* userParam)
{
	FeTcpClientServerConnection* connection = (FeTcpClientServerConnection*)userParam;

	if (connection->ConnectionCallback)
		connection->ConnectionCallback(connection, FeEConnectionCallbackType::Write);

	return 0;
}
int32 FeNetCreateClientConnectionThread(void* userParam)
{
	FeTcpClientServerConnection* connection = (FeTcpClientServerConnection*)userParam;
	
	FE_FAILEDRETURN(FeNetStartup());

	uint32 iResult = FeNetConnectToServer(*connection);
	FE_FAILEDRETURN(iResult);

	SDL_CreateThread(FeNetConnectionWriteCallback, "FeNetConnectionWriteCallback", connection);

	if (connection->ConnectionCallback)
		connection->ConnectionCallback(connection, FeEConnectionCallbackType::Read);

	return 0;
}
int32 FeNetCreateServerConnectionThread(void* userParam)
{
	FeTcpClientServerConnection* connection = (FeTcpClientServerConnection*)userParam;

	int iAddrSize;
	struct sockaddr_in client;
	iAddrSize = sizeof(client);

	FE_FAILEDRETURN(FeNetStartup());

	uint32 iResult = FeNetCreateSocketListener(*connection);
	FE_FAILEDRETURN(iResult);

	iResult = FeNetWaitForClientConnection(*connection);
	FE_FAILEDRETURN(iResult);

	SDL_CreateThread(FeNetConnectionWriteCallback, "FeNetConnectionWriteCallback", connection);

	if (connection->ConnectionCallback)
		connection->ConnectionCallback(connection, FeEConnectionCallbackType::Read);

	return 0;
}
void FeNetCreateClientConnection(struct FeTcpClientServerConnection& connection)
{
	SDL_CreateThread(FeNetCreateClientConnectionThread, "FeNetCreateClientConnection", &connection);
}
void FeNetCreateServerConnection(struct FeTcpClientServerConnection& connection)
{
	SDL_CreateThread(FeNetCreateServerConnectionThread, "FeNetCreateServerConnection", &connection);
}