#pragma once 

#include "common.hpp"
#include "serializable.hpp"
#include "path.hpp"

#include <typeinfo.h>
#include <map>
#include <taskscheduler.hpp>

struct SDL_WindowEvent;

uint32 FeFetchCommandLineParamsW(void* lpCmdLine, FeTArray<FeString>& output);

class FeAppWindowInfos : public FeSerializable
{
public:

#define FeAppWindowInfos_Properties(_d)			\
		_d(bool,				IsMaximized)	\
		_d(FeVector4,			Rect)

	FeAppWindowInfos() : IsMaximized(false), Rect(100,100,1280,720) {}

	FE_DECLARE_CLASS_BODY(FeAppWindowInfos_Properties, FeAppWindowInfos, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeAppWindowInfos)


struct FeModuleInit
{};

class FeModule
{
public:
	virtual uint32 Load(const FeModuleInit*) = 0;
	virtual uint32 Unload() = 0;
	virtual uint32 Update(const FeDt& fDt) = 0;
	virtual char* ComputeDebugOutput(const FeDt& fDt) = 0;
	virtual const char* GetName() = 0;
};

struct FeApplicationInit
{
public:
	void*		WindowsInstance;
	void*		WindowsPrevInstance;
	wchar_t*	WindowsCmdLine;
	int			WindowsCmdShow;
};
class FeApplication
{
public:

	FeApplication::FeApplication() : WindowHandle(nullptr), SdlWindow(nullptr) {}

	static FeApplication* GetStaticInstance()
	{
		return s_StaticInstance;
	}
	static FeTaskScheduler* GetTasksScheduler()
	{
		return s_TasksScheduler;
	}

	typedef std::map<size_t, FeModule*> ModulesMap;
	typedef ModulesMap::iterator ModulesMapIt;

	virtual uint32 Load(const FeApplicationInit&);
	virtual uint32 Unload();

	virtual void OnKeyPressed(uint32 key);
	virtual void OnKeyReleased(uint32 key);
	virtual void OnMouseWheel(int32 y);
	virtual void OnWindowEvent(const SDL_WindowEvent& winEvent);
	virtual void OnWindowSizeChanged(int32 width, int32 height);
	void Quit();

	uint32 CreateAppWindow();
	uint32 SetWindowInfosPath(const FePath& path);
	uint32 Run();

	template<class T>
	T* CreateModule()
	{
		T* pModule = FE_NEW(T, 0);
		Modules[GetModuleId<T>()] = pModule;
		return pModule;
	}

	template<class T>
	uint32 CreateAndLoadModule(const FeModuleInit& init)
	{
		T* pModule = CreateModule<T>();
		return pModule->Load(&init);
	}
	template<class T>
	T* GetModule()
	{
		return (T*)Modules[GetModuleId<T>()];
	}
	template<class T>
	size_t GetModuleId()
	{
		static size_t iTypeHash = typeid(T).hash_code();
		return iTypeHash;
	}
	uint32 GetModulesCount()
	{
		return Modules.size();
	}
	FeModule* GetModule(uint32 index)
	{
		ModulesMapIt it = Modules.begin();
		for (uint32 i = 0; i < index; ++i)
			it++;

		return it->second;
	}

	int32 GetThreadsCount();
	void ShowWindow();
	void HideWindow();
	bool IsShuttingDown() { return bIsShuttingDown; }
	void* GetWindowHandle() { return WindowHandle; }
	void* GetModuleHandle() { return ModuleHandle; }

protected:
	static FeApplication* s_StaticInstance;
	static FeTaskScheduler* s_TasksScheduler;

	FeTaskScheduler TasksScheduler;
	ModulesMap Modules;
	bool bIsShuttingDown;
	void* WindowHandle;
	void* ModuleHandle;

	FeAppWindowInfos WindowInfos;
	FePath WindowInfosPath;

private:
	void* SdlWindow;
};
