#pragma once 

#include "factory.hpp"
#include "string.hpp"
#include "typeinfo.hpp"

FeTArray<FeTypeInfo> FeTypeInfo::s_typeInfos;
bool FeTypeInfo::s_bIsInitialized = false;
FeCObjectsFactory FeObjectsFactory;

FeSerializable* FeCObjectsFactory::CreateObjectFromFactory(const FeString& sTypeName, uint32 iHeapId)
{
	FeSerializable* pResult = nullptr;
	FactoriesMapIt it = Factories.find(sTypeName.Id());
	FE_ASSERT(it != Factories.end(), "unknown type %s", sTypeName);
	return it->second.CreateFunc(iHeapId);
}

FeSerializable* FeCObjectsFactory::CreateObjectFromFactory(const char* sTypeName, uint32 iHeapId)
{
	FeSerializable* pResult = nullptr;
	uint32 iTypeHash = FeStringTools::GenerateUIntIdFromString(sTypeName);
	FactoriesMapIt it = Factories.find(iTypeHash);
	
	FE_ASSERT(it != Factories.end(), "unknown type %s", sTypeName);
	return it->second.CreateFunc(iHeapId);
}

FeCObjectsFactory& GetObjectsFactory()
{
	static FeCObjectsFactory FeObjectsFactory;

	return FeObjectsFactory;
}