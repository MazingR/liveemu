#pragma once 

#include <stdio.h>
#include <functional>

struct FeString;

#ifdef __cplusplus
#define C_BEGIN extern "C" {
#else
#define C_BEGIN
#endif

#ifdef __cplusplus
#define C_END }
#else
#define C_END
#endif

#define SafeRelease(a) {if (a) {a->Release();a=nullptr; } }
#define SafeReleaseDeleteSelf(a) {if (a) { a->Release(); a->DeleteSelf();a=nullptr; } }
#define SafeDeleteSelf(a) {if (a) { a->DeleteSelf();a=nullptr; } }

#define STR_SIZE_SMALL 32
#define STR_SIZE_MEDIUM 128
#define STR_SIZE_WIDE 512

#define COMMON_PATH_SIZE 512

#ifdef DEBUG
#define CONFIGSTR "Debug"
#elif defined(MASTER)
#define CONFIGSTR "Master"
#else
#define CONFIGSTR "Release"
#endif

typedef unsigned int THeapId;
#define INIT_HEAP_ID ((THeapId)-1)

typedef unsigned char uint8;
typedef short unsigned int uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;

typedef short int int16;
typedef int int32;
typedef long long int64;

#define FE_MAX_INT32 (INT_MAX)
#define FE_MAX_UINT32 (UINT_MAX)

#ifndef DEFAULT_HEAP
#define DEFAULT_HEAP (uint32)-1
#endif

namespace FeEReturnCode
{
	enum Type
	{
		Success = 0,
		Canceled,

		File_OpenFailed,
		File_ReadFailed,

		Rendering_CreateShaderFailed,

		Memory_CreateHeapFailed,
		Memory_AllocationFailedNotEnoughMemory,

		Serializer_SkipArrayChildren,

		FileSystem_Error,


		Failed = ((uint32)-1),
	};
};

int __cdecl vs_printf(const char *format, ...);

void FeSetLastError(const char* fmt, ...);
const char* FeGetLastError();

#define FE_FAILED(a) ((a)!=FeEReturnCode::Success)
#define FE_SUCCEEDED(a) ((a)==FeEReturnCode::Success)
#define FE_INVALID_ID ((uint32)-1)
#define FE_FAILEDRETURN(a) { uint32 ___iResult = (a); { if FE_FAILED(___iResult) return ___iResult; } }

#define FE_LOG(fmt, ...)				\
	{vs_printf(fmt, __VA_ARGS__);		\
	vs_printf("\n");}					\

#define FE_LOG_ERROR(fmt, ...)			\
	{vs_printf(fmt, __VA_ARGS__);		\
	vs_printf("\n");					\
	FeSetLastError(fmt, __VA_ARGS__);}	\
	

#ifdef DEBUG
	#define FE_ASSERT(condition, fmt, ...) { if (!(condition)) { FE_LOG(fmt, __VA_ARGS__);__debugbreak();  } }
	#define FE_ASSERT_NO_BREAK(condition, fmt, ...) { if (!(condition)) { FE_LOG(fmt, __VA_ARGS__); } }
#else
	#define FE_ASSERT(condition, fmt, ...) { if (!(condition)) { FE_LOG(fmt, __VA_ARGS__); } }
	#define FE_ASSERT_NO_BREAK(condition, fmt, ...) FE_ASSERT(condition, fmt, __VA_ARGS__)
#endif

#define FE_ASSERT_NOT_IMPLEMENTED FE_ASSERT(false, "not implemented")
#define FE_ASSERT_NOT_IMPLEMENTED_RETURN(a) FE_ASSERT_NOT_IMPLEMENTED;return a;


#ifndef FE_NO_CUSTOM_ALLOCATOR
#include <newhook.hpp>
#include <mallochook.hpp>

#define FE_NEW(type, heap, ...) FeNew<type>(heap, ##__VA_ARGS__)
#define FE_NEW_ARRAY(type, size, heap) FeNewA<type>(size, heap)

#define FE_NEWD(type) FeNew<type>(DEFAULT_HEAP)
#define FE_NEW_ARRAYD(type, size) FeNewA<type>(size, DEFAULT_HEAP)

#define FE_DELETE(type, ptr, heap) FeDelete<type>(ptr, heap)
#define FE_DELETE_ARRAY(type, ptr, size, heap) FeDeleteA<type>(ptr, size, heap, false)
#define FE_DELETE_ARRAY_DESTROY(type, ptr, size, heap) FeDeleteA<type>(ptr, size, heap, true)

#define FE_DELETED(type, ptr) FeDelete<type>(ptr, DEFAULT_HEAP)
#else
#define FE_FREE(ptr, heap) free(ptr)
#define FE_ALLOCATE(size, heap) malloc(size)

#define FE_NEW(type, heap, ...) new type (##__VA_ARGS__)
#define FE_NEW_ARRAY(type, size, heap) new type[size]

#define FE_NEWD(type) new type
#define FE_NEW_ARRAYD(type, size) new type[size]

#define FE_DELETE(type, ptr, heap) delete ptr
#define FE_DELETE_ARRAY(type, ptr, size, heap) delete [] ptr
#define FE_DELETE_ARRAY_DESTROY(type, ptr, size, heap) delete [] ptr

#define FE_DELETED(type, ptr) delete ptr
#endif

typedef struct FeRect
{
	int32 left, top, right, bottom;

	int32 GetWidth() const
	{
		return right - left;
	}
	int32 GetHeight() const
	{
		return bottom - top;
	}
	void SetZero()
	{
		left = top = right = bottom = 0;
	}
	

	bool operator==(const FeRect& other) const
	{
		return	left == other.left && top == other.top && right == other.right && bottom == other.bottom;
	}

	bool operator!=(const FeRect& other) const
	{
		return	!(*this == other);
	}

} FeRect;

void* FeNewAllocate(size_t size, THeapId iHeapId);
void* FeNewFree(void* ptr, THeapId iHeapId);

template<typename T, typename... Args>
inline T* FeNew(THeapId iHeapId = DEFAULT_HEAP, Args... args)
{
	void* ptr = FeNewAllocate(sizeof(T), iHeapId);
	new (ptr) T(args...);

	return (T*)ptr;
}
template<typename T, typename... Args>
inline T* FeNew(const T& other, THeapId iHeapId = DEFAULT_HEAP, Args... args)
{
	void* ptr = FeNewAllocate(sizeof(T), iHeapId);
	new (ptr)T(args...);

	return (T*)ptr;
}

template<typename T>
inline T* FeNewA(size_t count, THeapId iHeapId = DEFAULT_HEAP)
{
	void* ptr = FeNewAllocate(sizeof(T)*count, iHeapId);

	for (size_t i = 0; i < count; ++i)
		new (((T*)(ptr)) + i)T;
	return (T*)ptr;
}
template<typename T>
void FeDelete(T* ptr, THeapId iHeapId = DEFAULT_HEAP)
{
	((T*)(ptr))->~T();
	FeNewFree(ptr, iHeapId);
}
template<typename T>
void FeDeleteA(void* ptr, size_t count, THeapId iHeapId = DEFAULT_HEAP, bool bDetroyEntries=true)
{
	//if (bDetroyEntries)
	//{
	//	for (size_t i = 0; i < count; ++i)
	//	{
	//		T ptrTyped = (((T)(ptr)) + i);
	//		if (ptrTyped)
	//			ptrTyped.~T();
	//	}
	//}

	FeNewFree(ptr, iHeapId);
}

struct FeDt
{
	float	TotalSeconds;
	uint32	TotalMilliseconds;
	int		TotalCpuWaited;
};

//struct FePath
//{
//	char Value[COMMON_PATH_SIZE];
//
//	FePath();
//	FePath(const FePath& other);
//	void Clear();
//	bool IsEmpty();
//	FePath& operator=(const FePath& other);
//	FePath& operator=(const FeString& other);
//
//	bool operator==(const FePath& other) const
//	{
//		return strcmpi(Value, other.Value)==0;
//	}
//	bool operator!=(const FePath& other) const
//	{
//		return !(other == *this);
//	}
//	void Set(const char* str, bool bRelative=false);
//};

#define FE_STRING_SIZE_16  16
#define FE_STRING_SIZE_32  32
#define FE_STRING_SIZE_64  64
#define FE_STRING_SIZE_128 128
#define FE_STRING_SIZE_512 512

#define SafeDelete(ptr) {if (ptr) delete ptr; }
#define SafeDeleteArray(ptr) {if (ptr) delete[] ptr; }

#define INLINE_OP_VEC(output, a,b, index ,op) {output[index]=a[index] op b[index];}

#define INLINE_OP_VEC2(output, a,b, op ) INLINE_OP_VEC(output, a, b, 0, op )	INLINE_OP_VEC(output, a, b, 1, op )
#define INLINE_OP_VEC3(output, a,b, op ) INLINE_OP_VEC2(output,a,b, op )		INLINE_OP_VEC(output, a, b, 2, op )
#define INLINE_OP_VEC4(output, a,b, op ) INLINE_OP_VEC3(output,a,b, op )		INLINE_OP_VEC(output, a, b, 3, op )

#define INLINE_ADD_EULER(output, a,b) {output.getData()[0]=a.getData()[0]+b.getData()[0];}{output.getData()[1]=a.getData()[1]+b.getData()[1];}{output.getData()[2]=a.getData()[2]+b.getData()[2];}

class FeWidget;

namespace FeETraversalOperation
{
	enum Type
	{
		Modulate,
		Bitwise,
		Add
	};
}

class FeTraversalPropertyBase
{
public:
	uint32	Id;

	FeTraversalPropertyBase*	ParentProperty;

	virtual bool GetIsPropagated()	const = 0;
	virtual void PostDeSerialized() = 0;
	virtual void OnTransientChanged(bool bPropagate) = 0;
	
	virtual void* GetSerialized() = 0;
	virtual void* GetTransient() = 0;
	virtual void* GetTraversal() = 0;
};

template<typename T, FeETraversalOperation::Type Operation, bool IsPropagated>
struct FeApplyTraversalOperation
{
	static void Call(T& traversal, T& transient, T& parentTraversal)
	{
		FE_ASSERT_NOT_IMPLEMENTED;
	}
};

template<typename T>
struct FeApplyTraversalOperation<T, FeETraversalOperation::Modulate, true>
{
	static void Call(T& traversal, T& transient, T& parentTraversal)
	{
		traversal = transient * parentTraversal;
	}
};

template<typename T>
struct FeApplyTraversalOperation<T, FeETraversalOperation::Bitwise, true>
{
	static void Call(T& traversal, T& transient, T& parentTraversal)
	{
		traversal = transient & parentTraversal;
	}
};

template<typename T, FeETraversalOperation::Type Operation, bool IsPropagated>
class FeTraversalProperty : public FeTraversalPropertyBase
{
public:
	T		Serialized;
	T		Transient;
	T		Traversal;

	virtual bool GetIsPropagated()	const override { return IsPropagated; }

	virtual void* GetSerialized()	override { return &Serialized;	}
	virtual void* GetTransient()	override { return &Transient;	}
	virtual void* GetTraversal()	override { return &Traversal;	}

	FeTraversalProperty()
	{
		Id = 0;
	}
	FeTraversalProperty(const FeTraversalProperty<T, Operation, IsPropagated>& value) : this()
	{
		*this = value;
	}

	FeTraversalProperty<T, Operation, IsPropagated>& operator=(const T& value)
	{
		Serialized = value;
		Transient = value;
		Traversal = value;

		return *this;
	}
	FeTraversalProperty<T, Operation, IsPropagated>& operator=(const FeTraversalProperty<T, Operation, IsPropagated>& value)
	{
		Serialized = value.Serialized;
		Transient = value.Transient;
		Traversal = value.Traversal;

		return *this;
	}

	void PostDeSerialized()
	{
		Transient = Serialized;
		Traversal = Serialized;
	}
	void OnTransientChanged(bool bPropagate)
	{
		if (ParentProperty && bPropagate)
		{
			auto& parentProp = *(FeTraversalProperty<T, Operation, IsPropagated>*)ParentProperty;
			FeApplyTraversalOperation<T, Operation, IsPropagated>::Call(Traversal, Transient, parentProp.Traversal);
		}
		else
		{
			Traversal = Transient;
		}
	}
};

class FeISelfDeletable
{
public:
	virtual void SelfDelete(uint32 iHeapId) = 0;
};


bool FeConvert(int& output, const char* input);
bool FeConvert(uint32& output, const char* input);
bool FeConvert(uint64& output, const char* input);
bool FeConvert(bool& output, const char* input);
bool FeConvert(float& output, const char* input);
bool FeConvert(FeString& output, const char* input);

struct SDL_mutex;

struct FeScopeLockedMutex
{
	SDL_mutex* Mutex;

	FeScopeLockedMutex(SDL_mutex* pMutex);
	~FeScopeLockedMutex();
};

#define SCOPELOCK_NAMED(mutex, name) FeScopeLockedMutex name(mutex);
#define SCOPELOCK(mutex) FeScopeLockedMutex _local_mutex_(mutex);