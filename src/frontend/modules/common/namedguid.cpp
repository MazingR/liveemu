#include "namedguid.hpp"

bool FeNamedGUID::IsValid() const
{
	return Guid.IsValid();
}
bool FeNamedGUID::operator==(const FeNamedGUID& other) const
{
	return Guid == other.Guid;
}
bool FeNamedGUID::operator!=(const FeNamedGUID& other) const
{
	return false == (*this == other);
}
bool FeNamedGUID::operator==(const FeGuid& other) const
{
	return Guid == other;
}
bool FeNamedGUID::operator!=(const FeGuid& other) const
{
	return false == (*this == other);
}
void FeNamedGUID::Create()
{
	FeGuid::Create(Guid);
}
void FeNamedGUID::OnDeserialized()
{
	if (false == Guid.IsValid())
	{
		Create();
	}
}