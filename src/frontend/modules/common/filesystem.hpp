#pragma once

#define MAX_HEAP_COUNT 64
#define FE_HEAPID_FILESYSTEM 1
#define FE_HEAPNAME_FILESYSTEM "FileSystem"

#include "common.hpp"
#include <tarray.hpp>
#include <unordered_map>
#include <string.hpp>
#include <path.hpp>
#include <application.hpp>
#include <vadefs.h>
#include <stdarg.h>

namespace FeEFileChangeType
{
	enum Type
	{
		Deleted,
		Created,
		Changed
	};
}
//
//struct FeDirectory
//{
//	char Name[64];
//	void* Handle;
//	FePath Path;
//	FeTArray<FePath> Files;
//	FeTArray<FeDirectory> Directories;
//};

typedef void(*FeFileChangeEvent) (FeEFileChangeType::Type eChangeType, const char* szPath, void* pUserData);

class FeModuleFilesManager : public FeModule
{
public:
	virtual uint32 Load(const FeModuleInit*) override;
	virtual uint32 Unload() override;
	virtual uint32 Update(const FeDt& fDt) override;
	virtual const char* GetName()
	{
		static const char* sName = "FilesManager";
		return sName;
	}

	void WatchDirectory(const char* szPath, FeFileChangeEvent onFileChanged, void* pUserData);
	virtual char* ComputeDebugOutput(const FeDt& fDt) { return ""; }

private:
	struct FileChangeEventParam
	{
		FeEFileChangeType::Type Type;
		FePath					Path;
	};
	struct WatchedDirectory
	{
		FePath							Path;
		FePath							Filter;
		void*							Handle;
		void*							WatchHandle;
		//FeTArray<FileChangeEventParam>	OccuredEvents;
		FeFileChangeEvent				OnFileChangeEvent;
		void*							FileEventUserData;
	};
	FeTArray<WatchedDirectory> WatchedDirs;
};

namespace FeFileTools
{
	template<uint32 Size>
	void FormatFullPath(const FePath& path, FeStaticString<Size>& output, const char* szFormat = nullptr, ...)
	{
		output.Clear();

		output << FePath::GetRoot();
		output << path.Dir.CstrNotNull();

		if (path.Dir.Length()>0 && path.File.Length()>0)
			output << "/";

		output << path.File.CstrNotNull();
		output << path.Ext.CstrNotNull();

		if (szFormat && Size>output.Length)
		{
			va_list argptr;
			va_start(argptr, szFormat);
			uint32 maxCount = Size - output.Length;
			int ret = vsnprintf_s(output.Value + output.Length, maxCount, maxCount, szFormat, argptr);
			va_end(argptr);
		}
	}
	FePath GetWorkingDir();

	uint32 List(const char* szPath, FeDirectory& root, const char* szFilter);
	uint32 List(const FePath& path, FeDirectory& root, const char* szFilter);
	uint32 List(FeTArray<FePath>& files, const char* szPath, const char* szFilter);

	bool FileDelete(const FePath& file);
	bool FileExists(const FePath& file);
	void* OpenFile(const FePath& file);
	void CloseFile(void* handle);
	uint32 ReadTextFile(const FePath& file, char** ppOutput, size_t* iFileSize);
};