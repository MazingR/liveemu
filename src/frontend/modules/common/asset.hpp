#pragma once 

#include "common.hpp"
#include "serializable.hpp"
#include "string.hpp"
#include "namedguid.hpp"

class FeAsset : public FeSerializable
{
public:
	FeAsset();
private:
	#define FeAsset_Properties(_d)		\
		_d(FeNamedGUID,		ID)			\
		_d(FeString,		Type)		\

	FE_DECLARE_CLASS_BODY(FeAsset_Properties, FeAsset, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeAsset)


class FeAssetFile : public FeAsset
{
public:
	#define props(_d)				\
	_d(FeTPtr<FeAsset>,	Asset)		\

	#define propsN(_d)				\
	_d(FePath,			Path)		\
	_d(FeAssetRef,		AssetRef)	\

	FeAssetFile()
	{
		static const FeString assetType = "AssetFile";
		Type = assetType;
	}

	template<typename T>
	T* GetAsset()
	{
		if (Asset.IsValid() && Asset->IsChildOf(T::ClassName()))
			return static_cast<T*>(Asset.Get());
		else
			return nullptr;

	}

	template<typename T>
	bool IsOfType()
	{
		return GetAsset<T>() != nullptr;
	}

	FE_DECLARE_CLASS_BODY_EX(props, propsN, FeAssetFile, FeSerializable)
	#undef props
	#undef propsN
};
FE_DECLARE_CLASS_BOTTOM(FeAssetFile)


class FeAssetRegistry : public FeSerializable
{
public:
	typedef std::function<void(const FeAssetRef& assetRef)> RemoveAssetCallback;
	typedef std::function<void(FeAssetFile* assetFile)> CreateAssetCallback;

	#define FeAssetRegistry_Properties(_d)		\
	_d(FeTArray<FeTPtr<FeAssetFile>>,	Files)	\

	void LoadFiles(const char* path, const char* filter);
	void LoadFiles(const FePath& path, const char* filter);

	void RemoveAsset(const FeAssetRef& assetRef)
	{
		if (OnRemoveAset)
			OnRemoveAset(assetRef);
		
		Files.Remove([&](const FeTPtr<FeAssetFile>& o) -> bool { return o.Get()->AssetRef == assetRef; });
	}

	template<typename T>
	void GetAssets(FeTArray<T*>& output)
	{
		for (auto& assetFile : Files)
		{
			if (assetFile->IsOfType<T>())
				output.Add(assetFile->GetAsset<T>());
		}
	}
	void GetAssetFiles(const FeString& type, FeTArray<FeAssetFile*>& output)
	{
		if (type.IsEmpty())
			return;

		for (auto& assetFile : Files)
		{
			if (assetFile->AssetRef.Type == type)
				output.Add(assetFile.Get());
		}
	}

	void GetAssets(const FeString& type, FeTArray<FeAsset*>& output)
	{
		if (type.IsEmpty())
			return;

		for (auto& assetFile : Files)
		{
			if (assetFile->AssetRef.Type == type)
				output.Add(assetFile->Asset.Get());
		}
	}

	FeAsset* GetAsset(const FeAssetRef& assetRef)
	{
		if (assetRef.IsEmpty())
			return nullptr;

		auto found = Files.Find([&](const FeTPtr<FeAssetFile>& o) -> bool { return o.Get()->AssetRef == assetRef; });
		if (found)
			return found->Get()->Asset.Get();
		else
			return nullptr;
	}

	bool FetchAssetRef(FeAssetRef& assetRef)
	{
		if (assetRef.IsEmpty())
			return false;

		auto found = Files.Find([&](const FeTPtr<FeAssetFile>& o) -> bool { return o.Get()->AssetRef == assetRef; });
		if (found)
		{
			assetRef.Pointer = found->Get();
			return true;
		}
		else
		{
			return false;
		}
	}

	template<typename T>
	T* GetAsset(const FeAssetRef& assetRef)
	{
		auto found = GetAsset(assetRef);

		if (found)
			return static_cast<T*>(found);
		else
			return nullptr;
	}

	FeAssetRef ComputeNewAssetRef(const FePath& dirPath, FeAsset* asset);

	template<typename T>
	T* CreateAsset(const FePath& dirPath)
	{
		auto asset = FE_NEW(T, Files.GetHeapId());
		FeAssetRef newAssetRef = ComputeNewAssetRef(dirPath, asset);

		asset->ID.Create();
		auto& assetFilePtr = Files.Add();
		assetFilePtr.New();
		assetFilePtr->Asset.Assign(asset, Files.GetHeapId(), true);
		assetFilePtr->AssetRef = newAssetRef;
		assetFilePtr->Path = newAssetRef.Path;

		if (OnCreatedAsset)
			OnCreatedAsset(assetFilePtr.Get());

		return asset;
	}

	RemoveAssetCallback OnRemoveAset;
	CreateAssetCallback OnCreatedAsset;

	FeAssetRegistry()
	{
		OnCreatedAsset = nullptr;
		OnRemoveAset = nullptr;
	}

	FE_DECLARE_CLASS_BODY(FeAssetRegistry_Properties, FeAssetRegistry, FeSerializable)
};
