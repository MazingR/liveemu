#include <filesystem.hpp>
#include <windows.h>
#include <queue>
#include <regex>
#include "string.hpp"
#include "application.hpp"

#include <SDL.h>

#ifdef WIN32
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

__declspec(thread) FeStaticString<FE_STRING_SIZE_512> FileSystemWorkString;

FeStaticString<FE_STRING_SIZE_128> FePath::Root;
FePath::FePath()
{
	Clear();
}
FePath::FePath(const FePath& other)
{
	*this = other;
}
FePath::FePath(const char* other)
{
	*this = other;
}
uint32 FePath::GetId() const
{
	return Dir.Id() ^ File.Id() ^ Ext.Id();
}
void FePath::Clear()
{
	Dir.Clear();
	File.Clear();
	Ext.Clear();
}
bool FePath::IsDir() const
{
	return Ext.IsEmpty();
}
FePath& FePath::operator=(const FePath& other)
{
	Dir = other.Dir;
	File = other.File;
	Ext = other.Ext;

	return *this;
}
void FePath::GoToParent()
{
	Ext.Clear();
	
	FileSystemWorkString = Dir.Cstr();

	Dir.Clear();
	File.Clear();

	uint32 length = FileSystemWorkString.Length;
	uint32 fileOffset = FeStringTools::LastIndexOf(FileSystemWorkString.Value, '/');
	if (fileOffset != FE_INVALID_ID)
		fileOffset++;

	uint32 fileLen = fileOffset == FE_INVALID_ID ? 0 : length - fileOffset;
	uint32 dirLen = fileOffset>1 ? fileOffset - 1 : fileOffset;

	if (dirLen)		FeStringPool::GetInstance()->CreateString(FileSystemWorkString.Value, dirLen, Dir);
	if (fileLen)	FeStringPool::GetInstance()->CreateString(FileSystemWorkString.Value + fileOffset, fileLen, File);
	
}
FePath& FePath::operator=(const char* str)
{
	if (FeStringTools::HasLeft(str, "http://"))
	{
		Dir.Clear();
		File = str;
		Ext.Clear();
	}
	else
	{
		FileSystemWorkString = str;

		FeStringTools::Replace(FileSystemWorkString.Value, '\\', '/');

		uint32 dirOffset = 0;
		uint32 fileOffset = 0;
		uint32 extOffset = 0;

		if (FeStringTools::HasLeft(FileSystemWorkString.Value, Root.Value))
			dirOffset += Root.Length;

		fileOffset = FeStringTools::LastIndexOf(FileSystemWorkString.Value, '/', dirOffset);
		extOffset = FeStringTools::LastIndexOf(FileSystemWorkString.Value, '.', fileOffset);

		uint32 length = FileSystemWorkString.Length;

		uint32 extLen = extOffset == FE_INVALID_ID ? 0 : length - extOffset;
		length -= extLen;

		uint32 fileLen = fileOffset == FE_INVALID_ID ? 0 : length - fileOffset;
		length -= fileLen;

		if (fileLen) // skip '/' chars
		{
			fileOffset++;
			fileLen--;
		}

		uint32 dirLen = dirOffset == FE_INVALID_ID ? 0 : length - dirOffset;

		if (dirLen)		FeStringPool::GetInstance()->CreateString(FileSystemWorkString.Value + dirOffset, dirLen, Dir);
		if (fileLen)	FeStringPool::GetInstance()->CreateString(FileSystemWorkString.Value + fileOffset, fileLen, File);
		if (extLen)		FeStringPool::GetInstance()->CreateString(FileSystemWorkString.Value + extOffset, extLen, Ext);

		if (fileLen == 0) // depth 1 dir
		{
			File = Dir;
			Dir.Clear();
		}
	}
	return *this;
}
bool FePath::operator==(const FePath& other) const
{
	return Dir == other.Dir && File == other.File;
}
bool FePath::operator!=(const FePath& other) const
{
	return !(other == *this);
}
bool FePath::IsEmpty() const
{
	return File.IsEmpty();
}
bool FePath::IsRemote() const
{
	return FeStringTools::HasLeft(File.CstrNotNull(), "http://");
}
const char* FePath::GetRoot()
{
	return Root.Value;
}
void FePath::SetRoot(const char* szPath)
{
	FeStaticString<FE_STRING_SIZE_512> tmp;

	GetCurrentDir(tmp.Value, FE_STRING_SIZE_512);
	FeStringTools::Replace(tmp.Value, '\\', '/');

	strcat_s(tmp.Value, "/");
	strcat_s(tmp.Value, szPath);

	if (tmp.Value[strlen(tmp.Value)-1] != '/')
		strcat_s(tmp.Value, "/");
	
	Root = tmp.Value;
}



#define FE_LOCALASSERT(condition, fmt, ...) FE_ASSERT(condition, "[FileSystem] "fmt, __VA_ARGS__) 
#define FE_LOCALLOG(fmt, ...) FE_LOG("[FileSystem] "fmt, __VA_ARGS__) 
#define FILE_CHANGE_FLAGS				\
	FILE_NOTIFY_CHANGE_FILE_NAME |		\
	FILE_NOTIFY_CHANGE_DIR_NAME |		\
	FILE_NOTIFY_CHANGE_ATTRIBUTES |		\
	FILE_NOTIFY_CHANGE_SIZE |			\
	FILE_NOTIFY_CHANGE_LAST_WRITE |		\
	FILE_NOTIFY_CHANGE_LAST_ACCESS |	\
	FILE_NOTIFY_CHANGE_CREATION |		\
	FILE_NOTIFY_CHANGE_SECURITY

uint32 FeModuleFilesManager::Load(const FeModuleInit*)
{
	return FeEReturnCode::Success;
}
uint32 FeModuleFilesManager::Unload()
{
	return FeEReturnCode::Success;
}
uint32 FeModuleFilesManager::Update(const FeDt& fDt)
{
	const size_t iMaxFileChange = 512;

	const size_t bufLen = sizeof(FILE_NOTIFY_INFORMATION) * iMaxFileChange;
	static FILE_NOTIFY_INFORMATION buffer[iMaxFileChange];
	
	DWORD bytesRet = 0;

	for (auto& watchedDir : WatchedDirs)
	{

		DWORD dwWaitStatus = WaitForMultipleObjects(1, &watchedDir.WatchHandle, TRUE, 0);
		if (dwWaitStatus == WAIT_OBJECT_0)
		{
			if (ReadDirectoryChangesW(watchedDir.WatchHandle,
				&buffer,
				bufLen,
				true,
				FILE_NOTIFY_CHANGE_LAST_WRITE,
				&bytesRet,
				nullptr,
				nullptr))
			{
				size_t iFileChangedCount = bytesRet / sizeof(FILE_NOTIFY_INFORMATION);
				
				if (iFileChangedCount == 0)
				{
					watchedDir.OnFileChangeEvent(FeEFileChangeType::Changed, "", watchedDir.FileEventUserData);
				}
				//else
				//{
				//	for (size_t i = 0; i < iFileChangedCount; ++i)
				//	{
				//		FePath changedPath;
				//		size_t iConvertedCount;
				//		char szFileName[128];

				//		wcstombs_s(&iConvertedCount, szFileName, buffer[i].FileName, 128);
				//		sprintf_s(changedPath.Value, COMMON_PATH_SIZE, "%s/%s", watchedDir.Path.Value, szFileName);
				//		FeFileTools::FormatPath(changedPath);

				//		watchedDir.OnFileChangeEvent(FeEFileChangeType::Changed, changedPath.Value, watchedDir.FileEventUserData);
				//	}
				//}
			}

			FindNextChangeNotification(watchedDir.WatchHandle);
		}
	}
	return FeEReturnCode::Success;
}

void FeModuleFilesManager::WatchDirectory(const char* szPath, FeFileChangeEvent onFileChanged, void* pUserData)
{	
	HANDLE handle = CreateFile(
		szPath, // pointer to the directory containing the tex files
		FILE_LIST_DIRECTORY | GENERIC_READ,                // access (read-write) mode
		FILE_SHARE_READ | FILE_SHARE_DELETE | FILE_SHARE_WRITE,  // share mode
		nullptr, // security descriptor
		OPEN_EXISTING, // how to create
		FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED, // file attributes
		nullptr); // file with attributes to copy

	FePath path;
	path = szPath;
	FeStaticString<FE_STRING_SIZE_128> fullPath;

	FeFileTools::FormatFullPath(path, fullPath);

	HANDLE watchhandle = FindFirstChangeNotification(fullPath.Value, TRUE, FILE_CHANGE_FLAGS);

	if (handle == INVALID_HANDLE_VALUE || watchhandle == INVALID_HANDLE_VALUE)
	{
		FE_ASSERT(false,  "WatchDirectory function failed [%d].\n", GetLastError());
		return;
	}

	WatchedDirectory& watchedDir = WatchedDirs.Add();
	watchedDir.OnFileChangeEvent = onFileChanged;
	watchedDir.FileEventUserData = pUserData;
	watchedDir.Path = szPath;
	watchedDir.Handle = handle;
	watchedDir.WatchHandle = watchhandle;
}

uint32 sdl_file_write(const char* filename, const char* szContent)
{
	SDL_RWops *rw = SDL_RWFromFile(filename, "w");

	if (rw == nullptr)
		return FeEReturnCode::File_OpenFailed;

	if (rw != nullptr) {
		const char *str = "Hello World";
		size_t len = SDL_strlen(szContent);
		size_t iWritten = SDL_RWwrite(rw, str, 1, len);

		FE_ASSERT(iWritten == len, "Failed writting to file %s", filename);

		SDL_RWclose(rw);
	}

	return FeEReturnCode::Success;
}
uint32 sdl_file_read(const char* filename, void** outputBuff, size_t* pFileSize) 
{
	SDL_RWops *rw = SDL_RWFromFile(filename, "r");

	if (rw == nullptr)
		return FeEReturnCode::File_OpenFailed;

	size_t res_size = (size_t)SDL_RWsize(rw);
	*outputBuff = (char*)FE_ALLOCATE(res_size + 1, FE_HEAPID_FILESYSTEM);

	size_t nb_read_total = 0, nb_read = 1;
	char* buf = (char*)*outputBuff;

	while (nb_read_total < res_size && nb_read != 0) {
		nb_read = SDL_RWread(rw, buf, 1, (res_size - nb_read_total));
		nb_read_total += nb_read;
		buf += nb_read;
	}
	SDL_RWclose(rw);
	*pFileSize = nb_read_total;

	if (nb_read_total != res_size) {
		FE_FREE(*outputBuff, FE_HEAPID_FILESYSTEM);
		return FeEReturnCode::File_ReadFailed;
	}

	((char*)*outputBuff)[nb_read_total] = '\0';

	return FeEReturnCode::Success;
}

namespace FeFileTools
{
	FePath GetWorkingDir()
	{
		::GetCurrentDirectory(FileSystemWorkString.Length, FileSystemWorkString.Value);
		FePath output = FileSystemWorkString.Value;
		return output;
	}
	void ParseDirectory(FeDirectory& dir, std::regex* filter)
	{
		WIN32_FIND_DATA findData;
		
		while (FindNextFile(dir.Handle, &findData))
		{
			if (FeApplication::GetStaticInstance()->IsShuttingDown())
				break;

			const char* szFilePath = findData.cFileName;

			if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				bool bIsVirtual = strcmp(szFilePath, ".") == 0 || strcmp(szFilePath, "..") == 0;

				if (false == bIsVirtual)
				{
					auto& subDir = dir.Directories.Add();
					FormatFullPath(dir.Path, FileSystemWorkString, "/%s", findData.cFileName);
					subDir.Path = FileSystemWorkString.Value;

					FileSystemWorkString << "/*";
					subDir.Handle = FindFirstFileEx(FileSystemWorkString.Value, FindExInfoStandard, &findData, FindExSearchNameMatch, nullptr, 0);

					ParseDirectory(subDir, filter); // recursive call
					FindClose(subDir.Handle);
				}
			}
			else
			{
				bool bSatisfiesFilter = filter ? std::regex_match(findData.cFileName, *filter) : true;

				if (bSatisfiesFilter)
				{
					auto& addedFile = dir.Files.Add();
					FormatFullPath(dir.Path, FileSystemWorkString, "/%s", findData.cFileName);

					addedFile = FileSystemWorkString.Value;
				}
			}
		}
	}
	uint32 List(const FePath& path, FeDirectory& root, const char* szFilter)
	{
		FormatFullPath(path, FileSystemWorkString);
		return List(FileSystemWorkString.Value, root, szFilter);
	}
	uint32 List(FeTArray<FePath>& files, const char* szPath, const char* szFilter)
	{
		FeDirectory dir;
		uint32 iRes = List(szPath, dir, szFilter);
		dir.ListFiles(files);
		return iRes;
	}
	uint32 List(const char* szPath, FeDirectory& root, const char* szFilter)
	{
		root.Directories.Clear();
		root.Files.Clear();

		std::regex filterRegex(szFilter);

		FeStaticString<FE_STRING_SIZE_128> searchPath;
		root.Path = szPath;
		FormatFullPath(root.Path, searchPath, "/*");

		WIN32_FIND_DATA findData;
		root.Handle = FindFirstFileEx(searchPath.Value, FindExInfoStandard, &findData, FindExSearchNameMatch, nullptr, 0);

		ParseDirectory(root, &filterRegex);
		FindClose(root.Handle);

		return FeEReturnCode::Success;
	}
	bool FileDelete(const FePath& file)
	{
		FormatFullPath(file, FileSystemWorkString);
		return remove(FileSystemWorkString.Value) == 0;
	}
	bool FileExists(const FePath& file)
	{
		FormatFullPath(file, FileSystemWorkString);
		WIN32_FIND_DATA findData;
		HANDLE hFind = FindFirstFileEx(FileSystemWorkString.Value, FindExInfoStandard, &findData, FindExSearchNameMatch, nullptr, 0);
		return hFind != ((HANDLE)-1);
	}
	void* OpenFile(const FePath& file)
	{
		FormatFullPath(file, FileSystemWorkString);
		FILE* fp;
		fopen_s(&fp, FileSystemWorkString.Value, "wb"); // non-Windows use "w"
		return fp;
	}
	void CloseFile(void* handle)
	{
		if (handle)
			fclose((FILE*)handle);
	}
	uint32 ReadTextFile(const FePath& file, char** ppOutput, size_t* iFileSize)
	{
		FormatFullPath(file, FileSystemWorkString);
		return sdl_file_read(FileSystemWorkString.Value, (void**)ppOutput, iFileSize);
	}
};