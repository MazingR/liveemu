#pragma once 

#include "common.hpp"
#include <memory>

#define DEFAULT_HEAP_ID 1
#define ASSERT_ON_NULL_PTR 1

template<typename T>
struct FeTSharedPtr
{
public:
	FeTSharedPtr()
	{
	}
	FeTSharedPtr(T* ptr)
	{
		Shared.reset(ptr);
	}
	FeTSharedPtr(const FeTSharedPtr& other)
	{
		*this = other;
	}
	FeTSharedPtr<T>& operator=(const FeTSharedPtr<T>& other) // copy assignment
	{
		Shared = other.Shared;
		return *this;
	}
	bool operator==(const FeTSharedPtr<T>& other) const
	{
		return other.Get() == this->Get();
	}
	bool operator!=(const FeTSharedPtr<T>& other) const
	{
		return !(other == *this);
	}
	void Reset(T* ptr = nullptr)
	{
		Shared.reset(ptr);
	}
	T* Get()
	{
		return Shared.get();
	}
	const T* Get() const 
	{
		return Shared.get();
	}

	template<typename TPtr>
	TPtr* GetT()
	{
		return static_cast<TPtr*>(Shared.get());
	}
	template<typename TPtr>
	const TPtr* GetT() const
	{
		return static_cast<TPtr*>(Shared.get());
	}
	bool IsValid() const
	{
		return Shared.get() != nullptr;
	}
	T* operator->()
	{
		FE_ASSERT(Shared.get(), "null ptr !");
		return Shared.get();
	}
	const T* operator->() const
	{
		FE_ASSERT(Shared.get(), "null ptr !");
		return Shared.get();
	}
private:
	std::shared_ptr<T> Shared;
};

template<typename T=FeISelfDeletable>
struct FeTPtr
{
public:
	bool Owner : 1;
	THeapId HeapId;

	void Reset()
	{
		Owner = true;
		Ptr = nullptr;
		HeapId = DEFAULT_HEAP_ID;
	}
	FeTPtr() : Ptr(nullptr), HeapId(DEFAULT_HEAP_ID) {}
	FeTPtr(T* _Ptr, bool _Owner) : Ptr(_Ptr), Owner(_Owner), HeapId(DEFAULT_HEAP_ID) {}
	FeTPtr(uint32 iHeapId) : Ptr(nullptr), Owner(true), HeapId(iHeapId) {}
	FeTPtr(const FeTPtr<T>& other)
	{
		Owner = false;// other.Owner;
		Ptr = const_cast<FeTPtr<T>*>(&other)->GetNull();
		HeapId = other.HeapId;
	}
	~FeTPtr()
	{
		Delete();
	}

	T* operator->()
	{
		FE_ASSERT(Ptr, "null ptr !");
		return Ptr;
	}
	FeTPtr<T>& operator=(const FeTPtr<T>& other) // copy assignment
	{
		Owner = false;//other.Owner;
		Ptr = const_cast<FeTPtr<T>*>(&other)->GetNull();
		HeapId = other.HeapId;

		return *this;
	}
	bool operator==(const FeTPtr<T>& other) const
	{
		return other.Ptr == this->Ptr;
	}
	bool operator!=(const FeTPtr<T>& other) const
	{
		return ! (other == *this);
	}
	T* New()
	{
		Delete();
		Ptr = FE_NEW(T, HeapId);
		Owner = true;

		return Ptr;
	}
	bool IsValid() const
	{
		return Ptr != nullptr;
	}
	void Assign(T* ptr)
	{
		Assign(ptr, HeapId, false);
	}
	void Assign(T* ptr, THeapId _HeapId, bool _Owner)
	{
		Delete();

		Ptr = ptr;
		Owner = _Owner;
		HeapId = _HeapId;
	}
	void Delete()
	{
		if (Ptr && Owner)
		{
			Ptr->SelfDelete(HeapId);
			Ptr = nullptr;
		}
	}
	T* GetNull() const
	{
		return Ptr;
	}
	T* Get() const
	{
#if ASSERT_ON_NULL_PTR
		FE_ASSERT(Ptr, "null ptr !");
#endif
		return Ptr; 
	}

private:
	T* Ptr;
};

