#pragma once

#include "common.hpp"
#include <tarray.hpp>
#include <string.hpp>

struct FePath
{
public:
	FePath();
	FePath(const FePath& other);
	FePath(const char* other);
	
	FeString Dir;
	FeString File;
	FeString Ext;
	
	void Clear();
	bool IsEmpty() const;
	bool IsDir() const;

	FePath& operator=(const FePath& other);
	FePath& operator=(const FeString& other);
	FePath& operator=(const char* str);

	bool operator==(const FePath& other) const;
	bool operator!=(const FePath& other) const;

	bool IsRemote() const;
	
	static void SetRoot(const char* szPath);
	static const char* GetRoot();

	void GoToParent();
	uint32 GetId() const;

private:
	static FeStaticString<FE_STRING_SIZE_128> Root;
};

struct FeDirectory
{
	void* Handle;
	FePath Path;
	FeTArray<FePath> Files;
	FeTArray<FeDirectory> Directories;

	void ListFiles(FeTArray<FePath>& output) const
	{
		for (auto& file : Files)
			output.Add(file);

		for (auto& dir : Directories)
			dir.ListFiles(output);
	}
};
