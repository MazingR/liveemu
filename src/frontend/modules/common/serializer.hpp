#pragma once 

#include "common.hpp"
#include "pointer.hpp"
#include "tarray.hpp"
#include "string.hpp"
#include "path.hpp"
#include "guid.hpp"

class FeAssetFile;

struct FeAssetRef
{
	FePath Path;
	FeString Type;
	FeAssetFile* Pointer;

	FeAssetRef() : Pointer(nullptr) {}
	FeAssetRef(const FePath& path, const FeString& type) : Path(path), Type(type), Pointer(nullptr) {}

	bool IsEmpty() const
	{
		return Path.IsEmpty() || Type.IsEmpty();
	}
	bool operator==(const FeAssetRef& other) const
	{
		return other.Path == Path && other.Type == Type;
	}
};


struct FeString;
struct FePath;

class FeSerializable;
class FeWidget;

struct FeEPropretyIndirection
{
	enum Type
	{
		Property,
		ArrayIndex,
	};
};

struct FeIndirection
{
	FeEPropretyIndirection::Type	Type;
	
	FeString						Name;
	uint32							Index;

	FeIndirection() : Type(FeEPropretyIndirection::Property), Index(FE_INVALID_ID){}
};

#define FE_PROPERTY_PATH_MAX_DEPTH 32

typedef int32(*fromStringFunc)(const FeString& str);

struct FePropertyPath
{
	FeIndirection Indirections[FE_PROPERTY_PATH_MAX_DEPTH];
	int32 CurrentDepth;

	const char* GetCurrentName()
	{
		return Indirections[CurrentDepth].Name.CstrNotNull();
	}
	FePropertyPath() : CurrentDepth(-1) {}
};
class FeSerializer
{
public:
	FeSerializer(uint32 iHeapId) : HeapId(iHeapId), bReadOnly(false) {}
	
	const FeTArray<FeIArray*> GetArraysPath()
	{
		return ArraysPath;
	}
	const FeString& GetCurrentPropertyName()
	{
		return Path.Indirections[Path.CurrentDepth].Name;
	}

	const FePropertyPath& GetPath()
	{
		return Path;
	}
	void Push(uint32 index)
	{
		static FeString EmptyString = "";

		Path.CurrentDepth++;
		FeIndirection& current = Path.Indirections[Path.CurrentDepth];

		current.Name = EmptyString;
		current.Index = index;
		current.Type = FeEPropretyIndirection::ArrayIndex;
	}
	void Push(const FeString& sName)
	{
		Path.CurrentDepth++;
		FeIndirection& current = Path.Indirections[Path.CurrentDepth];

		current.Name	= sName;
		current.Index	= FE_INVALID_ID;
		current.Type	= FeEPropretyIndirection::Property;
	}
	void Pop()
	{
		FE_ASSERT(Path.CurrentDepth >= 0, "incohrent depth");
		Path.CurrentDepth--;
	}

	#define DECLARE_SERIALIZER_FUNCTIONS(type)			\
	virtual uint32 Serialize(const type value) = 0;		\
	virtual uint32 Deserialize(type value) = 0;

	virtual uint32 DeserializeEnum(const FeString* values, int32 valuesCount, fromStringFunc fromString, int32* output)
	{
		FeString strValue;
		uint32 iRes = Deserialize(&strValue);
		if (!strValue.IsEmpty())
			*output = fromString(strValue);

		return iRes;
	}

	DECLARE_SERIALIZER_FUNCTIONS(bool*)
	DECLARE_SERIALIZER_FUNCTIONS(int*)
	DECLARE_SERIALIZER_FUNCTIONS(float*)
	DECLARE_SERIALIZER_FUNCTIONS(uint32*)
	DECLARE_SERIALIZER_FUNCTIONS(uint64*)
	DECLARE_SERIALIZER_FUNCTIONS(FePath*)
	DECLARE_SERIALIZER_FUNCTIONS(FeVector2*)
	DECLARE_SERIALIZER_FUNCTIONS(FeVector3*)
	DECLARE_SERIALIZER_FUNCTIONS(FeColor*)
	DECLARE_SERIALIZER_FUNCTIONS(FeTransform*)
	DECLARE_SERIALIZER_FUNCTIONS(FeString*)
	DECLARE_SERIALIZER_FUNCTIONS(FeSerializable*)
	DECLARE_SERIALIZER_FUNCTIONS(FeAssetRef*)
	DECLARE_SERIALIZER_FUNCTIONS(FeGuid*)

	virtual uint32 DeserializeArrayBegin(FeIArray* value) = 0;
	virtual uint32 DeserializeArrayEnd(FeIArray* value) = 0;
	
	virtual uint32 SerializeArray(const FeIArray* value) = 0;
	
	// --------------------------------------------------------------------------------------------------------------

	// Pointer
	virtual uint32 DeserializePointerType(const FeString& thisClassName, FeString& output)
	{
		const FeString Property_Type = "_serialize_type_";
		{
			Push(Property_Type);
			FE_FAILEDRETURN(Deserialize(&output));
			Pop();
		}
		return FeEReturnCode::Success;
	}

	template<typename T>
	uint32 Deserialize(FeTPtr<T>* value)
	{
		if (!bReadOnly)
		{
			FeString sType;
			DeserializePointerType(T::ClassName(), sType);

			T* pObj = (T*)GetObjectsFactory().CreateObjectFromFactory(sType, HeapId);
			value->Assign(pObj, HeapId, true/*Owner*/);
		}
		FE_FAILEDRETURN(Deserialize(value->Get()));

		return FeEReturnCode::Success;
	}

	// Traversal property
	template<typename T, FeETraversalOperation::Type Op, bool Propa>
	uint32 Deserialize(FeTraversalProperty<T, Op, Propa>* value)
	{
		FE_FAILEDRETURN(Deserialize(&value->Serialized));
		return FeEReturnCode::Success;
	}

	// Array
	template<typename T>
	uint32 Deserialize(FeTArray<T>* value)
	{
		if (value)
		{
			if (!bReadOnly)
			{
				value->Free();
				value->SetHeapId(HeapId);
			}
			ArraysPath.Add(value);
			DeserializeArrayBegin(value);

			uint32 index = 0;
			for (auto& entry : *value)
			{
				Push(index++);
				FE_FAILEDRETURN(Deserialize(&entry));
				Pop();
			}

			DeserializeArrayEnd(value);
			ArraysPath.PopBack();
		}
		
		return FeEReturnCode::Success;
	}

	// ------------------------------------------------------------------------------------------------

	// Pointer
	template<typename T>
	uint32 Serialize(const FeTPtr<T>* value)
	{
		return Serialize(value->GetNull());
	}

	// Traversal property
	template<typename T, FeETraversalOperation::Type Op, bool Propa>
	uint32 Serialize(const FeTraversalProperty<T, Op, Propa>* value)
	{
		FE_FAILEDRETURN(Serialize(&value->Serialized));
		return FeEReturnCode::Success;
	}

	// Array
	template<typename T>
	uint32 Serialize(const FeTArray<T>* value)
	{
		SerializeArray(value);

		uint32 index = 0;
		
		for (auto& entry : *value)
		{
			Push(index++);
			FE_FAILEDRETURN(Serialize(&entry));
			Pop();
		}

		return FeEReturnCode::Success;
	}
protected:
	uint32 HeapId;
	FePropertyPath Path;
	bool bReadOnly;
	FeTArray<FeIArray*> ArraysPath;
};
