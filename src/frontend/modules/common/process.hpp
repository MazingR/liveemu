#pragma once 

#include "common.hpp"
#include <path.hpp>

struct FeProcessInfo
{
	void* Process;
	void* Thread;
	uint32 ProcessId;
	uint32 ThreadId;
};

namespace FeEProcessCreationFlags
{
	enum Type
	{
		NoWindow	= 1 << 0,
		Inherit		= 1 << 1
	};
};

uint32 FeProcessCreate(FeProcessInfo& procInfo, const FePath& path, const char* params, uint32 iFlags=0);
bool FeProcessIsRunning(FeProcessInfo& procInfo);
uint32 FeProcessWait(FeProcessInfo& procInfo);
uint32 FeProcessStop(FeProcessInfo& procInfo);