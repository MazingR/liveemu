#include "common.hpp"

#include <maths.hpp>
#include <stdio.h>
#include <windows.h>
#include <SDL.h>

#include "string.hpp"

int __cdecl vs_printf(const char *format, ...)
{
	static char str[1024];

	va_list argptr;
	va_start(argptr, format);
	int ret = vsnprintf_s(str, sizeof(str), format, argptr);
	va_end(argptr);

	OutputDebugStringA(str);

	return ret;
}

char LastError[2048] = "";

void FeSetLastError(const char* fmt, ...)
{
	va_list argptr;
	va_start(argptr, fmt);
	vsprintf_s(LastError, fmt, argptr);
	va_end(argptr);
}
const char* FeGetLastError()
{
	return LastError;
}

bool FeConvert(int& output, const char* input)
{
	char* pEnd;
	output = strtol(input, &pEnd, 10);
	return errno != ERANGE;
}
bool FeConvert(uint32& output, const char* input)
{
	char* pEnd;
	output = strtoul(input, &pEnd, 10);
	return errno != ERANGE;
}
bool FeConvert(uint64& output, const char* input)
{
	char* pEnd;
	output = strtoull(input, &pEnd, 10);
	return errno != ERANGE;
}
bool FeConvert(bool& output, const char* input)
{
	char* pEnd;
	output = strtol(input, &pEnd, 10) == 1;
	return errno != ERANGE;
}
bool FeConvert(float& output, const char* input)
{
	char* pEnd;
	output = strtof(input, &pEnd);
	return errno != ERANGE;
}
bool FeConvert(FeString& output, const char* input)
{
	output = input;
	return true;
}

//FePath::FePath()
//{
//	Clear();
//}
//FePath::FePath(const FePath& other)
//{
//	*this = other;
//}
//void FePath::Clear()
//{
//	Value[0] = '\0';
//}
//FePath& FePath::operator=(const FePath& other)
//{
//	memcpy_s(Value, COMMON_PATH_SIZE, other.Value, COMMON_PATH_SIZE);
//	return *this;
//}
//FePath& FePath::operator=(const FeString& other)
//{
//	sprintf_s(Value, COMMON_PATH_SIZE, other.CstrNotNull());
//	return *this;
//}
//bool FePath::IsEmpty()
//{
//	return Value[0] == '\0';
//}

FeScopeLockedMutex::FeScopeLockedMutex(SDL_mutex* pMutex)
{
	Mutex = pMutex;
	FE_ASSERT(Mutex, "FeScopeLockedMutex null mutex");
	int32 iMutexLock = SDL_LockMutex(Mutex);
	FE_ASSERT(iMutexLock == 0, "Lock mutex failed !");
}
FeScopeLockedMutex::~FeScopeLockedMutex()
{
	int32 iMutexLock = SDL_UnlockMutex(Mutex);
	FE_ASSERT(iMutexLock == 0, "Lock mutex failed !");
}