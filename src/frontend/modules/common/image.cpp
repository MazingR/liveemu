#include "image.hpp"
#include "buffer.hpp"
#include "filesystem.hpp"

#define WIN32
#define OPJ_STATIC
#define FREEIMAGE_LIB
#define _CRT_SECURE_NO_DEPRECATE
#define LIBRAW_NODLL

#include <FreeImage.h>

bool FeImageHelper::IsInitialized = false;

void ConvertFormat(FREE_IMAGE_FORMAT& output, FeEImageFormat::Type input)
{
	switch (input)
	{
		#define DECLARE_CASE_CONVERT(_name_) case FeEImageFormat::_name_:	output = FIF_##_name_;	break;

		DECLARE_CASE_CONVERT(DDS)
		DECLARE_CASE_CONVERT(JPEG)
		DECLARE_CASE_CONVERT(PNG)
		DECLARE_CASE_CONVERT(TARGA)
		DECLARE_CASE_CONVERT(BMP)
		DECLARE_CASE_CONVERT(RAW)
	default:  
		output = FIF_UNKNOWN;
		break;
	}

}
void ConvertFormat(FeEImageFormat::Type& output, FREE_IMAGE_FORMAT input)
{
	switch (input)
	{
		#define DECLARE_CASE_CONVERT_INVERT(_name_) case FIF_##_name_:	output = FeEImageFormat::_name_;	break;

		DECLARE_CASE_CONVERT_INVERT(DDS)
		DECLARE_CASE_CONVERT_INVERT(JPEG)
		DECLARE_CASE_CONVERT_INVERT(PNG)
		DECLARE_CASE_CONVERT_INVERT(TARGA)
		DECLARE_CASE_CONVERT_INVERT(BMP)
		DECLARE_CASE_CONVERT_INVERT(RAW)

	default:
		output = FeEImageFormat::UNKNOWN;
		break;
	}
}

void FeFreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char *message)
{
	FE_ASSERT(false, message);
}

FeEImageFormat::Type FeImageHelper::GetImageFormatFromBuffer(const FeDynamicBuffer& buffer)
{
	FeEImageFormat::Type eFormat = FeEImageFormat::UNKNOWN;

	FeImageHelper::Initialize();

	// attach the binary data to a memory stream
	FIMEMORY *hmem = FreeImage_OpenMemory((unsigned char*)buffer.Data, buffer.Length);
	if (hmem)
	{
		// get the file type
		FREE_IMAGE_FORMAT fif = FreeImage_GetFileTypeFromMemory(hmem, 0);
		ConvertFormat(eFormat, fif);
		// always close the memory stream
		FreeImage_CloseMemory(hmem);
	}

	return eFormat;
}
uint32 FeImageHelper::SaveImageFromBufferToFile(const FeDynamicBuffer& buffer, const FePath& outputPath, FeEImageFormat::Type outputFormat /*= FeEImageFormat::DDS*/)
{
	FeImageHelper::Initialize();
	
	uint32 iRes = FeEReturnCode::Failed;

	// attach the binary data to a memory stream
	FIMEMORY *hmem = FreeImage_OpenMemory((unsigned char*)buffer.Data, buffer.Length);
	if (hmem)
	{
		// get the file type
		FREE_IMAGE_FORMAT fif = FreeImage_GetFileTypeFromMemory(hmem, 0);
		if (fif != FIF_UNKNOWN)
		{
			// load an image from the memory stream
			FIBITMAP *check = FreeImage_LoadFromMemory(fif, hmem, 0);

			if (check)
			{
				// save as a regular file
				FREE_IMAGE_FORMAT fmt;
				ConvertFormat(fmt, outputFormat);
				FeStaticString<FE_STRING_SIZE_128> fullpath;
				FeFileTools::FormatFullPath(outputPath, fullpath);

				int32 result = FreeImage_Save(fmt, check, fullpath.Value, 0);
				if (result==1)
					iRes = FeEReturnCode::Success;

				FreeImage_Unload(check);
			}
		}
		// always close the memory stream
		FreeImage_CloseMemory(hmem);
	}
	return iRes;

}
uint32 FeImageHelper::Initialize()
{
	if (IsInitialized)
		return FeEReturnCode::Success;

	FreeImage_Initialise();
	FreeImage_SetOutputMessage(FeFreeImageErrorHandler);

	IsInitialized = true;

	return FeEReturnCode::Success;
}
void FeImageHelper::DeInitialize()
{
	FreeImage_DeInitialise();
}