#pragma once 

#include "common.hpp"

struct FeGuid
{
	uint32	Data1;
	uint16	Data2;
	uint16	Data3;
	uint32	Data4;
	uint32	Data5;

	FeGuid();
	bool IsValid() const;
	bool operator==(const FeGuid& other) const;
	bool operator!=(const FeGuid& other) const;
	static void Create(FeGuid& output);
	void ToString(FeString& output) const;
	void FromString(const FeString& input);
};
