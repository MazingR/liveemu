#pragma once 

#include "common.hpp"
#include "path.hpp"
class FeSerializable;

namespace FeJson
{
	//uint32 Serialize(FeSerializable* value, const FePath& path);
	//uint32 Deserialize(FeSerializable* output, const FePath& path, uint32 iHeapId);
	uint32 SerializeToMemory(const FeSerializable* value, FeStringBuffer& output, uint32 iHeap);
	uint32 Serialize(const FeSerializable* value, const FePath& path);

	uint32 Deserialize(FeSerializable* output, const FePath& path, uint32 iHeapId);
	uint32 Deserialize(FeSerializable* output, const char* str, uint32 iHeapId);
	
	template<class T>
	uint32 Deserialize(T& output, const FePath& path, uint32 iHeapId)
	{
		return Deserialize(&output, path, iHeapId);
	}

	//template<class T>
	//uint32 Deserialize(T& output, const FePath& path, uint32 iHeapId)
	//{
	//	return Deserialize(&output, path, iHeapId);
	//}

	template<class T>
	uint32 Deserialize(T& output, const char* str, uint32 iHeapId)
	{
		return Deserialize(&output, str, iHeapId);
	}
}

