#pragma once 

#include "common.hpp"
#include <string.hpp>

struct SDL_mutex;
struct SDL_Thread;

class FeTask : public FeISelfDeletable
{
public:
	FeString Name;

	virtual uint32 Execute() = 0;
};

template <typename... Ts>
class FeTaskT : public FeTask
{
private:
	template <int... Is>
	struct index {};

	template <int N, int... Is>
	struct gen_seq : gen_seq<N - 1, N - 1, Is...> {};

	template <int... Is>
	struct gen_seq<0, Is...> : index<Is...>{};

	std::function<uint32(Ts...)> f;
	std::tuple<Ts...> args;
public:
	template <typename F, typename... Args>
	FeTaskT(const FeString& name, F&& func, Args&&... args)
		: f(std::forward<F>(func)),
		args(std::forward<Args>(args)...)
	{
		Name = name;
	}
	uint32 Execute()
	{
		return func(args);
	}

	void SelfDelete(uint32 iHeapId)
	{
		FE_DELETE(FeTaskT<Ts...>, this, iHeapId);
	}
private:
	template <typename... Args, int... Is>
	uint32 func(std::tuple<Args...>& tup, index<Is...>)
	{
		return f(std::get<Is>(tup)...);
	}

	template <typename... Args>
	uint32 func(std::tuple<Args...>& tup)
	{
		return func(tup, gen_seq<sizeof...(Args)>{});
	}
};

class FeTaskQeue
{
public:
	FeTaskQeue(const FeString& name);
	~FeTaskQeue();

	template <typename F, typename... Args>
	FeTaskT<Args...>* CreateTask(const FeString& name, F f, Args... args)
	{
		auto newTask = FE_NEW(FeTaskT<Args...>, DEFAULT_HEAP, name, std::forward<F>(f), std::forward<Args>(args)...);
		Lock();
			Queue.Add(newTask);
		Unlock();

		return newTask;
	}
	bool Lock();
	void Unlock();
	void Stop(bool bWait=true);
	void Start();
	void Pause();
	int32 ThreadRun();
	FeTask* PopTask();

	FeString Name;
private:
	FeTArray<FeTask*> Queue;
	SDL_mutex* Mutex;
	SDL_Thread* Thread;
	uint32 bIsShuttingDown;
	uint32 Paused;
};
class FeTaskScheduler
{
public:
	FeTaskScheduler() : bIsShuttingDown(false) {}
	~FeTaskScheduler();
	FeTaskQeue* CreateQueue(const FeString& name);
	
	bool IsShuttingDown();
	void Stop();

	FeTArray<FeTaskQeue*> Queues;
private:
	bool bIsShuttingDown;
};
