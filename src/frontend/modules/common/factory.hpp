#pragma once 

#include "common.hpp"
#include <map>

class FeSerializable;
struct FeString;

typedef FeSerializable*(*FeCreateObjectFunc) (uint32 iHeapId);

struct FeFactory
{
	char				TypeName[FE_STRING_SIZE_64];
	FeCreateObjectFunc	CreateFunc;
};

class FeCObjectsFactory
{
public:
	void RegisterFactory(const char* sTypeName, FeCreateObjectFunc createFunc);
	FeSerializable* CreateObjectFromFactory(const char* sTypeName, uint32 iHeapId);
	FeSerializable* CreateObjectFromFactory(const FeString& sTypeName, uint32 iHeapId);

	typedef std::map<uint32, FeFactory> FactoriesMap;
	typedef FactoriesMap::iterator FactoriesMapIt;

	template<typename T>
	const char* GetTypeName()
	{
		return typeid(T).name()+6; // class Name
	}

	template<typename T>
	void CreateFactory(FeCreateObjectFunc func)
	{
		const char* sTypeName = GetTypeName<T>();
		uint32 iTypeHash = FeStringTools::GenerateUIntIdFromString(sTypeName);

		if (Factories.find(iTypeHash) == Factories.end())
		{
			FeFactory newFactory;
			newFactory.CreateFunc = func;
			sprintf_s(newFactory.TypeName, sTypeName);

			Factories[iTypeHash] = newFactory;
		}
	}
	
	template<class T>
	const FeFactory& GetFactory()
	{
		static uint32 iTypeHash = typeid(T).hash_code();
		return Factories[iTypeHash];
	}
private:
	FactoriesMap Factories;

};

FeCObjectsFactory& GetObjectsFactory();

template<typename T>
struct FeTFactory
{
	static FeSerializable* CreateInstance(uint32 iHeapId) { return FE_NEW(T, iHeapId); }

	FeTFactory()
	{
		GetObjectsFactory().CreateFactory<T>(&FeTFactory<T>::CreateInstance);
	}
};
