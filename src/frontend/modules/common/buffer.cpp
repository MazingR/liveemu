#include "buffer.hpp"

FeDynamicBuffer::FeDynamicBuffer(uint32 size, uint32 heap)
{
	Length = 0;
	Data = nullptr;
	Allocate(size, heap);
}
FeDynamicBuffer::~FeDynamicBuffer()
{
	Free();
}
void FeDynamicBuffer::Free()
{
	Length = 0;

	if (Data)
	{
		FE_FREE(Data, Heap);
		Data = nullptr;
		Size = 0;
	}
}
void FeDynamicBuffer::Clear()
{
	Length = 0;
	Memset(0);
}
void FeDynamicBuffer::Memset(char value)
{
	if (Data)
		memset(Data, value, Size);
}
void FeDynamicBuffer::Allocate(uint32 size, uint32 heap)
{
	const uint32 pageSize = 16;

	void* previousData = Data;
	uint32 previousSize = Size;
	uint32 previousHeap = Heap;

	Size = size;
	
	if (size && (size%pageSize))
	{
		Size += pageSize;
		Size -= Size%pageSize;
	}

	Heap = heap;

	if (Heap != FE_INVALID_ID)
	{
		if (Size > 0)
			Data = FE_ALLOCATE(Size, Heap);
		else
			Data = nullptr;

		// copy previous buffer
		if (Data && previousSize > 0)
			memcpy_s(Data, Size, previousData, previousSize);
	}

	if (previousData)
		FE_FREE(previousData, previousHeap);
}
uint32 FeDynamicBuffer::Write(const void* input, uint32 bytesCount)
{
	uint32 iWritten = bytesCount;

	if (bytesCount > 0)
	{
		FE_ASSERT(Size > Length, "buffer too small!");

		uint32 capacity = Size - Length -1;

		// Resize buffer if too small
		if (capacity < bytesCount)
		{
			uint32 iDesiredSize = Size;
			while (iDesiredSize < (bytesCount + Size))
				iDesiredSize *= 2;

			Allocate(iDesiredSize, Heap);
			capacity = Size - Length;
		}

		char* output = (char*)Data;

		memcpy_s(output + Length, capacity, input, iWritten);
		Length += iWritten;
	}
	return iWritten;
}