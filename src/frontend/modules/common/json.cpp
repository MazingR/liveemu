#pragma once 

#include <json.hpp>
#include <serializable.hpp>

#include "filesystem.hpp"
#include "string.hpp"

#define FE_HEAPID_JSONPARSER 2
#define FE_HEAPNAME_JSONPARSER "JsonParser"

#if HOOK_MALLOC && !defined(FE_NO_CUSTOM_ALLOCATOR)

#define RAPIDJSON_NEW(x) FeNew(x, FE_HEAPID_JSONPARSER)
#define RAPIDJSON_DELETE(x) FeDelete(x, FE_HEAPID_JSONPARSER)

#undef malloc
#undef calloc
#undef realloc
#undef free

#include <malloc.h>
#undef malloc
#undef calloc
#undef realloc
#undef free

#define malloc(size)		std_FeMallocHook(size, FE_HEAPID_JSONPARSER)
#define calloc(nmemb, size)	std_FeCallocHook(nmemb, size, FE_HEAPID_JSONPARSER)
#define realloc(ptr, size)	std_FeReallocHook(ptr, size, FE_HEAPID_JSONPARSER,1)
#define free(ptr)			std_FeFreeHook(ptr, FE_HEAPID_JSONPARSER)

#pragma warning(disable: 4244)
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include "rapidjson/filewritestream.h"

#undef malloc
#undef calloc
#undef realloc
#undef free

#define malloc(size)		FeMallocHook(size, FE_HEAPID_JSONPARSER)
#define calloc(nmemb, size)	FeCallocHook(nmemb, size, FE_HEAPID_JSONPARSER)
#define realloc(ptr, size)	FeReallocHook(ptr, size, FE_HEAPID_JSONPARSER, 1)
#define free(ptr)			FeFreeHook(ptr, FE_HEAPID_JSONPARSER)

#else
#pragma warning(disable: 4244)
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include "rapidjson/filewritestream.h"
#endif

typedef rapidjson::Value FeJsonValue;

namespace FeJson
{
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FeString* pInput);

	template<typename T>
	void CreateObject(FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const T* pInput)
	{
	}
	void CreateObject(FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FeSerializable* pInput)
	{
		if (!jsonValue->IsObject())
			jsonValue->SetObject();
	}
	void CreateObject(FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FeIArray* pInput)
	{
		jsonValue->SetArray();

		for (uint32 i = jsonValue->Size(); i < pInput->GetSize(); ++i)
		{
			FeJsonValue dummyValue;
			jsonValue->PushBack(dummyValue, allocator);
		}
	}
	FeJsonValue* FetchProperty(FeJsonValue* jsonValue, const char* sPropertyName, uint32 index, rapidjson::Document::AllocatorType* pAllocator=nullptr)
	{
		FeJsonValue* result = jsonValue;
		
		FE_ASSERT(sPropertyName != nullptr, "null property name!");

		// fetch array element by index
		if (index != FE_INVALID_ID)
		{
			FE_ASSERT(result->IsArray(), "incoherent property index!");
			result = &((*result)[index]);
		}
		// fetch property by name
		else if (strlen(sPropertyName))
		{
			if (pAllocator)
			{
				if (!jsonValue->IsObject())
					jsonValue->SetObject();

				if (!jsonValue->HasMember(sPropertyName))
				{
					FeJsonValue memberValue;
					FeJsonValue::StringRefType strName(sPropertyName);
					result->AddMember(strName, memberValue, *pAllocator);
				}
			}

			if (jsonValue->HasMember(sPropertyName))
				result = &((*result)[sPropertyName]);
			else
				result = nullptr;
		}

		return result;
	}
	template<typename T>
	bool FetchPropertyT(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue** pProperty, const T* pValue, rapidjson::Document::AllocatorType* pAllocator = nullptr)
	{
		FeJsonValue* result = jsonValue;
		const FePropertyPath& path = serializer->GetPath();
		
		for (int32 iDepth = 0; iDepth < path.CurrentDepth+1; ++iDepth)
		{
			const FeIndirection& indirection = path.Indirections[iDepth];
			result = FetchProperty(result, indirection.Name.CstrNotNull(), indirection.Index, pAllocator);

			if (!result)
				break;
		}
		*pProperty = result;

		if (pValue && pAllocator)
			CreateObject(result, *pAllocator, pValue);
		
		return result!=nullptr;
	}
	bool FetchProperty(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue** pProperty)
	{
		return FetchPropertyT(serializer, jsonValue, pProperty, (void*)nullptr, nullptr);
	}

	template<typename T>
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, T* pOutput)
	{
		FE_ASSERT_NOT_IMPLEMENTED;
	}

	template<typename T>
	uint32 JsonDeserialize(FeSerializer* serializer, FeJsonValue* jsonValue, T* pOutput)
	{
		FeJsonValue* jsonProperty;

		if (FetchProperty(serializer, jsonValue, &jsonProperty) && !jsonProperty->IsNull())
			JsonDeserializeValue(serializer, jsonProperty, pOutput);

		return FeEReturnCode::Success;
	}

	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, bool* pOutput)
	{
		*pOutput = jsonProperty->GetBool();
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, int* pOutput)
	{
		*pOutput = jsonProperty->GetInt();
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, uint32* pOutput)
	{
		*pOutput = jsonProperty->GetUint();
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, uint64* pOutput)
	{
		*pOutput = jsonProperty->GetUint64();
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, float* pOutput)
	{
		*pOutput = (float)jsonProperty->GetDouble();
	}
	
	template<typename T>
	void JsonDeserializeValueVector(FeJsonValue* jsonProperty, T* pOutput, int32 dimensions)
	{
		for (int32 i = 0; i < dimensions; ++i)
			pOutput->mData[i] = (float)(*jsonProperty)[i].GetDouble();
	}
	
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, FeVector3* pOutput)
	{
		JsonDeserializeValueVector(jsonProperty, pOutput, 3);
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, FeVector2* pOutput)
	{
		JsonDeserializeValueVector(jsonProperty, pOutput, 2);
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, FeColor* pOutput)
	{
		JsonDeserializeValueVector(jsonProperty, pOutput, 4);
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, FeTransform* pOutput)
	{
		if (jsonProperty->HasMember("T"))	JsonDeserializeValueVector(&((*jsonProperty)["T"]), &pOutput->Translation, 3);
		if (jsonProperty->HasMember("S"))	JsonDeserializeValueVector(&((*jsonProperty)["S"]), &pOutput->Scale, 3);

		if (jsonProperty->HasMember("R"))
		{
			pOutput->Rotation = gmtl::EulerAngleXYZf(
				(float)(*jsonProperty)["R"][0].GetDouble(),
				(float)(*jsonProperty)["R"][1].GetDouble(),
				(float)(*jsonProperty)["R"][2].GetDouble());
		}
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, FeString* pOutput)
	{
		*pOutput = FeStringPool::GetInstance()->CreateString(jsonProperty->GetString());
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, FePath* pOutput)
	{
		if (jsonProperty->HasMember("Dir"))
			JsonDeserializeValue(serializer, &(*jsonProperty)["Dir"], &pOutput->Dir);
		if (jsonProperty->HasMember("File"))
			JsonDeserializeValue(serializer, &(*jsonProperty)["File"], &pOutput->File);
		if (jsonProperty->HasMember("Ext"))
			JsonDeserializeValue(serializer, &(*jsonProperty)["Ext"], &pOutput->Ext);
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, FeAssetRef* pOutput)
	{
		if (jsonProperty->HasMember("Path"))
			JsonDeserializeValue(serializer, &(*jsonProperty)["Path"], &pOutput->Path);
		if (jsonProperty->HasMember("Type"))
			JsonDeserializeValue(serializer, &(*jsonProperty)["Type"], &pOutput->Type);
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, FeSerializable* pOutput)
	{
		pOutput->Deserialize(serializer);
	}
	void JsonDeserializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, FeGuid* pOutput)
	{
		FeString strValue = FeStringPool::GetInstance()->CreateString(jsonProperty->GetString());
		pOutput->FromString(strValue);
	}

	template<typename T>
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonProperty, const T* pInput)
	{
		FE_ASSERT_NOT_IMPLEMENTED;
	}

	template<typename T>
	uint32 JsonSerialize(FeSerializer* serializer, FeJsonValue* jsonParentValue, FeJsonValue::AllocatorType& allocator, const T* pInput)
	{
		FeJsonValue* jsonValue;
		
		if (FetchPropertyT(serializer, jsonParentValue, &jsonValue, pInput, &allocator))
		{
			JsonSerializeValue(serializer, jsonValue, allocator, pInput);
		}

		return FeEReturnCode::Success;
	}
	
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const bool* pInput)
	{
		jsonValue->SetBool(*pInput);
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const int* pInput)
	{
		jsonValue->SetInt(*pInput);
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const uint32* pInput)
	{
		jsonValue->SetUint(*pInput);
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const uint64* pInput)
	{
		jsonValue->SetUint64(*pInput);
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const float* pInput)
	{
		jsonValue->SetDouble((double)(*pInput));
	}
	
	template<typename T>
	void JsonSerializeValueVector(FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const T* pInput, int32 dimensions)
	{
		FeJsonValue& array = jsonValue->SetArray();

		for (int32 i = 0; i < dimensions; ++i)
			array.PushBack(pInput->getData()[i], allocator);
	}

	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FeVector3* pInput)
	{
		JsonSerializeValueVector(jsonValue, allocator, pInput, 3);
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FeVector2* pInput)
	{
		JsonSerializeValueVector(jsonValue, allocator, pInput, 2);
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FeColor* pInput)
	{
		JsonSerializeValueVector(jsonValue, allocator, pInput, 4);
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FePath* pInput)
	{
		FeJsonValue dir, file, ext;

		JsonSerializeValue(serializer, &dir, allocator, &pInput->Dir);
		JsonSerializeValue(serializer, &file, allocator, &pInput->File);
		JsonSerializeValue(serializer, &ext, allocator, &pInput->Ext);

		jsonValue->SetObject();
		jsonValue->AddMember("Dir", dir, allocator);
		jsonValue->AddMember("File", file, allocator);
		jsonValue->AddMember("Ext", ext, allocator);
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FeTransform* pInput)
	{
		FeJsonValue valueT, valueR, valueS;

		JsonSerializeValueVector(&valueT, allocator, &pInput->Translation, 3);
		JsonSerializeValueVector(&valueR, allocator, &pInput->Rotation, 3);
		JsonSerializeValueVector(&valueS, allocator, &pInput->Scale, 3);

		jsonValue->SetObject();
		jsonValue->AddMember("T", valueT, allocator);
		jsonValue->AddMember("R", valueR, allocator);
		jsonValue->AddMember("S", valueS, allocator);
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FeString* pInput)
	{
		jsonValue->SetString(pInput->CstrNotNull(), pInput->Length());
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FeAssetRef* pInput)
	{
		FeJsonValue path, type;

		JsonSerializeValue(serializer, &path, allocator, &pInput->Path);
		JsonSerializeValue(serializer, &type, allocator, &pInput->Type);
		
		jsonValue->SetObject();
		jsonValue->AddMember("Path", path, allocator);
		jsonValue->AddMember("Type", type, allocator);
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FeSerializable* pInput)
	{
		FeJsonValue typeName;
		JsonSerializeValue(serializer, &typeName, allocator, &pInput->GetThisClassName());
		jsonValue->AddMember("_serialize_type_", typeName, allocator);

		pInput->Serialize(serializer);
	}
	void JsonSerializeValue(FeSerializer* serializer, FeJsonValue* jsonValue, FeJsonValue::AllocatorType& allocator, const FeGuid* pInput)
	{
		FeString strValue;
		pInput->ToString(strValue);
		jsonValue->SetString(strValue.CstrNotNull(), strValue.Length());
	}

	class FeJsonSeserializer : public FeSerializer
	{
	public:
		FeJsonSeserializer(rapidjson::Document* jsonDocument, uint32 iHeapId) : FeSerializer(iHeapId), JsonDoc(jsonDocument)  {}

		#define DECLARE_JSON_SERIALIZER_FUNCTIONS(type)											\
		uint32 Serialize(const type value)	{ return JsonSerialize(this, JsonDoc, JsonDoc->GetAllocator(), value); }	\
		uint32 Deserialize(type value)		{ return JsonDeserialize(this, JsonDoc, value); }
		
		DECLARE_JSON_SERIALIZER_FUNCTIONS(bool*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(int*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(float*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(uint32*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(uint64*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(FePath*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(FeVector2*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(FeVector3*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(FeColor*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(FeTransform*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(FeString*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(FeAssetRef*)
		DECLARE_JSON_SERIALIZER_FUNCTIONS(FeGuid*)

		uint32 Serialize(const FeSerializable* value)
		{ 
			return JsonSerialize(this, JsonDoc, JsonDoc->GetAllocator(), value); 
		}
		uint32 Deserialize(FeSerializable* value)
		{
			uint32 result = JsonDeserialize(this, JsonDoc, value);
			value->OnDeserialized();
			return result;
		}

		// ------------------------------------------------------------------------------------------------------------------------------------------------

		uint32 SerializeArray(const FeIArray* value)
		{
			FeJsonValue* jsonArray;

			if (FetchPropertyT(this, JsonDoc, &jsonArray, value, &JsonDoc->GetAllocator()))
			{
				CreateObject(jsonArray, JsonDoc->GetAllocator(), value);
				return FeEReturnCode::Success;
			}
			return FeEReturnCode::Failed;
		}
		// ------------------------------------------------------------------------------------------------------------------------------------------------

		// Array
		uint32 DeserializeArrayBegin(FeIArray* value)
		{
			FeJsonValue* jsonProperty;

			if (FetchProperty(this, JsonDoc, &jsonProperty) && jsonProperty->IsArray())
			{
				size_t iSize = jsonProperty->Size();
				value->Resize(iSize);

				return FeEReturnCode::Success;
			}
			return FeEReturnCode::Failed;
		}
		uint32 DeserializeArrayEnd(FeIArray* value)
		{
			return 0;
		}
	
		rapidjson::Document* JsonDoc;
	};
	
	uint32 SerializeToMemory(const FeSerializable* input, FeStringBuffer& output, uint32 iHeap)
	{
		rapidjson::Document jsonDocument;
		jsonDocument.SetObject();

		FeJsonSeserializer jsonSerializer(&jsonDocument, FE_HEAPID_JSONPARSER);
		uint32 iRes = input->Serialize(&jsonSerializer);

		if (iRes == FeEReturnCode::Success)
		{
			FeDynamicBuffer writeBuffer;
			writeBuffer.Allocate(65535, FE_HEAPID_JSONPARSER);

			rapidjson::StringBuffer buffer;
			rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
			jsonDocument.Accept(writer);

			const char* strWritten = buffer.GetString();
			output.Allocate(strlen(strWritten) + 1, iHeap);
			output.Clear();
			output.Concat(strWritten);
		}
		return iRes;
	}
	uint32 Serialize(const FeSerializable* input, const FePath& path)
	{
		void* fp = FeFileTools::OpenFile(path);

		if (fp)
		{
			rapidjson::Document jsonDocument;
			jsonDocument.SetObject();

			FeJsonSeserializer jsonSerializer(&jsonDocument, FE_HEAPID_JSONPARSER);
			uint32 iRes = input->Serialize(&jsonSerializer);

			if (iRes == FeEReturnCode::Success)
			{
				FeDynamicBuffer writeBuffer;
				writeBuffer.Allocate(65535, FE_HEAPID_JSONPARSER);

				rapidjson::FileWriteStream os((FILE*)fp, (char*)writeBuffer.Data, writeBuffer.Size);
				rapidjson::Writer<rapidjson::FileWriteStream> writer(os);
				jsonDocument.Accept(writer);

				FeFileTools::CloseFile(fp);
			}
			return iRes;
		}
		else
		{
			return FeEReturnCode::Failed;
		}
	}
	uint32 Deserialize(FeSerializable* output, const char* str, uint32 iHeapId)
	{
		rapidjson::Document jsonDocument;
		rapidjson::ParseResult result = jsonDocument.Parse(str);

		if (result.IsError())
		{
			size_t iErrorOffset = result.Offset();
			size_t iTextLen = strlen(str);
			size_t iLineEnd = FeStringTools::IndexOf(str, '\n', iErrorOffset);

			if ((size_t)-1 == iLineEnd)
				iLineEnd = iTextLen;

			size_t iLineStart = FeStringTools::LastIndexOf(str, '\n', 0, iErrorOffset) + 1;
			size_t iLineCount = FeStringTools::Count(str, '\n', 0, iLineStart) + 1;

			size_t iLineLen = iLineEnd>iLineStart+1 ? iLineEnd - iLineStart - 1 : 0;
			if (iLineLen)
			{
				char* szLine = FE_NEW_ARRAYD(char, iLineLen + 1);
				if (iLineLen > 0 && (iLineStart + iLineLen) < iTextLen)
				{
					memcpy_s(szLine, iLineLen, str + iLineStart, iLineLen);
					szLine[iLineLen] = '\0';
				}
				FE_ASSERT(false, "Deserialize failed : at line %u\n===> '%s'", iLineCount, szLine);

				FE_DELETE_ARRAY(char, szLine, iLineLen, DEFAULT_HEAP);
			}
			return FeEReturnCode::Failed;
		}


		FeJsonSeserializer jsonSerializer(&jsonDocument, iHeapId);

		return output->Deserialize(&jsonSerializer);
	}
	//uint32 Deserialize(FeSerializable* output, const FePath& path, uint32 iHeapId)
	//{
	//	char* str;
	//	size_t iFileSize;

	//	auto iResult = FeFileTools::ReadTextFile(path, &str, &iFileSize);
	//	if (iResult != FeEReturnCode::Success)
	//		return iResult;

	//	FeStringTools::Remove(str, "\\\r\n");

	//	uint32 iRes = Deserialize(output, str, iHeapId);
	//	FE_FREE(str, FE_HEAPID_FILESYSTEM);

	//	return iRes;
	//}
	uint32 Deserialize(FeSerializable* output, const FePath& path, uint32 iHeapId)
	{
		char* str;
		size_t iFileSize;

		auto iResult = FeFileTools::ReadTextFile(path, &str, &iFileSize);
		if (iResult != FeEReturnCode::Success)
			return iResult;

		FeStringTools::Remove(str, "\\\r\n");

		uint32 iRes = Deserialize(output, str, iHeapId);
		FE_FREE(str, FE_HEAPID_FILESYSTEM);

		return iRes;
	}
}