#pragma once 

#include "common.hpp"
#include <string.hpp>

struct FeTypeInfo
{
public:
	static FeTypeInfo* FromName(const FeString& typeName)
	{
		if (!s_bIsInitialized)
			Initialize();

		uint32 typeId = typeName.Id();
		for (auto& typeInfo : s_typeInfos)
		{
			if (typeInfo.TypeId == typeId)
			{
				return &typeInfo;
			}
		}
		return nullptr;
	}

	uint32 TypeId;
	uint32 TypeSize;
	bool IsMemoryContiguous;
private:
	static bool s_bIsInitialized;
	static FeTArray<FeTypeInfo> s_typeInfos;

	static void Initialize()
	{
#define ADD_TYPE_TO_MAP(_typeName)					\
				{												\
				uint32 typeId = FeString(#_typeName).Id();	\
				FeTypeInfo& typeInfo = s_typeInfos.Add();	\
				typeInfo.TypeId = typeId;					\
				typeInfo.TypeSize = sizeof(_typeName);		\
				typeInfo.IsMemoryContiguous = true;			\
				}
		ADD_TYPE_TO_MAP(int32);
		ADD_TYPE_TO_MAP(uint32);
		ADD_TYPE_TO_MAP(bool);
		ADD_TYPE_TO_MAP(float);
	}
};