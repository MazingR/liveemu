#pragma once 

#include "common.hpp"
#include <pointer.hpp>


//  /!\ When resizing an array,  we need to transfer ownership of smart pointers /!\

template<typename T>
void TransportDataElement(T& to, T& from)
{
	to = from;
}
template<typename T>
void TransportDataElement(FeTPtr<T>& to, FeTPtr<T>& from)
{
	// transfer ownership
	if (from.Owner)
	{
		to.Assign(from.GetNull(), from.HeapId, true);
		from.Owner = false;
	}
	else
	{
		to = from;
	}
}

template<typename T>
void OnAddEntry(T& entry)
{
}
template<typename T>
void OnAddEntry(FeTPtr<T>& entry)
{
	entry.New();
}

class FeIReleasable
{
public:
	virtual void Release() = 0;

};

class FeIArray
{
public:
	virtual void Clear() = 0;
	virtual void SetHeapId(THeapId iHeapId) = 0;
	virtual void Reserve(uint32 size) = 0;
	virtual void Resize(uint32 size) = 0;
	virtual void RemoveEntry(uint32 size) = 0;
	virtual void* AddEntry() = 0;
	virtual uint32 GetSize() const = 0;
};
template <class T>
class FeTArrayBase : public FeIArray
{
public:
	typedef T*		Iterator;
	Iterator			Begin()			{ return BaseAdress; }
	const Iterator		Begin() const	{ return BaseAdress; }
	
	Iterator			End()			{ return Begin() + GetSize(); }
	const Iterator		End() const		{ return Begin() + GetSize(); }
	
	Iterator			begin()			{ return Begin(); }
	const Iterator		begin() const	{ return Begin(); }

	Iterator			end()			{ return End(); }
	const Iterator		end() const		{ return End(); }

	bool 			IsEmpty()	const 		{ return (Size == 0); }
	uint32			GetSize()	const		{ return Size; }

protected:
	virtual void DestroyElements(uint32 size) = 0;

public:

	FeTArrayBase(uint32 _iSize = 0, THeapId iHeapId = DEFAULT_HEAP)
		: BaseAdress(nullptr)
		, Size(0)
		, EffectiveSize(0)
		, HeapId(iHeapId)
	{
		CanExpand = true;
		if (_iSize>0)
			Reserve(_iSize);
	}
	FeTArrayBase(const FeTArrayBase& _copy, THeapId iHeapId = DEFAULT_HEAP)
		: BaseAdress(nullptr)
		, HeapId(iHeapId)
	{
		CanExpand = true;
		Size = _copy.Size;
		EffectiveSize = Size;
		BaseAdress = FE_NEW_ARRAY(T, Size, HeapId);

		for (uint32 i = 0; i < Size; i++)
		{
			BaseAdress[i] = _copy.BaseAdress[i];
		}
	}
	~FeTArrayBase()
	{
		Free();
	}

	const T&	operator[](uint32 i) const
	{
		return GetAt(i);
	}
	T&			operator[](uint32 i)
	{
		return GetAt(i);
	}
	bool		operator !=(const FeTArrayBase& _array) const
	{
		return !(*this == _array);
	}
	bool		operator ==(const FeTArrayBase& _array) const
	{
		uint32 s = GetSize();
		if (_array.GetSize() != s)
			return false;

		for (uint32 i = 0; i < s; ++i)
		{
			if (_array[i] != (*this)[i])
				return false;
		}

		return true;
	}

	FeTArrayBase&	operator =(const FeTArrayBase& _copy)
	{
		uint32 previousSize = Size;
		Size = _copy.Size;
		if (Size>EffectiveSize)
		{
			if (BaseAdress != nullptr)
			{
				DestroyElements(previousSize);
				FE_DELETE_ARRAY(T, BaseAdress, EffectiveSize, HeapId);
			}
			EffectiveSize = Size;
			BaseAdress = FE_NEW_ARRAY(T, EffectiveSize, HeapId);
		}
		if (Size>0)
		{
			for (uint32 i = 0; i < Size; i++)
			{
				BaseAdress[i] = _copy.BaseAdress[i];
			}
		}
		return *this;
	}
	const T&	GetAt(uint32 i) const
	{
		return BaseAdress[i];
	}
	T&			GetAt(uint32 i)
	{
		return BaseAdress[i];
	}
	void		Reserve(uint32 _iSize)
	{
		if (_iSize>EffectiveSize)
		{
			T* pNewBase = FE_NEW_ARRAY(T, _iSize, HeapId);
			if (Size>0)
			{
				for (uint32 i = 0; i < (_iSize<Size ? _iSize : Size); i++)
				{
					TransportDataElement(pNewBase[i], BaseAdress[i]);
				}
			}
			if (BaseAdress != nullptr)
			{
				DestroyElements(Size);
				FE_DELETE_ARRAY(T, BaseAdress, EffectiveSize, HeapId);
			}
			BaseAdress = pNewBase;
			EffectiveSize = _iSize;
		}
	}
	void		ReserveZeroMemory(uint32 _iSize)
	{
		Reserve(_iSize);
		memset(BaseAdress, 0, sizeof(T)*_iSize);
	}
	void		Resize(uint32 _iSize)
	{
		T* pNewBase = nullptr;
			
		if (_iSize>0)
			pNewBase = FE_NEW_ARRAY(T, _iSize, HeapId);

		if (_iSize>0 && Size>0)
		{
			uint32 size = _iSize > Size ? _iSize : Size;
			for (uint32 i = 0; i < size; i++)
			{
				TransportDataElement(pNewBase[i], BaseAdress[i]);
			}
		}
		if (BaseAdress != nullptr)
		{
			DestroyElements(Size);
			FE_DELETE_ARRAY(T, BaseAdress, EffectiveSize, HeapId);
		}
		BaseAdress = pNewBase;
		EffectiveSize = _iSize;
		Size = _iSize;
	}
	void		Resize(uint32 _iSize, const T& _kDefaultValue)
	{
		T* pNewBase = nullptr;
		if (_iSize>0)
			pNewBase = FE_NEW_ARRAY(T, _iSize, HeapId);
		if (_iSize>0 && Size>0)
		{
			for (uint32 i = 0; i < Math::Min(_iSize, Size); i++)
			{
				TransportDataElement(pNewBase[i], BaseAdress[i]);
			}
		}
		for (uint32 i = Size; i < _iSize; i++)
		{
			pNewBase[i] = _kDefaultValue;
		}
		if (BaseAdress != nullptr)
		{
			DestroyElements(Size);
			FE_DELETE_ARRAY(T, BaseAdress, EffectiveSize, HeapId);
		}
		BaseAdress = pNewBase;
		EffectiveSize = _iSize;
		Size = _iSize;
	}
	void RemoveEntry(uint32 index)
	{
		RemoveAt(index);
	}
	void* AddEntry()
	{
		Add();
		OnAddEntry(Back());

		return &Back();
	}
	T&			Add()
	{
		const uint32 uiMaxValue = uint32(~uint32(0));
		const uint32 uiUpperLimitMulByTwo = (uiMaxValue >> 1) + 1;
			
		FE_ASSERT(Size<uiMaxValue, "FeTArrayBase capacity is full already");

		uint32 iPreviousEffectiveSize = EffectiveSize;
		uint32 iPreviousSize = Size;

		Size++;

		if (Size > EffectiveSize)
		{
			FE_ASSERT(CanExpand, "can't resize this array !!");

			uint32 oldsize = EffectiveSize;

			// uint32 type must be unsigned !
			if (EffectiveSize >= uiUpperLimitMulByTwo)
			{
				if (EffectiveSize < uiMaxValue)
				{
					// more than half capacity is allocated and filled with data. We allocate all that's left. 
					EffectiveSize = uiMaxValue;
				}
				else
				{
					// full capacity is already allocated and filled with data !
					FE_ASSERT(false, "FeTArrayBase capacity is full already");
				}
			}
			else
			{
				EffectiveSize = ((EffectiveSize == 0) ? 8 : EffectiveSize) * 2;
			}

			T* pNewBase = FE_NEW_ARRAY(T, EffectiveSize, HeapId);
			for (uint32 i = 0; i < Size - 1; i++)
			{
				TransportDataElement(pNewBase[i], BaseAdress[i]);
			}

			if (BaseAdress != nullptr)
			{
				DestroyElements(iPreviousSize);
				FE_DELETE_ARRAY(T, BaseAdress, iPreviousEffectiveSize, HeapId);
			}
			BaseAdress = pNewBase;
		}
		return BaseAdress[Size - 1];
	}
	void		Add(const T& element)
	{
		T& baseelem = Add();
		baseelem = element;
	}
	void		AddUnique(const T& element)
	{
		if (IndexOf(element) == FE_INVALID_ID)
			Add(element);
	}
	void		Add(const FeTArrayBase<T>& elements)
	{
		for (auto& element : elements)
		{
			T& baseelem = Add();
			baseelem = element;
		}
	}
	void		Add(const T* _pElement, uint32 _num)
	{
		Reserve(Size + _num);
		for (uint32 i = 0; i <_num; ++i)
		{
			T& baseelem = Add();
			baseelem = _pElement[i];
		}
	}
	T			PopBack()
	{
		T& back = BaseAdress[Size - 1];
		Size--;
		return back;
	}
	T&			Back()
	{
		return BaseAdress[Size - 1];
	}
	const T&	Back() const
	{
		return BaseAdress[Size - 1];
	}
	static uint32	ErrorIndex(){ return uint32(-1); }
	uint32 IndexOf(const T& element) const
	{
		for (uint32 i = 0; i < Size; i++)
		{
			if (BaseAdress[i] == element)
				return i;
		}
		return ErrorIndex();
	}
	T* Find(std::function<bool(const T&)> predicate) const
	{
		for (uint32 i = 0; i < Size; i++)
		{
			if (predicate(BaseAdress[i]))
				return &BaseAdress[i];
		}
		return nullptr;
	}
	uint32 IndexOf(std::function<bool(const T&)> predicate) const
	{
		for (uint32 i = 0; i < Size; i++)
		{
			if (predicate(BaseAdress[i]))
				return i;
		}
		return ErrorIndex();
	}
	bool Contains(std::function<bool(const T&)> predicate) const
	{
		return IndexOf(predicate) != FE_INVALID_ID;
	}
	uint32 SortedFind(const T& element) const
	{
		uint32 _index = ErrorIndex();;

		uint32 uiBegin = 0;
		uint32 uiEnd = GetSize();
		while (uiBegin != uiEnd)
		{
			const uint32 uiMiddle = (uiBegin + uiEnd) / 2;
			const T& e = GetAt(uiMiddle);
			if (element < e)
			{
				uiEnd = uiMiddle;
			}
			else if (element == e)
			{
				_index = uiMiddle;
				break;
			}
			else
			{
				uiBegin = uiMiddle + 1;
			}
		}
		return _index;
	}
	bool		InsertAt(uint32 index, const T& element)
	{
		if (index<Size)
		{
			Add();
			if (Size>1)
			{
				for (int i = static_cast<int>(Size - 2); i >= static_cast<int>(index); i--)
				{
					TransportDataElement(BaseAdress[i + 1], BaseAdress[i]);
				}
			}
			BaseAdress[index] = element;
			return true;
		}
		return false;
	}
	uint32 Count(std::function<bool(const T&)> predicate)
	{
		uint32 result=0;

		for (uint32 i = 0; i < Size; i++)
		{
			if (predicate(BaseAdress[i]))
				result++;
		}
		return result;
	}
	uint32 Remove(std::function<bool(const T&)> predicate)
	{
		FeTArray<uint32> founds;

		for (uint32 i = 0; i < Size; i++)
		{
			if (predicate(BaseAdress[i]))
				founds.Add(i - founds.GetSize());
		}

		for (auto& found : founds)
		{
			RemoveAt(found);
		}

		return founds.GetSize();
	}
	T			RemoveAt(uint32 index)
	{
		T old = BaseAdress[index];
		for (uint32 i = index; i < Size - 1; i++)
		{
			TransportDataElement(BaseAdress[i], BaseAdress[i + 1]);
		}
		Size -= 1;
		return old;
	}

	///< Remove an element by swapping last element : do not preserve order. Fast o(1)
	T			RemoveAtNoOrdering(uint32 index)
	{
		T old = BaseAdress[index];
		Size -= 1;
		if (Size>0)
		{
			BaseAdress[index] = BaseAdress[Size];
		}
		return old;
	}
	T			SetAt(uint32 i, const T& element)
	{
		T old = BaseAdress[i];
		BaseAdress[i] = element;
		return old;
	}
	void		Clear() //nothing destroyed, capacity is unchanged, only size is 0
	{
		Size = 0;
	}
	void		Free()
	{
		if (BaseAdress != nullptr)
		{
			DestroyElements(Size);
			FE_DELETE_ARRAY(T, BaseAdress, EffectiveSize, HeapId);
			BaseAdress = nullptr;
		}
		Size = 0;
		EffectiveSize = 0;
	}
	void		SetZeroMemory()
	{
		memset(BaseAdress, 0, EffectiveSize*sizeof(T));
	}
	
	T*			GetBaseAdress() { return BaseAdress; }
	THeapId		GetHeapId() { return HeapId; }

	void		SetHeapId(THeapId iHeapId)
	{
		if (iHeapId != HeapId)
		{
			if (EffectiveSize > 0)
			{
				T* newBaseAdress = FE_NEW_ARRAY(T, EffectiveSize, iHeapId);
				
				for (uint32 i = 0; i < Size; i++)
				{
					newBaseAdress[i] = BaseAdress[i];
				}
			
				DestroyElements(Size);
				FE_DELETE_ARRAY(T, BaseAdress, EffectiveSize, HeapId);
				BaseAdress = newBaseAdress;
			}
			HeapId = iHeapId;
		}
	}
protected:
	T*	BaseAdress;
	uint32	Size;
	uint32	EffectiveSize;
	THeapId HeapId;
protected:
	bool CanExpand;
};

template <class T>
class FeTArray : public FeTArrayBase<T>
{
public:
	~FeTArray()
	{
		Free();
	}
protected:
	virtual void DestroyElements(uint32 size) override
	{
#ifndef FE_NO_CUSTOM_ALLOCATOR // no need to force call to dtor if we don't use custom allocator
		for (size_t i = 0; i < size; ++i)
			((T*)(&BaseAdress[i]))->~T();
#endif
	}
};

template <class T, bool bDestroyElements=true>
class FeTPtrArray : public FeTArrayBase<T*>
{
public:
	~FeTPtrArray()
	{
		Free();
	}
	T* AddNew()
	{
		T* pEntry = FE_NEW(T, HeapId);
		Add(pEntry);
		return pEntry;
	}
	void Delete(T* pEntry)
	{
		uint32 idx = Find(pEntry);
		if (idx != ErrorIndex())
			RemoveAt(idx);
		FE_DELETE(T, pEntry, HeapId);
	}
protected:
	virtual void DestroyElements(uint32 size) override
	{
		if (!bDestroyElements)
			return;

		for (size_t i = 0; i < size; ++i)
		{
			if (BaseAdress[i] != nullptr)
			{
				BaseAdress[i]->~T();
				FE_DELETE(T, BaseAdress[i], HeapId);
			}
		}
	}
};

template <class T>
class FeTPtrReleasableArray : public FeTArrayBase<T*>
{
public:
	~FeTPtrReleasableArray()
	{
		Free();
	}
protected:
	virtual void DestroyElements(uint32 size) override
	{
#ifdef DEBUG
		{
			FeIReleasable* pReleasable = dynamic_cast<FeIReleasable>(BaseAdress[i]);
			ASSERT(pReleasable, "array element type is not FeIReleasable !");
			if (!pReleasable)
				return;
		}
#endif
		for (size_t i = 0; i < size; ++i)
		{
			FeIReleasable* pReleasable = (FeIReleasable*)BaseAdress[i];
			if (pReleasable)
				pReleasable->~T();
			FE_DELETE(T, BaseAdress[i], HeapId);
		}
	}
};

template <class TKey, class TValue, class uint32 = uint32>
class FeTMap
{
#define FETASH_COMPARATOR_ENTRY_KEY(a,b) SGLIB_NUMERIC_COMPARATOR(a.Key, b)
#define FETASH_COMPARATOR_ENTRY_ENTRY(a,b) SGLIB_NUMERIC_COMPARATOR(a.Key, b.Key)

public:
	struct KeyEntry
	{
		TKey	Key;
		uint32		ValueIndex;
	};

	void Reserve(uint32 size)
	{
		Keys.Reserve(size);
		Values.Reserve(size);
	}
	
	void Remove(TKey key)
	{
		KeyEntry keyEntry;

		uint32 index = Find(key);

		if (index != Keys.ErrorIndex())
		{

		}
	}
	TValue& Add(TKey key)
	{
		return DoAdd(key, true);
	}
	TValue& AddNoSort(TKey key)
	{
		return DoAdd(key, false);
	}
	void AddNoSort(TKey key, const TValue& value)
	{
		DoAdd(key, value, false);
	}
	void Add(TKey key, const TValue& value)
	{
		DoAdd(key, value, true);
	}

	TValue& operator[](TKey key)
	{
		uint32 index = Find(key);

		FE_ASSERT(index != Values.ErrorIndex(), "FeTHash find failed !")

			return Values[index];
	}

	uint32 FindKey(TKey key)
	{
		int iFound = 0;
		uint32 iFoundIdx = 0;

		SGLIB_ARRAY_BINARY_SEARCH(uint32, Keys.GetBaseAdress(), 0, Keys.GetSize() - 1, key, FETASH_COMPARATOR_ENTRY_KEY, iFound, iFoundIdx);

		return iFound ? iFoundIdx : Keys.ErrorIndex();
	}
	uint32 Find(TKey key)
	{
		for (uint32 i = 0; i < Keys.GetSize(); ++i)
		{
			if (Keys[i].Key == key)
				return Keys[i].ValueIndex;
		}
		return Keys.ErrorIndex();

		//int iFound = 0;
		//uint32 iFoundIdx = 0;

		//SGLIB_ARRAY_BINARY_SEARCH(uint32, Keys.GetBaseAdress(), 0, Keys.GetSize() - 1, key, FETASH_COMPARATOR_ENTRY_KEY, iFound, iFoundIdx);
		//
		//return iFound ? Keys[iFoundIdx].ValueIndex : Keys.ErrorIndex();
	}
	void Sort()
	{
		//SGLIB_ARRAY_SINGLE_QUICK_SORT(KeyEntry, Keys.GetBaseAdress(), Keys.GetSize(), FETASH_COMPARATOR_ENTRY_ENTRY);
	}
	TValue& GetValueAt(uint32 idx)
	{
		return Values[idx];
	}
	TKey& GetKeyAt(uint32 idx)
	{
		return Keys[idx];
	}
	uint32 GetSize()
	{
		return Keys.GetSize();
	}
private:
	void DoAdd(TKey key, const TValue& value, bool bSort)
	{
		FE_ASSERT(Find(key) == Keys.ErrorIndex(), "FeTHash Add key already present !");

		Values.Add(value);

		KeyEntry& keyEntry = Keys.Add();
		keyEntry.Key = key;
		keyEntry.ValueIndex = Values.GetSize() - 1;

		if (bSort)
			Sort();
	}
	TValue& DoAdd(TKey key, bool bSort)
	{
		FE_ASSERT(Find(key) == Keys.ErrorIndex(), "FeTHash Add key already present !");

		TValue& newValue = Values.Add();

		KeyEntry& keyEntry = Keys.Add();
		keyEntry.Key = key;
		keyEntry.ValueIndex = Values.GetSize() - 1;

		if (bSort)
			Sort();

		return newValue;
	}

	FeTArray<KeyEntry> Keys;
	FeTArray<TValue> Values;
};


namespace FeArrayHelper
{
	template<typename T>
	class QuickSort
	{
	public:
		typedef std::function<int(const T& a, const T& b)> FeCompareFunc;

		static void Execute(FeTArray<T>& Array, FeCompareFunc CompareFunc, bool bInverted = false)
		{
			uint32 Size = Array.GetSize();
			if (Size>1)
				DoQuickSort(Array, CompareFunc, 0, Size - 1, bInverted);
		}
	private:
		static void DoQuickSort(FeTArray<T>& arr, FeCompareFunc compare, int left, int right, bool bInverted)
		{
			int i = left, j = right;
			T tmp;
			T pivot = arr[(left + right) / 2];

			/* partition */
			while (i <= j) {
				while (compare(arr[i], pivot) == (bInverted ? 2 : 1)) // <
					i++;
				while (compare(arr[j], pivot) == (bInverted ? 1 : 2)) // >
					j--;
				if (i <= j) {
					tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;
					i++;
					j--;
				}
			};

			/* recursion */
			if (left < j)
				DoQuickSort(arr, compare, left, j, bInverted);
			if (i < right)
				DoQuickSort(arr, compare, i, right, bInverted);
		}
	};
}
