#pragma once 

#include "common.hpp"
#include <string.hpp>
#include "typeinfo.hpp"

class FeSerializable;

class FeIPropertyAccessor
{
public:
	virtual void Set(void * value) = 0;
	virtual void* Get() const = 0;
};

template<typename T>
class FeTPropertyAccessor : public FeIPropertyAccessor
{
public:
	void Initialize(T* ptrToProperty, FeSerializable* owner, const FeString& propertyName)
	{
		Owner = owner;
		PropertyName = propertyName;
		PtrToProperty = ptrToProperty;
	}

	void Set(void* value)
	{
		*PtrToProperty = *((T*)value);
	}
	void* Get() const
	{
		return PtrToProperty;
	}

private:
	T* PtrToProperty;
	// debug
	FeSerializable* Owner;
	FeString PropertyName;
};


class FePropertyAccessor : FeIPropertyAccessor
{
public:
	void Initialize(void* ptrToProperty, FeSerializable* owner, const FeString& propertyName, const FeString& propertyTypeName)
	{
		Owner = owner;
		PropertyName = propertyName;
		PtrToProperty = ptrToProperty;
		PropertyTypeName = propertyTypeName;
	}

	void Set(void* value)
	{
		// mem copy for contiguous types
		FeTypeInfo* pTypeInfo = FeTypeInfo::FromName(PropertyTypeName);

		if (pTypeInfo && pTypeInfo->IsMemoryContiguous)
		{
			memcpy_s(PtrToProperty, pTypeInfo->TypeSize, value, pTypeInfo->TypeSize);
		}
	}
	void* Get() const
	{
		return PtrToProperty;
	}

private:
	void* PtrToProperty;
	// debug
	FeSerializable* Owner;
	FeString PropertyName;
	FeString PropertyTypeName;
};