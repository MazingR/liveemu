#include <pointer.hpp>
#include <SDL.h>


FePtrRefCounter::FePtrRefCounter()
{
	Mutex = SDL_CreateMutex();
}
FePtrRefCounter::~FePtrRefCounter()
{
	SDL_DestroyMutex(Mutex);
}
int32 FePtrRefCounter::Add(void* ptr, uint32 heapId)
{
	AddRemove(true, ptr, heapId);
}
int32 FePtrRefCounter::Remove(void* ptr, uint32 heapId)
{
	AddRemove(false, ptr, heapId);
}
int32 FePtrRefCounter::AddRemove(bool bAdd, void* ptr, uint32 heapId)
{
	if (nullptr == ptr)
		return 0;

	SCOPELOCK(Mutex);

	RefCountMapIt it = RefCounts.find(ptr);

	if (it == RefCounts.end())
	{
		if (bAdd)
		{
			RefCounts[ptr] = RefCount(1, heapId);
		}
		else
		{
			FE_ASSERT(false, "FePtrRefCounter invalid ref count !");
		}
	}
	else
	{
		RefCount& refCount = RefCounts[ptr];
		FE_ASSERT(heapId == refCount.HeapId, "FePtrRefCounter heap id shouldn't for same shared pointer references !");
		refCount.Value += bAdd ? 1 : -1;

		if (refCount.Value == 0)
		{
			RefCounts.erase(it);
			return 0;
		}
		else
		{
			return refCount.Value;
		}
	}
}