#pragma once 

#define EMPTY_MACRO
#define EMPTY_PROPERTIES_LIST(_d)

// -------------------------------------------------------------------------------------------------------------------------
// Class declaration preprocessing for serialization
// -------------------------------------------------------------------------------------------------------------------------

/// <summary>
/// ---------------------------------------------------------------------------------------------------------------------------
/// </summary>
#define DECLARE_PROPERTY(t,n)				t n;
#define DECLARE_CLASS_MEMBERS(properties)		\
	properties(DECLARE_PROPERTY)

/// <summary>
/// ---------------------------------------------------------------------------------------------------------------------------
/// </summary>
#define DECLARE_PROPERTY_SERIALIZE(t,n) FE_FAILEDRETURN( FeSerialize(output, __property_name_##n, &n) );

#define DECLARE_SERIALIZER(properties, thisClass, baseClass)	\
	virtual uint32 Serialize(FeSerializer* output) const override		\
	{															\
		baseClass::Serialize(output);							\
		properties(DECLARE_PROPERTY_SERIALIZE)					\
		return FeEReturnCode::Success;							\
	}															\

#define DECLARE_PROPERTY_DESERIALIZE(t,n) FE_FAILEDRETURN( FeDeserialize(input, __property_name_##n, &n) );

#define DECLARE_DESERIALIZER(properties, baseClass)					\
	virtual uint32 Deserialize(FeSerializer* input) override		\
	{																\
		baseClass::Deserialize(input);								\
		properties(DECLARE_PROPERTY_DESERIALIZE)					\
		return FeEReturnCode::Success;								\
	}																\

/// <summary>
/// ---------------------------------------------------------------------------------------------------------------------------
/// </summary>
#define DECLARE_REFLECTION_PROPERTY_NAME(t, n) FeString __property_name_##n = #n;

#define DECLARE_REFLECTION_GET_PROPERTY(t, n)			if (szName == __property_name_##n) return FeGetPropertyValuePtr(n);
#define DECLARE_REFLECTION_GET_PROPERTY_TRANSIENT(t, n)	if (szName == __property_name_##n) return FeGetTransientPropertyValuePtr(n);
#define DECLARE_REFLECTION_GET_PROPERTY_TRAVERSAL(t, n)	if (szName == __property_name_##n) return FeGetTraversalPropertyValuePtr(n);

#define DECLARE_REFLECTION_SET_PROPERTY(t, n) if (szName == __property_name_##n) {n = *(t*)value; return;}

#define DECLARE_REFLECTION_GET_TPROPERTYACCESSOR(t, n)								\
if (propertyName == __property_name_##n) 											\
{																					\
	FeTPropertyAccessor<t>* propAccessor = FE_NEW(FeTPropertyAccessor<t>, iHeapId);	\
	propAccessor->Initialize(&n, this, __property_name_##n);						\
	return propAccessor;															\
}

#define DECLARE_REFLECTION_GET_PROPERTYACCESSOR(t, n)								\
if (propertyName == __property_name_##n) 											\
{																					\
	output.Initialize(&n, this, __property_name_##n, #t);							\
	return true;																	\
}

#define DECLARE_REFLECTION_GET_PROPERTY_ISSERIALIZABLE(t, n)						\
if (szName == __property_name_##n) 													\
{																					\
	bResult = IsTypeSerializable(&n);												\
	propertyPtr=FeGetPropertyValuePtr(n);											\
}

#define DECLARE_REFLECTION(properties, propertiesBis, thisClass, baseClass)													\
private:																													\
	properties(DECLARE_REFLECTION_PROPERTY_NAME)																			\
	propertiesBis(DECLARE_REFLECTION_PROPERTY_NAME)																			\
																															\
	const FeString _name_##thisClass = #thisClass;																			\
public:																														\
	static const FeString& ClassName()																						\
	{																														\
		static thisClass staticInstance;																					\
		return staticInstance._name_##thisClass;																			\
	}																														\
	virtual bool IsChildOf(const FeString& className) const override														\
	{																														\
		if (_name_##thisClass == className)																					\
			return true;																									\
		return  baseClass::IsChildOf(className);																			\
	}																														\
	virtual const FeString& GetThisClassName() const override																\
	{																														\
		return _name_##thisClass;																							\
	}																														\
	virtual void SelfDelete(uint32 iHeapId) override																		\
	{																														\
		FE_DELETE(thisClass, this, iHeapId);																				\
	}																														\
	virtual void* GetPropertyByName(const FeString& szName) override														\
	{																														\
		void* Value = baseClass::GetPropertyByName(szName);																	\
		if (!Value)																											\
		{																													\
			properties(DECLARE_REFLECTION_GET_PROPERTY)																		\
			propertiesBis(DECLARE_REFLECTION_GET_PROPERTY)																	\
		}																													\
		return Value;																										\
	}																														\
	virtual void* GetTransientPropertyByName(const FeString& szName)  override												\
	{																														\
		void* Value = baseClass::GetTransientPropertyByName(szName);														\
		if (!Value)																											\
		{																													\
			properties(DECLARE_REFLECTION_GET_PROPERTY_TRANSIENT)															\
			propertiesBis(DECLARE_REFLECTION_GET_PROPERTY_TRANSIENT)														\
		}																													\
		return Value;																										\
	}																														\
	virtual void* GetTraversalPropertyByName(const FeString& szName) override												\
	{																														\
		void* Value = baseClass::GetTraversalPropertyByName(szName);														\
		if (!Value)																											\
		{																													\
			properties(DECLARE_REFLECTION_GET_PROPERTY_TRAVERSAL)															\
			propertiesBis(DECLARE_REFLECTION_GET_PROPERTY_TRAVERSAL)														\
		}																													\
		return Value;																										\
	}																														\
	virtual bool GetIsPropertySerializable(const FeString& szName, void*& propertyPtr) override								\
	{																														\
		propertyPtr = nullptr;																								\
		bool bResult = baseClass::GetIsPropertySerializable(szName, propertyPtr);											\
		if (!propertyPtr)																									\
		{																													\
			properties(DECLARE_REFLECTION_GET_PROPERTY_ISSERIALIZABLE)														\
			propertiesBis(DECLARE_REFLECTION_GET_PROPERTY_ISSERIALIZABLE)													\
		}																													\
		return bResult;																										\
	}																														\
	virtual FeIPropertyAccessor* CreatePropertyAccessor(const FeString& propertyName, uint32 iHeapId=DEFAULT_HEAP) override	\
	{																														\
		FeIPropertyAccessor* result = baseClass::CreatePropertyAccessor(propertyName, iHeapId);								\
		if (result)																											\
			return result;																									\
																															\
		properties(DECLARE_REFLECTION_GET_TPROPERTYACCESSOR)																\
		propertiesBis(DECLARE_REFLECTION_GET_TPROPERTYACCESSOR)																\
		return nullptr;																										\
	}																														\
	virtual bool CreatePropertyAccessor(const FeString& propertyName, FePropertyAccessor& output) override					\
	{																														\
		if (baseClass::CreatePropertyAccessor(propertyName, output))														\
			return true;																									\
																															\
		properties(DECLARE_REFLECTION_GET_PROPERTYACCESSOR)																	\
		propertiesBis(DECLARE_REFLECTION_GET_PROPERTYACCESSOR)																\
		return false;																										\
	}																														\

/// <summary>
/// ---------------------------------------------------------------------------------------------------------------------------
/// </summary>

#define DECLARE_PROPERTY_ACCESSOR(t, n)				\
	t const & Get##n () const { return n ; }		\
	t & Get##n () { return n ; }					\
	void Set##n (t const & value) { n = value; }	\

#define DECLARE_ACCESSORS(properties, propertiesBis, baseClass)	\
	properties(DECLARE_PROPERTY_ACCESSOR)			\
	propertiesBis(DECLARE_PROPERTY_ACCESSOR)		\

#define DECLARE_PROPERTY_COPY(t, n) pOutput->Set##n (n);

#define DECLARE_PROPERTIES_COPY(properties, propertiesBis, thisClass, baseClass)	\
	virtual FeSerializable* CreateInstance(uint32 heapId) const override			\
	{																				\
		thisClass* pCopy = FE_NEW(thisClass, heapId);								\
		return pCopy;																\
	}																				\
	virtual void CopyProperties(FeSerializable* output) const override				\
	{																				\
		baseClass::CopyProperties(output);											\
																					\
		thisClass* pOutput = dynamic_cast<thisClass*>(output);						\
		FE_ASSERT(pOutput, "invalid CopyProperties output type!");					\
		properties(DECLARE_PROPERTY_COPY)											\
		propertiesBis(DECLARE_PROPERTY_COPY)										\
	}																				\
	thisClass& operator=(thisClass const & other)									\
	{																				\
		other.CopyProperties(this);													\
		return *this;																\
	}																				\

/// <summary>
/// Declares the properties of a class with serialization functions following the steps defined above
/// </summary>
#define FE_DECLARE_CLASS_BODY(properties, thisClass, baseClass)							\
	typedef baseClass Super;															\
	public:																				\
	DECLARE_CLASS_MEMBERS(properties)													\
	public:																				\
	DECLARE_REFLECTION(properties, EMPTY_PROPERTIES_LIST, thisClass, baseClass)			\
	DECLARE_SERIALIZER(properties, thisClass, baseClass)								\
	DECLARE_DESERIALIZER(properties, baseClass)											\
	DECLARE_ACCESSORS(properties, EMPTY_PROPERTIES_LIST, baseClass)						\
	DECLARE_PROPERTIES_COPY(properties, EMPTY_PROPERTIES_LIST, thisClass, baseClass)	\

/// <summary>
/// Declares the properties of a class with serialization functions following the steps defined above
/// </summary>
#define FE_DECLARE_CLASS_BODY_EX(properties, propertiesNonSerialized, thisClass, baseClass)	\
	typedef baseClass Super;																\
	public:																					\
	DECLARE_CLASS_MEMBERS(properties)														\
	DECLARE_CLASS_MEMBERS(propertiesNonSerialized)											\
	public:																					\
	DECLARE_REFLECTION(properties, propertiesNonSerialized, thisClass, baseClass)			\
	DECLARE_SERIALIZER(properties, thisClass, baseClass)									\
	DECLARE_DESERIALIZER(properties, baseClass)												\
	DECLARE_ACCESSORS(properties, propertiesNonSerialized, baseClass)						\
	DECLARE_PROPERTIES_COPY(properties, propertiesNonSerialized, thisClass, baseClass)		\

#define FE_DECLARE_CLASS_BOTTOM(thisClass)													\
	static FeTFactory<thisClass> Factory_##thisClass;										\


