#pragma once 

#include <taskscheduler.hpp>
#include <SDL.h>

int FeTaskQeueThreadFunc(void* pData)
{
	auto pThis = (FeTaskQeue*)pData;
	
	SDL_SetThreadPriority(SDL_THREAD_PRIORITY_LOW);

	pThis->ThreadRun();
	return 0;
}

FeTaskQeue::FeTaskQeue(const FeString& name) : Name(name)
{
	bIsShuttingDown = 0;
	Mutex = SDL_CreateMutex();
	Thread = nullptr;
}
FeTaskQeue::~FeTaskQeue()
{
	Stop();
	
	for (auto task : Queue)
		task->SelfDelete(DEFAULT_HEAP);
}
int32 FeTaskQeue::ThreadRun()
{
	while(true)
	{
		bool bWaitDelay = true;

		if (bIsShuttingDown == 1)
			break;
		
		if (Paused == 0)
		{
			FeTask* task = PopTask();
			uint32 iResult = FeEReturnCode::Success;

			if (task)
			{
				bWaitDelay = false;
				iResult = task->Execute();
				task->SelfDelete(DEFAULT_HEAP);
			}
		}
		
		if (bWaitDelay)
			SDL_Delay(100);
	}
	return 0;
}
bool FeTaskQeue::Lock()
{
	return SDL_LockMutex(Mutex) == 0;
}
void FeTaskQeue::Unlock()
{
	SDL_UnlockMutex(Mutex);
}
void FeTaskQeue::Stop(bool bWait)
{
	bIsShuttingDown = 1;
	
	if (bWait)
	{
		int iReturned;
		SDL_WaitThread(Thread, &iReturned);
		SDL_DestroyMutex(Mutex);
	}
}
void FeTaskQeue::Pause()
{
	Paused = 1;
}
void FeTaskQeue::Start()
{
	bIsShuttingDown = 0;
	Paused = 0;
	
	if (!Thread)
		Thread = SDL_CreateThread(FeTaskQeueThreadFunc, Name.Cstr(), this);
}
FeTask* FeTaskQeue::PopTask()
{
	FeTask* pResult = nullptr;

	if (Lock())
	{
		if (Queue.GetSize() > 0)
		{
			pResult = Queue[0];
			Queue.RemoveAt(0);
		}
		Unlock();
	}
	return pResult;
}
FeTaskScheduler::~FeTaskScheduler()
{
	for (auto queue : Queues)
		FE_DELETE(FeTaskQeue, queue, DEFAULT_HEAP);
}
FeTaskQeue* FeTaskScheduler::CreateQueue(const FeString& name)
{
	auto queue = FE_NEW(FeTaskQeue, DEFAULT_HEAP, name);
	Queues.Add(queue);
	return queue;
}

bool FeTaskScheduler::IsShuttingDown()
{
	return bIsShuttingDown;
}
void FeTaskScheduler::Stop()
{
	bIsShuttingDown = true;
	for (auto queue : Queues)
		queue->Stop(false);
}