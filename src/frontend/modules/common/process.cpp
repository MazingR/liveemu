#include <process.hpp>
#include <filesystem.hpp>
#include <windows.h>

uint32 FeProcessCreate(FeProcessInfo& procInfo, const FePath& path, const char* params, uint32 iFlags /*= 0*/)
{
	// additional information
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	// set the size of the structures
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	
	FeStaticString<FE_STRING_SIZE_512> strPath;
	FeStaticString<FE_STRING_SIZE_512> strDirPath;

	FePath dirPath = path;
	dirPath.File = "";
	dirPath.Ext = "";

	FeFileTools::FormatFullPath(path, strPath);
	FeFileTools::FormatFullPath(dirPath, strDirPath);

	DWORD dwCreationFlags = 0;

	if (iFlags & FeEProcessCreationFlags::NoWindow)
		dwCreationFlags |= CREATE_NO_WINDOW;

	// start the program up
	bool bResult = CreateProcess(strPath.Value, // the path
		const_cast<char*>(params), // Command line
		NULL,
		NULL,
		(iFlags & FeEProcessCreationFlags::NoWindow) ? TRUE : FALSE,
		dwCreationFlags,
		NULL,	// env
		strDirPath.Value,	// working dir
		&si,
		&pi
		);

	if (!bResult)
		return FeEReturnCode::Failed;

	ZeroMemory(&procInfo, sizeof(FeProcessInfo));
	procInfo.Process	= pi.hProcess;
	procInfo.ProcessId	= pi.dwProcessId;
	procInfo.Thread		= pi.hThread;
	procInfo.ThreadId	= pi.dwThreadId;

	return FeEReturnCode::Success;
}
uint32 FeProcessStop(FeProcessInfo& procInfo)
{
	if (CloseHandle(procInfo.Process) &&
		CloseHandle(procInfo.Thread))
	{
		return FeEReturnCode::Success;
	}
	return FeEReturnCode::Failed;
}
bool FeProcessIsRunning(FeProcessInfo& procInfo)
{
	HANDLE process = OpenProcess(SYNCHRONIZE, FALSE, procInfo.ProcessId);

	if (process != NULL)
	{
		DWORD ret = WaitForSingleObject(process, 0);
		CloseHandle(process);
		return ret == WAIT_TIMEOUT;
	}
	else
	{
		return false;
	}
}
uint32 FeProcessWait(FeProcessInfo& procInfo)
{
	FE_ASSERT_NOT_IMPLEMENTED_RETURN(FeEReturnCode::Success);
}