#pragma once 

#include "string.hpp"
#include "serializable.hpp"

#include <string>
#include <stdarg.h>
#include <SDL.h>

#define FE_HEAPID_STRINGPOOL 5
#define FE_HEAPNAME_STRINGPOOL "StringPool"

SDL_mutex* PoolMutex = nullptr;
FeStringPool StaticInstance;
FeString FeString::EMPTY;

template<class ForwardIterator, class T>
void iota(ForwardIterator first, ForwardIterator last, T value)
{
	while (first != last) {
		*first++ = value;
		++value;
	}
}

int levenshtein_distance(const std::string &s1, const std::string &s2)
{
	// To change the type this function manipulates and returns, change
	// the return type and the types of the two variables below.
	int s1len = s1.size();
	int s2len = s2.size();

	auto column_start = (decltype(s1len))1;
	
	auto column = new decltype(s1len)[s1len + 1];
	iota(column + column_start, column + s1len + 1, column_start);

	for (auto x = column_start; x <= s2len; x++) {
		column[0] = x;
		auto last_diagonal = x - column_start;
		for (auto y = column_start; y <= s1len; y++) {
			auto old_diagonal = column[y];
			auto possibilities = {
				column[y] + 1,
				column[y - 1] + 1,
				last_diagonal + (s1[y - 1] == s2[x - 1] ? 0 : 1)
			};
			column[y] = std::min(possibilities);
			last_diagonal = old_diagonal;
		}
	}
	auto result = column[s1len];
	delete[] column;
	return result;
}

void FeStringConcat(const FeString& a, const FeString& b, FeString* pOutput)
{
	FeStringTools::Concat(a.Cstr(), b.Cstr(), pOutput);
}

FeString::FeString(const FeString& other) : FeString()
{
	if (other.Pooled)
	{
		Pooled = other.Pooled;
		FeStringPool::GetInstance()->IncRef(Pooled);
	}
}
FeString::FeString(const char* other) : FeString()
{
	FeStringPool::GetInstance()->CreateString(other, *this);
}
FeString::FeString(const char other) : FeString()
{
	FeStringPool::GetInstance()->CreateString(other, *this);
}
FeString::FeString(FePooledString& pooledStr) : FeString()
{
	Pooled = &pooledStr;
	FeStringPool::GetInstance()->IncRef(&pooledStr);
}
FeString& FeString::operator= (const FeString& other)
{
	FeStringPool::GetInstance()->DecRef(Pooled);
	Pooled = nullptr;

	if (other.Pooled)
	{
		Pooled = other.Pooled;
		FeStringPool::GetInstance()->IncRef(Pooled);
	}
	return *this;
}
FeString& FeString::operator= (const char* other)
{
	FeStringPool::GetInstance()->CreateString(other, *this);
	return *this;
}
FeString& FeString::operator= (const char other)
{
	FeStringPool::GetInstance()->CreateString(other, *this);
	return *this;
}
FeString FeString::operator+(const char* b)
{
	FeString result;
	FeStringTools::Concat(Cstr(), b, &result);
	return result;
}
void FeString::SetPooledStr(FePooledString* pooledStr)
{
	FeStringPool::GetInstance()->DecRef(Pooled);

	Pooled = pooledStr;
	FeStringPool::GetInstance()->IncRef(Pooled);
}
void FeString::Clear()
{
	FeStringPool::GetInstance()->DecRef(Pooled);
	Pooled = nullptr;
}
FeString::~FeString()
{
	FeStringPool::GetInstance()->DeleteString(*this);
}

int32 FeStringPool::IncRef(FePooledString* pooledStr)
{
	if (!pooledStr)
		return 0;
	return pooledStr->RefCount.fetch_add(1);
}
int32 FeStringPool::DecRef(FePooledString* pooledStr)
{
	if (!pooledStr)
		return 2;

	return pooledStr->RefCount.fetch_sub(1);
}
void FeStringPool::DeleteString(FeString& str)
{
	return;

	if (str.Pooled)
	{
		SCOPELOCK(PoolMutex);
		{
			int32 iPreviousValue = FeStringPool::GetInstance()->DecRef(str.Pooled);
			
			if (iPreviousValue == 1)
			{
				FeStringPool::StringPoolMap& Pool = *GetPoolFromStringSize(str.Length());

				uint32 id = str.Pooled->Id;
				// delete pooled string from memory
				FE_DELETE(FePooledString, str.Pooled, FE_HEAPID_STRINGPOOL);

				Pool.erase(id);
			}
		}
		str.Pooled = nullptr;
	}
}
FePooledString* FeStringPool::CreatePooledString(const char szValue)
{
	char szChar[2];
	szChar[0] = szValue;
	szChar[1] = '\0';

	return CreatePooledString(szChar);
}
FeStringPool::StringPoolMap* FeStringPool::GetPoolFromStringSize(uint32 iLen)
{
	for (auto& pool : Pools)
	{
		if (pool.StringSizeLimit >= iLen)
			return &pool.Map;
	}

	return &Pools.Back().Map;
}
FePooledString* FeStringPool::CreatePooledString(const char* szValue, uint32 len /*= 0*/)
{
	if (!szValue)
		return nullptr;

	size_t iLen = strlen(szValue);
	if (!iLen)
		return nullptr;

	if (len)
		iLen = len;

	FePooledString* pooledStr = nullptr;

	uint32 iId = FeStringTools::GenerateUIntIdFromString(szValue, nullptr, 0, len);

	SCOPELOCK(PoolMutex);
	{
		FeStringPool::StringPoolMap& Pool = *GetPoolFromStringSize(iLen);

		StringPoolMapIt itEnd = Pool.end();

		if (Pool.find(iId) != itEnd)
		{
			pooledStr = Pool[iId];
		}
		else
		{
			// Create new pooled string
			pooledStr = FE_NEW(FePooledString, FE_HEAPID_STRINGPOOL, iLen + 1);
			Pool[iId] = pooledStr;
			pooledStr->Id = iId;
			pooledStr->RefCount.store(1);

			memcpy_s(pooledStr->Cstr, iLen, szValue, iLen);
			pooledStr->Cstr[iLen] = '\0';
			//sprintf_s(pooledStr->Cstr, iLen, szValue);

			
		}
	}
	return pooledStr;
}
void FeStringPool::CreateString(const char* szValue, uint32 len, FeString& output)
{
	FePooledString* pooledStr = CreatePooledString(szValue, len);
	output.SetPooledStr(pooledStr);
}
void FeStringPool::CreateString(const char* szValue, FeString& output)
{
	FePooledString* pooledStr = CreatePooledString(szValue);
	output.SetPooledStr(pooledStr);
}
void FeStringPool::CreateString(const char szValue, FeString& output)
{
	FePooledString* pooledStr = CreatePooledString(szValue);
	output.SetPooledStr(pooledStr);
}
FeString FeStringPool::CreateString(const char* szValue)
{
	FePooledString* pooledStr = CreatePooledString(szValue);
	if (pooledStr)
		return FeString(*pooledStr);
	else
		return FeString();
}
void FeStringPool::CreateStringFromInt32(int32 value, FeString& output)
{
	char szValue[32];
	sprintf_s(szValue, 32, "%d", value);
	FePooledString* pooledStr = CreatePooledString(szValue);
	output.SetPooledStr(pooledStr);
}
void FeStringPool::CreateStringFromUInt32(uint32 value, FeString& output)
{
	char szValue[32];
	sprintf_s(szValue, 32, "%u", value);
	FePooledString* pooledStr = CreatePooledString(szValue);
	output.SetPooledStr(pooledStr);
}
FeStringPool* FeStringPool::GetInstance()
{
	return &StaticInstance;
}

namespace FeStringTools
{
	void Format(FeString& output, const char* sFormat, ...)
	{
		static char str[1024];

		va_list argptr;
		va_start(argptr, sFormat);
		int ret = vsnprintf_s(str, sizeof(str), sFormat, argptr);
		va_end(argptr);

		output = str;

	}
	FeString Format(const char* sFormat, ...)
	{
		static char str[1024];

		va_list argptr;
		va_start(argptr, sFormat);
		int ret = vsnprintf_s(str, sizeof(str), sFormat, argptr);
		va_end(argptr);

		return str;
	}
	void ToLower(char* szStr)
	{
		size_t iLen = strlen(szStr);
		for (size_t i = 0; i < iLen; ++i)
		{
			szStr[i] = tolower(szStr[i]);
		}
	}
	void ToUpper(char* szStr)
	{
		size_t iLen = strlen(szStr);
		for (size_t i = 0; i < iLen; ++i)
		{
			szStr[i] = toupper(szStr[i]);
		}
	}
	
	//-----------------------------------------------------------------------------
	// MurmurHash2, by Austin Appleby

	// Note - This code makes a few assumptions about how your machine behaves -

	// 1. We can read a 4-byte value from any address without crashing
	// 2. sizeof(int) == 4

	// And it has a few limitations -

	// 1. It will not work incrementally.
	// 2. It will not produce the same results on little-endian and big-endian
	//    machines.

	unsigned int GenerateIntIdFromBuffer(const void * key, int len, unsigned int seed)
	{
		// 'm' and 'r' are mixing constants generated offline.
		// They're not really 'magic', they just happen to work well.

		const unsigned int m = 0x5bd1e995;
		const int r = 24;

		// Initialize the hash to a 'random' value

		unsigned int h = seed ^ len;

		// Mix 4 bytes at a time into the hash

		const unsigned char * data = (const unsigned char *)key;

		while (len >= 4)
		{
			//		unsigned int k = *(unsigned int *)data;
			unsigned int k = (data[0]) + (data[1] << 8) + (data[2] << 16) + (data[3] << 24);

			k *= m;
			k ^= k >> r;
			k *= m;

			h *= m;
			h ^= k;

			data += 4;
			len -= 4;
		}

		// Handle the last few bytes of the input array

		switch (len)
		{
		case 3: h ^= data[2] << 16;
		case 2: h ^= data[1] << 8;
		case 1: h ^= data[0];
			h *= m;
		};

		// Do a few final mixes of the hash to ensure the last few
		// bytes are well-incorporated.

		h ^= h >> 13;
		h *= m;
		h ^= h >> 15;

		return h;
	}

	uint32 GenerateUIntIdFromString(const char* cptr, const char* cptr2 /*= nullptr*/, uint32 _crc /*= 0*/, uint32 len /*= 0*/)
	{
		if (!cptr)
			return 0;

		uint32 length = len ? len : (uint32)strlen(cptr);
		uint32 crc = GenerateIntIdFromBuffer(cptr, length, _crc);

		return crc;
	}

	size_t Count(const char* szString, char szChar, size_t iStart, size_t iEnd)
	{
		size_t iLen = iEnd ? iEnd : strlen(szString);
		size_t iCount = 0;

		for (size_t i = iStart; i < iLen; ++i)
		{
			if (szString[i] == szChar)
				iCount++;
		}

		return iCount;
	}
	size_t DoIndexOf(const char* szString, char szChar, size_t iStart, size_t iEnd, bool bReverse)
	{
		size_t iLen = iEnd ? iEnd : strlen(szString);

		for (size_t i = bReverse ? iLen - 1 : iStart; bReverse ? (i >= iStart) : (i < iLen); bReverse ? (--i) : (++i))
		{
			if (szString[i] == szChar)
				return  i;

			if (bReverse&&i == 0)
				break;
		}

		return (size_t)-1;
	}
	size_t DoIndexOf(const char* szString, const char* szFind, size_t iStart, size_t iEnd, bool bReverse)
	{
		size_t iLen = iEnd ? iEnd : strlen(szString);
		
		size_t iFindIdx = 0;
		size_t iFindLen = strlen(szFind);

		if (iFindLen > iLen)
			return (size_t)-1;

		for (size_t i = bReverse ? iLen - 1 : iStart; bReverse ? (i >= iStart) : (i < iLen); bReverse ? (--i) : (++i))
		{
			if (szString[i] == szFind[iFindIdx])
			{
				iFindIdx++;
				if (iFindIdx == iFindLen)
					return i - iFindLen + 1;
			}
			else
			{
				iFindIdx = 0;
			}

			if (bReverse&&i == 0)
				break;
		}

		return FE_INVALID_ID;
	}
	size_t IndexOf(const char* szString, char szChar, size_t iStart, size_t iEnd)
	{
		return DoIndexOf(szString, szChar, iStart, iEnd, false);
	}
	size_t LastIndexOf(const char* szString, char szChar, size_t iStart, size_t iEnd)
	{
		return DoIndexOf(szString, szChar, iStart, iEnd, true);
	}
	size_t IndexOf(const char* szString, const char* szFind, size_t iStart, size_t iEnd)
	{
		return DoIndexOf(szString, szFind, iStart, iEnd, false);
	}
	size_t Replace(char* szString, char szFind, char szReplace, size_t iStart, size_t iEnd)
	{
		size_t iLen = iEnd == 0 ? strlen(szString) : iEnd;
		size_t iFound = 0;

		for (size_t i = iStart; i < iLen; ++i)
		{
			if (szString[i] == szFind)
			{
				szString[i] = szReplace;
			}
		}

		return iFound;
	}
	size_t Remove(char* szString, char* szFind, size_t iStart /*= 0*/, size_t iEnd /*= 0*/)
	{
		uint32 iFoundCount = 0;
		uint32 iFound = iStart;
		uint32 iLen = strlen(szString);
		uint32 iRemoveLen = strlen(szFind);

		do
		{
			iFound = FeStringTools::IndexOf(szString, szFind, iFound);

			if (iFound != FE_INVALID_ID)
			{
				iFoundCount++;

				uint32 iCopySize = iLen>(iFound + iRemoveLen) ? iLen - iFound - iRemoveLen : 0;
				
				if (iCopySize>0)
					memcpy_s(szString + iFound, iCopySize, szString + iFound + iRemoveLen, iCopySize);

				iLen -= iRemoveLen;
			}
		} while (iFound != FE_INVALID_ID);


		szString[iLen] = '\0';

		return iFoundCount;
	}
	size_t Replace(char* szString, char* szFind, char* szReplace, size_t iStart /*= 0*/, size_t iEnd /*= 0*/)
	{
		FE_ASSERT_NOT_IMPLEMENTED_RETURN(0);
	}
	
	size_t TrimEnd(char* szString, char szChar)
	{
		size_t iLen = strlen(szString);
		size_t iNewSize = iLen;

		for (size_t i = iLen-1; i >= 0; --i)
		{
			if (szString[i] == szChar)
			{
				szString[i] = '\0';
			}
			else
			{
				iNewSize = i + 1;
				break;
			}
		}

		return iNewSize;
	}
	
	bool HasLeft(const char* szString, const char* szTrimed, uint32 strLimit)
	{
		size_t strLen = strLimit == 0 ? strlen(szString) : strLimit;
		size_t strTrimLen = strlen(szTrimed);

		if (strTrimLen > strLen)
			return false;

		for (size_t i = 0; i < strTrimLen; ++i)
		{
			if (szString[i] != szTrimed[i])
				return false;
		}
		return true;
	}
	bool TrimLeft(char** szString, const char* szTrimed, uint32 strLimit)
	{
		size_t strTrimLen = strlen(szTrimed);

		if (HasLeft(*szString, szTrimed, strLimit))
		{
			*szString += strTrimLen;
			return true;
		}
		return false;
	}
	
	void Concat(const char* a, const char* b, FeString* pOutput)
	{
		size_t aLen = a ? strlen(a) : 0;
		size_t bLen = b ? strlen(b) : 0;
		size_t len = aLen + bLen + 1;

		char* tmp = FE_NEW_ARRAY(char, len, DEFAULT_HEAP);
		
		if (aLen>0)
			memcpy_s(tmp, aLen, a, aLen);

		if (bLen>0)
			memcpy_s(tmp + aLen, bLen, b, bLen);

		FE_ASSERT((aLen + bLen) < len, "out of range !");

		tmp[len-1] = '\0';

		*pOutput = tmp;
		FE_DELETE_ARRAY(char, tmp, len, DEFAULT_HEAP);
	}

	FeString Concat(const FeString& a, const FeString& b)
	{
		FeString result;
		Concat(a.Cstr(), b.Cstr(), &result);
		return result;
	}
	FeString Concat(const FeString& a, const char* b)
	{
		FeString result;
		Concat(a.Cstr(), b, &result);
		return result;
	}
	FeString Concat(const char* a, const char* b)
	{
		FeString output;
		Concat(a, b, &output);
		return output;
	}
}
void FeStringBuffer::TrimRight(uint32 size)
{
	FE_ASSERT(Length >= size, "out of range");
	Length -= size;
	Cstr()[Length] = '\0';
}
uint32 FeStringBuffer::Concat(const char* str, uint32 size /*= 0*/)
{
	uint32 len = size ? size : strlen(str);
	Write(str, len);
	Cstr()[Length] = '\0';

	return len;
}
uint32 FeStringBuffer::Printf(const char *format, ...)
{
	uint32 len = 0;
	if (Size > (Length + 1))
	{
		char* output = &Cstr()[Length];
		uint32 capacity = Size - Length - 1;

		va_list argptr;
		va_start(argptr, format);
		len = vsnprintf_s(output, capacity, capacity, format, argptr);
		va_end(argptr);
		Length += len;
	}
	return len;
}
void FeStringPool::Initialize()
{
	PoolMutex = SDL_CreateMutex();

	StaticInstance.Pools.Add(FeStringPoolMap(4));
	StaticInstance.Pools.Add(FeStringPoolMap(8));
	StaticInstance.Pools.Add(FeStringPoolMap(16));
	StaticInstance.Pools.Add(FeStringPoolMap(64));
	StaticInstance.Pools.Add(FeStringPoolMap(96));
	StaticInstance.Pools.Add(FeStringPoolMap(128));
	StaticInstance.Pools.Add(FeStringPoolMap((uint32)-1));
}
FePooledString::FePooledString(uint32 size)
{
	Size = size;
	if (Size < 64) Size = 64;
	Cstr = FE_NEW_ARRAY(char, size, FE_HEAPID_STRINGPOOL);
}
FePooledString::~FePooledString()
{
	FE_DELETE_ARRAY(char, Cstr, Size, FE_HEAPID_STRINGPOOL);
}