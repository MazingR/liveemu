#include <color.hpp>

namespace FeColorHelper
{
	FeColor GetColor(FeEColor::Type eType)
	{
		FeColor output;
		GetColor(eType, output);
		return output;
	}
	void GetColor(FeEColor::Type eType, FeColor& output)
	{
		switch (eType)
		{
		case FeEColor::Snow:			output = FeColor(255, 250, 250, 1.f);
		case FeEColor::Snow2:			output = FeColor(238, 233, 233, 1.f);
		case FeEColor::Snow3:			output = FeColor(205, 201, 201, 1.f);
		case FeEColor::Snow4:			output = FeColor(139, 137, 137, 1.f);
		case FeEColor::GhostWhite:		output = FeColor(248, 248, 255, 1.f);
		case FeEColor::WhiteSmoke:		output = FeColor(245, 245, 245, 1.f);
		case FeEColor::Gainsboro:		output = FeColor(220, 220, 220, 1.f);
		case FeEColor::FloralWhite:		output = FeColor(255, 250, 240, 1.f);
		case FeEColor::OldLace:			output = FeColor(253, 245, 230, 1.f);
		case FeEColor::Linen:			output = FeColor(240, 240, 230, 1.f);
		case FeEColor::AntiqueWhite:	output = FeColor(250, 235, 215, 1.f);
		case FeEColor::AntiqueWhite2:	output = FeColor(238, 223, 204, 1.f);
		case FeEColor::AntiqueWhite3:	output = FeColor(205, 192, 176, 1.f);
		case FeEColor::AntiqueWhite4:	output = FeColor(139, 131, 120, 1.f);
		case FeEColor::PapayaWhip:		output = FeColor(255, 239, 213, 1.f);
		case FeEColor::BlanchedAlmond:	output = FeColor(255, 235, 205, 1.f);
		case FeEColor::Bisque:			output = FeColor(255, 228, 196, 1.f);
		case FeEColor::Bisque2:			output = FeColor(238, 213, 183, 1.f);
		case FeEColor::Bisque3:			output = FeColor(205, 183, 158, 1.f);
		case FeEColor::Bisque4:			output = FeColor(139, 125, 107, 1.f);
		case FeEColor::PeachPuff:		output = FeColor(255, 218, 185, 1.f);
		case FeEColor::PeachPuff2:		output = FeColor(238, 203, 173, 1.f);
		case FeEColor::PeachPuff3:		output = FeColor(205, 175, 149, 1.f);
		case FeEColor::PeachPuff4:		output = FeColor(139, 119, 101, 1.f);
		case FeEColor::NavajoWhite:		output = FeColor(255, 222, 173, 1.f);
		case FeEColor::Moccasin:		output = FeColor(255, 228, 181, 1.f);
		case FeEColor::Cornsilk:		output = FeColor(255, 248, 220, 1.f);
		case FeEColor::Cornsilk2:		output = FeColor(238, 232, 205, 1.f);
		case FeEColor::Cornsilk3:		output = FeColor(205, 200, 177, 1.f);
		case FeEColor::Cornsilk4:		output = FeColor(139, 136, 120, 1.f);
		case FeEColor::Ivory:			output = FeColor(255, 255, 240, 1.f);
		case FeEColor::Ivory2:			output = FeColor(238, 238, 224, 1.f);
		case FeEColor::Ivory3:			output = FeColor(205, 205, 193, 1.f);
		case FeEColor::Ivory4:			output = FeColor(139, 139, 131, 1.f);
		case FeEColor::LemonChiffon:	output = FeColor(255, 250, 205, 1.f);
		case FeEColor::Seashell:		output = FeColor(255, 245, 238, 1.f);
		case FeEColor::Seashell2:		output = FeColor(238, 229, 222, 1.f);
		case FeEColor::Seashell3:		output = FeColor(205, 197, 191, 1.f);
		case FeEColor::Seashell4:		output = FeColor(139, 134, 130, 1.f);
		case FeEColor::Honeydew:		output = FeColor(240, 255, 240, 1.f);
		case FeEColor::Honeydew2:		output = FeColor(244, 238, 224, 1.f);
		case FeEColor::Honeydew3:		output = FeColor(193, 205, 193, 1.f);
		case FeEColor::Honeydew4:		output = FeColor(131, 139, 131, 1.f);
		case FeEColor::MintCream:		output = FeColor(245, 255, 250, 1.f);
		case FeEColor::Azure:			output = FeColor(240, 255, 255, 1.f);
		case FeEColor::AliceBlue:		output = FeColor(240, 248, 255, 1.f);
		case FeEColor::Lavender:		output = FeColor(230, 230, 250, 1.f);
		case FeEColor::LavenderBlush:	output = FeColor(255, 240, 245, 1.f);
		case FeEColor::MistyRose:		output = FeColor(255, 228, 225, 1.f);
		case FeEColor::White:			output = FeColor(255, 255, 255, 1.f);

			//Grays
		case FeEColor::Black:			output = FeColor(0, 0, 0, 1.f);
		case FeEColor::DarkSlateGray:	output = FeColor(49, 79, 79, 1.f);
		case FeEColor::DimGray:			output = FeColor(105, 105, 105, 1.f);
		case FeEColor::SlateGray:		output = FeColor(112, 138, 144, 1.f);
		case FeEColor::LightSlateGray:	output = FeColor(119, 136, 153, 1.f);
		case FeEColor::Gray:			output = FeColor(190, 190, 190, 1.f);
		case FeEColor::LightGray:		output = FeColor(211, 211, 211, 1.f);

			//Blues
		case FeEColor::MidnightBlue:	output = FeColor(25, 25, 112, 1.f);
		case FeEColor::Navy:			output = FeColor(0, 0, 128, 1.f);
		case FeEColor::CornflowerBlue:	output = FeColor(100, 149, 237, 1.f);
		case FeEColor::DarkSlateBlue:	output = FeColor(72, 61, 139, 1.f);
		case FeEColor::SlateBlue:		output = FeColor(106, 90, 205, 1.f);
		case FeEColor::MediumSlateBlue:	output = FeColor(123, 104, 238, 1.f);
		case FeEColor::LightSlateBlue:	output = FeColor(132, 112, 255, 1.f);
		case FeEColor::MediumBlue:		output = FeColor(0, 0, 205, 1.f);
		case FeEColor::RoyalBlue:		output = FeColor(65, 105, 225, 1.f);
		case FeEColor::Blue:			output = FeColor(0, 0, 255, 1.f);
		case FeEColor::DodgerBlue:		output = FeColor(30, 144, 255, 1.f);
		case FeEColor::DeepSkyBlue:		output = FeColor(0, 191, 255, 1.f);
		case FeEColor::SkyBlue:			output = FeColor(135, 206, 250, 1.f);
		case FeEColor::LightSkyBlue:	output = FeColor(135, 206, 250, 1.f);
		case FeEColor::SteelBlue:		output = FeColor(70, 130, 180, 1.f);
		case FeEColor::LightSteelBlue:	output = FeColor(176, 196, 222, 1.f);
		case FeEColor::LightBlue:		output = FeColor(173, 216, 230, 1.f);
		case FeEColor::PowderBlue:		output = FeColor(176, 224, 230, 1.f);
		case FeEColor::PaleTurquoise:	output = FeColor(175, 238, 238, 1.f);
		case FeEColor::DarkTurquoise:	output = FeColor(0, 206, 209, 1.f);
		case FeEColor::MediumTurquoise:	output = FeColor(72, 209, 204, 1.f);
		case FeEColor::Turquoise:		output = FeColor(64, 224, 208, 1.f);
		case FeEColor::Cyan:			output = FeColor(0, 255, 255, 1.f);
		case FeEColor::LightCyan:		output = FeColor(224, 255, 255, 1.f);
		case FeEColor::CadetBlue:		output = FeColor(95, 158, 160, 1.f);

			//Greens
		case FeEColor::MediumAquamarine:	output = FeColor(102, 205, 170, 1.f);
		case FeEColor::Aquamarine:	output = FeColor(127, 255, 212, 1.f);
		case FeEColor::DarkGreen:	output = FeColor(0, 100, 0, 1.f);
		case FeEColor::DarkOliveGreen:	output = FeColor(85, 107, 47, 1.f);
		case FeEColor::DarkSeaGreen:	output = FeColor(143, 188, 143, 1.f);
		case FeEColor::SeaGreen:	output = FeColor(46, 139, 87, 1.f);
		case FeEColor::MediumSeaGreen:	output = FeColor(60, 179, 113, 1.f);
		case FeEColor::LightSeaGreen:	output = FeColor(32, 178, 170, 1.f);
		case FeEColor::PaleGreen:	output = FeColor(152, 251, 152, 1.f);
		case FeEColor::SpringGreen:	output = FeColor(0, 255, 127, 1.f);
		case FeEColor::LawnGreen:	output = FeColor(124, 252, 0, 1.f);
		case FeEColor::Chartreuse:	output = FeColor(127, 255, 0, 1.f);
		case FeEColor::MediumSpringGreen:	output = FeColor(0, 250, 154, 1.f);
		case FeEColor::GreenYellow:	output = FeColor(173, 255, 47, 1.f);
		case FeEColor::LimeGreen:	output = FeColor(50, 205, 50, 1.f);
		case FeEColor::YellowGreen:	output = FeColor(154, 205, 50, 1.f);
		case FeEColor::ForestGreen:	output = FeColor(34, 139, 34, 1.f);
		case FeEColor::OliveDrab:	output = FeColor(107, 142, 35, 1.f);
		case FeEColor::DarkKhaki:	output = FeColor(189, 183, 107, 1.f);
		case FeEColor::Khaki:	output = FeColor(240, 230, 140, 1.f);

			//Yellow
		case FeEColor::PaleGoldenrod:	output = FeColor(238, 232, 170, 1.f);
		case FeEColor::LightGoldenrodYellow:	output = FeColor(250, 250, 210, 1.f);
		case FeEColor::LightYellow:	output = FeColor(255, 255, 224, 1.f);
		case FeEColor::Yellow:	output = FeColor(255, 255, 0, 1.f);
		case FeEColor::Gold:	output = FeColor(255, 215, 0, 1.f);
		case FeEColor::LightGoldenrod:	output = FeColor(238, 221, 130, 1.f);
		case FeEColor::Goldenrod:	output = FeColor(218, 165, 32, 1.f);
		case FeEColor::DarkGoldenrod:	output = FeColor(184, 134, 11, 1.f);

			//Browns
		case FeEColor::RosyBrown:	output = FeColor(188, 143, 143, 1.f);
		case FeEColor::IndianRed:	output = FeColor(205, 92, 92, 1.f);
		case FeEColor::SaddleBrown:	output = FeColor(139, 69, 19, 1.f);
		case FeEColor::Sienna:	output = FeColor(160, 82, 45, 1.f);
		case FeEColor::Peru:	output = FeColor(205, 133, 63, 1.f);
		case FeEColor::Burlywood:	output = FeColor(222, 184, 135, 1.f);
		case FeEColor::Beige:	output = FeColor(245, 245, 220, 1.f);
		case FeEColor::Wheat:	output = FeColor(245, 222, 179, 1.f);
		case FeEColor::SandyBrown:	output = FeColor(244, 164, 96, 1.f);
		case FeEColor::Tan:	output = FeColor(210, 180, 140, 1.f);
		case FeEColor::Chocolate:	output = FeColor(210, 105, 30, 1.f);
		case FeEColor::Firebrick:	output = FeColor(178, 34, 34, 1.f);
		case FeEColor::Brown:	output = FeColor(165, 42, 42, 1.f);
			//Oranges
		case FeEColor::DarkSalmon:	output = FeColor(233, 150, 122, 1.f);
		case FeEColor::Salmon:	output = FeColor(250, 128, 114, 1.f);
		case FeEColor::LightSalmon:	output = FeColor(255, 160, 122, 1.f);
		case FeEColor::Orange:	output = FeColor(255, 165, 0, 1.f);
		case FeEColor::DarkOrange:	output = FeColor(255, 140, 0, 1.f);
		case FeEColor::Coral:	output = FeColor(255, 127, 80, 1.f);
		case FeEColor::LightCoral:	output = FeColor(240, 128, 128, 1.f);
		case FeEColor::Tomato:	output = FeColor(255, 99, 71, 1.f);
		case FeEColor::OrangeRed:	output = FeColor(255, 69, 0, 1.f);
		case FeEColor::Red:	output = FeColor(255, 0, 0, 1.f);

			//Pinks / Violets
		case FeEColor::HotPink:	output = FeColor(255, 105, 180, 1.f);
		case FeEColor::DeepPink:	output = FeColor(255, 20, 147, 1.f);
		case FeEColor::Pink:	output = FeColor(255, 192, 203, 1.f);
		case FeEColor::LightPink:	output = FeColor(255, 182, 193, 1.f);
		case FeEColor::PaleVioletRed:	output = FeColor(219, 112, 147, 1.f);
		case FeEColor::Maroon:	output = FeColor(176, 48, 96, 1.f);
		case FeEColor::MediumVioletRed:	output = FeColor(199, 21, 133, 1.f);
		case FeEColor::VioletRed:	output = FeColor(208, 32, 144, 1.f);
		case FeEColor::Violet:	output = FeColor(238, 130, 238, 1.f);
		case FeEColor::Plum:	output = FeColor(221, 160, 221, 1.f);
		case FeEColor::Orchid:	output = FeColor(218, 112, 214, 1.f);
		case FeEColor::MediumOrchid:	output = FeColor(186, 85, 211, 1.f);
		case FeEColor::DarkOrchid:	output = FeColor(153, 50, 204, 1.f);
		case FeEColor::DarkViolet:	output = FeColor(148, 0, 211, 1.f);
		case FeEColor::BlueViolet:	output = FeColor(138, 43, 226, 1.f);
		case FeEColor::Purple:	output = FeColor(160, 32, 240, 1.f);
		case FeEColor::MediumPurple:	output = FeColor(147, 112, 219, 1.f);
		case FeEColor::Thistle:	output = FeColor(216, 191, 216, 1.f);
		}
	}
}