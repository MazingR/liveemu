#pragma once 

#include <database.hpp>
#include <filesystem.hpp>
#include <sqlite3.h>

FeDatabase FeDatabase::StaticInstance;

struct FeEDbSerialize
{
	enum Type
	{
		Insert_PropertiesList,
		Insert_ValuesList,
		
		Select_PropertiesList,

		Raw,
	};
};

struct FeDbResultData
{
	uint32	ColumnsCount;
	char**	ColumnsValues;
	char**	ColumnsNames;
	uint32	CurrentColumn;

	void Reset()
	{
		ColumnsCount	= 0;
		ColumnsValues	= 0;
		ColumnsNames	= 0;
		CurrentColumn	= 0;
	}
	FeDbResultData()
	{
		Reset();
	}

	const char* GetCurrentValue()
	{
		FE_ASSERT(CurrentColumn < ColumnsCount, "incoherent column count!");
		return ColumnsValues[CurrentColumn];
	}
	const char* GetCurrentName()
	{
		FE_ASSERT(CurrentColumn < ColumnsCount, "incoherent column count!");
		return ColumnsNames[CurrentColumn];
	}
};

template<uint32 Size>
class FeDbSeserializer : public FeSerializer
{
	static const uint32 CHAR_MAX_SIZE = 2048 * 4;
	char szTmp[CHAR_MAX_SIZE];

	template<typename T>
	uint32 SerializeT(const T* input, const char* token)
	{
		sprintf_s(szTmp, CHAR_MAX_SIZE, token, *input);
		return SerializeProperty(szTmp);
	}

	uint32 SerializeT(const FePath* input, const char* token)
	{
		FE_ASSERT_NOT_IMPLEMENTED_RETURN(FeEReturnCode::Failed);
	}

	uint32 SerializeT(const FeString* input, const char* token)
	{
		return SerializeProperty(input->CstrNotNull());
	}

	uint32 SerializeT(const FeAssetRef* input, const char* token)
	{
		FE_ASSERT_NOT_IMPLEMENTED_RETURN(FeEReturnCode::Failed);
	}

	template<typename T>
	bool DeserializeValueT(T* output, const char* input)
	{
		return FeConvert(*output, input);
	}

	bool DeserializeValueT(FePath* output, const char* input)
	{
		FE_ASSERT_NOT_IMPLEMENTED_RETURN(false);
	}

	bool DeserializeValueT(FeAssetRef* output, const char* input)
	{
		FE_ASSERT_NOT_IMPLEMENTED_RETURN(false);
	}
	template<typename T>
	uint32 DeserializeT(T* output)
	{
		if (Path.CurrentDepth == 0)
		{
			FE_ASSERT(SerializedData.CurrentColumn < SerializedData.ColumnsCount, "incoherent column count!");
			FE_ASSERT(strcmp(SerializedData.GetCurrentName(), Path.GetCurrentName()) == 0, "incoherent column name!");

			bool bRes = DeserializeValueT(output, SerializedData.GetCurrentValue());

			if (!bRes)
				return FeEReturnCode::Failed;

			SerializedData.CurrentColumn++;
		}

		return FeEReturnCode::Success;
	}
public:

	FeStringBuffer Buffer;
	FeEDbSerialize::Type SerializeType;
	FeDbResultData SerializedData;

	FeDbSeserializer(uint32 iHeapId) : FeSerializer(iHeapId)
	{
		Buffer.Allocate(Size, iHeapId);
		SerializeType = FeEDbSerialize::Raw;
	}

	void Clear()
	{
		Buffer.Clear();
	}
	void TrimRight(uint32 size)
	{
		Buffer.TrimRight(size);
	}
	uint32 SerializeRaw(const char* str, bool bProcessQuotes = false)
	{
		int32 iPreviousLen = Buffer.Length;
		Buffer.Concat(str);
		int32 len = FeMath::Max((int32)Buffer.Length - iPreviousLen,0);
		
		if (bProcessQuotes)
		{
			// replace ' with space
			FeStringTools::Replace(Buffer.Cstr(), '\'', ' ', iPreviousLen, Buffer.Length);
		}

		return len;
	}
	bool PrintQuotes()
	{
		switch (SerializeType)
		{
		case FeEDbSerialize::Insert_PropertiesList:
		case FeEDbSerialize::Insert_ValuesList:
			return true;
		default:
			break;
		}
		return false;
	}
	uint32 SerializeProperty(const char* value)
	{
		uint32 iCharsCount = 0;

		if (SerializeType == FeEDbSerialize::Raw)
		{
			iCharsCount += SerializeRaw(value, false);
		}
		else
		{
			const char* sPropName = Path.Indirections[Path.CurrentDepth].Name.CstrNotNull();

			if (PrintQuotes()) 
				iCharsCount += SerializeRaw("'");

			if (SerializeType == FeEDbSerialize::Insert_ValuesList)
				iCharsCount += SerializeRaw(value, true);
			else
				iCharsCount += SerializeRaw(sPropName, true);

			if (PrintQuotes())	
				iCharsCount += SerializeRaw("'");
			
			iCharsCount += SerializeRaw(", ");
		}
		return FeEReturnCode::Success;
	}
	const char* Cstr()
	{
		return Buffer.Cstr();
	}
	
	// ***** Interface functions *****
	// ------------------------------------------------------------------------------------------------------------------------------
	
	#define DECLARE_JSON_SERIALIZER_FUNCTIONS(type, token)							\
	uint32 Serialize(const type* value)	{ return SerializeT(value, token); }	\
	uint32 Deserialize(type* value)		{ return DeserializeT(value); }

	DECLARE_JSON_SERIALIZER_FUNCTIONS(bool		,"%u")
	DECLARE_JSON_SERIALIZER_FUNCTIONS(int		,"%d")
	DECLARE_JSON_SERIALIZER_FUNCTIONS(float		,"%4.2f")
	DECLARE_JSON_SERIALIZER_FUNCTIONS(uint32	,"%u")
	DECLARE_JSON_SERIALIZER_FUNCTIONS(uint64,	"%u")
	DECLARE_JSON_SERIALIZER_FUNCTIONS(FePath	,"") // special case
	DECLARE_JSON_SERIALIZER_FUNCTIONS(FeString	,"") // special case
	DECLARE_JSON_SERIALIZER_FUNCTIONS(FeAssetRef,"") // special case
	
	#define DECLARE_JSON_SERIALIZER_DUMMY(type, token)								\
	uint32 Serialize(const type* value)	{ FE_ASSERT_NOT_IMPLEMENTED_RETURN(0); }	\
	uint32 Deserialize(type* value)		{ FE_ASSERT_NOT_IMPLEMENTED_RETURN(0); }

	DECLARE_JSON_SERIALIZER_DUMMY(FeVector2		,"%u")
	DECLARE_JSON_SERIALIZER_DUMMY(FeVector3		,"%u")
	DECLARE_JSON_SERIALIZER_DUMMY(FeColor		,"%u")
	DECLARE_JSON_SERIALIZER_DUMMY(FeTransform	,"%u")
	DECLARE_JSON_SERIALIZER_DUMMY(FeGuid, "%u")

	// Serializable
	uint32 Deserialize(FeSerializable* value)
	{
		FeDbSerializable* valueDb = (FeDbSerializable*)value;

		uint32 id;
		Deserialize(&id);
		valueDb->SetID(id);
		
		if (id  != FE_INVALID_ID)
			FeDatabase::StaticInstance.Deserialize(valueDb, HeapId);

		if (SerializedData.CurrentColumn == SerializedData.ColumnsCount)
			SerializedData.Reset();

		return FeEReturnCode::Success;
	}
	uint32 Serialize(const FeSerializable* value)
	{
		const uint32 iDefaultValue = FE_INVALID_ID;

		if (value)
			return Serialize(&((FeDbSerializable*)value)->GetID());
		else
			return Serialize(&iDefaultValue);
	}

	// Array
	uint32 DeserializeArrayBegin(FeIArray* value)
	{
		FE_ASSERT_NOT_IMPLEMENTED_RETURN(0);
	}
	uint32 DeserializeArrayEnd(FeIArray* value)
	{
		return 0;
	}
	uint32 SerializeArray(const FeIArray* value)
	{
		FE_ASSERT_NOT_IMPLEMENTED_RETURN(0);
	}

	virtual uint32 DeserializePointerType(const FeString& thisClassName, FeString& output) override
	{
		output = thisClassName;
		return FeEReturnCode::Success;
	}
};


int DbResultCallback(void *userData, int argc, char **argv, char **azColName)
{
	if (userData)
	{
		FeDbResultCallback& callback = *((FeDbResultCallback*)userData);
		callback(argc, argv, azColName);
	}

	return 0;
};

class FeDatabaseImpl
{
public:

	FeDatabaseImpl()
	{
		SqlDb = nullptr;
		IsDbLoaded = false;
	}
	uint32 Load(const FePath& path)
	{
		int rc;

		FeStaticString<FE_STRING_SIZE_128> fullPath;
		FeFileTools::FormatFullPath(path, fullPath);
		rc = sqlite3_open(fullPath.Value, &SqlDb);

		if (rc)
		{
			FE_ASSERT(false, "Can't open database: %s\n", sqlite3_errmsg(SqlDb))

			return FeEReturnCode::Failed;
		}

		IsDbLoaded = true;
		
		return FeEReturnCode::Success;
	}

	uint32 ExecuteInsert(const char* szExec, uint32& ID, FeDbResultCallback* callback /*= nullptr*/)
	{
		FE_ASSERT(IsDbLoaded, "Database not  loaded !");

		uint32 iRes = Execute(szExec, callback);
		if (!FE_FAILED(iRes))
		{
			ID = (uint32)sqlite3_last_insert_rowid(SqlDb);
		}
		return iRes;
	}
	uint32 Execute(const char* szExec, FeDbResultCallback* callback /*= nullptr*/)
	{
		FE_ASSERT_NO_BREAK(IsDbLoaded, "Database not  loaded !");

		char *zErrMsg = 0;
		int rc = sqlite3_exec(SqlDb, szExec, DbResultCallback, callback, &zErrMsg);
		
		if (rc != SQLITE_OK)
		{
			FE_LOG("SQL error: %s\n", zErrMsg);

			sqlite3_free(zErrMsg);

			return FeEReturnCode::Failed;
		}
		return FeEReturnCode::Success;
	}
	uint32 GetRowID(const char* sTable, const char* sSecondaryKey, const char* sValue)
	{
		FE_ASSERT(IsDbLoaded, "Database not  loaded !");

		static uint32 iID = 0;
		static char szSql[1024];
		memset(szSql, 0, 1024);

		iID = FE_INVALID_ID;

		FeDbResultCallback callback = [](int argc, char **argv, char **azColName) -> int
		{
			if (argc==1)
				iID = (uint32)atoi(argv[0]);

			return argc == 1 ? 0 : 1;
		};
		sprintf_s(szSql, "SELECT ID FROM %s WHERE %s='%s'", sTable, sSecondaryKey, sValue);

		Execute(szSql, &callback);

		return iID;
	}
	
	template<typename T>
	uint32 FetchEntryRowID(T* pEntry)
	{
		FE_ASSERT(IsDbLoaded, "Database not  loaded !");

		uint32 RowID = pEntry->GetID();

		if (RowID == FE_INVALID_ID)
		{
			if (pEntry->HasSecondaryKey())
			{
				static char szSecondaryKeyValue[128];

				if (pEntry->GetSecondaryKeyValue(szSecondaryKeyValue, 128))
				{
					RowID = GetRowID(pEntry->GetThisClassName().Cstr(), pEntry->GetSecondaryKey(), szSecondaryKeyValue);

					if (RowID == FE_INVALID_ID)
						RowID = FeStringTools::GenerateUIntIdFromString(szSecondaryKeyValue);

					pEntry->SetID(RowID);
				}
			}
			else
			{
				FE_ASSERT(false, "trying to insert invalid row id to db");
				return FeEReturnCode::Failed;
			}
		}
		return RowID;
	}

	void Unload()
	{
		if (IsDbLoaded)
		{
			//sqlite3_close(SqlDb);
			IsDbLoaded = false;
		}
	}

public:
	sqlite3 *SqlDb;
	bool IsDbLoaded;
};

FeDatabase::~FeDatabase()
{
	if (Impl)
	{
		Impl->Unload();
		FE_DELETE(FeDatabaseImpl, Impl, 0);
	}
}

uint32 FeDatabase::Load(const FePath& path)
{
	if (!Impl)
		Impl = FE_NEW(FeDatabaseImpl, 0);

	return Impl->Load(path);
}


uint32 FeDatabase::Execute(const char* szExec, FeDbResultCallback* callback /*= nullptr*/)
{
	return Impl ? Impl->Execute(szExec, callback) : FeEReturnCode::Failed;
}
uint32 FeDatabase::ExecuteInsert(const char* szExec, uint32& ID, FeDbResultCallback* callback /*= nullptr*/)
{
	return Impl ? Impl->ExecuteInsert(szExec, ID, callback) : FeEReturnCode::Failed;
}
uint32 FeDatabase::GetRowID(const char* sTable, const char* sSecondaryKey, const char* sValue)
{
	return Impl ? Impl->GetRowID(sTable, sSecondaryKey, sValue) : FE_INVALID_ID;
}

uint32 FeDatabase::Serialize(FeDbSerializable* input)
{
	FeDbSeserializer<128> sqlSerializer(FE_HEAPID_DATABASE);
	FE_ASSERT(input->GetID() != FE_INVALID_ID, "Serialize : invalid db entry ID");

	sqlSerializer.Clear();

	sqlSerializer.SerializeRaw("INSERT OR REPLACE INTO ");
	sqlSerializer.SerializeRaw(input->GetThisClassName().Cstr());
	sqlSerializer.SerializeRaw(" (");

	// Serialize columns
	sqlSerializer.SerializeType = FeEDbSerialize::Insert_PropertiesList;
	input->Serialize(&sqlSerializer);

	sqlSerializer.TrimRight(2); // remove ', '

	sqlSerializer.SerializeRaw(") VALUES(");

	// Serialize values
	sqlSerializer.SerializeType = FeEDbSerialize::Insert_ValuesList;
	input->Serialize(&sqlSerializer);

	sqlSerializer.TrimRight(2); // remove ', '

	sqlSerializer.SerializeRaw(")");

	return Execute(sqlSerializer.Cstr());
}
uint32 FeDatabase::Deserialize(FeDbSerializable* output, uint32 iHeapId)
{
	FeDbSeserializer<128> sqlSerializer(FE_HEAPID_DATABASE);

	FE_ASSERT(output->GetID() != FE_INVALID_ID, "Deserialize : invalid db entry ID");

	sqlSerializer.Clear();

	sqlSerializer.SerializeRaw("SELECT ");
	
	sqlSerializer.SerializeType = FeEDbSerialize::Select_PropertiesList;
	output->Serialize(&sqlSerializer);
	sqlSerializer.TrimRight(2); // remove ', '
	
	sqlSerializer.SerializeType = FeEDbSerialize::Raw;
	sqlSerializer.SerializeRaw(" FROM ");
	sqlSerializer.SerializeRaw(output->GetThisClassName().Cstr());
	sqlSerializer.SerializeRaw(" WHERE ID=");
	sqlSerializer.Serialize(&output->GetID());
	
	bool bAlreadyDeserialized = false;
	uint32 iResDeserialize = FeEReturnCode::Failed;

	FeDbResultCallback callback = [&](int colCount, char **colValues, char **colNames) -> int
	{
		if (!bAlreadyDeserialized)
		{
			FE_ASSERT(colCount > 0, "invalid columns count");

			sqlSerializer.SerializedData.ColumnsCount = colCount;
			sqlSerializer.SerializedData.ColumnsNames = colNames;
			sqlSerializer.SerializedData.ColumnsValues = colValues;
			
			iResDeserialize = output->Deserialize(&sqlSerializer);
			sqlSerializer.SerializedData.Reset();

			bAlreadyDeserialized = true;
		}
		return 0;
	};
	FE_FAILEDRETURN( Execute(sqlSerializer.Cstr(), &callback) );

	if (bAlreadyDeserialized)
		return iResDeserialize;
	else
		return FeEReturnCode::Failed;
}