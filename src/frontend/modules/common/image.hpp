#pragma once 

class FeDynamicBuffer;
struct FePath;

#include "common.hpp"

namespace FeEImageFormat
{
	enum Type
	{
		PNG,
		JPEG,
		DDS,
		TARGA,
		BMP,
		RAW,

		UNKNOWN
	};
}
class FeImageHelper
{
public:
	static uint32 SaveImageFromBufferToFile(const FeDynamicBuffer& buffer, const FePath& outputPath, FeEImageFormat::Type outputFormat=FeEImageFormat::DDS);
	static FeEImageFormat::Type GetImageFormatFromBuffer(const FeDynamicBuffer& buffer);
private:
	static uint32 Initialize();
	static void DeInitialize();
	static bool IsInitialized;
};