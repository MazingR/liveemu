#pragma once 

#include "common.hpp"
#include <string.hpp>

class FeStringBuffer;
class FeDynamicBuffer;
typedef void CURL;

class FeUrlTransferHandler
{
public:
	uint32 Initialize();
	uint32 Perform(FeStringBuffer* outputBuffer, const char* url, const char* userAgent = nullptr);
	uint32 Perform(FeDynamicBuffer* outputBuffer, const char* url, const char* userAgent = nullptr);
	void Shutdown();
	
	FeUrlTransferHandler() : CurlHandle(nullptr){}
	~FeUrlTransferHandler();
private:
	CURL* CurlHandle;
	
	uint32 Perform(const char* url, const char* userAgent = nullptr);
};

struct FeEConnectionCallbackType
{
	enum Type
	{
		Read,
		Write
	};
};
struct FeTcpClientServerConnection;
typedef std::function<void(FeTcpClientServerConnection*, FeEConnectionCallbackType::Type)> ClientServerConnectionCallback;

struct FeTcpClientServerConnection
{
	uint64				Socket;
	FeString			ServerName;
	uint64				ClientSocket;
	uint32				TcpPort;
	void*				UserParam;
	ClientServerConnectionCallback	ConnectionCallback;

	FeTcpClientServerConnection() : TcpPort(0), Socket(0), ClientSocket(0), UserParam(0), ConnectionCallback(nullptr)
	{
		ServerName = "127.0.0.1";
	}

	uint64 GetComSocket() const
	{
		return ClientSocket ? ClientSocket /* as server */ : Socket /* as client*/;
	}
};

class FeNetClientImpl;

class FeNetClient
{
	FeNetClient(const FeTcpClientServerConnection& connection);

private:
	class FeNetClientImpl* Impl;
	FeTcpClientServerConnection Connection;
};

uint32 FeNetSelectSocket(uint64 _socket);

uint32 FeNetReadUint(uint64 _socket, uint32& output);
uint32 FeNetReadString(uint64 _socket, FeStringBuffer& output);

uint32 FeNetWriteUint(uint64 _socket, uint32 input);

uint32 FeNetWriteString(uint64 _socket, const FeStringBuffer& input);
uint32 FeNetWriteString(uint64 _socket, const char* input);
uint32 FeNetWriteString(uint64 _socket, const FeString& input);

void FeNetCreateClientConnection(struct FeTcpClientServerConnection& connection);
void FeNetCreateServerConnection(struct FeTcpClientServerConnection& connection);
