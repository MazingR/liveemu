#pragma once 

#include "common.hpp"

#include <gmtl/Matrix.h>
#include <gmtl/MatrixOps.h>
#include <gmtl/Vec.h>
#include <gmtl/VecOps.h>
#include <gmtl/Generate.h>
#include <color.hpp>

typedef gmtl::Vec2i FeSize;
typedef gmtl::Vec2f FeVector2;
typedef gmtl::Vec3f FeVector3;
typedef gmtl::Matrix33f FeMatrix3;
typedef gmtl::EulerAngleXYZf FeRotation;
typedef gmtl::Vec4f FeVector4;
typedef gmtl::Matrix44f FeMatrix4;

typedef FeTraversalProperty<float, FeETraversalOperation::Modulate, true> FeTraversalPropertyFloat;
typedef FeTraversalProperty<FeColor, FeETraversalOperation::Modulate, false> FeTraversalPropertyColor;
typedef FeTraversalProperty<uint32, FeETraversalOperation::Bitwise, true> FeTraversalPropertyBitFlag;

template<>
struct FeApplyTraversalOperation<FeColor, FeETraversalOperation::Modulate, true>
{
	static void Call(FeColor& traversal, FeColor& transient, FeColor& parentTraversal)
	{
		INLINE_OP_VEC4(traversal, transient, parentTraversal, *)
	}
};

const FeMatrix4 FeMatrix4Identity = gmtl::MAT_IDENTITY44F;
const FeMatrix3 FeMatrix3Identity = gmtl::MAT_IDENTITY33F;

struct FeTransform
{
	FeVector3	Translation;
	FeRotation	Rotation;
	FeVector3	Scale;

	void SetIdentity()
	{
		static float vZero[3] = { 0.f, 0.f, 0.f };
		static float vOne[3] = { 1.f, 1.f, 1.f };

		Translation.set(vZero);
		Rotation.set(0.f, 0.f, 0.f);
		Scale.set(vOne);
	}

	bool operator==(const FeTransform& other) const
	{
		return	Translation == other.Translation &&
				Rotation[0] == other.Rotation[0] &&
				Rotation[1] == other.Rotation[1] &&
				Rotation[2] == other.Rotation[2] &&
				Scale == other.Scale;
	}

	bool operator!=(const FeTransform& other) const
	{
		return	!(*this == other);
	}

	FeTransform operator*(const FeTransform& b)
	{
		FeTransform result;

		INLINE_OP_VEC4(result.Translation, Translation, b.Translation, + );
		INLINE_OP_VEC3(result.Scale, Scale, b.Scale, * );

		// todo 
		//INLINE_MUL_VEC3(result.Rotation, Rotation, b.Rotation);

		return result;
	}
};

namespace FeMatrix
{
	inline FeMatrix4 FromTranslation(FeVector3 vec) { return gmtl::makeTrans< gmtl::Matrix44f >(vec); }
	inline FeMatrix4 FromRotation(FeRotation rot) { return gmtl::make< gmtl::Matrix44f >(rot); }
	inline FeMatrix4 FromScale(FeVector3 vec) { return gmtl::makeScale< gmtl::Matrix44f >(vec); }
}
namespace FeMath
{
	template <typename T> static inline T		Min(const T& a, const T& b)
	{
		return(a < b ? a : b);
	}
	template <typename T> static inline T		Max(const T& a, const T& b)
	{
		return(a > b ? a : b);
	}
	template <typename T> static inline T       Clamp(const T& a, const T& min, const T& max)
	{
		return Min<T>(max, Max<T>(min, a));
	}
	template <typename T> static inline T       Ceil(const T& a)
	{
		return ceil(a);
	}
	template <typename T> static inline T       Floor(const T& a)
	{
		return floor(a);
	}
	template <typename T> static inline T       FMod(const T& a, const T& mod)
	{
		return fmod(a, mod);
	}
	template <typename T> static inline T		Abs(const T& val)
	{
		return(val < 0 ? -val : val);
	}
	template <typename T> static inline T		Distance(const T& valA, const T& valB)
	{
		if (valA > valB)
			return valA - valB;
		else
			return valB - valA;
	}
	template <typename T> static inline T		Range(const T& min, const T& max, const T& val)
	{
		return (max > val ? (min < val ? val : min) : max);
	}
	template <typename T> static inline float	Ratio(const T& min, const T& max, const T& val)
	{
		if (val <= min) return 0.0f;
		if (val >= max) return 1.0f;

		return float(val - min) * (1.0f / float(max - min));
	}
	inline void TransformInvert(FeVector3& vec, const FeTransform& transform)
	{
		for (int32 i = 0; i < 3;++i)
			vec[i] = (vec[i] - transform.Translation[i]) * (1.f / transform.Scale[i]);
	}
	inline void TransformInvert(FeVector2& vec, const FeTransform& transform)
	{
		for (int32 i = 0; i < 2; ++i)
			vec[i] = (vec[i] - transform.Translation[i]) * (1.f / transform.Scale[i]);
	}
	template<typename T>
	float Length(const T& vec)
	{
		return gmtl::length(vec);
	}

	inline float Sign(float value)
	{
		return value>=0.f ? 1.f : -1.f;
	}
};
