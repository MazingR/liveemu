#pragma once 

#include "common.hpp"

// Ui specific proerties

void FeRegisterTraversalProperty(FeWidget* parent, FeTArray<FeTraversalPropertyBase*>& properties, FeTraversalPropertyBase* prop, const char* sName);

template<typename T>
void FeTRegisterTraversalProperty(FeWidget* parent, FeTArray<FeTraversalPropertyBase*>& properties, T& prop, const char* sName){}

template<typename T, FeETraversalOperation::Type Operation, bool IsPropagated>
void FeTRegisterTraversalProperty(FeWidget* parent, FeTArray<FeTraversalPropertyBase*>& properties, FeTraversalProperty<T, Operation, IsPropagated>& prop, const char* sName)
{
	FeRegisterTraversalProperty(parent, properties, &prop, sName);
}

// ------------------------------------------------------------------------------------

template<typename T>
void* FeGetPropertyValuePtr(T& prop)
{
	return &prop;
}

template<typename T, FeETraversalOperation::Type Operation, bool IsPropagated>
void* FeGetPropertyValuePtr(FeTraversalProperty<T, Operation, IsPropagated>& prop)
{
	return &prop.Serialized;
}

template<typename T>
void* FeGetTransientPropertyValuePtr(T& prop)
{
	return &prop;
}

template<typename T, FeETraversalOperation::Type Operation, bool IsPropagated>
void* FeGetTransientPropertyValuePtr(FeTraversalProperty<T, Operation, IsPropagated>& prop)
{
	return &prop.Transient;
}

template<typename T>
void* FeGetTraversalPropertyValuePtr(T& prop)
{
	return &prop;
}

template<typename T, FeETraversalOperation::Type Operation, bool IsPropagated>
void* FeGetTraversalPropertyValuePtr(FeTraversalProperty<T, Operation, IsPropagated>& prop)
{
	return &prop.Traversal;
}

// ------------------------------------------------------------------------------------

template<typename T>
bool IsTypeSerializable(const T* prop)
{
	return false;
}

inline bool IsTypeSerializable(const FeSerializable* prop)
{
	return true;
}
