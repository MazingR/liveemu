#define USE_LIMIT_FPS 1
#define FPS_LIMIT 60

#include <application.hpp>
#include "filesystem.hpp"
#include "memorymanager.hpp"

#include <map>
#include <iostream>
#include <ctime>

#define SDL_MAIN_HANDLED
#include <SDL_syswm.h>
#include <SDL.h>

#include <shellapi.h>
#include <string>

#define ENABLE_EVENT_LOG 0

#if ENABLE_EVENT_LOG
	#define FE_INTERNAL_LOG(fmt, ...) FE_LOG(fmt, __VA_ARGS__)
#else
	#define FE_INTERNAL_LOG(fmt, ...)
#endif

uint32 FeFetchCommandLineParamsW(void* lpCmdLine, FeTArray<FeString>& output)
{
	int32 argc = 0;
	LPWSTR* argv = CommandLineToArgvW((LPWSTR)lpCmdLine, &argc);
	
	for (int32 i = 0; i < argc; ++i)
	{
		// convert wide char to char
		std::wstring wstr = argv[i];
		std::string str;
		str.assign(wstr.begin(), wstr.end());
		
		if (str[0] == '\''  &&  str[str.length() - 1] == '\'') // remove simple quotes (at start & end)
			str = str.substr(1, str.length() - 2);

		for (char& c : str)
		{
			if (c == '$')
				c = '\"';
		}

		FeString entry = str.c_str();

		
		output.Add(entry);
	}

	return argc;
}

FeApplication* FeApplication::s_StaticInstance = nullptr;
FeTaskScheduler* FeApplication::s_TasksScheduler = nullptr;

void FeApplication::ShowWindow()
{
	FE_ASSERT(SdlWindow, "window not created !");
	SDL_ShowWindow((SDL_Window*)SdlWindow);
}
void FeApplication::HideWindow()
{
	FE_ASSERT(SdlWindow, "window not created !");
	SDL_HideWindow((SDL_Window*)SdlWindow);
}
uint32 FeApplication::SetWindowInfosPath(const FePath& path)
{
	WindowInfosPath = path;
	uint32 iRes = FeEReturnCode::Success;

	if (FeFileTools::FileExists(path))
		iRes = FeJson::Deserialize(WindowInfos, path, FE_HEAPID_FILESYSTEM);
	else 
		iRes = FeJson::Serialize(&WindowInfos, WindowInfosPath);
	
	return iRes;
}
uint32 FeApplication::CreateAppWindow()
{
	static char szWindowName[512] = "Hello World!";

	SDL_SysWMinfo wmInfo;
	SDL_VERSION(&wmInfo.version);
	const float* dimensions = WindowInfos.GetRect().getData();

	// compute window flags
	uint32 flags = SDL_WINDOW_HIDDEN;
	
	flags |= WindowInfos.GetIsMaximized() ? SDL_WINDOW_MAXIMIZED : 0;
	flags |= SDL_WINDOW_RESIZABLE;
	
	SDL_Window* window = SDL_CreateWindow(szWindowName, (int32)dimensions[0], (int32)dimensions[1], (int32)dimensions[2], (int32)dimensions[3], flags);
	if (window == nullptr)
	{
		FE_LOG("SDL_CreateWindow Error: %s", SDL_GetError());
		SDL_Quit();
		return FeEReturnCode::Failed;
	}

	SDL_GetWindowWMInfo(window, &wmInfo);
	WindowHandle = wmInfo.info.win.window;
	SdlWindow = window;

	return FeEReturnCode::Success;
}
uint32 FeApplication::Load(const FeApplicationInit& init)
{
	s_StaticInstance = this;
	s_TasksScheduler = &TasksScheduler;

	bIsShuttingDown = false;
	WindowHandle = 0;

	ModuleHandle = GetModuleHandle(NULL);

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		std::cerr << "SDL_Init error: " << SDL_GetError() << std::endl;
		return FeEReturnCode::Failed;
	}
	
	return FeEReturnCode::Success;
}
uint32 FeApplication::Unload()
{
	// save up window infos
	FeJson::Serialize(&WindowInfos, WindowInfosPath);

	FeMemoryManager::StaticInstance.Shutdown();

	FeApplication::GetTasksScheduler()->Stop();

	for (ModulesMapIt it = Modules.begin(); it != Modules.end(); ++it)
		FE_FAILEDRETURN(it->second->Unload());


	return FeEReturnCode::Success;
}
void FeApplication::OnMouseWheel(int32 y)
{

}
void FeApplication::OnKeyPressed(uint32 key)
{

}
void FeApplication::OnKeyReleased(uint32 key)
{

}
void FeApplication::OnWindowSizeChanged(int32 width, int32 height)
{

}
void FeApplication::OnWindowEvent(const SDL_WindowEvent& winEvent)
{
	switch (winEvent.event)
	{
	case SDL_WINDOWEVENT_SHOWN:
		FE_INTERNAL_LOG("Window %d shown", winEvent.windowID);
		break;
	case SDL_WINDOWEVENT_HIDDEN:
		FE_INTERNAL_LOG("Window %d hidden", winEvent.windowID);
		break;
	case SDL_WINDOWEVENT_EXPOSED:
		FE_INTERNAL_LOG("Window %d exposed", winEvent.windowID);
		break;
	case SDL_WINDOWEVENT_MOVED:
		FE_INTERNAL_LOG("Window %d moved to %d,%d", winEvent.windowID, winEvent.data1, winEvent.data2);
		WindowInfos.GetRect().mData[0] = (float)winEvent.data1;
		WindowInfos.GetRect().mData[1] = (float)winEvent.data2;
		break;
	case SDL_WINDOWEVENT_RESIZED:
		FE_INTERNAL_LOG("Window %d resized to %dx%d", winEvent.windowID, winEvent.data1, winEvent.data2);
		WindowInfos.GetRect().mData[2] = (float)winEvent.data1;
		WindowInfos.GetRect().mData[3] = (float)winEvent.data2;

		OnWindowSizeChanged(winEvent.data1, winEvent.data2);
		break;
	case SDL_WINDOWEVENT_SIZE_CHANGED:
		FE_INTERNAL_LOG("Window %d size changed to %dx%d", winEvent.windowID, winEvent.data1, winEvent.data2);
		WindowInfos.GetRect().mData[2] = (float)winEvent.data1;
		WindowInfos.GetRect().mData[3] = (float)winEvent.data2;

		OnWindowSizeChanged(winEvent.data1, winEvent.data2);
		break;
	case SDL_WINDOWEVENT_MINIMIZED:
		FE_INTERNAL_LOG("Window %d minimized", winEvent.windowID);
		break;
	case SDL_WINDOWEVENT_MAXIMIZED:
		WindowInfos.SetIsMaximized(true);
		FE_INTERNAL_LOG("Window %d maximized", winEvent.windowID);
		break;
	case SDL_WINDOWEVENT_RESTORED:
		WindowInfos.SetIsMaximized(false);
		FE_INTERNAL_LOG("Window %d restored", winEvent.windowID);
		break;
	case SDL_WINDOWEVENT_ENTER:
		FE_INTERNAL_LOG("Mouse entered window %d",
			winEvent.windowID);
		break;
	case SDL_WINDOWEVENT_LEAVE:
		FE_INTERNAL_LOG("Mouse left window %d", winEvent.windowID);
		break;
	case SDL_WINDOWEVENT_FOCUS_GAINED:
		FE_INTERNAL_LOG("Window %d gained keyboard focus",
			winEvent.windowID);
		break;
	case SDL_WINDOWEVENT_FOCUS_LOST:
		FE_INTERNAL_LOG("Window %d lost keyboard focus",
			winEvent.windowID);
		break;
	case SDL_WINDOWEVENT_CLOSE:
		FE_INTERNAL_LOG("Window %d closed", winEvent.windowID);
		break;
#if SDL_VERSION_ATLEAST(2, 0, 5)
	case SDL_WINDOWEVENT_TAKE_FOCUS:
		FE_INTERNAL_LOG("Window %d is offered a focus", winEvent.windowID);
		break;
	case SDL_WINDOWEVENT_HIT_TEST:
		FE_INTERNAL_LOG("Window %d has a special hit test", winEvent.windowID);
		break;
#endif
	default:
		FE_INTERNAL_LOG("Window %d got unknown event %d",
			winEvent.windowID, winEvent.event);
		break;
	}
}
uint32 FeApplication::Run()
{
	SDL_Event e;
	uint32 iTicks = SDL_GetTicks();
	uint32 iMaxPolledEvents = 1;

	FeDt fDt;
	ZeroMemory(&fDt, sizeof(fDt));

	bool bFirstFrame = true;

	while (!bIsShuttingDown)
	{
		uint32 iPreviousTicks = iTicks;
		uint32 iPolledEvents = 0;

		iTicks = SDL_GetTicks();
		fDt.TotalMilliseconds = iTicks - iPreviousTicks;
		fDt.TotalSeconds = fDt.TotalMilliseconds / 1000.0f;

		while (iPolledEvents++<iMaxPolledEvents && SDL_PollEvent(&e))
		{
			switch (e.type)
			{
			case SDL_KEYDOWN:
				OnKeyPressed(e.key.keysym.scancode);
				break;
			case SDL_KEYUP:
				OnKeyReleased(e.key.keysym.scancode);
				break;
			case SDL_QUIT:
				bIsShuttingDown = true;
				break;
			case SDL_WINDOWEVENT:
				OnWindowEvent(e.window);
				break;
			case SDL_MOUSEWHEEL:
				OnMouseWheel(e.wheel.y);

			}
		}

		if (!bIsShuttingDown)
		{
			for (ModulesMapIt it = Modules.begin(); it != Modules.end(); ++it)
			{
				FE_FAILEDRETURN(it->second->Update(fDt));
			}
#if USE_LIMIT_FPS
			// Limit framerate (60fps)
			uint32 iFrameTicks = SDL_GetTicks() - iTicks;
			fDt.TotalCpuWaited = (1000 / FPS_LIMIT) - iFrameTicks;

			if (fDt.TotalCpuWaited > 0)
				SDL_Delay(fDt.TotalCpuWaited);
#endif
		}

		if (bFirstFrame)
		{
			ShowWindow();
			bFirstFrame = false;
		}
	}

	return FeEReturnCode::Success;
}
void FeApplication::Quit()
{
	bIsShuttingDown = true;
}