#include "asset.hpp"
#include "filesystem.hpp"


FeAsset::FeAsset()
{
	ID.Create();
}

void FeAssetRegistry::LoadFiles(const FePath& path, const char* filter)
{
	FeStaticString<FE_STRING_SIZE_512> WorkString;

	FeFileTools::FormatFullPath(path, WorkString);
	LoadFiles(WorkString.Value, filter);
}
void FeAssetRegistry::LoadFiles(const char* path, const char* filter)
{
	FeTArray<FePath> files;
	files.SetHeapId(Files.GetHeapId());
	FeFileTools::List(files,path, filter);

	Files.Reserve(Files.GetSize()+files.GetSize());

	for (auto& file : files)
	{
		auto& assetFilePtr = Files.Add();
		auto assetFile = assetFilePtr.New();

		uint32 iRes = FeJson::Deserialize(*assetFile, file, Files.GetHeapId());

		if (FE_SUCCEEDED(iRes))
		{
			auto asset = assetFile->GetAsset();

			assetFile->ID = asset->ID;
			assetFile->ID.Guid = asset->ID.Guid;

			assetFile->Path = file;
			assetFile->AssetRef.Type = assetFile->Asset->Type;
			assetFile->AssetRef.Path = assetFile->Path;
		}
		else
		{
			Files.PopBack();
		}
	}
}
FeAssetRef FeAssetRegistry::ComputeNewAssetRef(const FePath& dirPath, FeAsset* asset)
{
	FeAssetRef newAssetRef;
	newAssetRef.Type = asset->Type;

	int32 widgetInstanceIdx = 1;
	do
	{
		FeStringTools::Format(asset->ID.Name, "%s%d", asset->Type.Cstr(), widgetInstanceIdx); // compute asset name
		FeStaticString<FE_STRING_SIZE_128> tmp;
		FeFileTools::FormatFullPath(dirPath, tmp, "/%s.fes", asset->ID.Name.Cstr());
		newAssetRef.Path = tmp.Value;


		widgetInstanceIdx++;
	} while (Files.Contains([&](const FeTPtr<FeAssetFile> o) -> bool { return o.Get()->AssetRef == newAssetRef; }));

	return newAssetRef;
}