#pragma once 

#include "common.hpp"

#include "pointer.hpp"
#include "factory.hpp"
#include "maths.hpp"

#include "serializer.hpp"
#include "serializablepreprocessor.hpp"
#include "serializableenum.hpp"
#include "serializabletools.hpp"
#include "propertyaccessor.hpp"
#include "pointer.hpp"

namespace FeESerializeFormat
{
	enum Type
	{
		Json,
		Database
	};
}

class FeSerializable : public FeISelfDeletable
{
private:
	const FeString ClassName = "FeSerializable";
public:
	virtual const FeString& GetThisClassName() const
	{
		return ClassName;
	}
	virtual bool IsChildOf(const FeString& className) const
	{
		return className == ClassName;
	}
	virtual void SelfDelete(uint32 iHeapId)
	{
		FE_DELETE(FeSerializable, this, iHeapId);
	}

	virtual FeSerializable* CreateInstance(uint32 heapId) const	{ return nullptr; }

	virtual void CopyProperties(FeSerializable* output) const { }
	virtual void LosePointersOwnership() { }
	virtual FeIPropertyAccessor* CreatePropertyAccessor(const FeString& propertyName, uint32 iHeapId = DEFAULT_HEAP) { return nullptr; }
	virtual bool CreatePropertyAccessor(const FeString& propertyName, FePropertyAccessor& output) { return false; }
	virtual bool GetIsPropertySerializable(const FeString& propertyName, void*& propertyPtr) { return false; }
	
	virtual void* GetPropertyByName(const FeString& propertyName) { return nullptr; }
	virtual void* GetTransientPropertyByName(const FeString& propertyName) { return nullptr; }
	virtual void* GetTraversalPropertyByName(const FeString& propertyName) { return nullptr; }

	virtual uint32 Serialize(FeSerializer* output) const { return FeEReturnCode::Failed; }
	virtual uint32 Deserialize(FeSerializer* input) { return FeEReturnCode::Failed; }
	virtual void OnDeserialized() { }
};

// ------------------------------------------------------------------------------------------------------------------------------------------------------------
#define DECLARE_SERIALIZE(type) inline uint32 FeSerialize(FeSerializer* output, const FeString& sPropName, type input)		\
{ 																															\
	output->Push(sPropName);																								\
	uint32 iRes = output->Serialize(input);																					\
	output->Pop();																											\
	return iRes;																											\
}

#define DECLARE_DESERIALIZE(type) inline uint32 FeDeserialize(FeSerializer* input, const FeString& sPropName, type output)	\
{																															\
	input->Push(sPropName);																									\
	uint32 iRes = input->Deserialize(output);																				\
	input->Pop();																											\
	return iRes;																											\
}

#define DECLARE_SERIALIZE_FUNCTIONS(type)	\
DECLARE_SERIALIZE(const type)				\
DECLARE_DESERIALIZE(type)

#define DECLARE_SERIALIZE_FUNCTIONS_T(type)	\
template<typename T>						\
DECLARE_SERIALIZE(const type)				\
template<typename T>						\
DECLARE_DESERIALIZE(type)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------

DECLARE_SERIALIZE_FUNCTIONS(bool*)
DECLARE_SERIALIZE_FUNCTIONS(int*)
DECLARE_SERIALIZE_FUNCTIONS(float*)
DECLARE_SERIALIZE_FUNCTIONS(uint32*)
DECLARE_SERIALIZE_FUNCTIONS(uint64*)
DECLARE_SERIALIZE_FUNCTIONS(FePath*)
DECLARE_SERIALIZE_FUNCTIONS(FeVector2*)
DECLARE_SERIALIZE_FUNCTIONS(FeVector3*)
DECLARE_SERIALIZE_FUNCTIONS(FeColor*)
DECLARE_SERIALIZE_FUNCTIONS(FeTransform*)
DECLARE_SERIALIZE_FUNCTIONS(FeString*)
DECLARE_SERIALIZE_FUNCTIONS(FeSerializable*)
DECLARE_SERIALIZE_FUNCTIONS(FeAssetRef*)
DECLARE_SERIALIZE_FUNCTIONS(FeGuid*);

DECLARE_SERIALIZE_FUNCTIONS_T(FeTArray<T>*)
DECLARE_SERIALIZE_FUNCTIONS_T(FeTPtr<T>*)


template<typename T, FeETraversalOperation::Type Op, bool Propa>
inline uint32 FeSerialize(FeSerializer* output, const FeString& sPropName, const FeTraversalProperty<T, Op, Propa>* input)	
{ 
	output->Push(sPropName);
	uint32 iRes = output->Serialize(input); 
	output->Pop();
	return iRes;
}


template<typename T, FeETraversalOperation::Type Op, bool Propa>
inline uint32 FeDeserialize(FeSerializer* input, const FeString& sPropName, FeTraversalProperty<T, Op, Propa>* output)	
{
	input->Push(sPropName);
	uint32 iRes = input->Deserialize(output);
	input->Pop();
	return iRes;
}

// ------------------------------------------------------------------------------------------------------------------------------------------------------------

#define DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(type)		\
inline void TransportData(type* to, const type* from)	\
{														\
}														\

DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(bool)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(int)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(float)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(uint32)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(uint64)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(FePath)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(FeVector2)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(FeVector3)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(FeColor)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(FeTransform)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(FeString)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(FeSerializable)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(FeAssetRef)
DECLARE_LOSE_PTR_OWNERSHIP_FUNCTIONS(FeGuid)

template<typename T>
void LosePointerOwnership(FeTPtr<T>* to, const FeTPtr<T>* from)
{
	value->Owner = false;
}

template<typename T>
void LosePointerOwnership(FeTArray<T>* value)
{
	for (auto& entry : (*value))
	{
		LosePointerOwnership(&entry);
	}
}
