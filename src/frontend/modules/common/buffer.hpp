#pragma once 

#include "common.hpp"

class FeDynamicBuffer
{
public:
	uint32 Size;
	uint32 Heap;
	uint32 Length;
	void* Data;

	uint32 GetAvailableSize() { return Size > Length ? Size - Length : 0; }

	FeDynamicBuffer() : FeDynamicBuffer(0, FE_INVALID_ID) {}

	FeDynamicBuffer(uint32 size, uint32 heap);
	~FeDynamicBuffer();

	void Free();
	void Clear();
	void Memset(char value);
	void Allocate(uint32 size, uint32 heap);
	uint32 Write(const void* input, uint32 size);
};