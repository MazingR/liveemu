#pragma once 

#include "common.hpp"
#include <json.hpp>
#include "maths.hpp"

// -------------------------------------------------------------------------------------------------------------------------
// Enum declaration preprocessing for serialization
// -------------------------------------------------------------------------------------------------------------------------
#define FE_DECLARE_ENUM_VALUE(_name_, _value_)	\
	_value_,									\

#define FE_DECLARE_STRING_ENUM_MAP_ENTRY(_name_, _value_)									\
	FeStringPool::GetInstance()->CreateString( #_value_, DoGetValues()[Type::_value_]);		\
	DoGetIdsMap()[DoGetValues()[Type::_value_].Id()] = _name_::_value_;						\


#define FE_DECLARE_STRING_ENUM_MAP(_name_, _values_)																\
	static FeString* GetValues() 																					\
	{ 																												\
		InitStaticMap();																							\
		return DoGetValues();																						\
	}																												\
	static std::map<uint32, _name_::Type>& GetIdsMap() 																\
	{ 																												\
		InitStaticMap();																							\
		return DoGetIdsMap();																						\
	}																												\
	static Type FromStringId(uint32 id, const FeString* str=nullptr)												\
	{																												\
		InitStaticMap();																							\
		FE_ASSERT(GetIdsMap().find(id) != GetIdsMap().end(),														\
		"unknown enum value %s to map from enum %s !", str?str->CstrNotNull() : "",  #_name_);						\
		return GetIdsMap()[id];																						\
	}																												\
	static Type FromString(const FeString& str)																		\
	{																												\
		return FromStringId(str.Id(),&str);																			\
	}																												\
	static int32 FromStringToInt(const FeString& str)																\
	{																												\
		return (int32)FromStringId(str.Id(),&str);																	\
	}																												\

#define FE_DECLARE_ENUM_TOSTRING(_name_, _value_)	\
	static FeString __str_##_value_ = #_value_;		\
	if (value == _value_) return __str_##_value_;


#define FE_DECLARE_ENUM(_name_, _values_)																						\
	struct _name_																												\
	{																															\
		enum Type																												\
		{																														\
			_values_(FE_DECLARE_ENUM_VALUE)																						\
			Count																												\
		};																														\
	private:																													\
		static FeString* DoGetValues() 																							\
		{ 																														\
			static FeString Values[Type::Count];																				\
			return Values;																										\
		}																														\
		static std::map<uint32, _name_::Type>& DoGetIdsMap() 																	\
		{ 																														\
			static std::map<uint32, _name_::Type> IdsMap;																		\
			return IdsMap;																										\
		}																														\
		static void InitStaticMap()																								\
		{																														\
			static bool bIsMapInit = false;																						\
																																\
			if (!bIsMapInit)																									\
			{																													\
				bIsMapInit = true;																								\
				_values_(FE_DECLARE_STRING_ENUM_MAP_ENTRY)																		\
			}																													\
		}																														\
	public:																														\
		static const FeString& ToString(_name_::Type value)																		\
		{																														\
			_values_(FE_DECLARE_ENUM_TOSTRING)																					\
			return FeString::EMPTY;																								\
		}																														\
		FE_DECLARE_STRING_ENUM_MAP(_name_, _values_)																			\
	};																															\
	inline void TransportData(_name_::Type* to, const _name_::Type* from)														\
	{																															\
		*to = *from;																											\
	}																															\
	static uint32 FeDeserialize(FeSerializer* serializer, const FeString& sPropName, _name_::Type* output)						\
	{																															\
		serializer->Push(sPropName);																							\
		FE_FAILEDRETURN(serializer->DeserializeEnum(&_name_::GetValues()[0], _name_::Type::Count, _name_::FromStringToInt, (int32*)output));		\
		serializer->Pop();																										\
		return FeEReturnCode::Success;																							\
	}																															\
	static uint32 FeSerialize(FeSerializer* serializer, const FeString& sPropName, const _name_::Type* input)					\
	{																															\
		serializer->Push(sPropName);																							\
		FE_FAILEDRETURN(serializer->Serialize( & (_name_::ToString(*input)) ));													\
		serializer->Pop();																										\
		return FeEReturnCode::Success;																							\
	}																															\

