#pragma once 

#include "guid.hpp"
#include "serializable.hpp"

class FeNamedGUID : public FeSerializable
{
public:
	FeNamedGUID() {}
	FeNamedGUID(const FeGuid& guid, const FeString& name) : Guid(guid), Name(name) { }
	
	void OnDeserialized() override;
	bool IsValid() const;
	bool operator==(const FeNamedGUID& other) const;
	bool operator!=(const FeNamedGUID& other) const;
	bool operator==(const FeGuid& other) const;
	bool operator!=(const FeGuid& other) const;

	void Create();

public:
#define FeNamedGUID_(_d)			\
		_d(FeGuid,		Guid)		\
		_d(FeString,	Name)		\


	FE_DECLARE_CLASS_BODY(FeNamedGUID_, FeNamedGUID, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeNamedGUID)
