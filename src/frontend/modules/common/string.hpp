#pragma once 

#include "common.hpp"
#include "buffer.hpp"
#include <tarray.hpp>
#include <map>
#include <atomic>

struct FeString;

namespace FeStringTools
{
	uint32 GenerateUIntIdFromString(const char* cptr, const char* cptr2 = nullptr, uint32  _crc = 0, uint32 len=0);
	void ToLower(char* szStr);
	void ToUpper(char* szStr);
	
	void Format(FeString& output, const char* sFormat, ...);
	FeString Format(const char* sFormat, ...);

	size_t IndexOf(const char* szString, char szChar, size_t iStart=0, size_t iEnd=0);
	size_t LastIndexOf(const char* szString, char szChar, size_t iStart = 0, size_t iEnd = 0);

	size_t IndexOf(const char* szString, const char* szFind, size_t iStart = 0, size_t iEnd =0);

	size_t Count(const char* szString, char szChar, size_t iStart = 0, size_t iEnd =0);
	size_t Replace(char* szString, char szFind, char szReplace, size_t iStart = 0, size_t iEnd = 0);
	size_t Remove(char* szString, char* szFind, size_t iStart = 0, size_t iEnd = 0);
	size_t Replace(char* szString, char* szFind, char* szReplace, size_t iStart = 0, size_t iEnd = 0);
	size_t TrimEnd(char* szString, char szChar);

	bool HasLeft(const char* szString, const char* szTrimed, uint32 strLimit = 0);
	bool TrimLeft(char** szString, const char* szTrimed, uint32 strLimit=0);

	void Concat(const char* a, const char* b, FeString* pOutput);

	FeString Concat(const FeString& a, const FeString& b);
	FeString Concat(const FeString& a, const char* b);
	FeString Concat(const char* a, const char* b);
}

struct FePooledString
{
	uint32 Id;
	size_t Size;
	char * Cstr;
	std::atomic<int32> RefCount;

	FePooledString(uint32 size);
	~FePooledString();
};

struct FeString
{
	friend class FeStringPool;

public:
	static FeString EMPTY;

	inline const char* Cstr() const { return Pooled ? Pooled->Cstr : nullptr; }
	inline const char* CstrNotNull() const { return Pooled ? Pooled->Cstr : ""; }
	inline const uint32 Id() const { return Pooled ? Pooled->Id : 0; }
	
	FeString() : Pooled(nullptr) {}
	FeString(FePooledString& pooledStr);
	FeString(const FeString& copy);
	FeString(const char* other);
	FeString(const char other);
	~FeString();

	FeString& operator=(const FeString& other);
	FeString& operator=(const char* other);
	FeString& operator=(const char other);
	FeString operator+(const char* other);
	
	bool IsEmpty() const
	{
		return !Pooled;
	}
	bool operator==(const FeString& other) const
	{
		return other.Id() == this->Id();
	}

	bool operator!=(const FeString& other) const
	{
		return !(other== (*this));
	}
	void SetPooledStr(FePooledString* pooledStr);

	size_t Length() const
	{
		return Pooled ? strlen(Cstr()) : 0;
	}
	void Clear();

private:
	FePooledString* Pooled;
};

class FeStringPool
{
public:
	static FeStringPool* GetInstance();
	static void Initialize();

	FeString CreateString(const char* szValue);
	void FeStringPool::CreateString(const char* szValue, FeString& output);
	void FeStringPool::CreateString(const char* szValue, uint32 len, FeString& output);
	void FeStringPool::CreateString(const char sChar, FeString& output);

	void CreateStringFromInt32(int32 value, FeString& output);
	void CreateStringFromUInt32(uint32 value, FeString& output);

	int32 IncRef(FePooledString* pooledStr);
	int32 DecRef(FePooledString* pooledStr);

	void DeleteString(FeString& str);

private:
	
	FePooledString* CreatePooledString(const char* szValue, uint32 len=0);
	FePooledString* CreatePooledString(const char szValue);
	
	typedef std::map<uint32, FePooledString*> StringPoolMap;
	typedef StringPoolMap::iterator StringPoolMapIt;

	struct FeStringPoolMap
	{
		uint32 StringSizeLimit;
		StringPoolMap Map;
		
		FeStringPoolMap() {}
		FeStringPoolMap(uint32 sizeLimit) : StringSizeLimit(sizeLimit) {}
	};

	StringPoolMap* GetPoolFromStringSize(uint32 iLen);

	FeTArray<FeStringPoolMap> Pools;
};
class FeStringBuffer : public FeDynamicBuffer
{
public:
	char* Cstr() const
	{
		return (char*)Data;
	}
	void TrimRight(uint32 size);

	uint32 Concat(const char* str, uint32 size = 0);
	uint32 Printf(const char *format, ...);

private:
};

template<uint32 Size>
struct FeStaticString
{
	char Value[Size];
	uint32 Length;

	void Clear()
	{
		Length = 0;
		Value[0] = '\0';
	}

	FeStaticString<Size>& operator<<(const char* str)
	{
		uint32 len = FeMath::Min(Size - Length - 1, strlen(str));
		if (len + Length < Size)
		{
			memcpy_s(Value + Length, len, str, len);
			Length += len;

			Value[Length] = '\0';
		}
		return *this;
	}
	FeStaticString<Size>& operator=(const char* str)
	{
		uint32 len = FeMath::Min(Size - 1, strlen(str));
		memcpy_s(Value, len, str, len);
		Length = len;
		Value[Length] = '\0';
		return *this;
	}
};