#pragma once 

#include "common.hpp"
#include <string.hpp>
#include <serializable.hpp>

#define FE_HEAPID_DATABASE 6
#define FE_HEAPNAME_DATABASE "Database"

class FeDbSerializable : public FeSerializable
{
public:
#define props(_d)							\
	_d(uint32,		ID)						\

	virtual const char* GetSecondaryKey() { return ""; }

protected:
	template<typename T>
	void ComputeValueToString(char* output, size_t size, const T* value)
	{
		FE_ASSERT_NOT_IMPLEMENTED;
	}

	void ComputeValueToString(char* output, size_t size, const FeString* value)
	{
		sprintf_s(output, size, "%s", value->Cstr());
	}

	void ComputeValueToString(char* output, size_t size, const uint32* value)
	{
		sprintf_s(output, size, "%u", *value);
	}

	template<typename T>
	void ComputeValueToString(char* output, size_t size, const FeTPtr<T>* value)
	{
		if (value->IsValid())
			sprintf_s(output, size, "%u", value->Get()->GetID());
	}

	template<typename T>
	bool GetSecondaryKeyTValue(char* output, size_t size)
	{
		const T* value = (const T*)GetPropertyByName(GetSecondaryKey());
		if (value)
		{
			ComputeValueToString(output, size, value);
			return true;
		}
		return false;
	}

public:
	FeDbSerializable()
	{
		ID = FE_INVALID_ID;
	}

	virtual bool GetSecondaryKeyValue(char* output, size_t size) { return GetSecondaryKeyTValue<FeString>(output, size); }

	bool HasSecondaryKey() { return strlen(GetSecondaryKey()) > 0; }

	FE_DECLARE_CLASS_BODY(props, FeDbSerializable, FeSerializable)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeDbSerializable)

// ------------------------------------------------------------------------------------------------------------------------------------

typedef std::function<int(int argc, char **argv, char **azColName)> FeDbResultCallback;

class FeDatabaseImpl;
class FeDatabase
{
public:
	static FeDatabase StaticInstance;
	~FeDatabase();

	uint32 Load(const FePath& path);
	uint32 Execute(const char* szExec, FeDbResultCallback* callback=nullptr);
	uint32 ExecuteInsert(const char* szExec, uint32& ID, FeDbResultCallback* callback = nullptr);
	uint32 GetRowID(const char* sTable, const char* sSecondaryKey, const char* sValue);

	uint32 Serialize(FeDbSerializable* input);
	uint32 Deserialize(FeDbSerializable* output, uint32 iHeapId);

	template<class T>
	uint32 Deserialize(T& output, uint32 iHeapId)
	{
		return Deserialize(&output, iHeapId);
	}
private:
	FeDatabaseImpl* Impl;
};
