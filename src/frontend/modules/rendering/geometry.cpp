#include <geometry.hpp>
#include <modulerenderer.hpp>

#include <d3dx11include.hpp>
#include <xnamath.h>

#define D3DFAILEDRETURN(func) { HRESULT ___hr = (func); if (___hr!=S_OK) return ___hr; }

struct Vertex_T0
{
	XMFLOAT3 Position;
	XMFLOAT2 Texcoord0;
};

FeTArray<FeRenderGeometryData> FeGeometryHelper::StaticGeometryData;
FeTArray<FeRenderGeometryData> FeGeometryHelper::GeometryData;
bool FeGeometryHelper::ComputedGeometry = false;

const FeMatrix4& FeGeometryHelper::IdentityMatrix()
{
	return FeMatrix4Identity;
}
void FeRenderGeometryData::Release()
{
	SafeRelease(VertexBuffer);
	SafeRelease(IndexBuffer);
}
uint32 FeGeometryHelper::CreateVertexAndIndexBuffer(ID3D11Buffer** pVertexBuffer, ID3D11Buffer** pIndexBuffer, void* vertexData, uint32 vertexCount, void* indexData, uint32 indexCount)
{
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;
	{
		ZeroMemory(&bd, sizeof(bd));
		ZeroMemory(&InitData, sizeof(InitData));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(Vertex_T0) * vertexCount;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		InitData.pSysMem = vertexData;

		D3DFAILEDRETURN(FeModuleRendering::GetDevice().GetD3DDevice()->CreateBuffer(&bd, &InitData, pVertexBuffer));
	}
	{
		ZeroMemory(&bd, sizeof(bd));
		ZeroMemory(&InitData, sizeof(InitData));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(uint16) * indexCount;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		InitData.pSysMem = indexData;

		D3DFAILEDRETURN(FeModuleRendering::GetDevice().GetD3DDevice()->CreateBuffer(&bd, &InitData, pIndexBuffer));
	}
	return FeEReturnCode::Success;
}
void FeGeometryHelper::AddStaticGeometry(FeEGemetryDataType::Type type, void* vertexData, int32 vertexCount, uint16* indexData, int32 indexCount)
{
	StaticGeometryData[type].Stride = sizeof(Vertex_T0);
	StaticGeometryData[type].IndexCount = indexCount;

	CreateVertexAndIndexBuffer(
		&StaticGeometryData[type].VertexBuffer,
		&StaticGeometryData[type].IndexBuffer, vertexData, vertexCount, indexData, indexCount);
}
uint32 FeGeometryHelper::ComputeStaticGeometry()
{
	StaticGeometryData.Resize(FeEGemetryDataType::Count);	
	StaticGeometryData.SetZeroMemory();
		
	{ // Quad
		// Create vertex buffer
		Vertex_T0 vertexData[] =
		{
			{ XMFLOAT3(0.0f, 0.0f, 0.f), XMFLOAT2(0.f, 0.f) },
			{ XMFLOAT3(1.0f, 0.0f, 0.f), XMFLOAT2(1.f, 0.f) },
			{ XMFLOAT3(1.0f,-1.0f, 0.f), XMFLOAT2(1.f, 1.f) },
			{ XMFLOAT3(0.0f,-1.0f, 0.f), XMFLOAT2(0.f, 1.f) },
		};
		//for (auto& vertex : vertexData)
		//{
		//	vertex.Position.x -= 0.5f;
		//	vertex.Position.y += 0.5f;
		//}

		uint16 indexData[] = { 0, 1, 2, 0, 2, 3 };
		AddStaticGeometry(FeEGemetryDataType::Quad, vertexData, 4, indexData, 6);
	}
	{ // Triangle
		// Create vertex buffer
		Vertex_T0 vertexData[] =
		{
			{ XMFLOAT3(0.0f, 0.0f, 0.f), XMFLOAT2(0.f, 0.f) },
			{ XMFLOAT3(1.0f, 0.0f, 0.f), XMFLOAT2(1.f, 0.f) },
			{ XMFLOAT3(1.0f, -1.0f, 0.f), XMFLOAT2(1.f, 1.f) },
		};
		//for (auto& vertex : vertexData)
		//{
		//	vertex.Position.x -= 0.5f;
		//	vertex.Position.y += 0.5f;
		//}
		uint16 indexData[] = { 0, 1, 2 };
		AddStaticGeometry(FeEGemetryDataType::Triangle, vertexData, 3, indexData, 3);
	}

	return FeEReturnCode::Success;
}
FeResourceId FeGeometryHelper::GetStaticGeometry(FeEGemetryDataType::Type eType)
{
	return (FeResourceId)eType + 1;
}
uint32 FeGeometryHelper::CreateStaticGeometry(FeEGemetryDataType::Type eType, FeRenderGeometryData* geometryData, FeResourceId* geometryId)
{
	if (!ComputedGeometry)
	{
		FE_FAILEDRETURN(ComputeStaticGeometry());
		ComputedGeometry = true;
	}
	*geometryData = StaticGeometryData[eType];
	*geometryId = (FeResourceId)eType + 1;

	return FeEReturnCode::Success;
}
uint32 FeGeometryHelper::CreateGeometry(void* vertexBuffer, uint32 iVertexCount, void* indexBuffer, uint32 iIndexCount, FeRenderGeometryData* geometryData, FeResourceId* geometryId)
{
	FeRenderGeometryData& newGeometryData = GeometryData.Add();
	*geometryId = GeometryData.GetSize();
	return FeEReturnCode::Success;
}

FeMatrix4 offset = FeMatrix::FromTranslation(FeVector3(0.5f, -0.5f, 0.0f));

void FeGeometryHelper::ComputeMatrix(FeMatrix4& output, const FeTransform& transform)
{
	ComputeMatrix(output, transform.Translation, transform.Rotation, transform.Scale);
}
void FeGeometryHelper::ComputeMatrix(FeMatrix4& output, const FeVector3 vTranslate, const FeRotation& vRotate, const FeVector3& vScale)
{
	//output = offset;
	output = FeMatrix4();
	output *= FeMatrix::FromTranslation(vTranslate);
	output *= FeMatrix::FromRotation(vRotate);
	output *= FeMatrix::FromScale(vScale);
}
void FeGeometryHelper::ReleaseGeometryData()
{
	for (uint32 i = 0; i < GeometryData.GetSize(); ++i)
		GeometryData[i].Release();
}
void FeGeometryHelper::ReleaseStaticGeometryData()
{
	for (uint32 i = 0; i < StaticGeometryData.GetSize(); ++i)
		StaticGeometryData[i].Release();

	ComputedGeometry = false;
}
float* FeGeometryHelper::GetMatrixTranslationData(FeMatrix4& mat)
{
	return &(mat.mData[12]);
}
float FeGeometryHelper::GetMatrixTranslationX(const FeMatrix4& mat)
{
	return (mat.getData()[12]);
}
float FeGeometryHelper::GetMatrixTranslationY(const FeMatrix4& mat)
{
	return (mat.getData()[13]);
}
float FeGeometryHelper::GetMatrixTranslationZ(const FeMatrix4& mat)
{
	return (mat.getData()[14]);
}

float FeGeometryHelper::GetMatrixScaleX(const FeMatrix4& mat)
{
	return (mat.getData()[0]);
}
float FeGeometryHelper::GetMatrixScaleY(const FeMatrix4& mat)
{
	return (mat.getData()[5]);
}
float FeGeometryHelper::GetMatrixScaleZ(const FeMatrix4& mat)
{
	return (mat.getData()[10]);
}
float& FeGeometryHelper::GetMatrixTranslationX(FeMatrix4& mat)
{
	return (mat.mData[12]);
}
float& FeGeometryHelper::GetMatrixTranslationY(FeMatrix4& mat)
{
	return (mat.mData[13]);
}
float& FeGeometryHelper::GetMatrixTranslationZ(FeMatrix4& mat)
{
	return (mat.mData[14]);
}

float& FeGeometryHelper::GetMatrixScaleX(FeMatrix4& mat)
{
	return (mat.mData[0]);
}
float& FeGeometryHelper::GetMatrixScaleY(FeMatrix4& mat)
{
	return (mat.mData[5]);
}
float& FeGeometryHelper::GetMatrixScaleZ(FeMatrix4& mat)
{
	return (mat.mData[10]);
}