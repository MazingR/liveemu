#include <commonrenderer.hpp>
#include <geometry.hpp>
#include <filesystem.hpp>
#include <d3dx11include.hpp>
#include <ft2build.h>
#include <freetype/freetype.h>
#include <string.hpp>

#include <d3dx11include.hpp>
#include <d3dcompiler.h>

namespace FeMathHelper
{
	bool IntersectPointWithRect(const FeVector2& Point, const FeMatrix4& Rect, float offset /*= 0.0f*/)
	{
		float fTx = FeGeometryHelper::GetMatrixTranslationX(Rect);
		float fTy = FeGeometryHelper::GetMatrixTranslationY(Rect);
		float fTz = FeGeometryHelper::GetMatrixTranslationZ(Rect);

		float fSx = FeGeometryHelper::GetMatrixScaleX(Rect);
		float fSy = FeGeometryHelper::GetMatrixScaleY(Rect);

		return	Point[0] >= (fTx - offset)
			&& Point[1] <= (fTy + offset)
			&& Point[0] <= (fTx + fSx + offset)
			&& Point[1] >= (fTy - fSy - offset);
	}
}

namespace FeRenderTools
{
	uint32 CompileShaderFromMemory(const char* srcData, size_t srcDataLen, const char* szFileName, const char* szEntryPoint, const char* szShaderModel, void** ppBlobOut)
	{
		DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
		dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

		ID3DBlob* pErrorBlob;
		auto hr = D3DX11CompileFromMemory(srcData, srcDataLen, szFileName, nullptr, nullptr, szEntryPoint, szShaderModel,
			dwShaderFlags, 0, nullptr, (ID3DBlob**)ppBlobOut, &pErrorBlob, nullptr);

		if (FAILED(hr))
		{
			FE_LOG_ERROR("Shader compile error '%s' (HR=%d) : %s ", szFileName, hr, pErrorBlob ? (char*)pErrorBlob->GetBufferPointer() : "Unknown error");

			SafeRelease(pErrorBlob);

			return FeEReturnCode::Rendering_CreateShaderFailed;
		}

		SafeRelease(pErrorBlob);
		return FeEReturnCode::Success;
	}
	uint32 CompileShaderFromFile(const char* szFileName, const char* szEntryPoint, const char* szShaderModel, void** ppBlobOut)
	{
		DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
	#if defined( DEBUG ) || defined( _DEBUG )
		dwShaderFlags |= D3DCOMPILE_DEBUG;
	#endif

		ID3DBlob* pErrorBlob;
		auto hr = D3DX11CompileFromFile(szFileName, nullptr, nullptr, szEntryPoint, szShaderModel,
			dwShaderFlags, 0, nullptr, (ID3DBlob**)ppBlobOut, &pErrorBlob, nullptr);

		if (FAILED(hr))
		{
			FE_LOG_ERROR("Shader compile error '%s' (HR=%d) : %s ", szFileName, hr, pErrorBlob ? (char*)pErrorBlob->GetBufferPointer() : "Unknown error");
			SafeRelease(pErrorBlob);

			return FeEReturnCode::Rendering_CreateShaderFailed;
		}
		SafeRelease(pErrorBlob);
		return FeEReturnCode::Success;
	}
}

void FeRenderTexture::Release()
{
 	SafeRelease(D3DResource);
	SafeRelease(D3DSRV);
}
ID3D11Resource* FeRenderTexture::GetD3DResource(uint32) const
{
	return this->D3DResource;
}
ID3D11ShaderResourceView* FeRenderTexture::GetD3DSRV(uint32) const
{
	return this->D3DSRV;
}

void FeRenderTargetTexture::Release()
{
	Texture.Release();
}
ID3D11Resource* FeRenderTargetTexture::GetD3DResource(uint32) const
{
	return this->Texture.D3DResource;
}
ID3D11ShaderResourceView* FeRenderTargetTexture::GetD3DSRV(uint32) const
{
	return this->Texture.D3DSRV;
}
ID3D11Resource* FeRenderFont::GetD3DResource(uint32) const
{
	return this->Texture.D3DResource;
}
ID3D11ShaderResourceView* FeRenderFont::GetD3DSRV(uint32) const
{
	return this->Texture.D3DSRV;
}
void FeRenderFont::Release()
{
	Texture.Release();
	//SafeDelete(FtFontFace);
	// todo@mgz : release stuff
}

void FeRenderWritableTexture::Release()
{
	Texture.Release();
	// todo@mgz : release stuff
}
ID3D11Resource* FeRenderWritableTexture::GetD3DResource(uint32) const
{
	return this->Texture.D3DResource;
}
ID3D11ShaderResourceView* FeRenderWritableTexture::GetD3DSRV(uint32) const
{
	return this->Texture.D3DSRV;
}


void FeRenderGeometryInstance::Reset()
{
	Viewport = nullptr;
	IsCulled = false;
	Geometry = 0;
	Effect = 0;
	Owner = nullptr;

	for (uint32 i = 0; i < FE_RENDERER_TEXTURES_CHANNELS_COUNT; ++i)
	{
		Textures[i].Reset();
	}
	Transform = FeGeometryHelper::IdentityMatrix();
}

void FeRenderLoadingResource::ComputeTexturePath(const FePath& path)
{
	static const FeString strCookFolder = "cooked/image";
	static const FeString strCookExt = ".dds";

	Path.Source = path;

	Path.Cooked = Path.Source;
	Path.Cooked.Dir = strCookFolder;
	Path.Cooked.Ext = strCookExt;

	FeStringTools::Format(Path.Cooked.File, "%u", Path.Source.GetId());
}
void FeRenderLoadingResource::ComputePath(const FePath& path)
{
	Path.Source = path;
	Path.Cooked = Path.Source;
}
FeRenderResource* FeRenderLoadingResource::AllocateResource(FeEResourceType::Type eResourceType)
{
	switch (eResourceType)
	{
	case FeEResourceType::Font:						return AllocateResourceT<FeRenderFont>();
	case FeEResourceType::Texture:					return AllocateResourceT<FeRenderTexture>();
	case FeEResourceType::RenderTargetTexture:		return AllocateResourceT<FeRenderTargetTexture>();
	case FeEResourceType::RenderWritableTexture:	return AllocateResourceT<FeRenderWritableTexture>();
	default:
		FE_ASSERT(false, "Unknown resource type!");
	}
	return nullptr;
}
void FeRenderLoadingResource::ReleaseResource(FeRenderResource* pResource)
{
	SafeReleaseDeleteSelf(pResource);
}

void FeRenderLoadingResource::AllocateResource()
{
	FE_ASSERT(Resource == nullptr, "resource already created !");
	Resource = AllocateResource(Type);
}
void FeRenderLoadingResource::Release()
{
	ReleaseResource(Resource);
	Resource = nullptr;
}