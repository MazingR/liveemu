#include <renderresourceshandler.hpp>
#include <modulerenderer.hpp>

#include <common/memorymanager.hpp>
#include <common/filesystem.hpp>
#include <common/string.hpp>
#include <common/network.hpp>
#include <common/image.hpp>

#include <d3dx11include.hpp>
#include <SDL.h>

#define USE_DDS_IF_EXISTS 1
#define SAVE_CREATED_RESOURCE 1
#define D3DFAILEDRETURN(func) { HRESULT ___hr = (func); if (___hr!=S_OK) return ___hr; }

#include <ft2build.h>
#include <freetype/freetype.h>

bool		StopThread = false;
bool		PauseThread = false;

int ResourcesHandlerThreadFunction(void* pData)
{
	auto pThis = (FeModuleRenderResourcesHandler*)pData;

	while (!StopThread)
	{
		pThis->ProcessThreadedResourcesLoading(StopThread);
		SDL_Delay(100); // todo: use wake up event
	}

	return 0;
}

uint32 FeModuleRenderResourcesHandler::ProcessThreadedResourcesLoading(bool& bThreadSopped)
{
	{
		SCOPELOCK(LoadingThreadMutex); // <------ Lock Mutex

		ResourcesLoadingMap resourcesToLoad;
		{
			SCOPELOCK(LoadingResources[FeEResourceLoadingState::Loading].Mutex); // <------ Lock Mutex
			resourcesToLoad = LoadingResources[FeEResourceLoadingState::Loading].Resources;
		}

		for (auto& idedResource : resourcesToLoad)
		{
			if (bThreadSopped || PauseThread)
				break;

			FeRenderLoadingResource& resource = idedResource.second;
			uint32 iRet = FeEReturnCode::Failed;

			switch (resource.Type)
			{
			case FeEResourceType::Texture:
			{
				iRet = LoadTexture(resource, (FeRenderTexture*)resource.Resource);
			} break;
			case FeEResourceType::RenderTargetTexture:
			{
				iRet = LoadRenderTargetTexture(resource, (FeRenderTargetTexture*)resource.Resource);
			} break;
			case FeEResourceType::RenderWritableTexture:
			{
				iRet = LoadRenderWritableTexture(resource, (FeRenderWritableTexture*)resource.Resource);
			} break;
			case FeEResourceType::Font:
			{
				iRet = LoadFont(resource, (FeRenderFont*)resource.Resource);
			} break;
			default:
				FE_ASSERT(false, "Unknown resource type!");
			}

			resource.Resource->LoadingState = FE_FAILED(iRet) ? FeEResourceLoadingState::LoadFailed : FeEResourceLoadingState::Loaded;


			{
				SCOPELOCK(LoadingResources[FeEResourceLoadingState::Loading].Mutex); // <------ Lock Mutex
				LoadingResources[FeEResourceLoadingState::Loading].Resources.erase(idedResource.first);
			}

			{
				SCOPELOCK(LoadingResources[FeEResourceLoadingState::Loaded].Mutex); // <------ Lock Mutex
				LoadingResources[FeEResourceLoadingState::Loaded].Resources[idedResource.first] = resource;
			}
		}
		// todo@mgz : is it really thread safe ? 
		//delete pResourcesToLoad;

	} // <----- ResourcesLoadingThreadMutex

	return FeEReturnCode::Success;
}

void FeModuleRenderResourcesHandler::ComputeDebugInfos(FeModuleRenderResourcesHandlerDebugInfos& infos)
{
	infos.LoadedResourcesCount = 0;
	infos.LoadedResourcesCountSizeInMemory = 0;
	infos.ResourcesPoolSize = ResourcePoolLimit;

	for (ResourcesMapIt it = Resources.begin(); it != Resources.end(); ++it)
	{
		FeRenderResource* pResource = it->second;

		if (pResource->LoadingState == FeEResourceLoadingState::Loaded)
		{
			infos.LoadedResourcesCount++;
			infos.LoadedResourcesCountSizeInMemory += pResource->SizeInMemory;
		}
	}
}
uint32 FeModuleRenderResourcesHandler::Load(const FeModuleInit*)
{
	ResourcePoolLimit = 256 * (1024 * 1024);
	ResourcePoolAllocated.store(0);

	for (uint32 i = 0; i < FeEResourceLoadingState::Count; ++i)
	{
		LoadingResources[(FeEResourceLoadingState::Type)(i)] = LockedLoadingResourcesMap();
		LoadingResources[(FeEResourceLoadingState::Type)(i)].Mutex = SDL_CreateMutex();
	}
	LoadingThreadMutex = SDL_CreateMutex();
	LoadingThread = SDL_CreateThread(ResourcesHandlerThreadFunction, "ModuleRenderResourcesHandler", this);

	// Initialize freetype
	auto error = FT_Init_FreeType(&FtLibrary);

	if (error)
	{
		return FeEReturnCode::Failed;
	}
	return FeEReturnCode::Success;
}
uint32 FeModuleRenderResourcesHandler::Unload()
{
	StopThread = true;
	int iReturned;
	SDL_WaitThread(LoadingThread, &iReturned);
	
	for (uint32 i = 0; i < FeEResourceLoadingState::Count; ++i)
		SDL_DestroyMutex(LoadingResources[(FeEResourceLoadingState::Type)(i)].Mutex);

	return FeEReturnCode::Success;
}

bool ConvertTexture(const FePath& inputPath, const FePath& outputPath, DXGI_FORMAT format, int32 width, int32 height, int32 mipLevels, ID3D11Resource** ppResource=nullptr)
{
	static FeStaticString<FE_STRING_SIZE_512> WorkString;

	ID3D11DeviceContext* pD3DContext = FeModuleRendering::GetDevice().GetImmediateContext();
	ID3D11Device* pD3DDevice = FeModuleRendering::GetDevice().GetD3DDevice();

	D3DX11_IMAGE_INFO imgInfos;
	HRESULT hr;

	FeFileTools::FormatFullPath(inputPath, WorkString);
	D3DX11GetImageInfoFromFile(WorkString.Value, nullptr, &imgInfos, &hr);

	D3DX11_IMAGE_LOAD_INFO loadinfos;
	ZeroMemory(&loadinfos, sizeof(D3DX11_IMAGE_LOAD_INFO));

	loadinfos.Width = width != 0 ? width : imgInfos.Width;
	loadinfos.Height = height != 0 ? height : imgInfos.Height;
	loadinfos.Depth = imgInfos.Depth;
	loadinfos.Usage = D3D11_USAGE_DEFAULT;
	loadinfos.CpuAccessFlags = 0;
	loadinfos.MipFilter = loadinfos.Filter = D3DX11_FILTER_LINEAR;
	loadinfos.pSrcInfo = &imgInfos;
	loadinfos.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	loadinfos.Format = format;
	loadinfos.MiscFlags = 0;
	loadinfos.FirstMipLevel = 0;
	loadinfos.MipLevels = mipLevels;

	ID3D11Resource* pResource = ppResource ? *ppResource : nullptr;
	SafeRelease(pResource);

	D3DX11CreateTextureFromFile(pD3DDevice, WorkString.Value, &loadinfos, nullptr, ppResource ? ppResource : &pResource, &hr);

	if (FAILED(hr))
	{
		return false;
	}
	else
	{
		FeFileTools::FormatFullPath(outputPath, WorkString);
		hr = D3DX11SaveTextureToFile(pD3DContext, ppResource ? *ppResource : pResource, D3DX11_IFF_DDS, WorkString.Value);
		
		if (FAILED(hr))
			return false;
	}
	return true;
}
void FeModuleRenderResourcesHandler::ProcessLoadedResources()
{
	static FeStaticString<FE_STRING_SIZE_512> WorkString;

	ID3D11DeviceContext* pD3DContext = FeModuleRendering::GetDevice().GetImmediateContext();
	ID3D11Device* pD3DDevice = FeModuleRendering::GetDevice().GetD3DDevice();

	{
		SCOPELOCK(LoadingResources[FeEResourceLoadingState::Loaded].Mutex); // <------ Lock Mutex

		ResourcesLoadingMap& loadedResources = LoadingResources[FeEResourceLoadingState::Loaded].Resources;

		for (ResourcesLoadingMapIt it = loadedResources.begin(); it != loadedResources.end(); ++it)
		{
			FeRenderLoadingResource& loadingResource = it->second;
			FeRenderResource* pResource = loadingResource.Resource;

			if (pResource->LoadingState == FeEResourceLoadingState::Loaded)
			{
				if (loadingResource.Type == FeEResourceType::Font)
					PostLoadFont(loadingResource, (FeRenderFont*)pResource);

				ResourcePoolAllocated.fetch_add(pResource->SizeInMemory);
#if SAVE_CREATED_RESOURCE
				if (pResource->RuntimeCreated && pResource->LoadingState == FeEResourceLoadingState::Loaded && loadingResource.Type == FeEResourceType::Texture)
#else
				if (false)
#endif
				{
					LoadingResources[FeEResourceLoadingState::Saving].Resources[it->first] = loadingResource;
				}
			}
			else
			{
				pResource->Release();
			}
		}

		loadedResources.clear();
	}

	ResourcesLoadingMap& savingResources = LoadingResources[FeEResourceLoadingState::Saving].Resources;

	for (ResourcesLoadingMapIt it = savingResources.begin(); it != savingResources.end(); ++it)
	{
		FeRenderLoadingResource& resource = it->second;

		FeFileTools::FormatFullPath(resource.Path.Cooked, WorkString);
		FeRenderTexture* pTextureData = (FeRenderTexture*)resource.Resource;

		if (pTextureData)
		{
			// save rgba32
			HRESULT hr = D3DX11SaveTextureToFile(pD3DContext, pTextureData->D3DResource, D3DX11_IFF_DDS, WorkString.Value);
			bool bSucceeded = true;

			if (FAILED(hr))
			{
				bSucceeded = false;
			}
			else
			{
				auto format = pTextureData->HasChannelAlpha ? DXGI_FORMAT_BC3_UNORM : DXGI_FORMAT_BC1_UNORM;
				
				{ // save BC3/1 dds full size and replace shader resource
					ConvertTexture(resource.Path.Cooked, resource.Path.Cooked, format, 0, 0, 4, &pTextureData->D3DResource);
				}
				{ //  save BC3/1 dds thumb size
					FePath thumbPath = resource.Path.Cooked;
					static const FeString cookedThumbDir = "cooked/image_thumb";
					thumbPath.Dir = cookedThumbDir;
					ConvertTexture(resource.Path.Cooked, thumbPath, format, 64, 64, 1, nullptr);
				}
				if (bSucceeded)
				{
					FE_LOG("Save converted resource %s", resource.Path.Cooked.File.Cstr());
				}
				else
				{
					FE_ASSERT(false, "converted resource save failed");
				}
				
			}
		}
		savingResources.erase(it);

		break; //  process one file per frame
	}
}

uint32 FeModuleRenderResourcesHandler::Update(const FeDt& fDt)
{
	ProcessLoadedResources();
	return FeEReturnCode::Success;
}
FeRenderResource* FeModuleRenderResourcesHandler::GetResource(const FeResourcePath& path)
{
	ResourcesMap::const_iterator it = Resources.find(path.Source.GetId());
	return (it != Resources.end()) ? it->second : nullptr;
}
FeRenderResource* FeModuleRenderResourcesHandler::GetResource(const FeResourceId& resourceId)
{
	ResourcesMap::const_iterator it = Resources.find(resourceId);
	return (it != Resources.end()) ? it->second : nullptr;
}
uint32 FeModuleRenderResourcesHandler::UnloadResource(const FeResourceId& resourceId)
{
	ResourcesMap::const_iterator it = Resources.find(resourceId);

	if (it != Resources.end())
	{
		auto loadingResource = (*it).second;

		while (loadingResource->LoadingState == FeEResourceLoadingState::Loading)
			Sleep(10);

		loadingResource->Release();
		loadingResource->DeleteSelf();

		{
			SCOPELOCK(LoadingResources[FeEResourceLoadingState::Loading].Mutex); // <------ Lock Mutex
			LoadingResources[FeEResourceLoadingState::Loading].Resources.erase(resourceId);
		}

		{
			SCOPELOCK(LoadingResources[FeEResourceLoadingState::Loaded].Mutex); // <------ Lock Mutex
			LoadingResources[FeEResourceLoadingState::Loaded].Resources.erase(resourceId);
		}
		Resources.erase(resourceId);
	}
	return FeEReturnCode::Success;
}
uint32 FeModuleRenderResourcesHandler::LoadResource(FeRenderLoadingResource& loadingResource)
{
	if (loadingResource.Id == 0) // compute resource sting id
		loadingResource.Id = loadingResource.Path.Source.GetId();

	if (GetResource(loadingResource.Id)) // resource is alread loaded/loading
	{
		loadingResource.Release();
	}
	else
	{
		if (nullptr == loadingResource.Resource)
			loadingResource.AllocateResource(); // allocate resource

		FE_ASSERT(loadingResource.Resource, "couldn't created render resource !");

		Resources[loadingResource.Id] = loadingResource.Resource;
		loadingResource.Resource->LoadingState = FeEResourceLoadingState::Loading;

		{
			SCOPELOCK(LoadingResources[FeEResourceLoadingState::Loading].Mutex); // <------ Lock Mutex
			LoadingResources[FeEResourceLoadingState::Loading].Resources[loadingResource.Id] = loadingResource;
		} // <------ Unlock Mutex
	}
	return FeEReturnCode::Success;
}
uint32 FeModuleRenderResourcesHandler::LoadFont(FeRenderLoadingResource& resource, FeRenderFont* pFont)
{
	bool bLoadingFailed = false;
	pFont->MapTmpData = nullptr;
	pFont->MapDepthPitch = 0;
	
	static FeStaticString<FE_STRING_SIZE_512> WorkString;
	FeFileTools::FormatFullPath(resource.Path.Source, WorkString);

	auto error = FT_New_Face(FtLibrary, WorkString.Value, 0, &pFont->FtFontFace);
	
	FT_FaceRec_* face = pFont->FtFontFace;
	
	if (error == FT_Err_Unknown_File_Format)
	{
		FE_LOG("the font file could be opened and read, but it appears that its font format is unsupported");
		return FeEReturnCode::Failed;
	}
	else if (error)
	{
		FE_LOG("that the font file could not be opened or read, or that it is broken..");
		return FeEReturnCode::Failed;
	}
	struct ReplaceGroup
	{
		char Character;
		FeTArray<char> Replaced;

		ReplaceGroup(){}

		ReplaceGroup(char master, char a)
		{
			Character = master;
			Replaced.Add(a);
		}
		ReplaceGroup(char master, char a, char b) : ReplaceGroup(master, a)				{ Replaced.Add(b); }
		ReplaceGroup(char master, char a, char b, char c) : ReplaceGroup(master, a, b)			{ Replaced.Add(c); }
		ReplaceGroup(char master, char a, char b, char c, char d) : ReplaceGroup(master, a, b, c)		{ Replaced.Add(d); }
		ReplaceGroup(char master, char a, char b, char c, char d, char e) : ReplaceGroup(master, a, b, c, d)	{ Replaced.Add(e); }
	};
	FeTArray<ReplaceGroup> replaceGroups;

	replaceGroups.Add(ReplaceGroup('e', '�', '�', '�'));
	replaceGroups.Add(ReplaceGroup('a', '�', '�'));
	replaceGroups.Add(ReplaceGroup('u', '�', '�', '�'));
	replaceGroups.Add(ReplaceGroup('c', '�'));
	replaceGroups.Add(ReplaceGroup('o', '�', '�'));
	replaceGroups.Add(ReplaceGroup('i', '�', '�'));

	const char szTemplateFontContent[] = 
	{	"abcdefghijklmnopqrstuvwxyz\
		ABCDEFGHIJKLMNOPQRSTUVWXYZ\
		123456789\
		??????????\
		,;:!?./?%*?$\\&#'{([-|_@)]})="	};

	uint32 iCharSize = pFont->Size;
	uint32 iMapWidth = 0;
	uint32 iMapHeight = 0;
	uint32 iCharInterval = 4;
	uint32 iCharCount = 0;
	const uint32 iCharsPerLine = 16;
	const uint32 iMaxCharCount = sizeof(szTemplateFontContent);

	char szFontContent[iMaxCharCount];

	// Compute actual charCount
	for (size_t iChar = 0; iChar < iMaxCharCount; ++iChar)
	{
		char cChar = szTemplateFontContent[iChar];

		/* retrieve glyph index from character code */
		if (FT_Get_Char_Index(face, cChar) == 0)
			continue;

		szFontContent[iCharCount++] = cChar;
	}

	while (iMapWidth < (iCharsPerLine*(iCharSize + iCharInterval)))
		iMapWidth += 2;

	uint32 iCharPerLine = (uint32)ceil(iMapWidth / (float)(iCharSize + iCharInterval));
	uint32 iLinesCount	= (uint32)ceil(iCharCount / (float)iCharPerLine);

	while (iMapHeight <(iCharSize*iLinesCount))
		iMapHeight += 2;

	pFont->MapSize[0] = iMapWidth;
	pFont->MapSize[1] = 0;

	uint32 iXOffset = 0;
	uint32 iYOffset = 0;
	uint32 iDepthPitch = iMapWidth*iMapHeight;

	error = FT_Set_Char_Size(
		face,				/* handle to face object           */
		0,					/* char_width in 1/64th of points  */
		iCharSize * 64,	/* char_height in 1/64th of points */
		iMapWidth,			/* horizontal device resolution    */
		iMapHeight);		/* vertical device resolution      */

	error = FT_Set_Pixel_Sizes(
		face,			/* handle to face object */
		0,				/* pixel_width           */
		iCharSize);	/* pixel_height          */

	FT_GlyphSlot  slot = face->glyph;  /* a small shortcut */

	pFont->MapTmpData = FE_ALLOCATE(iDepthPitch, FE_HEAPID_RENDERER);
	pFont->MapDepthPitch = iDepthPitch;

	memset(pFont->MapTmpData, 0, iDepthPitch);

	for (size_t iChar = 0; iChar < iCharCount; ++iChar)
	{
		FT_UInt  glyph_index;

		/* retrieve glyph index from character code */
		glyph_index = FT_Get_Char_Index(face, szFontContent[iChar]);

		/* load glyph image into the slot (erase previous one) */
		error = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);
		
		if (error)
			continue;  /* ignore errors */

		/* convert to an anti-aliased bitmap */
		error = FT_Render_Glyph(slot, FT_RENDER_MODE_NORMAL);

		if (error)
			continue;

		if (!slot->bitmap.buffer)
			continue;

		char* pOutput = (char*)pFont->MapTmpData;

		// Copy char texture to font atlas map
		for (uint32 iY = 0; iY < slot->bitmap.rows; ++iY)
		{
			uint32 inputBufferIdx = slot->bitmap.pitch*iY;// +slot->bitmap_left;
			uint32 outputBufferIdx = iMapWidth*(iY + iYOffset + slot->advance.y) + iXOffset;

			if (outputBufferIdx + slot->bitmap.width > iDepthPitch)
				break;

			memcpy_s(&pOutput[outputBufferIdx], slot->bitmap.width, &slot->bitmap.buffer[inputBufferIdx], slot->bitmap.width);
		}
		FeRenderFontChar charData;
		memset(&charData, 0, sizeof(FeRenderFontChar));

		charData.Left = iXOffset;
		charData.Top = iYOffset;

#define SetIfValid(a, b) if (b!=FE_INVALID_ID)  { a=b; }
		
		SetIfValid (charData.OffsetLeft, slot->bitmap_left);
		SetIfValid (charData.OffsetTop, slot->bitmap_top);
		SetIfValid (charData.Width, slot->bitmap.width);
		SetIfValid (charData.Height, slot->bitmap.rows);

		pFont->Chars[szFontContent[iChar]] = charData;
		for (auto& replaceGroup : replaceGroups)
		{
			if (replaceGroup.Character == szFontContent[iChar])
			{
				for (auto& replaced : replaceGroup.Replaced)
				{
					pFont->Chars[replaced] = charData;
				}
			}
		}

		iXOffset += slot->bitmap.width + slot->bitmap_left + iCharInterval;

		if (iXOffset > (iMapWidth - iCharSize))
		{
			iYOffset += iCharSize;
			iXOffset = 0;

			pFont->MapSize[1] += iCharSize;
		}
	}
	pFont->MapSize[1] += iCharSize;

	FT_Done_Face(face);

	return FeEReturnCode::Success;
}
uint32 FeModuleRenderResourcesHandler::PostLoadFont(FeRenderLoadingResource& resource, FeRenderFont* pFont)
{
	ID3D11DeviceContext* pD3DContext = FeModuleRendering::GetDevice().GetImmediateContext();
	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(D3D11_TEXTURE2D_DESC));

	desc.Width				= pFont->MapSize[0];
	desc.Height				= pFont->MapSize[1];
	desc.MipLevels			= desc.ArraySize = 1;
	desc.Format				= DXGI_FORMAT_R8_UNORM;
	desc.SampleDesc.Count	= 1;
	desc.Usage				= D3D11_USAGE_DYNAMIC;
	desc.BindFlags			= D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags		= D3D11_CPU_ACCESS_WRITE;

	ID3D11Device* pD3DDevice = FeModuleRendering::GetDevice().GetD3DDevice();
	ID3D11Texture2D *pTexture = nullptr;

	auto hr = pD3DDevice->CreateTexture2D(&desc, nullptr, &pTexture);

	if (SUCCEEDED(hr))
	{
		pFont->Texture.D3DResource = reinterpret_cast<ID3D11Resource*>(pTexture);

		D3D11_MAPPED_SUBRESOURCE textureMap;
		hr = pD3DContext->Map(pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &textureMap);

		if (SUCCEEDED(hr))
		{
			char* pOutput = (char*)textureMap.pData;
			char* pInput = (char*)pFont->MapTmpData;

			uint32 iFontMapWidth = pFont->MapSize[0];
			uint32 iFontMapHeight = pFont->MapSize[1];

			for (uint32 i = 0; i < iFontMapHeight; ++i)
				memcpy_s(pOutput + (i*textureMap.RowPitch), iFontMapWidth, pInput + (i*iFontMapWidth), iFontMapWidth);

			pD3DContext->Unmap(pTexture, 0);

			hr = pD3DDevice->CreateShaderResourceView(pTexture, nullptr, &pFont->Texture.D3DSRV);
		}
	}

	FE_FREE(pFont->MapTmpData, FE_HEAPID_RENDERER);

	if (FAILED(hr))
		return FeEReturnCode::Failed;

	return FeEReturnCode::Success;
}
uint32 FeModuleRenderResourcesHandler::TryLoadOneTexture(FeRenderResource* pTextureData, FeRenderTexture* RenderTexture, const D3D11_TEXTURE2D_DESC& desc)
{
	ID3D11Device* pD3DDevice = FeModuleRendering::GetDevice().GetD3DDevice();
	ID3D11Texture2D *pTexture = nullptr;

	pTextureData->SizeInMemory = ComputeResourceSizeInMemoryFromFormat(desc.Width, desc.Height, desc.Format, true);

	if (pTextureData->SizeInMemory + ResourcePoolAllocated <= ResourcePoolLimit)
	{
		auto hr = pD3DDevice->CreateTexture2D(&desc, nullptr, &pTexture);
		RenderTexture->D3DResource = reinterpret_cast<ID3D11Resource*>(pTexture);
		RenderTexture->RuntimeCreated = true;

		if (SUCCEEDED(hr))
		{
			hr = pD3DDevice->CreateShaderResourceView(RenderTexture->D3DResource, nullptr, &RenderTexture->D3DSRV);
		}

		return SUCCEEDED(hr) ? FeEReturnCode::Success : FeEReturnCode::Failed;
	}
	else
	{
		return FeEReturnCode::Failed;
	}

	return FeEReturnCode::Success;
}

DXGI_FORMAT ConvertTexFormatToDXGI(FeEWritableTextureFormat::Type format)
{
	switch (format)
	{
	case FeEWritableTextureFormat::R8:			return DXGI_FORMAT_R8_UNORM;
	case FeEWritableTextureFormat::R8G8B8A8:	return DXGI_FORMAT_R8G8B8A8_UNORM;
	case FeEWritableTextureFormat::B8G8R8A8:	return DXGI_FORMAT_B8G8R8A8_UNORM;
	case FeEWritableTextureFormat::R8G8B8X8:	return DXGI_FORMAT_B8G8R8X8_UNORM;
	default:
		break;
	}

	return DXGI_FORMAT_UNKNOWN;
}
uint32 FeModuleRenderResourcesHandler::LoadRenderWritableTexture(FeRenderLoadingResource& resource, FeRenderWritableTexture* pTextureData)
{
	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(D3D11_TEXTURE2D_DESC));

	desc.Width = pTextureData->Width;
	desc.Height = pTextureData->Height;
	desc.MipLevels = desc.ArraySize = 1;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.Format = ConvertTexFormatToDXGI(pTextureData->Format);

	return TryLoadOneTexture(pTextureData, &pTextureData->Texture, desc);
}
uint32 FeModuleRenderResourcesHandler::LoadRenderTargetTexture(FeRenderLoadingResource& resource, FeRenderTargetTexture* pTextureData)
{
	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(D3D11_TEXTURE2D_DESC));

	desc.Width = pTextureData->Width;
	desc.Height = pTextureData->Height;
	desc.MipLevels = desc.ArraySize = 1;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
	
	if (pTextureData->Shared)
		desc.MiscFlags = D3D11_RESOURCE_MISC_SHARED;

	desc.Format = ConvertTexFormatToDXGI(pTextureData->Format);

	uint32 iResult = TryLoadOneTexture(pTextureData, &pTextureData->Texture, desc);

	if (FE_SUCCEEDED(iResult) && pTextureData->Shared)
	{
		// QI IDXGIResource interface to synchronized shared surface.
		IDXGIResource* pDXGIResource = NULL;
		pTextureData->Texture.D3DResource->QueryInterface(__uuidof(IDXGIResource), (LPVOID*)&pDXGIResource);

		// obtain handle to IDXGIResource object.
		pDXGIResource->GetSharedHandle((void**)(&pTextureData->Texture.SharedHandle));
		pDXGIResource->Release();

		if (!pTextureData->Texture.SharedHandle)
			return E_FAIL;
	}

	return iResult;
}
uint32 FeModuleRenderResourcesHandler::LoadTexture(FeRenderLoadingResource& resource, FeRenderTexture* pTextureData)
{
	ID3D11Device* pD3DDevice = FeModuleRendering::GetDevice().GetD3DDevice();

	uint32 iResult = FeEReturnCode::Failed;

	HRESULT hr;
	D3DX11_IMAGE_LOAD_INFO loadinfos;
	D3DX11_IMAGE_INFO imgInfos;
	ZeroMemory(&loadinfos, sizeof(D3DX11_IMAGE_LOAD_INFO));
	bool bCookedFileExists = FeFileTools::FileExists(resource.Path.Cooked);
	bool bIsNativeFormat = bCookedFileExists;
	FePath loadedTexturePath = resource.Path.Cooked;

	FeDynamicBuffer ImageBuffer;

	bool bHasAlpha = true;
	bool bIsRemoteFile = !bCookedFileExists && resource.Path.Source.IsRemote();

	bool bDeleteFileFromDisk = false;
	bool bCreateFromMemory = false;

	if (bIsRemoteFile)
	{
		bHasAlpha = false;
		FeUrlTransferHandler UrlTranfser;

		UrlTranfser.Initialize();
		FeStringBuffer RequestBuffer;

		RequestBuffer.Allocate(1024, FE_HEAPID_RENDERER);
		ImageBuffer.Allocate(1024 * 1024, FE_HEAPID_RENDERER);

		RequestBuffer.Clear();
		RequestBuffer.Printf("%s", resource.Path.Source.File.Cstr());

		const FeString UserAgent = "liveemu";

		FE_FAILEDRETURN(UrlTranfser.Perform(&ImageBuffer, RequestBuffer.Cstr(), UserAgent.Cstr()));

		FeEImageFormat::Type eImgFormat = FeImageHelper::GetImageFormatFromBuffer(ImageBuffer);

		if (eImgFormat != FeEImageFormat::JPEG)
		{
			loadedTexturePath.Ext = ".bmp";
			FE_FAILEDRETURN(FeImageHelper::SaveImageFromBufferToFile(ImageBuffer, loadedTexturePath, FeEImageFormat::BMP));
			bDeleteFileFromDisk = true;
		}
		else
		{
			bCreateFromMemory = true;
			D3DX11GetImageInfoFromMemory(ImageBuffer.Data, ImageBuffer.Length, nullptr, &imgInfos, &hr);
		}
	}
	else if (!bCookedFileExists && FeFileTools::FileExists(resource.Path.Source))
	{
		loadedTexturePath = resource.Path.Source;
	}

	if (!bCreateFromMemory)
	{
		static FeStaticString<FE_STRING_SIZE_512> WorkString;
		FeFileTools::FormatFullPath(loadedTexturePath, WorkString);
		D3DX11GetImageInfoFromFile(WorkString.Value, nullptr, &imgInfos, &hr);
	}

	if (SUCCEEDED(hr))
	{
		uint32 iResize = 1;

		loadinfos.Width = imgInfos.Width / iResize;
		loadinfos.Height = imgInfos.Height / iResize;

		loadinfos.Depth = imgInfos.Depth;
		loadinfos.Usage = D3D11_USAGE_DEFAULT;//D3D11_USAGE_DYNAMIC
		loadinfos.CpuAccessFlags = 0;//D3D11_CPU_ACCESS_WRITE
		loadinfos.Filter = D3DX11_FILTER_LINEAR;
		loadinfos.MipFilter = D3DX11_FILTER_LINEAR;
		loadinfos.pSrcInfo = &imgInfos;
		loadinfos.MipLevels = 4;

		if (bIsNativeFormat)
		{
			loadinfos.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			loadinfos.Format = imgInfos.Format;
			loadinfos.MiscFlags = 0;
			loadinfos.FirstMipLevel = FeMath::Min(pTextureData->FirstMipLevel, loadinfos.MipLevels-1);
			loadinfos.MipLevels = 1;
		}
		else
		{
			loadinfos.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
			loadinfos.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			loadinfos.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
		}
		pTextureData->Width = loadinfos.Width;
		pTextureData->Height = loadinfos.Height;
		pTextureData->HasChannelAlpha = bHasAlpha;
		pTextureData->RuntimeCreated = !bIsNativeFormat;
		pTextureData->SizeInMemory = ComputeResourceSizeInMemoryFromFormat(loadinfos.Width, loadinfos.Height, loadinfos.Format, true);

		if (pTextureData->SizeInMemory + ResourcePoolAllocated <= ResourcePoolLimit)
		{
			if (bCreateFromMemory)
			{
				D3DX11CreateTextureFromMemory(pD3DDevice, ImageBuffer.Data, ImageBuffer.Length, &loadinfos, nullptr, &pTextureData->D3DResource, &hr);
			}
			else
			{
				static FeStaticString<FE_STRING_SIZE_512> WorkString;
				FeFileTools::FormatFullPath(loadedTexturePath, WorkString);
				D3DX11CreateTextureFromFile(pD3DDevice, WorkString.Value, &loadinfos, nullptr, &pTextureData->D3DResource, &hr);
			}

			if (SUCCEEDED(hr))
			{
				hr = pD3DDevice->CreateShaderResourceView(pTextureData->D3DResource, nullptr, &pTextureData->D3DSRV);
			}

			iResult = SUCCEEDED(hr) ? FeEReturnCode::Success : FeEReturnCode::Failed;
		}
	}

	if (bDeleteFileFromDisk)
		FeFileTools::FileDelete(loadedTexturePath);

	return iResult;
}
void FeModuleRenderResourcesHandler::UnloadResources()
{
	// Release loaded resources
	{
		PauseThread = true;
		SCOPELOCK(LoadingThreadMutex); // <------ Lock Mutex
		PauseThread = false;

		ProcessLoadedResources();

		for (auto& resource : Resources)
		{
			uint32 iReleasedSize = resource.second->GetSizeInMemory();
			FE_ASSERT(ResourcePoolAllocated >= iReleasedSize, "incoherent resources pool size");

			ResourcePoolAllocated.fetch_sub(iReleasedSize);

			resource.second->Release();
			resource.second->DeleteSelf();
		}

		Resources.clear();

		// clear all loading resource maps
		for (uint32 i = 0; i < FeEResourceLoadingState::Count; ++i)
			LoadingResources[(FeEResourceLoadingState::Type)(i)].Resources.clear();

	}// <------ Unlock Mutex
}