#pragma once

#include <common/common.hpp>
#include <common/tarray.hpp>
#include <common/maths.hpp>
#include <map>
#include <common/serializable.hpp>
#include <common/asset.hpp>

struct FeString;

#define INLINE_VEC3_MUL_XYZ(output,x,y,z) {output.mData[0]*=x;output.mData[1]*=y;output.mData[2]*=z;}

#define DEBUG_STRING_SIZE 2048
#define FE_HEAPID_RENDERER 3
#define FE_HEAPNAME_RENDERER "Renderer"
#define RENDERER_DEFAULT_EFFECT_ID 1

// forward declares of DXSDK
struct ID3D11Device;
struct ID3D11DeviceContext;
struct IDXGISwapChain;
struct ID3D11Resource;
struct ID3D11ShaderResourceView;
struct ID3D11DepthStencilView;
struct ID3D11Buffer;
struct ID3D11SamplerState;
struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11InputLayout;
struct ID3D11RenderTargetView;
struct ID3D11Texture2D;
struct ID3D11BlendState;

// forward declares of FW1 FontWrapper 
struct IFW1Factory;
struct IFW1FontWrapper;
struct  FT_FaceRec_;
typedef uint32 FeResourceId;

namespace FeMathHelper
{
	bool IntersectPointWithRect(const FeVector2& Point, const FeMatrix4& Rect, float offset = 0.0f);
}
namespace FeRenderTools
{
	uint32 CompileShaderFromMemory(const char* srcData, size_t srcDataLen, const char* szFileName, const char* szEntryPoint, const char* szShaderModel, void** ppBlobOut);
	uint32 CompileShaderFromFile(const char* szFileName, const char* szEntryPoint, const char* szShaderModel, void** ppBlobOut);
}
struct FeResourcePath
{
	FePath Cooked;
	FePath Source;

};
struct FeRenderCamera
{
	FeVector3	VecPosition;
	FeVector3	VecLookAt;
	FeVector3	VecUp;

	FeMatrix4	MatrixProjection;
	FeMatrix4	MatrixView;
};
struct FeRenderConstantBuffer
{
	ID3D11Buffer*	Buffer;
	size_t			Size;
	void*			CpuBuffer;
};
struct FeRenderSampler
{
	ID3D11SamplerState*	State;
};

namespace FeEResourceLoadingState
{
	enum Type
	{
		Idle,
		Loading,
		Loaded,
		Saving,
		LoadFailed,
		Count
	};
}
typedef uint32 FeResourceId;

struct FeTextureLoadingQueryResult
{
	FeResourceId TextureId;
};

namespace FeEResourceType
{
	enum Type
	{
		Texture,
		RenderTargetTexture,
		RenderWritableTexture,
		Font,
		Geometry,
		Sound,
		Unknown
	};
}

class FeRenderResource
{
public:
	FeEResourceLoadingState::Type	LoadingState;
	uint32							SizeInMemory;
	bool							RuntimeCreated;

	FeRenderResource() : 
		SizeInMemory(0)
	{}

	virtual uint32 GetSizeInMemory()
	{
		return SizeInMemory;
	}
	virtual void DeleteSelf()
	{
		FE_ASSERT(false, "shouldn't get here");
	}

	virtual void Release() = 0;
	virtual ID3D11Resource* GetD3DResource(uint32 iIdx) const = 0;
	virtual ID3D11ShaderResourceView* GetD3DSRV(uint32 iIdx) const = 0;

};
typedef void(*FeResourceLoadingCallbackFunc) (FeRenderResource* pResource, void* pUserData);


namespace FeEWritableTextureFormat
{
	enum Type
	{
		R8,
		R8G8B8A8,
		R8G8B8X8,
		B8G8R8A8
	};
}
struct FeResourceLoadingCallback
{
	FeResourceLoadingCallbackFunc	Functor;
	void*							UserData;
};

class FeRenderFont;
class FeRenderTexture;
class FeRenderTargetTexture;
class FeRenderWritableTexture;

class FeRenderLoadingResource
{
public:
	FeResourcePath				Path;
	FeResourceId				Id;
	FeResourceLoadingCallback	LoadingFinishedCallback;
	FeRenderResource*			Resource;
	FeEResourceType::Type		Type;

	FeRenderLoadingResource() : 
		Id(0),
		Resource(nullptr)
	{}

	template<typename T>
	static FeRenderResource* AllocateResourceT()
	{
		return FE_NEW(T, FE_HEAPID_RENDERER);
	}
	template<typename T>
	static void DeleteResourceT(T* pResource)
	{
		FE_DELETE(T, pResource, FE_HEAPID_RENDERER);
	}
	template<typename T>
	T* GetResourceT()
	{
		return static_cast<T*>(Resource);
	}

	void ComputeTexturePath(const FePath& path);
	void ComputePath(const FePath& path);
	static FeRenderResource* AllocateResource(FeEResourceType::Type eResourceType);
	static void ReleaseResource(FeRenderResource* pResource);
	void AllocateResource();
	void Release();

};
struct FeRenderViewport
{
	bool					UseBackBuffer;
	FeRect					Rect;
	ID3D11RenderTargetView* RenderTargetView;
	ID3D11DepthStencilView*	DepthStencilView;
	ID3D11Texture2D*		DepthStencil;
	FeMatrix4				Transform;
	ID3D11Texture2D*		Texture;

	FeRenderViewport();
	~FeRenderViewport();

	uint32 CreateFromBackBuffer();

	void Bind()  const;
	void Clear()  const;
	void Unload();
};

struct FeRenderGeometryData
{
	ID3D11Buffer*	VertexBuffer;
	ID3D11Buffer*	IndexBuffer;
	uint32			Stride;
	uint32			IndexCount;

	void Release();
};

#define FeEImageResize_Values(_d)		\
		_d(FeEImageResize, Stretch)		\
		_d(FeEImageResize, PanScan)		\
		_d(FeEImageResize, None)		\
		_d(FeEImageResize, LetterBox)	\

FE_DECLARE_ENUM(FeEImageResize, FeEImageResize_Values)

#define FeEImageSample_Values(_d)		\
		_d(FeEImageSample, Wrap)		\
		_d(FeEImageSample, Clamp)		\
		_d(FeEImageSample, Normal)		\
		_d(FeEImageSample, Mirror)		\
		_d(FeEImageSample, MirrorOnce)	\

FE_DECLARE_ENUM(FeEImageSample, FeEImageSample_Values)

#define FeEImageAlign_Values(_d)	\
		_d(FeEImageAlign, Min)		\
		_d(FeEImageAlign, Center)	\
		_d(FeEImageAlign, Max)		\

FE_DECLARE_ENUM(FeEImageAlign, FeEImageAlign_Values)


class FeIShaderPropertyContainer
{
public:
	virtual float GetShaderPropertyScalar(const FeString& PropertyName) = 0;
	virtual FeVector4& GetShaderPropertyVector(const FeString& PropertyName)  = 0;
	virtual FeColor& GetShaderPropertyColor(const FeString& PropertyName)  = 0;

	virtual FeEImageAlign::Type GetImageHorizontalAlign() = 0;
	virtual FeEImageAlign::Type GetImageVerticalAlign() = 0;
	virtual FeEImageResize::Type GetImageResizeMode() = 0;
	virtual FeEImageSample::Type GetImageSampleMode() = 0;
};

#define FE_RENDERER_TEXTURES_CHANNELS_COUNT 2

struct DualResourceId
{
	FeResourceId Desired;
	FeResourceId Fallback;

	void Reset()
	{
		Desired = 0;
		Fallback = 0;
	}
};
struct FeTextureAspect
{
	FeEImageResize::Type	ResizeMode;
	FeEImageSample::Type	SampleMode;
	FeEImageAlign::Type		HorizontalAlign;
	FeEImageAlign::Type		VerticalAlign;

	void Reset()
	{
		SampleMode = FeEImageSample::Normal;
		ResizeMode = FeEImageResize::Stretch;
		HorizontalAlign = FeEImageAlign::Center;
		VerticalAlign = FeEImageAlign::Center;
	}
};

struct FeRenderGeometryInstance
{
public:
	FeResourceId					Geometry;
	FeResourceId					Effect;
	FeMatrix4						Transform;
	DualResourceId					Textures[FE_RENDERER_TEXTURES_CHANNELS_COUNT];
	bool							IsCulled;
	FeIShaderPropertyContainer*		Owner;
	FeRenderViewport*				Viewport;

	void Reset();
};
namespace FeEGemetryDataType
{
	enum Type
	{
		Quad,
		Triangle,
		Count
	};
};

struct FeRenderBatch
{
	FeTArray<FeRenderGeometryInstance*> GeometryInstances;
	FeRenderViewport Viewport;

	FeRenderBatch()
	{
		GeometryInstances.SetHeapId(FE_HEAPID_RENDERER);
	}
	bool IsEmpty() const
	{
		return GeometryInstances.GetSize() == 0;
	}
};

// ---------------------------------------------------------------------------------------------------------------------------------------

class FeRenderTexture : public FeRenderResource
{
public:
	ID3D11Resource*					D3DResource;
	ID3D11ShaderResourceView*		D3DSRV;
	uint32							FirstMipLevel;
	uint32							Id;
	bool							HasChannelAlpha;
	uint32							Width;
	uint32							Height;
	uint64							SharedHandle;

	FeRenderTexture() :
		D3DResource(nullptr),
		D3DSRV(nullptr),
		FirstMipLevel(0),
		HasChannelAlpha(true),
		Width(0),
		Height(0),
		Id(0),
		SharedHandle(0)
	{}
	virtual void DeleteSelf() override
	{
		FeRenderLoadingResource::DeleteResourceT(this);
	}

	void Release();
	ID3D11Resource* GetD3DResource(uint32 iIdx) const;
	ID3D11ShaderResourceView* GetD3DSRV(uint32 iIdx) const;
};

class FeRenderTargetTexture : public FeRenderResource
{
public:
	FeRenderTexture					Texture;
	uint32							Width;
	uint32							Height;
	bool							Shared;
	FeEWritableTextureFormat::Type	Format;

	FeRenderTargetTexture() : Shared(false)
	{}
	virtual uint32 GetSizeInMemory() override
	{
		return Texture.SizeInMemory;
	}
	virtual void DeleteSelf() override
	{
		FeRenderLoadingResource::DeleteResourceT(this);
	}

	void Release();
	ID3D11Resource* GetD3DResource(uint32 iIdx) const;
	ID3D11ShaderResourceView* GetD3DSRV(uint32 iIdx) const;
};

class FeRenderWritableTexture : public FeRenderResource
{
public:
	FeRenderTexture					Texture;
	uint32							Width;
	uint32							Height;
	FeEWritableTextureFormat::Type	Format;

	FeRenderWritableTexture()
	{}
	virtual uint32 GetSizeInMemory() override
	{
		return Texture.SizeInMemory;
	}
	virtual void DeleteSelf() override
	{
		FeRenderLoadingResource::DeleteResourceT(this);
	}

	void Release();
	ID3D11Resource* GetD3DResource(uint32 iIdx) const;
	ID3D11ShaderResourceView* GetD3DSRV(uint32 iIdx) const;
};
struct FeRenderFontChar
{
	uint32 Top;
	uint32 Left;
	uint32 OffsetTop;
	uint32 OffsetLeft;
	uint32 Width;
	uint32 Height;
};

class FeRenderFont : public FeRenderResource
{
public:
	FeRenderTexture		Texture;
	FT_FaceRec_*		FtFontFace;
	void*				MapTmpData;
	uint32				MapDepthPitch;
	FeSize				MapSize;
	uint32				Size;
	uint32				Space;
	uint32				LineSpace;
	uint32				Interval;
	FePath				TrueTypeFile;

	std::map<uint8, FeRenderFontChar> Chars;

	FeRenderFont() :
		FtFontFace(nullptr),
		MapTmpData(nullptr)
	{}
	virtual uint32 GetSizeInMemory() override
	{
		return Texture.SizeInMemory;
	}
	virtual void DeleteSelf() override
	{
		FeRenderLoadingResource::DeleteResourceT(this);
	}

	void Release();
	ID3D11Resource* GetD3DResource(uint32 iIdx) const;
	ID3D11ShaderResourceView* GetD3DSRV(uint32 iIdx) const;
};
