#include <effect.hpp>
#include <modulerenderer.hpp>
#include <renderresourceshandler.hpp>
#include "filesystem.hpp"

#include <d3dx11include.hpp>
#include <xnamath.h>

#define F_TIME_LOOP 1000.0f

struct FeCBPerFrame
{
	XMMATRIX MatrixView;
	XMMATRIX MatrixProj;
	float Time;
	XMVECTOR Viewport;
};
bool FeRenderEffect::IsValid()
{
	return VertexLayout
		&& VertexShader
		&& PixelShader;
}
FeRenderEffect::FeRenderEffect()
{
	static const FeString AssetType = "Effect";
	Type = AssetType;

	TextureLevels = 0;
	UseAlphaBlending = false;
	EffectType = FeERenderEffectType::Image;

	CBPerFrame.CpuBuffer = nullptr;
	CBPerObject.CpuBuffer = nullptr;

	CBPerFrame.Size = 0;
	CBPerObject.Size = 0;

	VertexLayout = nullptr;
	VertexShader = nullptr;
	PixelShader = nullptr;

	CBPerFrame.Buffer = nullptr;
	CBPerObject.Buffer = nullptr;
}
void FeRenderEffect::Release()
{
	for (auto& sampler : DefaultSamplers)
	{
		SafeRelease(sampler.second.State);
	}
	FE_DELETE_ARRAY(char, CBPerFrame.CpuBuffer, CBPerFrame.Size, FE_HEAPID_RENDERER);
	FE_DELETE_ARRAY(char, CBPerObject.CpuBuffer, CBPerObject.Size, FE_HEAPID_RENDERER);
	
	CBPerObject.CpuBuffer = nullptr;
	CBPerFrame.CpuBuffer = nullptr;

	SafeRelease(VertexLayout);
	SafeRelease(VertexShader);
	SafeRelease(PixelShader);

	SafeRelease(CBPerFrame.Buffer);
	SafeRelease(CBPerObject.Buffer);
}
void FeRenderEffect::BeginFrame(const FeRenderViewport& viewport, float fDt)
{
	ID3D11DeviceContext* pContext = FeModuleRendering::GetDevice().GetImmediateContext();

	XMVECTOR Eye	= XMVectorSet(0.0f, 0.0f, -2.0f, 0.0f);
	XMVECTOR At		= XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
	XMVECTOR Up		= XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	FeCBPerFrame cbPerFrame;

	//cbPerFrame.MatrixProj = XMMatrixPerspectiveFovLH(XM_PIDIV4, ((float)viewport.Width / (float)viewport.Height), 0.01f, 100.0f);
	cbPerFrame.MatrixProj = XMMatrixOrthographicLH(1.0f, 1.0f, -1.0f, 1.0f);
	cbPerFrame.MatrixView = XMMatrixIdentity();// XMMatrixLookAtLH(Eye, At, Up);

	static float matViewOffset[3] = { -0.5f, 0.5f, -1.0f };

	cbPerFrame.MatrixView._41 = matViewOffset[0];
	cbPerFrame.MatrixView._42 = matViewOffset[1];

	cbPerFrame.MatrixProj = XMMatrixTranspose(cbPerFrame.MatrixProj);
	cbPerFrame.MatrixView = XMMatrixTranspose(cbPerFrame.MatrixView);

	static float fTime = 0.0f;

	fTime += fDt * 0.001f;

	if (fTime>F_TIME_LOOP)
		fTime = fmodf(fTime, F_TIME_LOOP);

	cbPerFrame.Time = fTime;
	//cbPerFrame.Viewport = XMVectorSet((float)viewport.Rect.left, (float)viewport.Rect.top, (float)viewport.Rect.GetWidth(), (float)viewport.Rect.GetHeight());
	cbPerFrame.Viewport = XMVectorSet(0, 0, 1920.f, 1080.f);

	FE_ASSERT(CBPerFrame.Buffer, "invalid CB !");
	pContext->UpdateSubresource(CBPerFrame.Buffer, 0, nullptr, &cbPerFrame, 0, 0);
}
void FeRenderEffect::EndFrame()
{

}
void FeRenderEffect::ComputeTextureTransform(FeVector4& vTexTransform, const FeRenderGeometryInstance* geometryInstance, const FeRenderViewport& viewport)
{
	auto pResourcesHandler = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();

	const FeRenderResource* pResource = pResourcesHandler->GetResource(geometryInstance->Textures[0].Desired);
	if (!pResource)
		pResource = pResourcesHandler->GetResource(geometryInstance->Textures[0].Fallback);

	if (!pResource)
		return;

	const FeRenderTexture* pTexture = static_cast<const FeRenderTexture*>(pResource);

	if (!geometryInstance->Owner)
		return;

	float fScreenW = 1920.f;//viewport.Rect.GetWidth();
	float fScreenH = 1080.f;//viewport.Rect.GetHeight();

	float fTexW = pTexture->Width;
	float fTexH = pTexture->Height;

	float fScreenRatioH = fScreenW / fScreenH;

	float fNodeWN = FeGeometryHelper::GetMatrixScaleX(geometryInstance->Transform);
	float fNodeHN = FeGeometryHelper::GetMatrixScaleY(geometryInstance->Transform);

	float fNodeW = fNodeWN * fScreenW;
	float fNodeH = fNodeHN * fScreenH;

	float fNodeRatioH = fNodeH / fNodeW;
	float fTexRatioH = fTexH / fTexW;

	float fNodeRatioW = fNodeW / fNodeH;
	float fTexRatioW = fTexW / fTexH;

	bool bResizeX = false;
	bool bResizeY = false;
	
	vTexTransform[0] = vTexTransform[1] = 0.0f;
	vTexTransform[2] = vTexTransform[3] = 1.0f;

	switch (geometryInstance->Owner->GetImageResizeMode())
	{
	case FeEImageResize::Stretch:
	{
		// nothing to do
	} break;
	case FeEImageResize::LetterBox:
	{
		bResizeX = fNodeRatioH < fTexRatioH;
		bResizeY = !bResizeX;
	} break;
	case FeEImageResize::None:
	{
		vTexTransform[2] = (fNodeW / fTexW) * (1.f / FeGeometryHelper::GetMatrixScaleX(viewport.Transform));
		vTexTransform[3] = (fNodeH / fTexH) * (1.f / FeGeometryHelper::GetMatrixScaleX(viewport.Transform));
	} break;
	case FeEImageResize::PanScan:
	{
		bResizeX = fNodeRatioH>fTexRatioH;
		bResizeY = !bResizeX;
	} break;

	default:
		FE_ASSERT_NOT_IMPLEMENTED;
	}
	
	// 
	// Alignement values
	//   ______________
	//  |  0 |  1 |  2 |
	//  |____|____|____|
	//  |  3 |  4 |  5 |
	//  |____|____|____|
	//  |  6 |  7 |  8 |
	//  |____|____|____|
	// 
	// 

	float iAlignementH = (float)geometryInstance->Owner->GetImageHorizontalAlign();
	float iAlignementV = (float)geometryInstance->Owner->GetImageVerticalAlign();

	if (bResizeX)
		vTexTransform[2] = fTexRatioH*fNodeRatioW;

	if (bResizeY)
		vTexTransform[3] = fTexRatioW*fNodeRatioH;

	float fXOffset = iAlignementH*0.5f; // offset to : 1=center, 2=right
	vTexTransform[0] = (1.0f - vTexTransform[2])*fXOffset;

	float fYOffset = iAlignementV*0.5f; // offset to : 1=center, 2=bottom
	vTexTransform[1] = (1.0f - vTexTransform[3])*fYOffset;
}
void FeRenderEffect::BindGeometryInstance(const FeRenderGeometryInstance* geometryInstance, const FeModuleRenderResourcesHandler* resouresHandler, const FeRenderViewport& viewport)
{
	FeIShaderPropertyContainer* pOwner = geometryInstance->Owner;
	ID3D11DeviceContext* pContext = FeModuleRendering::GetDevice().GetImmediateContext();
	
	size_t offset = 0;
	char* outputBuffer = (char*)CBPerObject.CpuBuffer;
	
	XMMATRIX mat = XMMatrixTranspose(geometryInstance->Transform.getData());
	memcpy_s(outputBuffer, sizeof(XMMATRIX), &mat, sizeof(XMMATRIX));
	outputBuffer += sizeof(XMMATRIX);
	
	//memcpy_s(outputBuffer, 8, ratio, 8);
	FeVector4 textureTransform;
	ComputeTextureTransform(textureTransform, geometryInstance, viewport);
	memcpy_s(outputBuffer, 16, textureTransform.getData(), 16);
	outputBuffer += 16;

	if (pOwner)
	{
		auto eSampleMode = pOwner->GetImageSampleMode();
		pContext->PSSetSamplers(0, 1, &DefaultSamplers[eSampleMode].State); // set texture sample 0

		for (auto& Property : Properties)
		{
			switch (Property.GetType())
			{
			case FeERenderEffectPropertyType::Scalar:
			{
				const float& value = pOwner->GetShaderPropertyScalar(Property.GetName());
				memcpy_s(outputBuffer, 4, &value, 4);
				outputBuffer += 16;
			}break;
			case FeERenderEffectPropertyType::Vector:
			{
				const FeVector4& value = pOwner->GetShaderPropertyVector(Property.GetName());
				memcpy_s(outputBuffer, 16, value.getData(), 16);
				outputBuffer += 16;
			}break;
			case FeERenderEffectPropertyType::Color:
			{
				const FeColor& value = pOwner->GetShaderPropertyColor(Property.GetName());
				memcpy_s(outputBuffer, 16, value.getData(), 16);
				outputBuffer += 16;
			}break;
			}
		}
		
	}
	pContext->UpdateSubresource(CBPerObject.Buffer, 0, nullptr, CBPerObject.CpuBuffer, 0, 0);
}
void FeRenderEffect::Bind()
{
	ID3D11DeviceContext* pContext = FeModuleRendering::GetDevice().GetImmediateContext();

	pContext->IASetInputLayout((ID3D11InputLayout*)VertexLayout);
	pContext->VSSetShader(VertexShader, nullptr, 0);
	pContext->PSSetShader(PixelShader, nullptr, 0);

	pContext->VSSetConstantBuffers(0, 1, &CBPerFrame.Buffer);
	pContext->VSSetConstantBuffers(1, 1, &CBPerObject.Buffer);

	pContext->PSSetConstantBuffers(0, 1, &CBPerFrame.Buffer);
	pContext->PSSetConstantBuffers(1, 1, &CBPerObject.Buffer);

	//if (UseAlphaBlending)
		pContext->OMSetBlendState(BlendState, 0, 0xffffffff);	
	//else
	//	pContext->OMSetBlendState(nullptr, 0, 0xffffffff);
}
uint32 FeRenderEffect::ComputePerObjectCBSize()
{
	uint32 result = 0;

	result += sizeof(XMMATRIX); //MatrixWorld
	result += 16; // Screen dimensions

	for (auto& Property : Properties)
	{
		result += 16;
	}
	return result;
}
uint32 FeRenderEffect::CreateFromFile(const FePath& filePath)
{
	static FeStaticString<FE_STRING_SIZE_512> WorkString;

	FeFileTools::FormatFullPath(filePath, WorkString);

	// Compile the vertex shader
	HRESULT hr = S_OK;
	ID3DBlob* pVSBlob = nullptr;
	ID3D11Device* pD3DDevice = FeModuleRendering::GetDevice().GetD3DDevice();

	FE_FAILEDRETURN(FeRenderTools::CompileShaderFromFile(WorkString.Value, "VS", "vs_5_0", (void**)&pVSBlob));

	// Create the vertex shader
	hr = pD3DDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, (ID3D11VertexShader**)&VertexShader);
		
	if (FAILED(hr))
	{
		SafeRelease(pVSBlob);
		return FeEReturnCode::Rendering_CreateShaderFailed;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,	0, 0,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,		0, 3*4,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = pD3DDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), (ID3D11InputLayout**)&VertexLayout);
	SafeRelease(pVSBlob);

	if (FAILED(hr)) return FeEReturnCode::Rendering_CreateShaderFailed;

	// Compile the pixel shader
	ID3DBlob* pPSBlob = nullptr;
	hr = FeRenderTools::CompileShaderFromFile(WorkString.Value, "PS", "ps_5_0", (void**)&pPSBlob);

	if (FAILED(hr) || !pPSBlob)
	{
		MessageBox(nullptr, "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
		return hr;
	}

	// Create the pixel shader
	hr = pD3DDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, (ID3D11PixelShader**)&PixelShader);
	SafeRelease(pPSBlob);

	if (FAILED(hr)) return FeEReturnCode::Rendering_CreateShaderFailed;

	// Create the constant buffers
	{
		D3D11_BUFFER_DESC desc;
		ZeroMemory(&desc, sizeof(desc));

		CBPerFrame.Size = sizeof(FeCBPerFrame);
		CBPerFrame.CpuBuffer = FE_NEW_ARRAY(char, CBPerFrame.Size, FE_HEAPID_RENDERER);

		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = CBPerFrame.Size;
		desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		desc.CPUAccessFlags = 0;
		hr = pD3DDevice->CreateBuffer(&desc, nullptr, &CBPerFrame.Buffer);
		if (FAILED(hr))
			return FeEReturnCode::Failed;

		CBPerObject.Size = ComputePerObjectCBSize();
		CBPerObject.CpuBuffer = FE_NEW_ARRAY(char, CBPerObject.Size, FE_HEAPID_RENDERER);
		
		desc.ByteWidth = CBPerObject.Size;
		hr = pD3DDevice->CreateBuffer(&desc, nullptr, &CBPerObject.Buffer);

		if (FAILED(hr))
			return FeEReturnCode::Failed;
	}
	// Create blend state
	{
		D3D11_BLEND_DESC desc;
		ZeroMemory(&desc, sizeof(desc));

		D3D11_RENDER_TARGET_BLEND_DESC& targetDesc = desc.RenderTarget[0];
		targetDesc.BlendEnable		= true;
		targetDesc.SrcBlend			= D3D11_BLEND_SRC_ALPHA;
		targetDesc.DestBlend		= D3D11_BLEND_INV_SRC_ALPHA;

		targetDesc.BlendOp			= D3D11_BLEND_OP_ADD;
		
		targetDesc.SrcBlendAlpha	= D3D11_BLEND_ZERO;
		targetDesc.DestBlendAlpha	= D3D11_BLEND_ZERO;
		targetDesc.BlendOpAlpha		= D3D11_BLEND_OP_ADD;

		targetDesc.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

		hr = pD3DDevice->CreateBlendState(&desc, &BlendState);
		if (FAILED(hr))
			return FeEReturnCode::Failed;
	}
	// Create the sample state
	{
		for (int32 i = 0; i < FeEImageSample::Count; ++i)
		{
			auto eSampleMode = (FeEImageSample::Type)i;

			DefaultSamplers[eSampleMode] = FeRenderSampler();
			FeRenderSampler& sampler = DefaultSamplers[eSampleMode];

			D3D11_SAMPLER_DESC desc;
			ZeroMemory(&desc, sizeof(desc));

			desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;

			D3D11_TEXTURE_ADDRESS_MODE eAddressMode = D3D11_TEXTURE_ADDRESS_BORDER;

			switch (eSampleMode)
			{
			case FeEImageSample::Clamp:			eAddressMode = D3D11_TEXTURE_ADDRESS_CLAMP;			break;
			case FeEImageSample::Mirror:		eAddressMode = D3D11_TEXTURE_ADDRESS_MIRROR;		break;
			case FeEImageSample::MirrorOnce:	eAddressMode = D3D11_TEXTURE_ADDRESS_MIRROR_ONCE;	break;
			case FeEImageSample::Wrap:			eAddressMode = D3D11_TEXTURE_ADDRESS_WRAP;			break;
			}
			desc.AddressU = desc.AddressV = desc.AddressW = eAddressMode;

			desc.ComparisonFunc = D3D11_COMPARISON_NEVER;

			desc.MinLOD = 0;
			desc.MaxLOD = D3D11_FLOAT32_MAX;
			hr = pD3DDevice->CreateSamplerState(&desc, &sampler.State);

			if (FAILED(hr))

				return FeEReturnCode::Failed;
		}
	}

	return FeEReturnCode::Success;
}
