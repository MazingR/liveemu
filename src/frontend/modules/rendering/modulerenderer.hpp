#pragma once

#include <commonrenderer.hpp>
#include <common/application.hpp>

#include <geometry.hpp>
#include <effect.hpp>
#include <device.hpp>

typedef void(*FeRenderCallback) (FeRenderDevice& renderDevice);

struct FeRenderDebugInfos
{
	uint32	FrameDrawCallsCount;
	uint32	FrameBindEffectCount;
	uint32	FrameBindGeometryCount;
	uint32	FrameBindTextureCount;

	uint32 Framerate;
	uint32 CpuFrame;
	uint32 GpuFrame;
	uint32 CpuWait;
	uint32 DrawCalls;
	uint32 EffectBind;
	uint32 GeometryBind;
};

struct FeModuleRenderingInit : public FeModuleInit
{
public:
	FeRect		ScreenDimensions;
	bool		Debug;
	void*		WindowHandle;
	void*		WindowsInstance;
	int			WindowsCmdShow;
	bool		EnableFileWatcher;

	FeModuleRenderingInit()
	{
		ScreenDimensions.SetZero();

		Debug				 = 0;
		WindowHandle		 = 0;
		WindowsInstance		 = 0;
		WindowsCmdShow		 = 0;
		EnableFileWatcher	 = 0;
	}
};

struct FeRendererCache
{
	FeRenderViewport*		Viewport;
	FeRenderEffect*			Effect;
	FeResourceId			EffectId;
	FeRenderGeometryData*	Geometry;
	FeResourceId			GeometryId;

	FeResourceId Textures[16];


	void Clear()
	{
		Viewport	= nullptr;
		Effect		= nullptr;
		Geometry	= nullptr;

		EffectId	= 0;
		GeometryId	= FE_INVALID_ID;

		memset(Textures, 0, sizeof(FeResourceId) * 16); // reset texture binding cache
	}
};

/// <summary>
/// Module main class to manage rendering
/// </summary>
class FeModuleRendering : public FeModule
{
public:
	static FeModuleRendering* Get()
	{
		static FeModuleRendering* instance = nullptr;

		if (!instance)
			instance = FeApplication::GetStaticInstance()->GetModule<FeModuleRendering>();

		return instance;
	}
	FeModuleRendering();

	static FeRenderDevice& GetDevice() { return Device; }

	virtual uint32 Load(const FeModuleInit*) override;
	virtual uint32 Unload() override;
	virtual uint32 Update(const FeDt& fDt) override;
	virtual const char* GetName()
	{
		static const char* sName = "Rendering";
		return sName;
	}

	uint32 Reload();
	void RegisterRenderBatch(FeRenderBatch* batch);
	void UnloadEffects();
	uint32 ReloadEffects();
	uint32 LoadEffect(FeAssetFile* assetFile);
	const FeRenderViewport& GetDefaultViewport() const { return DefaultViewport; }
	virtual char* ComputeDebugOutput(const FeDt& fDt);
	void DebugOutputNextModule();
	FeRect GetScreenDimensions() const;
	void RegisterRenderCallback(FeRenderCallback callback);
	bool ResizeScreen(FeRect newDimensions);
	bool BindSharedTargetFromHandle(uint64 value);

private:
	void BeginRender();
	void EndRender();
	void RenderBatch(FeTArray<FeRenderGeometryInstance*>& instances, const FeDt& fDt);
	void RenderEffectBatch(const FeRenderBatch* batch, const FeDt& fDt, uint32 effectId, FeRenderEffect* pEffect);
	void RenderDebugText(const FeDt& fDt);

	typedef std::map<FeResourceId, FeRenderEffect*> FeEffectsMap;
	static FeRenderDevice Device;
	FeEffectsMap Effects;

	FeTArray<FeRenderGeometryData> Geometries;
		
	FeTArray<FeRenderBatch*>		RegisteredRenderBatches;
	IFW1Factory*					FW1Factory;
	IFW1FontWrapper*				FontWrapper;

	uint32							CurrentModuleDebugIndex;
	char							DebugString[DEBUG_STRING_SIZE];
	FeRenderDebugInfos				RenderDebugInfos;
	FeRenderViewport				DefaultViewport;
	uint64							SharedTargetHandle;
	void*							SharedTargetTexture;
	FeModuleRenderingInit			ModuleInit;
	FeTArray<FeRenderCallback>		RegisteredRenderCallbacks;
	FeRect							ScreenDimensions;
	FeRendererCache					Cache;
	FeTArray<FeRenderGeometryInstance*> DefaultViewportBatch;
};
