#pragma once

#include <commonrenderer.hpp>
#include <common/application.hpp>

uint32 ComputeResourceSizeInMemoryFromFormat(uint32 iWidth, uint32 iHeight, uint32 iResourceFormat, bool bHasAlpha);

// forward declares
struct SDL_mutex;
struct SDL_Thread;
struct FT_LibraryRec_;
struct D3D11_TEXTURE2D_DESC;

struct FeModuleRenderResourcesHandlerDebugInfos
{
	uint32 LoadedResourcesCount;
	uint32 ResourcesPoolSize;
	uint32 LoadedResourcesCountSizeInMemory;
};
class FeModuleRenderResourcesHandler : public FeModule
{

	typedef std::map<FeResourceId, FeRenderLoadingResource> ResourcesLoadingMap;
	typedef ResourcesLoadingMap::iterator ResourcesLoadingMapIt;
	typedef std::map<FeResourceId, FeRenderResource*> ResourcesMap;
	typedef ResourcesMap::iterator ResourcesMapIt;

	struct LockedLoadingResourcesMap
	{
		SDL_mutex*			Mutex;
		ResourcesLoadingMap	Resources;
	};
	

public:
	static FeModuleRenderResourcesHandler* Get()
	{
		static FeModuleRenderResourcesHandler* instance = nullptr;

		if (!instance)
			instance = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();

		return instance;
	}

	virtual uint32 Load(const FeModuleInit*) override;
	virtual uint32 Unload() override;
	virtual uint32 Update(const FeDt& fDt) override;
	virtual char* ComputeDebugOutput(const FeDt& fDt) { return ""; }
	virtual const char* GetName()
	{
		static const char* sName = "ResourcesHandler";
		return sName;
	}

	uint32 LoadResource(FeRenderLoadingResource& resourceLoading);
	uint32 UnloadResource(const FeResourceId&);
	FeRenderResource* GetResource(const FeResourceId&);
	FeRenderResource* GetResource(const FeResourcePath&);

	void ComputeDebugInfos(FeModuleRenderResourcesHandlerDebugInfos& infos);
	uint32 ProcessThreadedResourcesLoading(bool& bThreadSopped);
	void UnloadResources();
private:
	void ProcessLoadedResources();
	uint32 LoadRenderTargetTexture(FeRenderLoadingResource& resource, FeRenderTargetTexture* pTextureData);
	uint32 LoadRenderWritableTexture(FeRenderLoadingResource& resource, FeRenderWritableTexture* pTextureData);
	uint32 LoadTexture(FeRenderLoadingResource& resource, FeRenderTexture* pTextureData);

	uint32 TryLoadOneTexture(FeRenderResource* pTextureData, FeRenderTexture* RenderTexture, const D3D11_TEXTURE2D_DESC& desc);

	uint32 LoadFont(FeRenderLoadingResource& resource, FeRenderFont* pTextureData);
	uint32 PostLoadFont(FeRenderLoadingResource& resource, FeRenderFont* pTextureData);
	uint32 SaveTexture(FeRenderLoadingResource& resource, FeRenderTexture* pTextureData);

	std::map<FeEResourceLoadingState::Type, LockedLoadingResourcesMap> LoadingResources;

	SDL_Thread*			LoadingThread;
	SDL_mutex*			LoadingThreadMutex;
	ResourcesMap		Resources;

	std::atomic_uint	ResourcePoolAllocated;
	uint32				ResourcePoolLimit;

	FT_LibraryRec_*		FtLibrary;
};