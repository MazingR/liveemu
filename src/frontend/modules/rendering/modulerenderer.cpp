#include <modulerenderer.hpp>
#include <renderresourceshandler.hpp>
#include <common/memorymanager.hpp>
#include <common/filesystem.hpp>

#include <d3dx11include.hpp>
#include "FW1FontWrapper.h"
#include <SDL.h>

#define UPDATE_RENDER_INFOS_FREQUENCY 30
#define D3DFAILEDRETURN(func) { HRESULT ___hr = (func); if (___hr!=S_OK) return ___hr; }

void OnEffectFileChanged(FeEFileChangeType::Type eChangeType, const char* szPath, void* pUserData)
{
	FE_LOG_ERROR("");
	((FeModuleRendering*)pUserData)->ReloadEffects();
}

FeModuleRendering::FeModuleRendering()
{
	FW1Factory = nullptr;
	FontWrapper = nullptr;
	SharedTargetHandle = 0;
	SharedTargetTexture = nullptr;
}
uint32 FeModuleRendering::ReloadEffects()
{
	FeTArray<FeResourceId> EffectsFailed;

	for (auto& effect : Effects)
	{
		auto pEffect = effect.second;
		pEffect->Release();

		if (FE_FAILED(pEffect->CreateFromFile(pEffect->File)))
			pEffect->Release();
	}
	return FeEReturnCode::Success;
}
void FeModuleRendering::UnloadEffects()
{
	for (auto& effect : Effects)
	{
		effect.second->Release();
		effect.second->SelfDelete(FE_HEAPID_RENDERER);
	}
	Effects.clear();
}
uint32 FeModuleRendering::LoadEffect(FeAssetFile* assetFile)
{
	auto effect = assetFile->GetAsset<FeRenderEffect>();

	uint32 iId = assetFile->AssetRef.Path.GetId();
	FeRenderEffect* pEffect = nullptr;

	if (Effects.find(iId) != Effects.end())
		pEffect = Effects[iId];

		
	if (!pEffect)
		pEffect = FE_NEW(FeRenderEffect, FE_HEAPID_RENDERER);
	else
		pEffect->Release();

	*pEffect = *effect; // copy
	Effects[iId] = pEffect;

	if (FE_FAILED(pEffect->CreateFromFile(pEffect->File)))
	{
		pEffect->Release();
		return FeEReturnCode::Failed;
	}

	return FeEReturnCode::Success;
}
uint32 FeModuleRendering::Reload()
{
	Unload();

	CurrentModuleDebugIndex = 0;
	ScreenDimensions = ModuleInit.ScreenDimensions;

	FE_FAILEDRETURN(Device.Initialize(ModuleInit.WindowHandle));

	// Creat static geometries (primitive forms)
	Geometries.Reserve(16);
	Geometries.SetZeroMemory();
	FeRenderGeometryData& geometryData = Geometries.Add();
	FeResourceId geometryId;
	FeGeometryHelper::CreateStaticGeometry(FeEGemetryDataType::Quad, &geometryData, &geometryId);

	HRESULT hResult = FW1CreateFactory(FW1_VERSION, &FW1Factory);
	hResult = FW1Factory->CreateFontWrapper(Device.GetD3DDevice(), L"Areial", &FontWrapper);

	ZeroMemory(&RenderDebugInfos, sizeof(FeRenderDebugInfos));

	DefaultViewport.CreateFromBackBuffer();

	auto pFileManagerModule = FeApplication::GetStaticInstance()->GetModule<FeModuleFilesManager>();

	if (ModuleInit.EnableFileWatcher)
	{
		char szWatchPath[COMMON_PATH_SIZE];
		sprintf_s(szWatchPath, "%s/%s", FePath::GetRoot(), "");
		pFileManagerModule->WatchDirectory(szWatchPath, OnEffectFileChanged, this);
	}

	return FeEReturnCode::Success;
}
uint32 FeModuleRendering::Load(const FeModuleInit* initBase)
{
	ModuleInit = *(FeModuleRenderingInit*)initBase;
	return Reload();
}
uint32 FeModuleRendering::Unload()
{
	RegisteredRenderBatches.Clear();

	Geometries.Clear();
	Cache.Clear();
	UnloadEffects();

	FeGeometryHelper::ReleaseGeometryData();
	FeGeometryHelper::ReleaseStaticGeometryData();

	SafeRelease(FontWrapper);
	SafeRelease(FW1Factory);

	DefaultViewport.Unload();
	Device.Release();

	return FeEReturnCode::Success;
}

uint32 FeModuleRendering::Update(const FeDt& fDt)
{
	if (!Device.IsValid())
		return FeEReturnCode::Success;

	// Render frame
	BeginRender();
	{
		DefaultViewportBatch.Clear();

		for (auto& batch : RegisteredRenderBatches)
		{
			if (batch->Viewport.UseBackBuffer)
				DefaultViewportBatch.Add(batch->GeometryInstances);
		}

		// Render all registered batches
		RenderBatch(DefaultViewportBatch, fDt);

		for (auto& batch : RegisteredRenderBatches)
		{
			if (false==batch->Viewport.UseBackBuffer)
				RenderBatch(batch->GeometryInstances, fDt);
		}

		//RenderDebugText(fDt);

		for (auto& callback : RegisteredRenderCallbacks)
			callback(Device);
	}
	EndRender();
	return FeEReturnCode::Success;
}
void FeModuleRendering::BeginRender()
{
	RenderDebugInfos.FrameDrawCallsCount = 0;
	RenderDebugInfos.FrameBindEffectCount = 0;
	RenderDebugInfos.FrameBindGeometryCount = 0;
	RenderDebugInfos.FrameBindTextureCount = 0;

	DefaultViewport.Clear();

	for (uint32 i = 0; i < RegisteredRenderBatches.GetSize(); ++i)
		RegisteredRenderBatches[i]->Viewport.Clear();

	Cache.Clear();
		
}
void FeModuleRendering::EndRender()
{
	uint32 iTicks = SDL_GetTicks();

	if (SharedTargetTexture)
		Device.GetImmediateContext()->CopyResource((ID3D11Resource*)SharedTargetTexture, DefaultViewport.Texture);

	// Present the information rendered to the back buffer to the front buffer (the screen)
	Device.GetSwapChain()->Present(0, 0);
		
	RegisteredRenderBatches.Clear();
}

bool TryBindTexture(FeResourceId textureId, uint32 iChannel, ID3D11DeviceContext* pContext, FeResourceId& lastBindedTexture, FeModuleRenderResourcesHandler* pResourcesHandler, bool& bChangedTexture)
{
	if (textureId)
	{
		if (lastBindedTexture != textureId)
		{
			lastBindedTexture = textureId;

			const FeRenderResource* pResource = pResourcesHandler->GetResource(textureId);

			if (pResource && pResource->LoadingState == FeEResourceLoadingState::Loaded)
			{
				ID3D11ShaderResourceView* pSRV = pResource->GetD3DSRV(0);

				pContext->PSSetShaderResources(iChannel, 1, &pSRV);
				bChangedTexture = true;
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	return false;
}

void FeModuleRendering::RenderBatch(FeTArray<FeRenderGeometryInstance*>& instances, const FeDt& fDt)
{
	{ // Sort geometry instances along Z axis
		auto ComputeGeometryZCompare = [](const FeRenderGeometryInstance* a, const FeRenderGeometryInstance* b) -> int
		{
			float aZ = FeGeometryHelper::GetMatrixTranslationZ(a->Transform);
			float bZ = FeGeometryHelper::GetMatrixTranslationZ(b->Transform);

			if (aZ > bZ)	return 1;
			else if (aZ < bZ)	return 2;
			else				return 0;
		};

		FeArrayHelper::QuickSort<FeRenderGeometryInstance*>::Execute(instances, ComputeGeometryZCompare);
	}
	
	static bool bForceDefaultEffect = false;
	
	auto pResourcesHandler = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();
	ID3D11DeviceContext* pContext = GetDevice().GetImmediateContext();

	for (uint32 iInstanceIdx = 0; iInstanceIdx < instances.GetSize(); ++iInstanceIdx)
	{
		const FeRenderGeometryInstance* pGeomInstance = instances[iInstanceIdx];
		if (pGeomInstance->IsCulled)
			continue;

		FE_ASSERT(pGeomInstance, "null geometry");

		const FeRenderViewport* Viewport = pGeomInstance->Viewport;
		
		{ // bind viewport & apply scissor
			D3D11_RECT scissorRect;

			FE_ASSERT(Viewport, "null viewport");

			memcpy_s(&scissorRect, sizeof(D3D11_RECT), &Viewport->Rect, sizeof(D3D11_RECT));
			pContext->RSSetScissorRects(1, &scissorRect);

			const FeRenderViewport* ViewportToBind = Viewport;

			if (Viewport->UseBackBuffer)
				ViewportToBind = &DefaultViewport;

			if (Cache.Viewport != ViewportToBind)
				ViewportToBind->Bind();
		}
		
		FeRenderEffect* pEffect = Cache.Effect;

		{ // bind effect
			if (Cache.EffectId != pGeomInstance->Effect)
			{
				pEffect = nullptr;
				FeEffectsMap::iterator itFound = Effects.find(pGeomInstance->Effect);

				if (itFound != Effects.end() && itFound->second->IsValid())
				{
					pEffect = itFound->second;

					pEffect->BeginFrame(*Viewport, fDt.TotalSeconds);
					pEffect->Bind();

					Cache.EffectId = pGeomInstance->Effect;
					Cache.Effect = pEffect;

					RenderDebugInfos.FrameBindEffectCount++;
				}
			}
		}

		if (!pEffect)
			continue; // <-- no effect to bind

		{ // bind geometry (constant buffer)
			pEffect->BindGeometryInstance(pGeomInstance, pResourcesHandler, *Viewport);
		}

		bool bCull = false;

		{ // bind resources (textures)
			for (uint32 iChannel = 0; iChannel < pEffect->GetTextureLevels(); ++iChannel)
			{
				bool bBinded = false;
				bool bChangedTexture = false;

				bBinded = TryBindTexture(pGeomInstance->Textures[iChannel].Desired, iChannel, pContext, Cache.Textures[iChannel], pResourcesHandler, bChangedTexture);

				if (!bBinded)
					bBinded = TryBindTexture(pGeomInstance->Textures[iChannel].Fallback, iChannel, pContext, Cache.Textures[iChannel], pResourcesHandler, bChangedTexture);

				if (bChangedTexture)
					RenderDebugInfos.FrameBindTextureCount++;

				if (!bBinded)
				{
					bCull = true;
					break;
				}
			}
		}
		if (bCull)
			continue;

		FeRenderGeometryData* pGeometryData = Cache.Geometry;

		{// bind geometry data (index/vertex buffers)

			if (pGeomInstance->Geometry != Cache.GeometryId)
			{
				FE_ASSERT(Geometries.GetSize() >= pGeomInstance->Geometry, "Invalid geometry Id !");
				pGeometryData = &Geometries[pGeomInstance->Geometry - 1];
				Cache.Geometry = pGeometryData;
				Cache.GeometryId = pGeomInstance->Geometry;

				UINT offset = 0;
				pContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
				pContext->IASetVertexBuffers(0, 1, (ID3D11Buffer**)&pGeometryData->VertexBuffer, &pGeometryData->Stride, &offset);
				pContext->IASetIndexBuffer((ID3D11Buffer*)pGeometryData->IndexBuffer, DXGI_FORMAT_R16_UINT, 0);
				RenderDebugInfos.FrameBindGeometryCount++;
			}
		}
		{ // draw instance to viewport
			RenderDebugInfos.FrameDrawCallsCount++;
			pContext->DrawIndexed(pGeometryData->IndexCount, 0, 0);
		}
	}
}

void DrawTextLines(const char* szText, uint32& iLinesCount, uint32 iLineLength, float fFontSize, uint32 iColor, IFW1FontWrapper* pFontWrapper, ID3D11DeviceContext *pContext)
{
	wchar_t szWTmp[DEBUG_STRING_SIZE];
	char szTmp[DEBUG_STRING_SIZE];
	
	uint32 iNewLinesCount = 0;
	uint32 iTextLen = strlen(szText);
	uint32 iLineStart = 0;
	uint32 iLineEnd = FeStringTools::IndexOf(szText, '\n');
	uint32 iLineSize = iLineEnd - iLineStart;

	memset(szTmp, 0, DEBUG_STRING_SIZE);
	memcpy_s(szTmp, DEBUG_STRING_SIZE, szText, iTextLen);

	while (iLineStart < iTextLen && iLineLength<iTextLen)
	{
		bool bCut = false;

		// didn't find a '\n'
		if ((size_t)-1 == iLineEnd)
		{
			iLineStart += min(iTextLen - iLineStart, iLineLength);
			bCut = true;
		}
		else if (iLineSize <= iLineLength)
		{
			iLineStart = iLineEnd + 1;
		}
		// '\n' is too far, cut the line
		else
		{
			iLineStart += iLineLength;
			bCut = true;
		}
		if (bCut)
		{
			szTmp[iLineStart] = '\n';
			memcpy_s(szTmp + iLineStart + 1, DEBUG_STRING_SIZE - iLineStart - 1, szText + iLineStart, iTextLen - iLineStart);
		}
		iNewLinesCount++;

		if (iLineStart + iLineLength >= iTextLen)
		{
			iNewLinesCount++;
			break;
		}

		iLineEnd = FeStringTools::IndexOf(szText, '\n', iLineStart);
		iLineSize = iLineEnd - iLineStart;
	}
	size_t iWSize;
	mbstowcs_s(&iWSize, szWTmp, szTmp, DEBUG_STRING_SIZE);
	pFontWrapper->DrawString(
		pContext,
		szWTmp,// String
		fFontSize,// Font size
		10.0f,// X position
		iLinesCount*fFontSize,// Y position
		iColor,// Text color, 0xAaBbGgRr
		FW1_RESTORESTATE // Flags (for example FW1_RESTORESTATE to keep context states unchanged)
		);

	iLinesCount += iNewLinesCount;
}
void FeModuleRendering::RenderDebugText(const FeDt& fDt)
{
	uint32 iLinesCount = 0;
	uint32 iLineLength = (ScreenDimensions.GetWidth() / 15) * 2 - 1;
	DrawTextLines(FeGetLastError(), iLinesCount, iLineLength, 15.0f, 0xff0000ff, FontWrapper, Device.GetImmediateContext());
	iLinesCount += 3;

	FeModule* pModule = FeApplication::GetStaticInstance()->GetModule(CurrentModuleDebugIndex);
	const char* szModuleName = pModule->GetName();
	DrawTextLines(szModuleName, iLinesCount, iLineLength, 15.0f, 0xffaafffff, FontWrapper, Device.GetImmediateContext());
	iLinesCount += 1;
	
	const char* szTxt = pModule->ComputeDebugOutput(fDt);
	DrawTextLines(szTxt, iLinesCount, iLineLength, 15.0f, 0xff00ffff, FontWrapper, Device.GetImmediateContext());
}
char* FeModuleRendering::ComputeDebugOutput(const FeDt& fDt)
{
	auto pResourcesHandler = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();

	static uint32 iFrameCount = 0;
	if (fDt.TotalMilliseconds == 0)
		return "";

	size_t iOffset = 0;

	if (++iFrameCount == UPDATE_RENDER_INFOS_FREQUENCY)
	{
		iFrameCount = 0;

		RenderDebugInfos.Framerate = (1000 / fDt.TotalMilliseconds);
		RenderDebugInfos.CpuFrame = fDt.TotalMilliseconds;
		RenderDebugInfos.GpuFrame = 0;
		RenderDebugInfos.CpuWait = fDt.TotalCpuWaited;
		RenderDebugInfos.DrawCalls = RenderDebugInfos.FrameDrawCallsCount;
		RenderDebugInfos.EffectBind = RenderDebugInfos.FrameBindEffectCount;
		RenderDebugInfos.GeometryBind = RenderDebugInfos.FrameBindGeometryCount;
	}

	sprintf_s(&DebugString[iOffset], DEBUG_STRING_SIZE - iOffset, "\nMemory\n");

	iOffset = strlen(DebugString);
	FeMemoryManager::StaticInstance.GetDebugInfos(&DebugString[iOffset], DEBUG_STRING_SIZE - iOffset);

	iOffset = strlen(DebugString);
	sprintf_s(&DebugString[iOffset], DEBUG_STRING_SIZE - iOffset, "\nPerformance\n");

	iOffset = strlen(DebugString);
	FeModuleRenderResourcesHandlerDebugInfos resourcesDebugInfos;
	pResourcesHandler->ComputeDebugInfos(resourcesDebugInfos);
	sprintf_s(&DebugString[iOffset], DEBUG_STRING_SIZE - iOffset, "\
Texture Count\t%d \n\
Texture Mem.\t%4.4f / %4.0f (MB) \n\
",
		resourcesDebugInfos.LoadedResourcesCount,
		(resourcesDebugInfos.LoadedResourcesCountSizeInMemory) / (1024.0f*1024.0f),
		(resourcesDebugInfos.ResourcesPoolSize) / (1024.0f*1024.0f));
		
	uint32 fGpuFrameTime = 0;
	uint32 fFramerate = (1000 / fDt.TotalMilliseconds);

	iOffset = strlen(DebugString);
	sprintf_s(&DebugString[iOffset], DEBUG_STRING_SIZE - iOffset,
"\
Mode          \t: %s\n\
Fps           \t: %d\n\
Cpu (ms)      \t: %d\n\
Gpu (ms)      \t: %d\n\
CpuWait (ms)\t: %d\n\
Draw calls\t: %d \n\
Effect bind\t: %d \n\
Geom. bind\t: %d \n\
Texture bind\t: %d \n\
",
		CONFIGSTR
		, RenderDebugInfos.Framerate
		, RenderDebugInfos.CpuFrame
		, RenderDebugInfos.GpuFrame
		, RenderDebugInfos.CpuWait
		, RenderDebugInfos.DrawCalls
		, RenderDebugInfos.EffectBind
		, RenderDebugInfos.GeometryBind
		, RenderDebugInfos.FrameBindTextureCount);


	return DebugString;
}
void FeModuleRendering::RegisterRenderBatch(FeRenderBatch* batch)
{
	RegisteredRenderBatches.Add(batch);
}
void FeModuleRendering::DebugOutputNextModule()
{
	CurrentModuleDebugIndex = (CurrentModuleDebugIndex + 1) % FeApplication::GetStaticInstance()->GetModulesCount();
}
FeRect FeModuleRendering::GetScreenDimensions() const
{
	return ScreenDimensions;
}
void FeModuleRendering::RegisterRenderCallback(FeRenderCallback callback)
{
	RegisteredRenderCallbacks.AddUnique(callback);
}
bool FeModuleRendering::ResizeScreen(FeRect newDimensions)
{
	if (ModuleInit.ScreenDimensions == newDimensions) // nothging changed
		return false;

	ScreenDimensions = newDimensions;
	Device.SetNativeResolution(newDimensions);

	auto moduleResources = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();
	moduleResources->UnloadResources();
	ModuleInit.ScreenDimensions = newDimensions;
	Reload();

	return true;
}
bool FeModuleRendering::BindSharedTargetFromHandle(uint64 value)
{
	HRESULT hr;

	if (value != 0)
	{
		hr = GetDevice().GetD3DDevice()->OpenSharedResource((HANDLE)value, __uuidof(ID3D11Texture2D), (LPVOID*)&SharedTargetTexture);

		if (FAILED(hr))
			return false;

		SharedTargetHandle = value;
	}

	return true;
}