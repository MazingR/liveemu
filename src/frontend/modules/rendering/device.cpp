#include <device.hpp>
#include <modulerenderer.hpp>

#include <d3dx11include.hpp>

FeRenderDevice FeModuleRendering::Device;

void FeRenderDevice::Release()
{
	if (ImmediateContext) ImmediateContext->ClearState();
	if (LoadingThreadContext) LoadingThreadContext->ClearState();
	
	SafeRelease(ImmediateContext);
	SafeRelease(LoadingThreadContext);
	SafeRelease(SwapChain);
	SafeRelease(D3dDevice);
}
uint32 FeRenderDevice::Initialize(void* hwnd)
{
	WindowHandle = hwnd;

	RECT rc;
	GetClientRect((HWND)hwnd, &rc);

	// Save up window resolution
	memccpy(&NativeResolution, &rc, sizeof(FeRect), sizeof(FeRect));


	return CreateDevice();
}
uint32 FeRenderDevice::CreateDevice()
{
	HRESULT hr = S_OK;
	D3D_DRIVER_TYPE         DriverType = D3D_DRIVER_TYPE_NULL;
	D3D_FEATURE_LEVEL       FeatureLevel = D3D_FEATURE_LEVEL_11_0;

	UINT createDeviceFlags = 0;
#ifdef DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = NativeResolution.right;
	sd.BufferDesc.Height = NativeResolution.bottom;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = (HWND)WindowHandle;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		DriverType = driverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain(nullptr, DriverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, &sd, &SwapChain, &D3dDevice, &FeatureLevel, &ImmediateContext);

		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
		return FeEReturnCode::Failed;

	// create deferred contexts
	D3dDevice->CreateDeferredContext(0, &LoadingThreadContext);
	
	// Create the rasterizer state
	{
		D3D11_RASTERIZER_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.FillMode = D3D11_FILL_SOLID;
		desc.CullMode = D3D11_CULL_NONE;
		desc.ScissorEnable = true;
		desc.DepthClipEnable = false;
		D3dDevice->CreateRasterizerState(&desc, &RasterizerState);
	}
	{
		D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
		depthStencilDesc.DepthEnable		= TRUE;
		depthStencilDesc.DepthWriteMask		= D3D11_DEPTH_WRITE_MASK_ALL;
		depthStencilDesc.DepthFunc			= D3D11_COMPARISON_LESS;
		depthStencilDesc.StencilEnable		= FALSE;
		depthStencilDesc.StencilReadMask	= 0xFF;
		depthStencilDesc.StencilWriteMask	= 0xFF;

		// Stencil operations if pixel is front-facing
		depthStencilDesc.FrontFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilDepthFailOp	= D3D11_STENCIL_OP_INCR;
		depthStencilDesc.FrontFace.StencilPassOp		= D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;

		// Stencil operations if pixel is back-facing
		depthStencilDesc.BackFace.StencilFailOp			= D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilDepthFailOp	= D3D11_STENCIL_OP_DECR;
		depthStencilDesc.BackFace.StencilPassOp			= D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;

		ID3D11DepthStencilState *m_DepthStencilState;
		D3dDevice->CreateDepthStencilState(&depthStencilDesc, &m_DepthStencilState);
		ImmediateContext->OMSetDepthStencilState(m_DepthStencilState, 0);
	}

	ImmediateContext->RSSetState(RasterizerState);

	return FeEReturnCode::Success;

}
void FeRenderDevice::SetNativeResolution(const FeRect& rect)
{
	NativeResolution = rect;
}