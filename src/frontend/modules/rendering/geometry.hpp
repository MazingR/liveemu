#pragma once

#include <commonrenderer.hpp>

class FeGeometryHelper
{
public:
	static float* GetMatrixTranslationData(FeMatrix4& mat);

	static float GetMatrixTranslationX(const FeMatrix4& mat);
	static float GetMatrixTranslationY(const FeMatrix4& mat);
	static float GetMatrixTranslationZ(const FeMatrix4& mat);

	static float GetMatrixScaleX(const FeMatrix4& mat);
	static float GetMatrixScaleY(const FeMatrix4& mat);
	static float GetMatrixScaleZ(const FeMatrix4& mat);

	static float& GetMatrixTranslationX(FeMatrix4& mat);
	static float& GetMatrixTranslationY(FeMatrix4& mat);
	static float& GetMatrixTranslationZ(FeMatrix4& mat);

	static float& GetMatrixScaleX(FeMatrix4& mat);
	static float& GetMatrixScaleY(FeMatrix4& mat);
	static float& GetMatrixScaleZ(FeMatrix4& mat);

	static const FeMatrix4& IdentityMatrix();

	static uint32 CreateVertexAndIndexBuffer(ID3D11Buffer** pVertexBuffer, ID3D11Buffer** pIndexBuffer, void* vertexData, uint32 vertexCount, void* indexData, uint32 indexCount);
	static uint32 CreateStaticGeometry(FeEGemetryDataType::Type eType, FeRenderGeometryData* geometryData, FeResourceId* geometryId);
	static uint32 CreateGeometry(void* vertexBuffer, uint32 iVertexCount, void* indexBuffer, uint32 iIndexCount, FeRenderGeometryData* geometryData, FeResourceId* geometryId);
	static void ComputeMatrix(FeMatrix4& output, const FeVector3 vTranslate, const FeRotation& vRotate, const FeVector3& vScale);
	static void ComputeMatrix(FeMatrix4& output, const FeTransform& transform);

	static FeResourceId GetStaticGeometry(FeEGemetryDataType::Type eType);

	static void ReleaseGeometryData();
	static void ReleaseStaticGeometryData();
private:
	static uint32 ComputeStaticGeometry();
	static void AddStaticGeometry(FeEGemetryDataType::Type type, void* vertexData, int32 vertexCount, uint16* indexData, int32 indexCount);
	static FeTArray<FeRenderGeometryData> StaticGeometryData;
	static FeTArray<FeRenderGeometryData> GeometryData;
	static bool ComputedGeometry;
};