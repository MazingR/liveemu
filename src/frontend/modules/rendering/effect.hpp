#pragma once

#include <commonrenderer.hpp>

// forward declares
struct FeRenderGeometryInstance;
struct FeRenderViewport;
class FeModuleRenderResourcesHandler;


#define FeERenderEffectType_Values(_d)		\
		_d(FeERenderEffectType, Image)		\
		_d(FeERenderEffectType, Font)		\

FE_DECLARE_ENUM(FeERenderEffectType, FeERenderEffectType_Values)

#define FeERenderEffectPropertyType_Values(_d)		\
		_d(FeERenderEffectPropertyType, Scalar)		\
		_d(FeERenderEffectPropertyType, Color)		\
		_d(FeERenderEffectPropertyType, Vector)		\

FE_DECLARE_ENUM(FeERenderEffectPropertyType, FeERenderEffectPropertyType_Values)


class FeRenderEffectProperty : public FeSerializable
{
public:
#define FeRenderEffectProperty_Properties(_d)			\
		_d(FeString,							Name)	\
		_d(FeERenderEffectPropertyType::Type,	Type)	\

	FeRenderEffectProperty()
	{
		Type = FeERenderEffectPropertyType::Scalar;
	}

	FE_DECLARE_CLASS_BODY(FeRenderEffectProperty_Properties, FeRenderEffectProperty, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeRenderEffectProperty)

class FeRenderEffect : public FeAsset
{
public:
	void BeginFrame(const FeRenderViewport& viewport, float fDt);
	void EndFrame();

	void BindGeometryInstance(const FeRenderGeometryInstance* geometry, const FeModuleRenderResourcesHandler* resouresHandler, const FeRenderViewport& viewport);
	void Bind();
	void Release();
	uint32 CreateFromFile(const FePath& szFilePath);
	bool IsValid();

	FeRenderEffect();
private:
	uint32 ComputePerObjectCBSize();
	void ComputeTextureTransform(FeVector4& vTexTransform, const FeRenderGeometryInstance* geometryInstance, const FeRenderViewport& viewport);

	ID3D11VertexShader*					VertexShader;
	ID3D11PixelShader*					PixelShader;
	ID3D11InputLayout*					VertexLayout;
	ID3D11BlendState*					BlendState;

	std::map<FeEImageSample::Type, FeRenderSampler> DefaultSamplers;

	FeRenderConstantBuffer				CBPerFrame;
	FeRenderConstantBuffer				CBPerObject;

	#define FeRenderEffect_Properties(_d)							\
		_d(FePath,								File)				\
		_d(int32,								TextureLevels)		\
		_d(bool,								UseAlphaBlending)	\
		_d(FeTArray<FeRenderEffectProperty>,	Properties)			\
		_d(FeERenderEffectType::Type,			EffectType)			\

	FE_DECLARE_CLASS_BODY(FeRenderEffect_Properties, FeRenderEffect, FeAsset)
};
FE_DECLARE_CLASS_BOTTOM(FeRenderEffect)