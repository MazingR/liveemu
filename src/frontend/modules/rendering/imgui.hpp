#pragma once


struct ID3D11Device;
struct ID3D11DeviceContext;
class FeRenderDevice;
struct FeRect;
struct ImVec4;

#include <common/maths.hpp>
#include <imgui.h>

bool ImGui_ImplDX11_Init(ID3D11Device* device, ID3D11DeviceContext* device_context);
void ImGui_ImplDX11_Shutdown();

// Use if you want to reset your rendering device without losing ImGui state.
void ImGui_ImplDX11_InvalidateDeviceObjects();
bool ImGui_ImplDX11_CreateDeviceObjects();

void RenderImgui(FeRenderDevice& renderDevice);
void SetImguiDirty(bool bValue);

void ImguiConvert(ImVec4& output, const FeColor& input);


struct PropertyGridScopeWidget
{
	PropertyGridScopeWidget(const char* header);
	~PropertyGridScopeWidget();
};
struct PropertyGridScopeID
{
	PropertyGridScopeID(void* ID);
	~PropertyGridScopeID();
};

template<typename T>
struct ScopedStyleVar
{
	ScopedStyleVar(ImGuiStyleVar_ var, const T& value)
	{
		ImGui::PushStyleVar(var, value);
	}
	~ScopedStyleVar()
	{
		ImGui::PopStyleVar();
	}
};
struct ScopedStyleColor
{
	ScopedStyleColor(ImGuiCol_ color, const ImVec4& value)
	{
		ImGui::PushStyleColor(color, value);
	}
	~ScopedStyleColor()
	{
		ImGui::PopStyleColor();
	}
};

bool DrawPushButton(bool bPressed, bool bEnabled, const char* label);
bool DrawString(FeString* value, bool bMultiline, ImVec2 dimensions, bool bPushId);

#define SCOPE_PROPERTYGRID_ELEMENT(_name_) PropertyGridScopeWidget _widget_##_name_ ( #_name_ )
#define SCOPE_ID(_name_, _id_) PropertyGridScopeID _scopeId_##_name_ ( _id_ )

#define SCOPE_STYLEVAR_VEC2(_var_, _value_) auto var_##_var_ = ScopedStyleVar<ImVec2>(_var_, _value_);
#define SCOPE_STYLEVAR_FLOAT(_var_, _value_) auto var_##_var_ = ScopedStyleVar<float>(_var_, _value_);

#define SCOPE_STYLECOLOR(_var_, _value_) auto var_##_var_ = ScopedStyleColor(_var_, _value_);
