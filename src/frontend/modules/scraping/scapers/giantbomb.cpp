
#include <curl/curl.h>

#include <modulescraping.hpp>

#include <common/serializable.hpp>
#include <common/network.hpp>
#include <common/maths.hpp>
#include <common/filesystem.hpp>
#include <queue>

class FeGBName : public FeSerializable
{
public:
#define props(_d)						\
	_d(FeString,	name   )			\

	FE_DECLARE_CLASS_BODY(props, FeGBName, FeSerializable)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeGBName)

class FeGBGameImage : public FeSerializable
{
public:
#define props(_d)						\
	_d(FeString,	icon_url   )		\
	_d(FeString,	medium_url )		\
	_d(FeString,	screen_url )		\
	_d(FeString,	small_url  )		\
	_d(FeString,	super_url  )		\
	_d(FeString,	thumb_url  )		\
	_d(FeString,	tiny_url   )		\
	_d(FeString,	tags   )			\

	FE_DECLARE_CLASS_BODY(props, FeGBGameImage, FeSerializable)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeGBGameImage)

class FeGBGame : public FeSerializable
{
public:
#define props(_d)										\
	_d(uint32,					id)						\
	_d(FeString,				name)					\
	_d(FeString,				description)			\
	_d(FeString,				original_release_date)	\
	_d(FeString,				deck)					\
	_d(FeTArray<FeGBName>,		genres)					\
	_d(FeTArray<FeGBName>,		publishers)				\
	_d(FeTArray<FeGBName>,		developers)				\
	_d(FeGBGameImage,			image)					\
	_d(FeTArray<FeGBGameImage>,	images)					\
	
	FE_DECLARE_CLASS_BODY(props, FeGBGame, FeSerializable)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeGBGame)

class FeGBGameSearchResult : public FeSerializable
{
public:
#define props(_d)								\
	_d(uint32,			id)						\
	_d(FeString,		name)					\
	_d(FeString,		shortname)				\

	FE_DECLARE_CLASS_BODY(props, FeGBGameSearchResult, FeSerializable)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeGBGameSearchResult)

class FeGBGameSearchResults : public FeSerializable
{
public:
	#define FeGBGameSearchResults_Props(_d) _d(FeTArray<FeGBGameSearchResult>, results)
	FE_DECLARE_CLASS_BODY(FeGBGameSearchResults_Props, FeGBGameSearchResults, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeGBGameSearchResults)

class FeGBGameResults : public FeSerializable
{
public:
	#define FeGBGameResults_Props(_d) _d(FeGBGame, results)
	FE_DECLARE_CLASS_BODY(FeGBGameResults_Props, FeGBGameResults, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeGBGameResults)

class FeGameScrapperGiantBombImpl
{
public:
	struct FeGBImageTagType
	{
		FeEGameImage::Type Type;
		FeString Tag;

		void Set(const char* tag, FeEGameImage::Type type)
		{
			Tag = tag;
			Type = type;
		}
	};
	struct FeGBCache
	{
		FeTArray<FeGBGameSearchResult> GameSearch;
		FeTArray<FeGBGame> Game;
	};
	
	const FeString Host = "http://www.giantbomb.com/";
	const FeString BaseUrl = "http://www.giantbomb.com/api";
	const FeString ApiKey = "d3d7c07ecb24ce3988c3512437a702419eb8d7f2";
	const FeString UserAgent = "liveemu";
	
	static const uint32 REQUEST_BUFFER_SIZE = 1<<10; //1k
	static const uint32 RESPONSE_BUFFER_SIZE = 16 * (1 << 17 /*64K*/);

	FeStringBuffer RequestBuffer;
	FeStringBuffer ResponseBuffer;
	
	FeGBCache Cache;

	FeUrlTransferHandler UrlTranfser;

	FeTArray<FeGBImageTagType> GBImageTagToType;

	uint32 Initialize()
	{
		Cache.GameSearch.Reserve(1000);
		Cache.Game.Reserve(1000);

		RequestBuffer.Allocate(REQUEST_BUFFER_SIZE, FE_HEAPID_DATABASE);
		ResponseBuffer.Allocate(RESPONSE_BUFFER_SIZE, FE_HEAPID_DATABASE);

		GBImageTagToType.Add().Set("Artwork", FeEGameImage::Artwork);
		GBImageTagToType.Add().Set("Fan art", FeEGameImage::FanArt);
		GBImageTagToType.Add().Set("Concept Art", FeEGameImage::ConceptArt);
		GBImageTagToType.Add().Set("Box Art", FeEGameImage::BoxArt_Front);
		GBImageTagToType.Add().Set("Flyers", FeEGameImage::BoxArt_Front);
		GBImageTagToType.Add().Set("Screenshots", FeEGameImage::Screenshot);

		return UrlTranfser.Initialize();
	}
	FeEGameImage::Type GetGameImageTypeFromTags(const FeString& tags)
	{
		for (auto& imgTag : GBImageTagToType)
		{
			if (FeStringTools::IndexOf(tags.Cstr(), imgTag.Tag.Cstr()) != FE_INVALID_ID)
				return imgTag.Type;
		}
		return FeEGameImage::Unknown;
	}
	void CacheResults(const FeGBGameSearchResults& results)
	{
		for (auto& result : results.Getresults())
		{
			uint32 foundIdx = Cache.GameSearch.IndexOf([&](const FeGBGameSearchResult& p) -> bool {return p.Getid() == result.Getid(); });
			if (foundIdx == FE_INVALID_ID)
			{
				Cache.GameSearch.Add(result);
			}
		}
	}
	void CacheResults(const FeGBGameResults& results)
	{
		auto& result = results.Getresults();
		uint32 foundIdx = Cache.Game.IndexOf([&](const FeGBGame& p) -> bool {return p.Getid() == result.Getid(); });
		if (foundIdx == FE_INVALID_ID)
		{
			Cache.Game.Add(result);
		}
	}
	void Shutdown()
	{
	}
	bool IsSpecialChar(char c)
	{
		static char specialChars[] = { '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '!', '�', '#', '$', '%', '&', '�', '(', ')', '*', '+', ',', '�',
			'.', ':', '<', '=', '>', '?', '@', '[', '\\', ']', ']', '_', '`', '{', '|', '}', '~', '�', '�', '-', '\'', '\"' };

		const uint32 specialCharsCount = sizeof(specialChars);

		for (uint32 i = 0; i < specialCharsCount; ++i)
		{
			if (c == specialChars[i])
				return true;
		}
		return false;
	}
	void ComputeSearchName(const FeString& longName, char* shortName, size_t outputSize)
	{
		const char* sLongName = longName.Cstr();

		uint32 iShortameLen = 0;
		for (uint32 iChar = 0; iChar < longName.Length(); ++iChar)
		{
			char c = sLongName[iChar];

			if (!IsSpecialChar(c) && c != ' ')
				shortName[iShortameLen++] = tolower(c);
			else
				shortName[iShortameLen++] = '%';

			if (iShortameLen >= outputSize)
				break;
		}

		shortName[iShortameLen] = '\0';
	}
	void ComputeShortName(const FeString& longName, FeString& shortName)
	{
		const char* sLongName = longName.Cstr();

		const size_t iMaxLen = 256;
		char sShortName[iMaxLen];

		uint32 iShortameLen = 0;
		for (uint32 iChar = 0; iChar < longName.Length(); ++iChar)
		{
			if ((iShortameLen + 1) >= iMaxLen)
				break;

			char c = sLongName[iChar];

			if (!IsSpecialChar(c) && c!=' ')
				sShortName[iShortameLen++] = tolower(c);
		}

		sShortName[iShortameLen] = '\0';
		shortName = sShortName;
	}
	bool MatchGame(const FeString& searchedName, const FeTArray<FeGBGameSearchResult>& list, FeGBGameSearchResult& match)
	{
		for (auto& entry : list)
		{
			if (entry.Getshortname() == searchedName)
			{
				match = entry;
				FE_LOG("[FeGameScrapperGiantBomb] matched game '%s' !", match.Getname().Cstr());
				return true;
			}
		}
		return false;
	}
	uint32 GetGame(FeGBGame& output, const FeGBGameSearchResult& gameResult)
	{
		uint32 foundIdx = Cache.Game.IndexOf([&](const FeGBGame& p) -> bool {return p.Getid() == gameResult.Getid(); });
		if (foundIdx != FE_INVALID_ID)
		{
			output = Cache.Game[foundIdx];
			return FeEReturnCode::Success;
		}

		FeGBGameResults tmpResults;

		RequestBuffer.Clear();
		ResponseBuffer.Clear();

		RequestBuffer.Printf("%s/game/3030-%d/?api_key=%s&format=json", BaseUrl.Cstr(), gameResult.Getid(), ApiKey.Cstr());
		RequestBuffer.Printf("&field_list=id,name,description,deck,original_release_date,image,images,genres,developers,publishers");

		UrlTranfser.Perform(&ResponseBuffer, RequestBuffer.Cstr(), UserAgent.Cstr());

		if (ResponseBuffer.Length > 0)
		{
			FE_FAILEDRETURN(FeJson::Deserialize(tmpResults, ResponseBuffer.Cstr(), FE_HEAPID_DATABASE));

			output = tmpResults.Getresults();
			CacheResults(tmpResults);
			return FeEReturnCode::Success;
		}
		return FeEReturnCode::Failed;
	}
	uint32 SearchGame(const FeString& searchedLongName, FeGBGameSearchResults& results, FeGBGameSearchResult& match, uint32 maxResults = 100)
	{
		FeString shortName;
		char searchName[128];
		
		ComputeShortName(searchedLongName, shortName);
		ComputeSearchName(searchedLongName, searchName, 128);

		// Try matching search with cache
		if (MatchGame(shortName, Cache.GameSearch, match))
			return FeEReturnCode::Success;

		FeGBGameSearchResults tmpResults;
		uint32 offset = 0;
		uint32 pageSize = 10;
		uint32 limit = maxResults;
		
		// clear results containers
		match.Setid(FE_INVALID_ID);
		results.Getresults().Clear();

		do
		{ 
			RequestBuffer.Clear();
			ResponseBuffer.Clear();

			RequestBuffer.Printf("%s/search/?api_key=%s&format=json&query=%s&resources=%s&limit=%d&offset=%d", BaseUrl.Cstr(), ApiKey.Cstr(), searchName, "game", pageSize, offset);
			RequestBuffer.Printf("&field_list=id,name");
			
			UrlTranfser.Perform(&ResponseBuffer, RequestBuffer.Cstr(), UserAgent.Cstr());
			
			if (ResponseBuffer.Length > 0)
			{
				FE_FAILEDRETURN(FeJson::Deserialize(tmpResults, ResponseBuffer.Cstr(), FE_HEAPID_DATABASE));

				offset += pageSize;

				const FeTArray<FeGBGameSearchResult>& resultEntries = tmpResults.Getresults();
				
				// compute short names
				for (auto& resultEntry : resultEntries)
					ComputeShortName(resultEntry.Getname(), resultEntry.Getshortname());

				CacheResults(tmpResults);
				results.Getresults().Add(resultEntries); // save results
				
				if (MatchGame(shortName, resultEntries, match))
					break;

				if (offset >= limit)
					break;
			}
		} while (tmpResults.Getresults().GetSize() == pageSize-1);

		return FeEReturnCode::Success;
	}
};

uint32 FeGameScrapperGiantBomb::Load(FeModuleScraping* module)
{
	Impl = FE_NEW(FeGameScrapperGiantBombImpl, 0);
	return Impl->Initialize();
}
uint32 FeGameScrapperGiantBomb::Unload()
{
	Impl->Shutdown();
	FE_DELETE(FeGameScrapperGiantBombImpl, Impl, 0);
	
	return FeEReturnCode::Success;
}

uint32 FeGameScrapperGiantBomb::Scrap(FeDataGameDump& dump, FeDataPlatform& platform)
{
	FeGBGameSearchResults results;
	FeGBGameSearchResult match;
	FeDataGame game;
	FeString searchName;
	FeDataGameDumpScrap scrap;

	const FeString ScrapperName = "GiantBomb";
	scrap.SetID(dump.GetID() ^ ScrapperName.Id());

	if (FE_SUCCEEDED(FeDatabase::StaticInstance.Deserialize(scrap, FE_HEAPID_DATABASE)))
		return FeEReturnCode::Success;

	scrap.SetScrapper(ScrapperName);
	scrap.GetDump().Assign(&dump);

	// use game reference if it's valid
	if (dump.GetGame().IsValid())
	{
		game.SetID(dump.GetGame()->GetID());
		searchName = dump.GetGame()->GetName();
	}
	// If game is not found in FeDataGame table use dump file name
	else
	{
		FePath path = dump.GetLocalPath().CstrNotNull();
		searchName = path.File;
	}
		
	// Search matching game entry in remote scrapper
	Impl->SearchGame(searchName, results, match);

	// If we found a match save scrapped infos to database
	if (match.Getid() != FE_INVALID_ID)
	{
		FeGBGame gbGame;

		if (FE_SUCCEEDED(Impl->GetGame(gbGame, match)))
		{
			game.GetPlatform().Assign(&platform);
			game.SetName(gbGame.Getname());
			game.ComputeIDFromName();
			game.SetOverview(gbGame.Getdeck());
			
			auto addImg = [&](const FeGBGameImage& gbImage, FeEGameImage::Type forcedType) -> void
			{
				FeDataGameImage image;
				image.SetRemotePath(gbImage.Getmedium_url());
				image.SetID(FeStringTools::GenerateUIntIdFromString(image.GetRemotePath().Cstr()));

				if (forcedType != FeEGameImage::Unknown)
					image.SetType(forcedType);
				else
					image.SetType(Impl->GetGameImageTypeFromTags(gbImage.Gettags()));
				image.GetGame().Assign(&game);

				FeDatabase::StaticInstance.Serialize(&image);
			};
			addImg(gbGame.Getimage(), FeEGameImage::BoxArt_Front);

			for (auto& gbImage : gbGame.Getimages())
			{
				addImg(gbImage, FeEGameImage::Unknown);
			}
		}
	}
	
	if (game.GetID() != FE_INVALID_ID)
	{
		FeDatabase::StaticInstance.Serialize(&game);

		// save dump to DB with link to associated game
		dump.GetGame().Assign(&game);
		FeDatabase::StaticInstance.Serialize(&dump);
	}

	FeDatabase::StaticInstance.Serialize(&scrap);

	return FeEReturnCode::Success;
}
uint32 FeGameScrapperGiantBomb::FeedDatabase(FeModuleScraping* module)
{
	return FeEReturnCode::Success;
}
