#include <modulescraping.hpp>

#include <common/serializable.hpp>
#include <common/maths.hpp>
#include <common/filesystem.hpp>
#include <queue>

class FeGameScrapperArcadeHistoryImpl
{
public:
	void LoadDatFileInfos(FeModuleScraping* module)
	{
		char* szContent;
		size_t iFileSize;

		auto iResult = FeFileTools::ReadTextFile("scrappers/arcadehistory/history.dat", &szContent, &iFileSize);
		
		uint32 platformIdx = module->GetPlatforms().IndexOf([&](const FeDataPlatform& p) -> bool { return p.GetName() == "Arcade"; });
		FeDataPlatform& platformArcade = module->GetPlatforms()[platformIdx];

		uint32 iDebugGamesCountLimit = 50;
		const uint32 STRING_SEARCH_LIMIT = 2048 * 4;

		if (iResult == FeEReturnCode::Success)
		{
			uint32 iReadLines = 0;
			char* line = szContent;
			uint32 iLineLen = FeStringTools::IndexOf(line, '\r', 0, STRING_SEARCH_LIMIT);
			
			uint32 iReadChars = 0;

			auto readChar = [&]() 
			{
				line++;
				iReadChars++;
			};
			auto readLine = [&]() -> bool
			{
				iLineLen = FeStringTools::IndexOf(line, '\r', 0, STRING_SEARCH_LIMIT);

				if (iLineLen > iFileSize - iReadChars)
				{
					FE_ASSERT((iFileSize - iReadChars)<STRING_SEARCH_LIMIT, "bug??!");
					iLineLen = iFileSize - iReadChars;
				}

				line += iLineLen + 1;
				iReadChars += iLineLen + 1;
				iReadLines++;

				if (iReadChars >= iFileSize)
					return false;

				if (line[0] == '\n')
				{
					readChar(); // read '\n
				}

				if (iReadChars < iFileSize)
					iLineLen = FeStringTools::IndexOf(line, '\r', 0, STRING_SEARCH_LIMIT);

				return true;
			};
			auto readLines = [&](uint32 linesCount)
			{
				for (uint32 i = 0; i < linesCount; ++i)
				{
					bool bRes = readLine();
					if (!bRes)
						return false;
				}
				return true;
			};

			auto getLineLen = [&](const char* str) -> uint32
			{
				return FeStringTools::IndexOf(str, '\r', 0, STRING_SEARCH_LIMIT);
			};

			
			
			const uint32 MAX_NAME_SIZE = 256;
			const uint32 MAX_OVERVIEW_SIZE = 2048;

			char szParsedRomNames[MAX_NAME_SIZE];
			char szParsedName[MAX_NAME_SIZE];
			char szParsedRomName[MAX_NAME_SIZE];
			char szParsedOverview[MAX_OVERVIEW_SIZE];
			
			uint32 ProcessedEntries = 0;

			while (iReadChars < iFileSize)
			{
				uint32 iCharsLeft = iFileSize - iReadChars;

				char* parsedLine = line;

				if (FeStringTools::TrimLeft(&parsedLine, "$info=", STRING_SEARCH_LIMIT))
				{
					memset(szParsedRomNames, 0, MAX_NAME_SIZE);
					memset(szParsedName, 0, MAX_NAME_SIZE);
					memset(szParsedOverview, 0, MAX_OVERVIEW_SIZE);
					
					memcpy_s(szParsedRomNames, MAX_NAME_SIZE, parsedLine, getLineLen(parsedLine) - 1); // line minus 1 char ','

					if (!readLines(3))// jump 3 lines
						break;

					uint32 iNameLen = FeStringTools::IndexOf(line, " (c)", 0, iLineLen);
					
					if (iNameLen == FE_INVALID_ID)
						iNameLen = FeStringTools::IndexOf(line, "(c)", 0, iLineLen);
					
					if (iNameLen == FE_INVALID_ID)
						iNameLen = FeMath::Min(MAX_NAME_SIZE, iLineLen);

					memcpy_s(szParsedName, MAX_NAME_SIZE, line, iNameLen); // line minus 1 char '.'
					
					if (strlen(szParsedName) && strlen(szParsedRomNames))
					{
						FeDataGame game;
						
						FeStringTools::Replace(szParsedName, '\'', ' ');

						game.GetPlatform().Assign(&platformArcade);
						game.SetName(szParsedName);
						game.ComputeIDFromName();

						uint32 iRomNamesOffset = 0;
						uint32 iRomNamesLen = strlen(szParsedRomNames);
						
						while (iRomNamesOffset < iRomNamesLen && iRomNamesOffset<MAX_NAME_SIZE)
						{
							memset(szParsedRomName, 0, MAX_NAME_SIZE);
							uint32 nameLen = FeStringTools::IndexOf(szParsedRomNames + iRomNamesOffset, ',');

							if (nameLen == FE_INVALID_ID)
								nameLen = MAX_NAME_SIZE;

							memcpy_s(szParsedRomName, MAX_NAME_SIZE, szParsedRomNames+iRomNamesOffset, nameLen);
							iRomNamesOffset += nameLen+1;

							FeDataGameDump dump;
							dump.SetName(szParsedRomName);
							dump.GetGame().Assign(&game);
							dump.ComputeIDFromName();
							dump.SetChecksum(0);
							dump.SetCountryCode(1);
							dump.SetFlags(0);

							FeDatabase::StaticInstance.Serialize(&dump);
						}

						// read bio

						if (!readLines(2))// jump 2 lines
							break;


						uint32 iOverviewLen = 0;
						while (true)
						{
							uint32 iLineLen = getLineLen(line);

							if (iLineLen > 0)
							{
								if (FeStringTools::TrimLeft(&line, "$end", STRING_SEARCH_LIMIT))
									break;

								if (line[0] == '-')
									break;
								
								if (MAX_OVERVIEW_SIZE > (iOverviewLen + iLineLen))
								{
									memcpy_s(szParsedOverview + iOverviewLen, MAX_OVERVIEW_SIZE - iOverviewLen, line, iLineLen);
									iOverviewLen += iLineLen;
								}
							}

							if (!readLines(1))
								break;
						}

						game.SetOverview(szParsedOverview);

						FeDatabase::StaticInstance.Serialize(&game);

						FE_LOG("[Scraping][ArcadeHistory] Processed game %d %s", ProcessedEntries, szParsedName);
						ProcessedEntries++;

						//if (ProcessedEntries == SCRAPPER_DEBUG_LIMIT)
						//	break;
					}
				}
				else
				{
					if (!readLines(1))
						break;
				}
			}

			FE_FREE(szContent, FE_HEAPID_FILESYSTEM);
		}
	}
};
uint32 FeGameScrapperArcadeHistory::Load(FeModuleScraping* module)
{
	Impl = FE_NEW(FeGameScrapperArcadeHistoryImpl, 0);
	
	return FeEReturnCode::Success;
}
uint32 FeGameScrapperArcadeHistory::Unload()
{
	FE_DELETE(FeGameScrapperArcadeHistoryImpl, Impl, 0);
	return FeEReturnCode::Success;
}
uint32 FeGameScrapperArcadeHistory::Scrap(FeDataGameDump& dump, FeDataPlatform& platform)
{
	return FeEReturnCode::Success;
}
uint32 FeGameScrapperArcadeHistory::FeedDatabase(FeModuleScraping* module)
{
	Impl->LoadDatFileInfos(module);
	return FeEReturnCode::Success;
}
