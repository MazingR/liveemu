#pragma once

#include <common/common.hpp>
#include <common/database.hpp>
#include <common/serializable.hpp>


#ifdef DEBUG
	#define SCRAPPER_DEBUG_LIMIT 10
#else
	#define SCRAPPER_DEBUG_LIMIT 100
#endif

#define DECLARE_NAMED_DATA_TYPE(className)											\
class className : public FeDbSerializableNamed										\
{																					\
public:																				\
	className() : FeDbSerializableNamed() {}										\
	FE_DECLARE_CLASS_BODY(EMPTY_PROPERTIES_LIST, className, FeDbSerializableNamed)	\
};																					\
FE_DECLARE_CLASS_BOTTOM(className)

// /!\ do not change the order of this enum
#define FeEGameImage_Values(_d)			\
		_d(FeEGameImage, Unknown)		\
		_d(FeEGameImage, BoxArt_Front	)\
		_d(FeEGameImage, BoxArt_Back	)\
		_d(FeEGameImage, Screenshot		)\
		_d(FeEGameImage, Artwork		)\
		_d(FeEGameImage, ConceptArt		)\
		_d(FeEGameImage, FanArt			)\

FE_DECLARE_ENUM(FeEGameImage, FeEGameImage_Values)

// ------------------------------------------------------------------------------------------
class FeDbSerializableNamed : public FeDbSerializable
{
public:
	#define props(_d)							\
	_d(FeString,	Name)						\

	FeDbSerializableNamed() : FeDbSerializable()
	{
	}
	void ComputeIDFromName()
	{
		char szSecondaryKeyValue[128];
		if (GetSecondaryKeyValue(szSecondaryKeyValue, 128))
			ID = FeStringTools::GenerateUIntIdFromString(szSecondaryKeyValue);
	}
	virtual const char* GetSecondaryKey() override { return "Name"; }

	FE_DECLARE_CLASS_BODY(props, FeDbSerializableNamed, FeDbSerializable)
	#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeDbSerializableNamed)
// ------------------------------------------------------------------------------------------
DECLARE_NAMED_DATA_TYPE(FeDataGameGenre);
// ------------------------------------------------------------------------------------------
DECLARE_NAMED_DATA_TYPE(FeDataGamePublisher);
// ------------------------------------------------------------------------------------------
DECLARE_NAMED_DATA_TYPE(FeDataGameDeveloper);
// ------------------------------------------------------------------------------------------
class FeDataPlatform : public FeDbSerializableNamed
{
public:
	#define props(_d)							\
	_d(FeString,				Overview)		\

	FeDataPlatform() : FeDbSerializableNamed()
	{}
	FE_DECLARE_CLASS_BODY(props, FeDataPlatform, FeDbSerializableNamed)
	#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeDataPlatform)
// ------------------------------------------------------------------------------------------
class FeDataGame : public FeDbSerializableNamed
{
public:
	#define props(_d)											\
	_d(FeTPtr<FeDataPlatform>,		Platform)					\
	_d(FeString,					ReleaseDate)				\
	_d(FeString,					Overview)					\
	_d(bool,						Adult)						\
	_d(uint32,						PlayersCount)				\
	_d(uint32,						PlayersCountSimultaneous)	\

	FeDataGame() : FeDbSerializableNamed()
	{
		Adult = false;
		PlayersCount = 1;
		PlayersCountSimultaneous = 1;
	}

	FE_DECLARE_CLASS_BODY(props, FeDataGame, FeDbSerializableNamed)
	#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeDataGame)
// ------------------------------------------------------------------------------------------
class FeDataGameDump : public FeDbSerializableNamed
{
public:
#define props(_d)						\
	_d(uint32,				Checksum)		\
	_d(FeTPtr<FeDataGame>,	Game)			\
	_d(uint32,				CountryCode)	\
	_d(uint32,				Flags)			\
	_d(FeString,			LocalPath)		\
	_d(bool,				IsOnDisk)		\

	FeDataGameDump() : FeDbSerializableNamed(), IsOnDisk(false)
	{}

	FE_DECLARE_CLASS_BODY(props, FeDataGameDump, FeDbSerializableNamed)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeDataGameDump)
// ------------------------------------------------------------------------------------------
class FeDataGameDumpScrap : public FeDbSerializable
{
public:
#define props(_d)							\
	_d(FeTPtr<FeDataGameDump>,	Dump)		\
	_d(FeString,				Scrapper)	\

	FeDataGameDumpScrap() : FeDbSerializable()
	{}

	FE_DECLARE_CLASS_BODY(props, FeDataGameDumpScrap, FeDbSerializable)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeDataGameDumpScrap)
// ------------------------------------------------------------------------------------------
class FeDataGameImage : public FeDbSerializable
{
public:
#define props(_d)						\
	_d(FeTPtr<FeDataGame>,	Game)		\
	_d(FeString,			RemotePath)	\
	_d(uint32,				Type)		\

	FeDataGameImage() : FeDbSerializable(), Type(0)
	{}

	FE_DECLARE_CLASS_BODY(props, FeDataGameImage, FeDbSerializable)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeDataGameImage)
// ------------------------------------------------------------------------------------------
class FeDumpsLocation : public FeSerializable
{
public:
#define FeDumpsLocation_Properties(_d)		\
		_d(FeString,			Platform)	\
		_d(FeString,			Path)		\
		_d(FeTArray<FeString>,	Filters)	\

	FE_DECLARE_CLASS_BODY(FeDumpsLocation_Properties, FeDumpsLocation, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeDumpsLocation)

class FeScrapingConfig : public FeSerializable
{
public:
#define FeScrapingConfig_Properties(_d)					\
		_d(FeTArray<FeString>,			StartupTasks)	\
		_d(FeTArray<FeString>,			Platforms)		\
		_d(FeString,					Images)			\
		_d(FeTArray<FeDumpsLocation>,	DumpsLocations)	\

	FE_DECLARE_CLASS_BODY(FeScrapingConfig_Properties, FeScrapingConfig, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeScrapingConfig)
