#include <modulescraping.hpp>

#include <common/serializable.hpp>
#include <common/maths.hpp>
#include <common/filesystem.hpp>
#include <queue>
#include <common/taskscheduler.hpp>

template<typename TableType, typename PropertyType>
void FetchListEntriesPropertyFromDb(const char* szPropertyName, FeTArray<PropertyType>& Result)
{
	char szSql[512];
	memset(szSql, 0, 512);
	sprintf_s(szSql, "SELECT %s FROM %s", szPropertyName, TableType::ClassName());

	int(*callback)(void*, int, char**, char**) = [](void *userData, int argc, char **argv, char **azColName) -> int
	{
		auto results = (FeTArray<PropertyType>*)userData;
		if (argc == 1)
		{
			PropertyType& result = results->Add();
			result = argv[0];
		}
		return 0;
	};
	FeDatabase::StaticInstance.Execute(szSql, callback, &Result);
}
uint32 FeModuleScraping::FeedDatabase()
{
	// Feed Database
	{
		// Feed platforms table
		{
			Platforms.Reserve(16);

			for (auto& platformName : InitDesc.Config.GetPlatforms())
			{
				FeDataPlatform& platform = Platforms.Add();

				platform.SetName(platformName);
				platform.SetID(Platforms.GetSize());

				FeDatabase::StaticInstance.Serialize(&platform);
			}
			FE_ASSERT(Platforms.GetSize() < 512, "Too much platforms!");
		}
	}

	return FeEReturnCode::Success;
}

uint32 FeModuleScraping::ParseDumpsDirectories()
{
	char szTmp[512];
	
	FeTArray<uint32> DumpsAlreadyScrapped;
	FeDbResultCallback callback = [&](int argc, char **argv, char **azColName) -> int
	{
		if (argc >= 1)
		{
			uint32 id;
			FeConvert(id, argv[0]);
			DumpsAlreadyScrapped.Add(id);
		}

		return 0;
	};

	FeDatabase::StaticInstance.Execute("SELECT Dump FROM FeDataGameDumpScrap", &callback);

	static FeString ScrappAllDumpsTaskName = "ScrapAllDumps";
	bool bSchedulScrapTask = (InitDesc.Config.GetStartupTasks().IndexOf(ScrappAllDumpsTaskName) != FE_INVALID_ID);
	uint32 iParseIndex = 0;

	for (auto& dumpsLocation : InitDesc.Config.GetDumpsLocations())
	{
		FeString strPlatform = dumpsLocation.GetPlatform();

		if (strPlatform == "Arcade")
		{
			uint32 platformIdx = GetPlatforms().IndexOf([&](const FeDataPlatform& p) -> bool { return p.GetName() == strPlatform; });
			FE_ASSERT(platformIdx != FE_INVALID_ID, "platform '%s'not found !", strPlatform.Cstr())
			FeDataPlatform& platform = GetPlatforms()[platformIdx];

			FeTArray<FePath> files;

			files.SetHeapId(FE_HEAPID_DATABASE);

			auto& dirPath = dumpsLocation.GetPath();

			for (auto& filter : dumpsLocation.GetFilters())
				FeFileTools::List(files, dirPath.Cstr(), filter.Cstr());

			for (auto& filePath : files)
			{
				if (FeApplication::GetTasksScheduler()->IsShuttingDown())
					return FeEReturnCode::Success;

				const FeString& dumpName = filePath.File;

				if (DumpsAlreadyScrapped.IndexOf(dumpName.Id()) == FE_INVALID_ID) // avoid scrapping twice
				{
					FeDataGameDump dbDump;
					dbDump.SetName(dumpName);
					dbDump.SetID(dumpName.Id());

					uint32 iRes = FeDatabase::StaticInstance.Deserialize(dbDump, FE_HEAPID_DATABASE);

					dbDump.SetLocalPath(szTmp);
					dbDump.SetIsOnDisk(true);

					FE_LOG("[Scrapper] Parsed file %d %s", ++iParseIndex, dbDump.GetName().CstrNotNull());

					FeDatabase::StaticInstance.Serialize(&dbDump);

					if (bSchedulScrapTask)
					{
						TasksQueue->CreateTask(dbDump.GetName(), [](FeModuleScraping* pThis, uint32 _dumpID, uint32 iPlatformIdx) -> uint32
						{
							FeDataGameDump _dump;
							_dump.SetID(_dumpID);

							uint32 iRes = FeDatabase::StaticInstance.Deserialize(_dump, FE_HEAPID_DATABASE);
							FeDataPlatform&  _platform = pThis->GetPlatforms()[iPlatformIdx];
							return pThis->GetScrapper<FeGameScrapperGiantBomb>()->Scrap(_dump, _platform);

						}, this, dumpName.Id(), platformIdx);
					}

					if (iParseIndex > 256)
						break;
				}
			}
			break;
		}
	}

	return FeEReturnCode::Success;
}

uint32 FeModuleScraping::Load(const FeModuleInit* initBase)
{
	InitDesc = *(FeModuleScrapingInit*)initBase;

	// Load database
	FePath dbPath = "db/main.db";
	FeDatabase::StaticInstance.Load(dbPath);

	CreateScrapper<FeGameScrapperArcadeHistory>();
	CreateScrapper<FeGameScrapperGiantBomb>();

	GetScrapper<FeGameScrapperArcadeHistory>()->Load(this);
	GetScrapper<FeGameScrapperGiantBomb>()->Load(this);

	TasksQueue = FeApplication::GetTasksScheduler()->CreateQueue("Scrapper");

	auto checkStartupTask = [&](const FeString& name) -> bool { return InitDesc.Config.GetStartupTasks().IndexOf(name) != FE_INVALID_ID; };

	FeString taskName = "FeedDatabase";
	TasksQueue->CreateTask(taskName, [](FeModuleScraping* pThis) -> uint32 { return pThis->FeedDatabase(); }, this);

	if (checkStartupTask("FeedDatabaseFromScrappers"))
	{
		TasksQueue->CreateTask("Scrap:ArcadeHistory", [](FeModuleScraping* pThis) -> uint32 { return pThis->GetScrapper<FeGameScrapperArcadeHistory>()->FeedDatabase(pThis); }, this);
		TasksQueue->CreateTask("Scrap:GiantBomb", [](FeModuleScraping* pThis) -> uint32 { return pThis->GetScrapper<FeGameScrapperGiantBomb>()->FeedDatabase(pThis); }, this);
	}

	taskName = "ParseDumpsDirectories";
	if (checkStartupTask(taskName))
		TasksQueue->CreateTask(taskName, [](FeModuleScraping* pThis) -> uint32 { return pThis->ParseDumpsDirectories(); }, this);

	//FeedDatabase();

	TasksQueue->Start();

	return FeEReturnCode::Success;
}
uint32 FeModuleScraping::Unload()
{
	return FeEReturnCode::Success;
}
uint32 FeModuleScraping::Update(const FeDt& fDt)
{
	return FeEReturnCode::Success;
}