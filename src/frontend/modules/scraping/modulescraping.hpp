#pragma once

#include <commonscraping.hpp>
#include <common/application.hpp>

class FeModuleScraping;

	// -----------------------------------------------------------------------------
struct FeModuleScrapingInit : public FeModuleInit
{
public:
	FeScrapingConfig Config;
};

class FeIGameScrapper
{
public:
	virtual uint32 Load(FeModuleScraping* module) = 0;
	virtual uint32 Unload() = 0;
	virtual uint32 Scrap(FeDataGameDump& dump, FeDataPlatform& platform) = 0;
	virtual uint32 FeedDatabase(FeModuleScraping* module) = 0;
};
// -----------------------------------------------------------------------------
class FeGameScrapperArcadeHistoryImpl;
class FeGameScrapperArcadeHistory : public FeIGameScrapper
{
public:
	uint32 Load(FeModuleScraping* module);
	uint32 Unload();
	uint32 Scrap(FeDataGameDump& dump, FeDataPlatform& platform);
	uint32 FeedDatabase(FeModuleScraping* module);
private:
	FeGameScrapperArcadeHistoryImpl* Impl;
};
// -----------------------------------------------------------------------------
class FeGameScrapperGiantBombImpl;
class FeGameScrapperGiantBomb : public FeIGameScrapper
{
public:
	uint32 Load(FeModuleScraping* module);
	uint32 Unload();
	uint32 Scrap(FeDataGameDump& dump, FeDataPlatform& platform);
	uint32 FeedDatabase(FeModuleScraping* module);
private:
	FeGameScrapperGiantBombImpl* Impl;
};
// -----------------------------------------------------------------------------

class FeModuleScraping : public FeModule
{
	typedef std::map<size_t, FeIGameScrapper*> ScrappersMap;
	typedef ScrappersMap::iterator ScrappersMapIt;

public:
	virtual uint32 Load(const FeModuleInit*) override;
	virtual uint32 Unload() override;
	virtual uint32 Update(const FeDt& fDt) override;
	virtual char* ComputeDebugOutput(const FeDt& fDt) { return ""; }
	virtual const char* GetName()
	{
		static const char* sName = "Scraping";
		return sName;
	}

	template<class T>
	T* CreateScrapper()
	{
		T* pScrapper = FE_NEW(T, 0);
		Scrappers[GetScrapperID<T>()] = pScrapper;
		return pScrapper;
	}

	template<class T>
	size_t GetScrapperID()
	{
		static uint32 iTypeHash = typeid(T).hash_code();
		return iTypeHash;
	}
	template<class T>
	T* GetScrapper()
	{
		return (T*)Scrappers[GetScrapperID<T>()];
	}

	uint32 FeedDatabase();
	uint32 ParseDumpsDirectories();

	FeTArray<FeDataPlatform>& GetPlatforms() { return Platforms; }

private:
	FeTaskQeue* TasksQueue;
	FeModuleScrapingInit InitDesc;
	ScrappersMap Scrappers;
	FeTArray<FeDataPlatform> Platforms;
};
