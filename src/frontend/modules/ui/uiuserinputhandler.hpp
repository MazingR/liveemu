#pragma once

#include <common/common.hpp>
#include <common/tarray.hpp>
#include <rendering/commonrenderer.hpp>

#include <commonui.hpp>

class FeUiUserInputHandler : public FeISelfDeletable
{
public:
	virtual void Update() = 0;
	virtual bool Moved() = 0;
	virtual bool ButtonStateChanged(uint32 button) = 0;
	virtual bool IsButtonDown(uint32 button) = 0;
	virtual const FeVector2& GetPosition() = 0;
	virtual const FeVector2& GetPositionDelta() = 0;
};


class FeUiUserMouseInputHandler : public FeUiUserInputHandler
{
public:
	FeUiUserMouseInputHandler() : bMoved(false), bMovedOnce(false) 
	{
		memset(CurrentButtonsState, 0, MouseButtonsSize);
		memset(LastButtonsState, 0, MouseButtonsSize);
	}

	virtual void Update();
	virtual bool Moved();
	virtual bool ButtonStateChanged(uint32 button);
	virtual bool IsButtonDown(uint32 button);
	virtual const FeVector2& GetPosition();
	virtual const FeVector2& GetPositionDelta();

	virtual void SelfDelete(uint32 iHeapId) 
	{
		FE_DELETE(FeUiUserMouseInputHandler, this, FE_HEAPID_UI);
	}
private:
	static const int32 MouseButtonsCount = 5;
	static const size_t MouseButtonsSize = MouseButtonsCount*sizeof(bool);

	FeVector2 LastPosition;
	FeVector2 CurrentPosition;
	FeVector2 PositionDelta;

	bool LastButtonsState[MouseButtonsCount];
	bool CurrentButtonsState[MouseButtonsCount];

	bool bMoved;
	bool bMovedOnce;
};
