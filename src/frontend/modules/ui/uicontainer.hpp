#pragma once

#include <widget.hpp>

class FeUiContainer : public FeWidget
{
public:
	#define FeUiContainer_Props(_d)							\
	_d(FeAssetRef,					WidgetsTemplate)		\
	_d(FeTArray<FeUiDataBinding>,	WidgetsBindings)		\
	_d(FeTransform,					WidgetsTransform)		\

	#define FeUiContainer_PropsNotSerialized(_d)					\
	_d(FeTArray<FeTPtr<FeWidget>>,		ContainerChildren)		\
	_d(FeWidget*,						WidgetsTemplateRef)	\
	
	FeUiContainer();
	~FeUiContainer();
	virtual void PostInitialize() override;
	virtual void OnParentChanged() override;
	virtual bool Traverse(std::function<void(FeTraversalState& traversal)> callback, int32 maxDepth = FE_MAX_INT32) override;
	
	void OnScaleChanged();
	virtual uint32 OnDataChanged() override;
	virtual void OnTemplateChanged(bool bKeepTransform = true, bool bKeepName = true) override;
	virtual void OnPropertyChanged(void* propertyPtr) override;
	void OnWidgetsTransformChanged(bool bClearChildren);

	FE_DECLARE_CLASS_BODY_EX(FeUiContainer_Props, FeUiContainer_PropsNotSerialized, FeUiContainer, FeWidget)
};
FE_DECLARE_CLASS_BOTTOM(FeUiContainer)

class FeUiContainerList : public FeUiContainer
{
public:
	#define FeUiContainerList_Props(_d)				\
	_d(FeEUiOrientation::Type,		Orientation)	\

	FE_DECLARE_CLASS_BODY(FeUiContainerList_Props, FeUiContainerList, FeUiContainer)
};
FE_DECLARE_CLASS_BOTTOM(FeUiContainerList)
