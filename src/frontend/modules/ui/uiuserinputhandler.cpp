#include <uiuserinputhandler.hpp>
#include <rendering/modulerenderer.hpp>

#include <SDL.h>

void FeUiUserMouseInputHandler::Update()
{
	auto pRenderer = FeModuleRendering::Get();
	auto& resolution = pRenderer->GetScreenDimensions();
	FeVector2 vRes(1.0f / (float)resolution.right, 1.0f / (float)resolution.bottom);

	int32 x, y;
	uint32 iMouseState = SDL_GetMouseState(&x, &y);
	
	// save up current state
	LastPosition = CurrentPosition;
	memcpy_s(LastButtonsState, MouseButtonsSize, CurrentButtonsState, MouseButtonsSize);

	// update pos
	CurrentPosition[0] = x*vRes[0];
	CurrentPosition[1] = -y*vRes[1];
	
	// update buttons
	CurrentButtonsState[0] = (SDL_BUTTON(1)&iMouseState)	!=0;
	CurrentButtonsState[1] = (SDL_BUTTON(3)&iMouseState)	!=0;
	CurrentButtonsState[2] = (SDL_BUTTON(2)&iMouseState)	!=0;
	CurrentButtonsState[3] = (SDL_BUTTON(4)&iMouseState)	!=0;
	CurrentButtonsState[4] = (SDL_BUTTON(5)&iMouseState)	!=0;

	if (!bMovedOnce)
	{
		LastPosition = CurrentPosition;
		bMovedOnce = true;
	}

	PositionDelta = CurrentPosition - LastPosition;

	bMoved = LastPosition != CurrentPosition;
}
bool FeUiUserMouseInputHandler::Moved()
{
	return bMoved;
}
bool FeUiUserMouseInputHandler::IsButtonDown(uint32 button)
{
	return CurrentButtonsState[button];
}
bool FeUiUserMouseInputHandler::ButtonStateChanged(uint32 button)
{
	if (MouseButtonsCount <= button)
		return false;

	return CurrentButtonsState[button] != LastButtonsState[button];
}
const FeVector2& FeUiUserMouseInputHandler::GetPosition()
{
	return CurrentPosition;
}
const FeVector2& FeUiUserMouseInputHandler::GetPositionDelta()
{
	return PositionDelta;
}
