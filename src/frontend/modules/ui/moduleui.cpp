#include <moduleui.hpp>

#include <rendering/modulerenderer.hpp>
#include <rendering/renderresourceshandler.hpp>

#include <common/database.hpp>
#include <common/serializable.hpp>
#include <common/maths.hpp>
#include <queue>

FeUiRootWidget::FeUiRootWidget()
{
	RenderBatch.GeometryInstances.SetHeapId(FE_HEAPID_UI);
}
void OnScriptFileChanged(FeEFileChangeType::Type eChangeType, const char* szPath, void* pUserData)
{
	FeSetLastError("");
	((FeModuleUi*)pUserData)->Reload();
}

uint32 FeModuleUi::SaveScripts()
{
	for (auto& assetFile : AssetsRegistry.Files)
	{
		if (FE_FAILED(FeJson::Serialize(assetFile.Get(), assetFile->Path)))
			return FeEReturnCode::Failed;
	}
	if (FE_FAILED(FeJson::Serialize(&Theme, Theme.Path)))
		return FeEReturnCode::Failed;
	
	return FeEReturnCode::Success;
}
uint32 FeModuleUi::LoadScripts()
{
	auto pRenderingModule = FeModuleRendering::Get();

	NavigationHandler.OnReloadUi();
	Sequencer.OnReloadUi();

	// Load scripts from files
	{
		FeTArray<FePath> themeFiles;
		themeFiles.SetHeapId(FE_HEAPID_UI);
		FeFileTools::List(themeFiles, "themes", ".*\\.theme");

		for (auto& filePath : themeFiles)
		{
			FePath dirPath = filePath;
			dirPath.GoToParent();

			uint32 iRes = FeEReturnCode::Success;

			// Deserialize theme file
			iRes = FeJson::Deserialize(Theme, filePath, FE_HEAPID_UI);
			Theme.Directory = dirPath;
			Theme.Path = filePath;

			if (ModuleInit.ThemeToLoad == Theme.ID.Name)
			{
				if (FE_SUCCEEDED(iRes))
					AssetsRegistry.LoadFiles(dirPath, ".*\\.fes"); // load all script files assets

				break;
			}
		}
		// process loaded asset files
		for (auto& assetFile : AssetsRegistry.Files)
		{
			if (assetFile->IsOfType<FeRenderEffect>())
			{
				pRenderingModule->LoadEffect(assetFile.Get());
			}
			else if (assetFile->IsOfType<FeWidget>())
			{
				auto widget = assetFile->GetAsset<FeWidget>();

				FeUiRootWidget& rootWidget = RootWidgets.Add();

				rootWidget.RenderBatch.GeometryInstances.Reserve(1024);
				rootWidget.RenderBatch.Viewport.UseBackBuffer = true;
				rootWidget.RenderBatch.Viewport.Rect = GlobalRect;
				rootWidget.AssetFile = assetFile.Get();
				rootWidget.IsVisible = false;

				rootWidget.Root = widget;
			}
		}
	}
	RefreshWidgets();
	LoadResources(); // load all needed resources

	for (auto& widget : RootWidgets)
		widget.IsVisible = widget.AssetFile->AssetRef == Theme.RootWidgets.GetStart();
	
	return FeEReturnCode::Success;
}
void FeModuleUi::RefreshWidgets(bool bOnlyVisible /*= false*/)
{
	Sequencer.OnReloadUi();
	NavigationHandler.OnReloadUi();

	// Create templated widgets
	for (auto& widget : RootWidgets)
	{
		if (!bOnlyVisible || widget.IsVisible)
			widget.Root->Traverse([&, this](FeTraversalState& traversal) -> void { traversal.Current->OnTemplateChanged(); });
	}

	// Update Parent property
	for (auto& widget : RootWidgets)
	{
		if (!bOnlyVisible || widget.IsVisible)
			widget.Root->OnParentChanged();
	}

	// Compute geometry and other stuff
	for (auto& widget : RootWidgets)
	{
		if (!bOnlyVisible || widget.IsVisible)
			widget.Root->Traverse([&, this](FeTraversalState& traversal) -> void 
		{ 
			traversal.Current->PostInitialize(); 
			traversal.Current->GetGeometryInstance().Viewport = &widget.RenderBatch.Viewport;
		});
	}

	// Refresh bounded data (constant, variables, database)
	for (auto& widget : RootWidgets)
	{
		if (!bOnlyVisible || widget.IsVisible)
			widget.Root->Traverse([&, this](FeTraversalState& traversal) -> void { traversal.Current->OnDataChanged(); });
	}

	OnViewportChanged();

}
uint32 FeModuleUi::LoadResources()
{
	auto pResourcesHandler = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();

	// Load font resouces
	for (auto& assetFile : AssetsRegistry.Files)
	{
		if (assetFile->IsOfType<FeFont>())
		{
			auto uiFont = assetFile->GetAsset<FeFont>();
			
			FeRenderLoadingResource resource;
			resource.ComputePath(uiFont->TrueTypeFile);
			resource.Type = FeEResourceType::Font;
			resource.Id = assetFile->AssetRef.Path.GetId();
			resource.AllocateResource();

			auto* pFontData = (FeRenderFont*)resource.Resource;

			pFontData->Size = uiFont->GetSize();
			pFontData->Interval = uiFont->GetInterval();
			pFontData->Space = uiFont->GetSpace();
			pFontData->LineSpace = uiFont->GetLineSpace();
			pFontData->TrueTypeFile = uiFont->GetTrueTypeFile();

			pResourcesHandler->UnloadResource(resource.Id);
			pResourcesHandler->LoadResource(resource);
		}
	}

	return FeEReturnCode::Success;
}
uint32 FeModuleUi::FetchBindingSourceData(const FeUiBinding& binding, FeBindingDataResults& results)
{
	uint32 iResultsCount = 0;
	
	switch (binding.GetType())
	{
		case FeEUiBindingType::Text:
		{
			auto& entry = results.Entries.Add();
			
			auto& field = entry.Fields.Add();
			field.Value = binding.GetValue();

			iResultsCount++;

		} break;
		case FeEUiBindingType::Database:
		{
			static char szSql[512]; // todo : not thread safe
			memset(szSql, 0, 512);
			memcpy_s(szSql, 512, binding.GetValue().Cstr(), binding.GetValue().Length());

			FeDbResultCallback callback = [&](int argc, char **argv, char **azColName) -> int
			{
				if (argc >= 1)
				{
					auto& entry = results.Entries.Add();
					for (int32 i = 0; i < argc; ++i)
					{
						auto& field = entry.Fields.Add();
						field.Value = argv[i];
					}
				}

				return 0;
			};

			FeDatabase::StaticInstance.Execute(szSql, &callback);
			iResultsCount = results.Entries.GetSize();

		} break;
		case FeEUiBindingType::Asset_Image:
		{
			auto assetImg = AssetsRegistry.GetAsset<FeImage>(binding.Asset);

			if (assetImg)
			{
				auto& entry = results.Entries.Add();

				auto& field = entry.Fields.Add();
				field.Pointer = &assetImg->File;
				field.PointerType = FeEUiBindingType::Asset_Image;

				iResultsCount++;
			}
		}break;
		case FeEUiBindingType::Variable:
		{
			// todo@mgz : fetch value from ...
		} break;
	}

	return iResultsCount;
}
uint32 FeModuleUi::Load(const FeModuleInit* initBase)
{
	AssetsRegistry.Files.SetHeapId(FE_HEAPID_UI);

	AssetsRegistry.OnCreatedAsset = [this](FeAssetFile* assetFile) -> void
	{
		if (assetFile->IsOfType<FeWidget>())
		{
			auto newWidget = assetFile->GetAsset<FeWidget>();
			AddRootWidget(assetFile, newWidget);
		}
	}; 
	AssetsRegistry.OnRemoveAset = [this](const FeAssetRef& assetRef) -> void
	{
		if (assetRef.Type == FeAssetType::Widget())
			GetRootWidgets().Remove([&](const FeUiRootWidget& rootWidget) -> bool { return rootWidget.AssetFile->AssetRef == assetRef; });
	};

	bIsRenderEnabled = true;
	ModuleInit = *(FeModuleUiInit*)initBase;
	
	return Reload(true);
}
FeUiRootWidget& FeModuleUi::AddRootWidget(FeAssetFile* assetFile, FeWidget* widget)
{
	FeUiRootWidget& rootWidget = RootWidgets.Add();

	rootWidget.RenderBatch.GeometryInstances.Reserve(1024);
	rootWidget.RenderBatch.Viewport.UseBackBuffer = true;
	rootWidget.RenderBatch.Viewport.Rect = GlobalRect;
	rootWidget.AssetFile = assetFile;
	rootWidget.IsVisible = false;
	rootWidget.Root = widget;

	widget->GetGeometryInstance().Viewport = &rootWidget.RenderBatch.Viewport;

	return rootWidget;
}
uint32 FeModuleUi::Reload(bool bInitialize /*=false*/)
{
	Unload();

	auto pRenderingModule = FeModuleRendering::Get();
	GlobalRect = pRenderingModule->GetDefaultViewport().Rect;

	NavigationHandler.ModuleUi = this;
	NavigationHandler.SetIsEnabled(true);

	GlobalTransform.SetIdentity();
	LoadScripts(); // <------ all the stuff is done here
	//LoadUnitTest(0);

	if (bInitialize)
	{
		if (ModuleInit.EnableFileWatcher)
		{
			// register file watcher for script files
			auto pFileManagerModule = FeApplication::GetStaticInstance()->GetModule<FeModuleFilesManager>();

			char szWatchPath[COMMON_PATH_SIZE];
			sprintf_s(szWatchPath, "%s/%s", FePath::GetRoot(), "themes");
			pFileManagerModule->WatchDirectory(szWatchPath, OnScriptFileChanged, this);
		}
		NavigationHandler.AddUser(0, FeEUiUserInputDeviceType::Mouse);
	}

	return FeEReturnCode::Success;
}
uint32 FeModuleUi::Unload()
{
	RootWidgets.Free();
	AssetsRegistry.Files.Free();

	return FeEReturnCode::Success;
}
uint32 FeModuleUi::Update(const FeDt& fDt)
{
	NavigationHandler.Update(fDt);
	
	// play animations due to navigation events
	for (auto& navEvent : NavigationHandler.PendingEvents)
	{
		if (navEvent.Type == FeEUiEvent::Cursor_Enter
			|| navEvent.Type == FeEUiEvent::Cursor_Leave
			|| navEvent.Type == FeEUiEvent::Cursor_Leave_Long
			|| navEvent.Type == FeEUiEvent::Cursor_Leave_Very_Long
			|| navEvent.Type == FeEUiEvent::Cursor_Hover_Long
			|| navEvent.Type == FeEUiEvent::Cursor_Hover_Very_Long
			|| navEvent.Type == FeEUiEvent::Widget_Focus
			|| navEvent.Type == FeEUiEvent::Widget_UnFocus
			|| navEvent.Type == FeEUiEvent::Widget_Enable
			|| navEvent.Type == FeEUiEvent::Widget_Disable
			|| navEvent.Type == FeEUiEvent::Page_Enter
			|| navEvent.Type == FeEUiEvent::Page_Leave
			|| navEvent.Type == FeEUiEvent::Button_Pressed
			|| navEvent.Type == FeEUiEvent::Button_Released)
		{
			if (navEvent.Target)
			{
				for (auto& eventSequence : navEvent.Target->Sequences)
					AssetsRegistry.FetchAssetRef(eventSequence.Sequence);

				Sequencer.PlayWidgetEventSequence(navEvent.Target, navEvent.Type);
			}
		}
	}

	Sequencer.Update(fDt);

	if (bIsRenderEnabled)
	{
		auto pRenderingModule = FeModuleRendering::Get();

		if (pRenderingModule->GetDevice().IsValid())
		{
			for (auto& widget : RootWidgets)
			{
				if (widget.IsVisible)
				{
					widget.RenderBatch.GeometryInstances.Free();
					widget.Root->Traverse([&, this](FeTraversalState& traversal) -> void
					{
						FE_ASSERT(traversal.Current, "");

						if (traversal.Current->IsDataDirty())
							traversal.Current->OnDataChanged();

						if (traversal.Current->IsVisible() && traversal.Current->GetOpacity().Traversal > 0.0f)
							widget.RenderBatch.GeometryInstances.Add(&traversal.Current->GetGeometryInstance());
						else
							traversal.bCancelBranch = true;
					});

					pRenderingModule->RegisterRenderBatch(&widget.RenderBatch);
				}
			}
		}
	}

	return FeEReturnCode::Success;
}
FeUiRootWidget* FeModuleUi::GetRootWidget(FeWidget* widget)
{
	return RootWidgets.Find([&](const FeUiRootWidget& rootWidget) -> bool { return rootWidget.Root == widget; });
}
FeUiRootWidget& FeModuleUi::GetRootWidget(uint32 index)
{
	FE_ASSERT(index < RootWidgets.GetSize(), "out of range");

	return RootWidgets[index];
}
char* FeModuleUi::ComputeDebugOutput(const FeDt& fDt)
{
	struct PendingEventLogging
	{
		char szLog[128];
		float fDt;
		PendingEventLogging() : fDt(0.0f) {}
	};
	static FeTArray<PendingEventLogging> pendingLogs;
	pendingLogs.Reserve(32);

	const size_t STRING_SIZE = 2048;
	static char szOutput[STRING_SIZE];

	uint32 iUserIdx = 0;
	size_t iOffset = 0;

	for (auto& user : NavigationHandler.Users)
	{
		FeTPtr<FeWidget>& Hovered = NavigationHandler.UsersFocus[iUserIdx].Ptr;

		iUserIdx++;

		if (!user.IsValid())
			continue;

		const FeVector2& pos = user->GetPosition();
		
		iOffset += sprintf_s(szOutput + iOffset, STRING_SIZE - iOffset, "\nCusror (%d)  : %4.2f , %4.2f", iUserIdx, pos.getData()[0], pos.getData()[1]);
		iOffset += sprintf_s(szOutput + iOffset, STRING_SIZE - iOffset, "\nHovered (%d) : %s | %s", iUserIdx, Hovered.IsValid() ? Hovered->GetThisClassName().Cstr() : "", Hovered.IsValid() ? Hovered->ID.Name.Cstr() : "");
	}

	iOffset += sprintf_s(szOutput + iOffset, STRING_SIZE - iOffset, "\n\nPendingEvents");


	for (auto& pendingEvent : NavigationHandler.PendingEvents)
	{
		if (pendingEvent.Type != FeEUiEvent::Cursor_Moved)
		{
			auto& log = pendingLogs.Add();
			log.fDt = 0.0f;
			sprintf_s(log.szLog, 128, "\nEvent (%s) (User:%d) (Button%d) (Target:%s) ",
				FeEUiEvent::ToString(pendingEvent.Type).Cstr(), pendingEvent.UserIndex, pendingEvent.Button, pendingEvent.Target ? pendingEvent.Target->ID.Name.Cstr() : "");
		}
	}

	FeTArray<uint32> iLogsToRemove;
	uint32 iLogIdx = 0;

	for (auto& pendingLog : pendingLogs)
	{
		if (pendingLog.fDt > 2.0f)
		{
			iLogsToRemove.Add(iLogIdx);
		}
		else
		{
			if (STRING_SIZE> iOffset + strlen(pendingLog.szLog))
				iOffset += sprintf_s(szOutput + iOffset, STRING_SIZE - iOffset, pendingLog.szLog);

			iLogIdx++;
		}
		pendingLog.fDt += fDt.TotalSeconds;
	}
	for (auto logToRemove : iLogsToRemove)
		pendingLogs.RemoveAt(logToRemove);

	return szOutput;
}
void FeModuleUi::OnViewportChanged()
{
	FeTransform tmp;

	for (auto& widget : RootWidgets)
	{
		widget.RenderBatch.Viewport.Rect = GlobalRect;
		FeGeometryHelper::ComputeMatrix(widget.RenderBatch.Viewport.Transform, GlobalTransform);
		FeTransform& serializedT = widget.Root->GetTransform();

		INLINE_OP_VEC3(tmp.Translation, GlobalTransform.Translation, serializedT.Translation, +);
		INLINE_OP_VEC3(tmp.Scale, GlobalTransform.Scale, serializedT.Scale, *);

		widget.Root->SetTransientTransform(tmp);
		widget.Root->OnPropertyChangedTransform();
	}
}
void FeModuleUi::SetViewport(const FeTransform& tranform, const FeRect& rect, bool bForceUpdate/* = false*/)
{
	if (bForceUpdate || GlobalTransform != tranform || GlobalRect != rect)
	{
		GlobalTransform = tranform;
		GlobalRect = rect;

		OnViewportChanged();
	}
}
uint32 FeModuleUi::LoadImage(FeImage* pImg)
{
	auto pResourcesHandler = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();

	FeRenderLoadingResource resource;
	resource.ComputeTexturePath(pImg->File);
	resource.Type = FeEResourceType::Texture;
	resource.AllocateResource();

	resource.GetResourceT<FeRenderTexture>()->FirstMipLevel = 0;
	resource.GetResourceT<FeRenderTexture>()->Id = pImg->File.GetId();

	pResourcesHandler->LoadResource(resource); // schedule resource loading

	return resource.Id;
}
void FeUiNavigationHandler::UpdateUserFocus(uint32 userIndex)
{
	FeTPtr<FeUiUserInputHandler>& userInput = Users[userIndex];
	auto& userFocus = UsersFocus[userIndex];
	FeUiUserInputHandler* pUserInput = userInput.Get();

	const FeVector2& cursorPos = pUserInput->GetPosition();

	float cursorX = cursorPos.getData()[0];
	float cursorY = cursorPos.getData()[1];

	auto pModuleUi = FeApplication::GetStaticInstance()->GetModule<FeModuleUi>();
	FeWidget* pHoveredWidget = nullptr;
	FeWidget* previousHoveredWidget = userFocus.Ptr.GetNull();

	const FeVector2& delta = pUserInput->GetPositionDelta();

	// Compute hovered widget
	{
		float fHoveredZ = 999.0f;
		FeTArray<FeWidget*> hoveredStack;

		for (auto& widget : pModuleUi->GetRootWidgets())
		{
			const float* viewportScale = ModuleUi->GlobalTransform.Scale.getData();
			if (cursorX < viewportScale[0] && (-cursorY) < viewportScale[1])
			{
				widget.Root->Traverse([&, this](FeTraversalState& traversal) -> void
				{
					if (traversal.Current->IsVisible() && traversal.Current->GetOpacity().Traversal > 0.0f)
					{
						if (traversal.Current->IsFocusable)
						{
							const FeMatrix4& mat = traversal.Current->GetTraversalTransform();
							float fTz = FeGeometryHelper::GetMatrixTranslationZ(mat);

							if (FeMathHelper::IntersectPointWithRect(cursorPos, mat))
							{
								hoveredStack.Add(traversal.Current);
							}
						}
					}
					else
					{
						traversal.bCancelBranch = true;
					}
				});
			}
		}
		{ // Sort instances along Z axis
			auto compareZ = [](const FeWidget* a, const FeWidget* b) -> int
			{
				float aZ = FeGeometryHelper::GetMatrixTranslationZ(a->GetTraversalTransform());
				float bZ = FeGeometryHelper::GetMatrixTranslationZ(b->GetTraversalTransform());

				if (a->IsRoot()) aZ += 1.f;
				if (b->IsRoot()) bZ += 1.f;

				if (aZ > bZ)		return 1;
				else if (aZ < bZ)	return 2;
				else				return 0;
			};

			FeArrayHelper::QuickSort<FeWidget*>::Execute(hoveredStack, compareZ);
		}
		if (hoveredStack.GetSize())
			pHoveredWidget = hoveredStack.Back();

		userFocus.Ptr.Assign(pHoveredWidget);
	}

	AddPendingEvent(FeEUiEvent::Cursor_Moved, pHoveredWidget, userIndex);

	if (previousHoveredWidget != pHoveredWidget)
	{
		if (previousHoveredWidget)
		{
			if (userFocus.ToggledVeryLongDuration)
				AddPendingEvent(FeEUiEvent::Cursor_Leave_Very_Long, previousHoveredWidget, userIndex);

			if (userFocus.ToggledLongDuration)
				AddPendingEvent(FeEUiEvent::Cursor_Leave_Long, previousHoveredWidget, userIndex);

			AddPendingEvent(FeEUiEvent::Cursor_Leave, previousHoveredWidget, userIndex);
		}

		if (pHoveredWidget)
			AddPendingEvent(FeEUiEvent::Cursor_Enter, pHoveredWidget, userIndex);

		userFocus.ResetDuration();
	}
}
void FeUiNavigationHandler::Update(const FeDt& fDt)
{
	PendingEvents.Clear();

	if (!bIsEnabled)
		return;

	for (uint32 userIdx = 0; userIdx < FE_UI_MAX_PLAYER_COUNT; ++userIdx)
	{
		FeTPtr<FeUiUserInputHandler>& userInput = Users[userIdx];
		auto& userFocus = UsersFocus[userIdx];

		if (userInput.IsValid())
		{
			userInput->Update();
			
			if (userInput->Moved() || userInput->ButtonStateChanged(0))
				UpdateUserFocus(userIdx);

			if (userFocus.Ptr.IsValid())
			{
				userFocus.Duration += fDt.TotalSeconds;

				if (!userFocus.ToggledLongDuration && userFocus.Duration > 1.0f)
				{
					userFocus.ToggledLongDuration = true;
					AddPendingEvent(FeEUiEvent::Cursor_Hover_Long, userFocus.Ptr.Get(), userIdx);
				}
				if (!userFocus.ToggledVeryLongDuration && userFocus.Duration > 3.0f)
				{
					userFocus.ToggledVeryLongDuration = true;
					AddPendingEvent(FeEUiEvent::Cursor_Hover_Very_Long, userFocus.Ptr.Get(), userIdx);
				}
			}

			for (uint32 i = 0; i < 11; ++i)
			{
				if (userInput->ButtonStateChanged(i))
				{
					FeEUiEvent::Type eButtonEvent = userInput->IsButtonDown(i) ? FeEUiEvent::Button_Pressed : FeEUiEvent::Button_Released;
					AddPendingEvent(eButtonEvent, userFocus.Ptr.GetNull(), userIdx, i);
				}
					
			}
		}
	}

	for (auto& pendingEvent : PendingEvents)
	{
		if (pendingEvent.Target)
		{
			pendingEvent.Target->SendEvent(pendingEvent);
		}
	}
}
FeUiEventParameters& FeUiNavigationHandler::AddPendingEvent(FeEUiEvent::Type type, FeWidget* target/* = nullptr*/, uint32 userIndex/* = FE_INVALID_ID*/, uint32 button/* = FE_INVALID_ID*/)
{
	auto& eventMove = PendingEvents.Add();
	eventMove.Type = type;
	eventMove.UserIndex = userIndex;
	eventMove.Button = button;
	eventMove.Target = target;

	return eventMove;
}
void FeUiNavigationHandler::AddUser(uint32 userIndex, FeEUiUserInputDeviceType::Type inputType)
{
	FE_ASSERT(userIndex < FE_UI_MAX_PLAYER_COUNT, "FeUiNavigationHandler::AddUser  : out of range");

	Users[userIndex].Delete();

	switch (inputType)
	{
	case FeEUiUserInputDeviceType::Mouse:
		
		Users[userIndex].Assign(FE_NEW(FeUiUserMouseInputHandler, FE_HEAPID_UI), FE_HEAPID_UI, true);

		break;
	case FeEUiUserInputDeviceType::GamePad:
		break;
	default:
		break;
	}
}
FeTPtr<FeUiUserInputHandler> FeUiNavigationHandler::GetUser(int32 index)
{
	FE_ASSERT(index < FE_UI_MAX_PLAYER_COUNT, "out of range!");

	return Users[index];
}
FeNavigationUserFocus& FeUiNavigationHandler::GetUserFocus(int32 index)
{
	FE_ASSERT(index < FE_UI_MAX_PLAYER_COUNT, "out of range!");

	return UsersFocus[index];
}
void FeUiNavigationHandler::OnReloadUi()
{
	for (auto& userFocus : UsersFocus)
		userFocus.Reset();
}
void FeUiNavigationHandler::SetIsEnabled(bool bValue)
{
	bIsEnabled = bValue;
}

void FeUiSequencer::PlayWidgetEventSequence(FeWidget* target, FeEUiEvent::Type eventType)
{
	if (!target)
		return;

	auto& targetEventSequences = target->GetSequences();

	for (auto& eventSequence : targetEventSequences)
	{
		if (eventSequence.GetEventType() == eventType)
		{
			//uint32 iPreviousSequence = Sequences.IndexOf([&, this](const FeUiSequenceInstance& seq) -> bool { return !seq.bEnded && seq.Target == target; });

			//if (iPreviousSequence != Sequences.ErrorIndex())
			//	Sequences[iPreviousSequence].bCanceled = true;

			auto& sequenceInstance = Sequences.Add();
			sequenceInstance.EventSequence = &eventSequence;
			sequenceInstance.Target = target;

			if (eventSequence.Sequence.Pointer)
			{
				auto sequence = eventSequence.Sequence.Pointer->GetAsset<FeUiSequence>();

				for (auto& interpolation : sequence->GetInterpolations())
				{
					FeInterpolatationInstance& interpolInstance = sequenceInstance.Interpolations.Add();

					interpolInstance.Clear();
					interpolInstance.Interpolation = interpolation.Get();
					interpolation->ComputeTargetPropertyPtr(sequenceInstance, interpolInstance);

					FE_ASSERT(interpolInstance.TargetProperty, "couldn't compute targetproperty ptr");

					if (!interpolInstance.TargetProperty)
						sequenceInstance.Interpolations.PopBack();
				}
			}
		}
	}
}
void FeUiSequencer::Update(const FeDt& fDt)
{
	// process sequences interpolations
	if (State == FeESequencerState::Stop)
		return;
	else if (State == FeESequencerState::Play)
		Time = fDt.TotalSeconds;

	FeTArray<uint32> SequencesToRemove;
	uint32 iIndex = 0;
	uint32 iFinishedSequences = 0;
	FeTArray<FeWidget*> ProcessedWidgets;
	const float fCanceledSequenceSpeed = 5.0f;

	for (auto& sequenceInstance : Sequences)
	{
		if (sequenceInstance.bEnded)
		{
			iFinishedSequences++;
		}
		else
		{
			if (ProcessedWidgets.IndexOf(sequenceInstance.Target) == ProcessedWidgets.ErrorIndex())
			{
				auto sequence = sequenceInstance.EventSequence->Sequence.Pointer->GetAsset<FeUiSequence>();

				ProcessedWidgets.Add(sequenceInstance.Target);

				sequenceInstance.Time += Time * (sequenceInstance.bCanceled ? fCanceledSequenceSpeed : 1.0f);

				uint32 iInterpolIdx = 0;

				bool bReversed = sequenceInstance.EventSequence->bReversed;
				float fTime = bReversed ? sequence->GetDuration() - sequenceInstance.Time : sequenceInstance.Time;
				fTime = FeMath::Clamp(fTime, 0.f, sequence->GetDuration());

				for (auto& interpolInstance : sequenceInstance.Interpolations)
				{
					interpolInstance.Interpolation->UpdatePropertyInterpolation(sequenceInstance, interpolInstance, fTime);
				}

				if (sequenceInstance.Time >= sequence->GetDuration())
				{
					sequenceInstance.bEnded = true;
				}
			}
		}
	}

	if (iFinishedSequences >= 100)
	{
		FeTArray<FeUiSequenceInstance> SequencesCopy;
		SequencesCopy = Sequences;
		Sequences.Free();
		Sequences.Reserve(128);

		for (auto& sequenceInstance : SequencesCopy)
		{
			if (!sequenceInstance.bEnded)
				Sequences.Add(sequenceInstance);
		}
	}
}
void FeUiSequencer::OnReloadUi()
{
	Sequences.Free();
	Sequences.Reserve(128);
}
void FeUiSequencer::SetPaused(bool bValue)
{
	Paused = bValue;

	//for (auto& sequence : Sequences)
	//{
	//	if (sequence.Time == 0.0f)
	//		sequence.bEnded = true;
	//	else
	//		sequence.bCanceled = true;
	//}
}

uint32 FeImage::GetResourceId()
{
	if (ResourceID == 0)
	{
		FeRenderLoadingResource resource;
		resource.ComputeTexturePath(File);
		ResourceID = resource.Path.Source.GetId();
	}

	return ResourceID;
}