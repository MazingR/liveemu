#pragma once

#include <rendering/effect.hpp>
#include <rendering/commonrenderer.hpp>
#include <common/serializable.hpp>
#include <common/maths.hpp>
#include <commonui.hpp>
#include <guid.hpp>

#define DECLARE_REGISTER_TRAVERSAL_PROPERTY(t, n)						\
	FeTRegisterTraversalProperty(Parent, TraversalProperties, n, #n);	\

#define DECLARE_REGISTER_TRAVERSAL_PROPERTIES_BASE(properties, propertiesNonSerialized)	\
virtual void RegisterTraversalProperties()												\
{																						\
	TraversalProperties.Free();															\
	properties(DECLARE_REGISTER_TRAVERSAL_PROPERTY)										\
	propertiesNonSerialized(DECLARE_REGISTER_TRAVERSAL_PROPERTY)						\
}

#define DECLARE_REGISTER_TRAVERSAL_PROPERTIES(properties, propertiesNonSerialized)	\
virtual void RegisterTraversalProperties()	override								\
{																					\
	Super::RegisterTraversalProperties();											\
	properties(DECLARE_REGISTER_TRAVERSAL_PROPERTY)									\
	propertiesNonSerialized(DECLARE_REGISTER_TRAVERSAL_PROPERTY)					\
}

class FeImageAspect : public FeSerializable
{
public:
#define FeImageAspect_Props(_d)					\
	_d(FeEImageResize::Type,	Resize)			\
	_d(FeEImageAlign::Type,		HorizontalAlign)\
	_d(FeEImageAlign::Type,		VerticalAlign)	\
	_d(FeEImageSample::Type,	SampleMode)		\

	FE_DECLARE_CLASS_BODY(FeImageAspect_Props, FeImageAspect, FeSerializable)
public:
	FeImageAspect() : Resize(FeEImageResize::Stretch), HorizontalAlign(FeEImageAlign::Min), VerticalAlign(FeEImageAlign::Min), SampleMode(FeEImageSample::Normal)
	{}
};
FE_DECLARE_CLASS_BOTTOM(FeImageAspect)


class FeWidget;
class FeUiTextChar;

struct FeTraversalState
{
	FeWidget* Parent;
	FeWidget* Current;
	bool bCancelBranch;
	bool bCancelAll;

	FeTraversalState() :
		Parent(nullptr),
		Current(nullptr),
		bCancelBranch(false),
		bCancelAll(false)
	{}
};
struct FeUiRootWidget;

class FeWidget : public FeAsset, public FeIShaderPropertyContainer
{
public:
	FeWidget();
	~FeWidget();

	bool HasState(uint32 states);
	void SetState(bool bVal, FeEWidgetState::Type state);

	inline bool IsVisible()		{ return HasState(FeEWidgetState::Visible	); }
	inline bool IsEnabled()		{ return HasState(FeEWidgetState::Enabled	); }
	inline bool IsCollapsed()	{ return HasState(FeEWidgetState::Collapsed	); }

	inline void SetVisible(bool bVal)	{ SetState(bVal, FeEWidgetState::Visible);	 OnVisibleChanged(bVal);	}
	inline void SetEnabled(bool bVal)	{ SetState(bVal, FeEWidgetState::Enabled);	 OnEnabledChanged(bVal); }
	inline void SetCollapsed(bool bVal)	{ SetState(bVal, FeEWidgetState::Collapsed);  OnCollapsedChanged(bVal); }

	virtual void OnVisibleChanged(bool bVal)	{}
	virtual void OnEnabledChanged(bool bVal)	{}
	virtual void OnCollapsedChanged(bool bVal)	{}
	virtual void OnFocusedChanged(bool bVal)	{}
	virtual void OnSelectedChanged(bool bVal)	{}

	#define FeWidget_Properties(_d)										\
		_d(FeTransform,							Transform)				\
		_d(FeTransform,							DesignerTransform)		\
		_d(int32,								DesignerGrid)			\
		_d(FeAssetRef,							Effect)					\
		_d(FeAssetRef,							Font)					\
		_d(FeAssetRef,							Template)				\
		_d(FeTArray<FeUiDataBinding>,			Bindings)				\
		_d(FeTraversalPropertyFloat,			Opacity)				\
		_d(FeTraversalPropertyFloat,			Luminance)				\
		_d(FeTraversalPropertyColor,			BorderColor)			\
		_d(FeTArray<FeUiEventSequence>,			Sequences)				\
		_d(FeImageAspect,						ImageAspect)			\
		_d(FeTArray<FeTPtr<FeWidget>>,			Children)				\
		_d(bool,								IsFocusable)			\

	
	#define FeWidget_PropertiesNotSerialized(_d)						\
		_d(uint32,								DataId)					\
		_d(FeTransform,							TransientTransform)		\
		_d(FeMatrix4,							TraversalTransform)		\
		_d(FeTraversalPropertyBitFlag,			States)					\
		_d(FeTArray<FeTPtr<FeUiTextChar>>,		TextNodes)				\
		_d(FeRenderGeometryInstance,			GeometryInstance)		\
		_d(bool,								IsScaleAbsolute)		\

	FE_DECLARE_CLASS_BODY_EX(FeWidget_Properties, FeWidget_PropertiesNotSerialized, FeWidget, FeAsset)
public:

	virtual void ComputeGeometry();
	virtual void PostInitialize();
	virtual void OnTemplateChanged(bool bKeepTransform = true, bool bKeepName = true);
	virtual uint32 OnDataChanged();
	virtual void CopyFrom(FeWidget* copied);
	virtual void OnParentChanged();
	virtual bool IsDataDirty() const;
	void SetPixelScale(FeVector3 value);
	FeVector3 GetPixelScale();

	virtual uint32 SetProperty(const FeString& PropertyName, const FeUiBindingDataField& Value, int32 Index);
	virtual uint32 SetProperty(const FeString& PropertyName, const FeString& Value, int32 Index);

	// FeIShaderPropertyContainer interface
	float GetShaderPropertyScalar(const FeString& PropertyName)  override;
	FeVector4& GetShaderPropertyVector(const FeString& PropertyName)  override;
	FeColor& GetShaderPropertyColor(const FeString& PropertyName)  override;
	FeEImageAlign::Type GetImageHorizontalAlign()	{ return ImageAspect.HorizontalAlign; }
	FeEImageAlign::Type GetImageVerticalAlign()		{ return ImageAspect.VerticalAlign; }
	FeEImageResize::Type GetImageResizeMode()		{ return ImageAspect.Resize; }
	FeEImageSample::Type GetImageSampleMode()		{ return ImageAspect.SampleMode; }

	virtual bool Traverse(std::function<void(FeTraversalState& traversal)> callback, int32 maxDepth=FE_MAX_INT32);

	FeWidget* GetParent() const;
	FeWidget* GetRootParent();
	bool IsRoot() const { return Parent == nullptr; }
	int32 GetDepth() const;
	bool IsParentOf(FeWidget* widget);

	void SetParent(FeWidget* parent)  { Parent = parent; OnParentChanged(); }
	uint32 RefreshCurrentWidgetDataBinding();

	virtual void OnPropertyChanged(void* propertyPtr);
	void OnPropertyChangedTransform();

	virtual void SendEvent(FeUiEventParameters& eventParams);
	
	static FeWidget* GetWidgetFromPath(FeWidget* root, const FeWidgetPath& path, uint32 offset=0);
	void ComputePath(FeWidgetPath& path, bool bClearArray = true);

	FeTraversalPropertyBase* GetTraversalPropertyFromId(uint32 Id);
	FeTraversalPropertyBase* GetTraversalPropertyFromTransientPtr(void* Ptr);
	FeTraversalPropertyBase* GetTraversalPropertyFromSerializedPtr(void* Ptr);

	const FeNamedGUID& GetGuid() const;
	void GetGuid(FeNamedGUID& output) const;

protected:
	DECLARE_REGISTER_TRAVERSAL_PROPERTIES_BASE(FeWidget_Properties, FeWidget_PropertiesNotSerialized)

	const FeAssetRef* GetRecursiveFont();
	const FeAssetRef* GetRecursiveEffect();

	uint32 ApplyBindingToTargetProperty(FeWidget* root, const FeUiBindingDataField& sourceData, const FeUiBinding& targetBinding);
	virtual void OnScaleChanged();

	FeWidget* Parent;
	bool bIsDataDirty;
	uint32 GenerateTextRenderingNodes(const FeString& Value);
	void ComputeTraversalTransform();

	FeTArray<FeTraversalPropertyBase*> TraversalProperties;
};
FE_DECLARE_CLASS_BOTTOM(FeWidget)

// ----------------------------------------------------------------------------------------------------------------------------------------------------------

class FeUiTextChar : public FeWidget
{
public:
	virtual void PostInitialize() override;

	#define FeUiTextChar_Props(_d)	\
		_d(FeVector4,	FontData)	\

	FE_DECLARE_CLASS_BODY_EX(EMPTY_PROPERTIES_LIST, FeUiTextChar_Props, FeUiTextChar, FeWidget)
};
FE_DECLARE_CLASS_BOTTOM(FeUiTextChar)

class FeUiButton : public FeWidget
{
public:
	#define FeUiButton_Props(_d)				\

	virtual void SendEvent(FeUiEventParameters& eventParams) override;
	FE_DECLARE_CLASS_BODY(FeUiButton_Props, FeUiButton, FeWidget)
};
FE_DECLARE_CLASS_BOTTOM(FeUiButton)

class FeUiImage : public FeWidget
{
public:
	#define FeUiImage_Props(_d)				\
	_d(FeAssetRef, Image)

	FeUiImage() : FeWidget()
	{
		Image.Type = "Image";
	}
	virtual void PostInitialize() override;
	virtual void OnPropertyChanged(void* propertyPtr) override;

	void OnPropertyImageChanged();

	FE_DECLARE_CLASS_BODY(FeUiImage_Props, FeUiImage, FeWidget)
};
FE_DECLARE_CLASS_BOTTOM(FeUiImage)

class FeUiEmulatorViewport : public FeWidget
{
public:
	#define FeUiEmulatorViewport_Props(_d)

	FeUiEmulatorViewport() : FeWidget()
	{}
	virtual void PostInitialize() override;

	FE_DECLARE_CLASS_BODY(FeUiEmulatorViewport_Props, FeUiEmulatorViewport, FeWidget)
};
FE_DECLARE_CLASS_BOTTOM(FeUiEmulatorViewport)


class FeUiText : public FeWidget
{
public:
#define FeUiText_Props(_d)				\
	_d(FeString, Text)

	FeUiText() : FeWidget()
	{
		bIsTextDirty = true;
	}
	virtual bool IsDataDirty() const override;
	virtual void OnScaleChanged();
	virtual uint32 OnDataChanged() override;
	virtual void PostInitialize() override;
	virtual void OnPropertyChanged(void* propertyPtr) override;

	FE_DECLARE_CLASS_BODY(FeUiText_Props, FeUiText, FeWidget)

private:
	bool bIsTextDirty;
};
FE_DECLARE_CLASS_BOTTOM(FeUiText)
