#include <widget.hpp>

#include <moduleui.hpp>
#include <rendering/modulerenderer.hpp>
#include <rendering/renderresourceshandler.hpp>

void FeRegisterTraversalProperty(FeWidget* parent, FeTArray<FeTraversalPropertyBase*>& properties, FeTraversalPropertyBase* prop, const char* sName)
{
	prop->Id = FeStringTools::GenerateUIntIdFromString(sName);
	properties.Add(prop);
	if (parent)
		prop->ParentProperty = (FeTraversalPropertyBase*)parent->GetTraversalPropertyFromId(prop->Id);
	else
		prop->ParentProperty = nullptr;
}

FeWidget::~FeWidget()
{
	FE_ASSERT(true, "");
}
FeWidget::FeWidget()
{
	bIsDataDirty = true;
	Type = FeAssetType::Widget();
	DataId = FE_INVALID_ID;
	IsFocusable = false;
	IsScaleAbsolute = false;

	Opacity = 1.0f;
	Luminance = 1.0f;
	BorderColor = FeColor(0, 0, 0, 0);

	Transform.SetIdentity();
	DesignerTransform.SetIdentity();
	DesignerTransform.Scale = FeVector3(0.6f, 0.6f, 1.f);
	DesignerGrid = 32;

	Parent = nullptr;
	States = FeEWidgetState::Visible | FeEWidgetState::Enabled;
	bIsDataDirty = false;
	TextNodes.SetHeapId(FE_HEAPID_UI);

	Effect.Type		= FeAssetType::Effect();
	Font.Type		= FeAssetType::Font();
	Template.Type	= FeAssetType::Widget();
}
bool FeWidget::HasState(uint32 states)
{
	return (States.Transient & states) != 0;
}
void FeWidget::SetState(bool bVal, FeEWidgetState::Type state)
{
	uint32 iVal = (uint32)state;
	if (bVal)
		States.Transient |= iVal;
	else
		States.Transient &= ~iVal;
}
void FeWidget::ComputeGeometry()
{
	GeometryInstance.Reset();
	GeometryInstance.Owner = this;

	if (Parent)
		GeometryInstance.Viewport = Parent->GeometryInstance.Viewport;
	
	const FeAssetRef* effetRef = GetRecursiveEffect();
	GeometryInstance.Effect = effetRef ? effetRef->Path.GetId() : 0;
	GeometryInstance.Geometry = FeGeometryHelper::GetStaticGeometry(FeEGemetryDataType::Quad);
	GeometryInstance.Transform = TraversalTransform;
}
void FeWidget::ComputeTraversalTransform()
{
	FeMatrix4& mat = GetTraversalTransform();
	FeGeometryHelper::ComputeMatrix(mat, TransientTransform);

	if (IsRoot())
		return;

	if (IsScaleAbsolute)
	{
		float* parentT = FeGeometryHelper::GetMatrixTranslationData(GetParent()->GetTraversalTransform());
		float* matT = FeGeometryHelper::GetMatrixTranslationData(mat);

		float& sX = FeGeometryHelper::GetMatrixScaleX(mat);
		float& sY = FeGeometryHelper::GetMatrixScaleY(mat);

		const auto& globalT = FeModuleUi::Get()->GetGlobalTransform();
		auto rootParent = GetRootParent();

		float pSx, pSy;

		pSx = globalT.Scale[0];
		pSy = globalT.Scale[1];

		sX *= pSx;
		sY *= pSy;

		matT[0] *= pSx;
		matT[1] *= pSy;

		matT[0] += parentT[0];
		matT[1] += parentT[1];
		matT[2] += parentT[2];
	}
	else
	{
		gmtl::mult(mat, GetParent()->GetTraversalTransform(), mat);
	}
}
void FeWidget::PostInitialize()
{
	RegisterTraversalProperties();

	for (auto& prop : TraversalProperties)
		prop->PostDeSerialized();

	TransientTransform		= Transform;
	States.Traversal = States.Transient;
	ComputeTraversalTransform();

	if (Parent)
	{
		#define PROPAGATE_PROPERTY(_a_, _op_) _a_.Traversal = _a_.Transient _op_ Parent->Get##_a_().Traversal

		PROPAGATE_PROPERTY(Opacity, *);
		PROPAGATE_PROPERTY(Luminance, *);
		PROPAGATE_PROPERTY(States, &);
	}

	ComputeGeometry();

}
uint32 FeWidget::SetProperty(const FeString& PropertyName, const FeUiBindingDataField& Value, int32 Index)
{
	auto pResourcesHandler = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();

	const FeString PropName_Image = "Image";
	const FeString PropName_DefaultImage = "DefaultImage";

	uint32 Result = FeEReturnCode::Success;

	if (PropName_Image == PropertyName || PropName_DefaultImage == PropertyName)
	{
		FE_ASSERT(Value.Pointer && Value.PointerType == FeEUiBindingType::Asset_Image, "invalid binding type");

		auto imageRef = static_cast<FeAssetRef*>(Value.Pointer);

		FeRenderLoadingResource resource;
		resource.ComputeTexturePath(imageRef->Path);
		resource.Type = FeEResourceType::Texture;
		resource.AllocateResource();

		resource.GetResourceT<FeRenderTexture>()->FirstMipLevel = 0;
		resource.GetResourceT<FeRenderTexture>()->Id = imageRef->Path.GetId();

		pResourcesHandler->LoadResource(resource); // schedule resource loading

		if (PropName_Image == PropertyName)
			GeometryInstance.Textures[Index].Desired = resource.Id;
		else
			GeometryInstance.Textures[Index].Fallback = resource.Id;
	}
	else
	{
		return SetProperty(PropertyName, Value.Value, Index);
	}

	return Result;
}
uint32 FeWidget::SetProperty(const FeString& PropertyName, const FeString& Value, int32 Index)
{
	const FeString PropName_Text			= "Text";
	const FeString PropName_Data			= "Data";

	uint32 Result = FeEReturnCode::Success;

	if (PropName_Text == PropertyName)
	{
		Result = GenerateTextRenderingNodes(Value);
	}
	else if (PropName_Data == PropertyName)
	{
		if (!Value.IsEmpty())
			FeConvert(DataId, Value.Cstr());
	}
	else
	{
		FE_LOG("FeWidget::SetProperty : unknown property named %s", PropertyName.Cstr())
	}
	return Result;
}
void FeWidget::OnParentChanged()
{
	if (Parent)
		GeometryInstance.Viewport = Parent->GeometryInstance.Viewport;

	RegisterTraversalProperties();

	for (auto& child : Children)
		child->SetParent(this);
	for (auto& child : TextNodes)
		child->SetParent(this);
}
bool FeWidget::Traverse(std::function<void(FeTraversalState& traversal)> callback, int32 maxDepth /*= FE_MAX_INT32*/)
{
	FeTraversalState state;

	state.Parent = this->Parent;
	state.Current = this;

	callback(state);

	if (maxDepth > 0)
	{
		maxDepth--;

		if (!state.bCancelBranch && !state.bCancelAll)
		{
			uint32 iChildIdx = 0;

			for (auto& child : Children)
			{
				state.bCancelAll = child->Traverse(callback, maxDepth);

				if (state.bCancelAll)
					break;

				iChildIdx++;
			}
		}

		if (!state.bCancelBranch && !state.bCancelAll)
		{
			for (auto& child : TextNodes)
			{
				state.bCancelAll = child->Traverse(callback, maxDepth);

				if (state.bCancelAll)
					break;
			}
		}
	}
	return state.bCancelAll;
}

uint32 FeWidget::GenerateTextRenderingNodes(const FeString& Value)
{
	TextNodes.Free();

	auto pResourcesHandler = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();
	auto pRenderer = FeModuleRendering::Get();
	auto pModuleUi = FeApplication::GetStaticInstance()->GetModule<FeModuleUi>();

	const FeAssetRef* fontAssetRef = GetRecursiveFont();

	if (!fontAssetRef || fontAssetRef->IsEmpty())
		return FeEReturnCode::Failed;

	const FeRenderResource* pResource = pResourcesHandler->GetResource(fontAssetRef->Path.GetId());
	auto pFont = pModuleUi->AssetsRegistry.GetAsset<FeFont>(*fontAssetRef);

	if (!pResource || pResource->LoadingState != FeEResourceLoadingState::Loaded)
		return FeEReturnCode::Failed;

	auto* pFontData = (FeRenderFont*)pResource;
	const char* szText = Value.Cstr();

	if (szText)
	{
		uint32 iTextLen = strlen(szText);

		FeVector2 vMapSize(1.0f / (float)pFontData->MapSize[0], 1.0f / (float)pFontData->MapSize[1]);
		float fSize		= (float)pFontData->Size;
		float fLineSpace= (float)pFontData->LineSpace;
		float fSpace	= (float)pFontData->Space;
		float fInterval = (float)pFontData->Interval;
		FeVector3 textBounds = GetPixelScale();
		float invertScale[3] {0};

		auto ComputeNextWordSize = [&](int32 iCharIdx) -> float
		{
			uint32 iNextSpace = FeStringTools::IndexOf(szText, ' ', iCharIdx + 1, iTextLen);

			if (iNextSpace > iTextLen)
				iNextSpace = iTextLen;

			float wordSize = 0;
			for (uint32 idx = iCharIdx; idx < iNextSpace; ++idx)
			{
				char szChar = szText[iCharIdx];

				if (pFontData->Chars.find(szChar) != pFontData->Chars.end())
				{
					const FeRenderFontChar& charData = pFontData->Chars[szChar];
					wordSize += charData.Width;
				}
				else
				{
					wordSize += fSize;
				}

				if (idx < (iNextSpace - 1))
					wordSize += fInterval;
			}
			return wordSize;
		};

		FeRotation	r;
		FeVector3 tOffset;

		for (uint32 iCharIdx = 0; iCharIdx < iTextLen; ++iCharIdx)
		{
			char szChar = szText[iCharIdx];

			if (szChar == ' ')
			{
				float nextWordSize = ComputeNextWordSize(iCharIdx + 1);

				if ((nextWordSize + fSpace + tOffset[0]) > textBounds.mData[0])
				{
					tOffset[0] = 0;
					tOffset[1] += fSize + fLineSpace;
				}
				else
				{
					tOffset[0] += fSpace;
				}

				continue;
			}

			if (szChar == '\n')
			{
				tOffset[0] = 0;
				tOffset[1] += fSize + fLineSpace;
				continue;
			}

			if (pFontData->Chars.find(szChar) == pFontData->Chars.end())
				continue;

			if (tOffset[1]> textBounds.mData[1])
				break;

			const FeRenderFontChar& charData = pFontData->Chars[szChar];
			FeVector4 vCharData(
				charData.Left*vMapSize[0],
				charData.Top*vMapSize[1],
				charData.Width*vMapSize[0],
				charData.Height*vMapSize[1]);

			float fCharWidth = (float)charData.Width;
			float fCharHeight = (float)charData.Height;

			float fCharX = (float)charData.OffsetLeft;
			float fCharY = fSize - charData.OffsetTop;

			FeTPtr<FeUiTextChar>& widget = TextNodes.Add();
			widget.Owner = true;
			widget.HeapId = FE_HEAPID_UI;
			FeUiTextChar* pCharWidget = widget.New();

			FeTransform& transform = pCharWidget->GetTransform();

			// Compute absolute scale
			transform.Scale.mData[0] = fCharWidth;
			transform.Scale.mData[1] = fCharHeight;

			// Add char offsets
			transform.Translation.mData[0]  = tOffset[0] + fCharX;
			transform.Translation.mData[1] -= tOffset[1] + fCharY;

			//pCharWidget->SetIsScaleAbsolute(true);
			pCharWidget->FontData = vCharData;

			pCharWidget->SetParent(this);

			if (invertScale[0]==0) // compute once
				ComputePixelConvertFactors(pCharWidget, invertScale, 1.0f);

			transform.Translation[0] /= invertScale[0];
			transform.Translation[1] /= invertScale[1];
			transform.Scale[0] /= invertScale[0];
			transform.Scale[1] /= invertScale[1];

			pCharWidget->PostInitialize();
			pCharWidget->ID.Name = szChar;
			
			auto& geomInstance = pCharWidget->GetGeometryInstance();
			geomInstance.Effect = pFont->GetEffect().Path.GetId();
			geomInstance.Textures[0].Desired = fontAssetRef->Path.GetId();

			tOffset[0] += fCharWidth + fInterval; // move offset for next char
		}
	}
	return FeEReturnCode::Success;
}
float FeWidget::GetShaderPropertyScalar(const FeString& PropertyName)
{
	float* Value = (float*)GetTraversalPropertyByName(PropertyName);
	
	if (Value)
		return *Value;
	
	return 0.0f;
}
FeVector4& FeWidget::GetShaderPropertyVector(const FeString& PropertyName)
{
	static FeVector4 zero(0.f, 0.f, 0.f, 0.f);

	FeVector4* Value = (FeVector4*)GetTraversalPropertyByName(PropertyName);
	if (Value)
		return *Value;

	return zero;
}
FeColor& FeWidget::GetShaderPropertyColor(const FeString&PropertyName)
{
	static FeVector4 black(0.f, 0.f, 0.f, 1.f);

	FeVector4* Value = (FeVector4*)GetTraversalPropertyByName(PropertyName);
	if (Value)
		return *Value;

	return black;
}
uint32 FeWidget::RefreshCurrentWidgetDataBinding()
{
	auto pModuleUi = FeApplication::GetStaticInstance()->GetModule<FeModuleUi>();

	for (auto& dataBind : GetBindings())
	{
		FeBindingDataResults results;
		pModuleUi->FetchBindingSourceData(dataBind.GetSource(), results);

		if (results.Entries.GetSize())
		{
			auto& fields = results.Entries[0].Fields;
			auto& targets = dataBind.GetTargets();

			if (fields.GetSize() && targets.GetSize())
			{
				auto& data = fields[0];
				auto iRes = ApplyBindingToTargetProperty(this, data, targets[0]);

				if (iRes != FeEReturnCode::Success)
					return iRes;
			}
		}
	}
	return FeEReturnCode::Success;
}
uint32 FeWidget::OnDataChanged()
{
	bIsDataDirty = true;
	
	uint32 iRes = RefreshCurrentWidgetDataBinding();
	if (iRes != FeEReturnCode::Success)
		return iRes;

	bIsDataDirty = false;

	return iRes;
}
void FeWidget::OnTemplateChanged(bool bKeepTransform /*= true*/, bool bKeepName /*= true*/)
{
	if (!Template.Path.IsEmpty())
	{
		bIsDataDirty = true;
		auto pModuleUi = FeApplication::GetStaticInstance()->GetModule<FeModuleUi>();

		FeWidget* pTemplate = pModuleUi->AssetsRegistry.GetAsset<FeWidget>(Template);

		if (pTemplate)
		{
			FeTArray<FeUiDataBinding> bindingsCopy = Bindings; // save up bindings
			FeTransform transformCopy = Transform;
			FeString nameCopy = ID.Name;

			FeAssetRef backupRef = Template;
			CopyFrom(pTemplate);
			Template = backupRef;

			// apply "copy specific" properties
			Bindings.Add(bindingsCopy);
			if (bKeepTransform)
				Transform = transformCopy;
			if (bKeepName)
				ID.Name = nameCopy;
			else
				ID.Name = FeStringTools::Concat("_copy_of ", ID.Name);
		}
	}
}
FeWidget* FeWidget::GetWidgetFromPath(FeWidget* root, const FeWidgetPath& path, uint32 offset /*= 0*/)
{
	uint32					iDepth = offset;
	FeWidget*				pTargetWidget = nullptr;

	if (path.GetSize() <= iDepth)
		return root;
	
	root->Traverse([&](FeTraversalState& traversal) -> void
	{
		if (traversal.Current->ID == path[iDepth])
		{
			iDepth++;
			if (iDepth == path.GetSize())
			{
				pTargetWidget = traversal.Current;
				traversal.bCancelAll = true;
			}
				
		}
	});

	return iDepth == path.GetSize() ? pTargetWidget : nullptr;
}
void FeWidget::ComputePath(FeWidgetPath& path, bool bClearArray /*= true*/)
{
	if (bClearArray)
		path.Clear();

	FeTArray<FeWidget*> traversal;
	traversal.Add(this);

	while (false==traversal.Back()->IsRoot())
	{
		traversal.Add(traversal.Back()->GetParent());
	}
	for (int32 i = traversal.GetSize() - 1; i >= 0; --i)
	{
		FeNamedGUID& id = path.Add();
		traversal[i]->GetGuid(id);
	}
}
uint32 FeWidget::ApplyBindingToTargetProperty(FeWidget* root, const FeUiBindingDataField& sourceData, const FeUiBinding& targetBinding)
{
	auto pResourcesHandler = FeApplication::GetStaticInstance()->GetModule<FeModuleRenderResourcesHandler>();
	auto pRenderer = FeModuleRendering::Get();

	FeWidget* pTargetWidget = GetWidgetFromPath(root, targetBinding.GetPath());
	
	if (!pTargetWidget)
		return FeEReturnCode::Failed;

	return pTargetWidget->SetProperty(targetBinding.GetProperty(), sourceData, targetBinding.GetIndex());
}
void FeWidget::CopyFrom(FeWidget* copied)
{
	copied->CopyProperties(this);
	Children.Free();

	for (auto& copiedChild : copied->GetChildren())
	{
		FeTPtr<FeWidget>& newChildPtr = Children.Add();
		newChildPtr.Reset();

		auto newChild = static_cast<FeWidget*>(GetObjectsFactory().CreateObjectFromFactory(copiedChild->GetThisClassName(), FE_HEAPID_UI));
		newChild->CopyFrom(copiedChild.Get());
		newChild->PostInitialize();
		newChildPtr.Assign(newChild, FE_HEAPID_UI, true);
	}
}
const FeAssetRef* FeWidget::GetRecursiveFont()
{
	const FeAssetRef* pResult = nullptr;
	FeWidget* pCurrent = this;
	do
	{
		pResult = &pCurrent->GetFont();
		pCurrent = pCurrent->GetParent();

	} while (pResult->IsEmpty() && pCurrent);

	return pResult;
}
const FeAssetRef* FeWidget::GetRecursiveEffect()
{
	const FeAssetRef* pResult = nullptr;
	FeWidget* pCurrent = this;
	do
	{
		pResult = &pCurrent->GetEffect();
		pCurrent = pCurrent->GetParent();

	} while (pResult->IsEmpty() && pCurrent);

	return pResult;
}
void FeWidget::OnPropertyChangedTransform()
{
	Traverse([&, this](FeTraversalState& traversal) -> void
	{
		FeMatrix4& mat = traversal.Current->GetTraversalTransform();

		float sX = FeGeometryHelper::GetMatrixScaleX(mat);
		float sY = FeGeometryHelper::GetMatrixScaleY(mat);

		traversal.Current->ComputeTraversalTransform();
		FeRenderGeometryInstance& geom = traversal.Current->GetGeometryInstance();

		float newSx = FeGeometryHelper::GetMatrixScaleX(mat);
		float newSy = FeGeometryHelper::GetMatrixScaleY(mat);

		bool bScaleChanged = newSx != sX || newSy != sY;

		if (bScaleChanged)
		{
			traversal.Current->OnScaleChanged();

			if (traversal.Current->IsChildOf(FeUiContainer::ClassName()))
				traversal.bCancelBranch = true;
		}
		
		geom.Transform = mat;
	});
}
void FeWidget::OnScaleChanged()
{
}
void FeWidget::SendEvent(FeUiEventParameters& eventParams)
{
}
FeWidget* FeWidget::GetParent() const
{
	return Parent;
}
FeWidget* FeWidget::GetRootParent()
{
	FeWidget* RootParent = this;

	while (RootParent && RootParent->Parent)
		RootParent = RootParent->Parent;

	return RootParent;
}
int32 FeWidget::GetDepth() const
{
	int32 depth = 0;
	
	FeWidget* parent = GetParent();

	while (parent)
	{
		parent = parent->GetParent();
		depth++;
	}
	return depth;
}
bool FeWidget::IsParentOf(FeWidget* widget)
{
	FeWidget* child = widget;

	while (child)
	{
		if (child->GetParent() == this)
			return true;

		child = child->GetParent();
	}
	return false;
}
void FeUiButton::SendEvent(FeUiEventParameters& eventParams)
{
	if (eventParams.Type == FeEUiEvent::Button_Released)
	{
		// todo : execute event
	}
	
	if (!eventParams.bHandled)
		Super::SendEvent(eventParams);
}
FeTraversalPropertyBase* FeWidget::GetTraversalPropertyFromSerializedPtr(void* Ptr)
{
	uint32 idx = TraversalProperties.IndexOf([&, this](FeTraversalPropertyBase* prop) -> bool { return prop->GetSerialized() == Ptr; });

	if (idx != (uint32)-1)
		return TraversalProperties[idx];
	else
		return  nullptr;
}
FeTraversalPropertyBase* FeWidget::GetTraversalPropertyFromTransientPtr(void* Ptr)
{
	uint32 idx = TraversalProperties.IndexOf([&, this]( FeTraversalPropertyBase* prop) -> bool { return prop->GetTransient() == Ptr; });

	if (idx != (uint32)-1)
		return TraversalProperties[idx];
	else
		return  nullptr;
}
FeTraversalPropertyBase* FeWidget::GetTraversalPropertyFromId(uint32 Id)
{
	uint32 idx = TraversalProperties.IndexOf([&, this](const FeTraversalPropertyBase* prop) -> bool { return prop->Id==Id; });
	
	if (idx != (uint32)-1)
		return TraversalProperties[idx];
	else
		return  nullptr;
}
void FeWidget::OnPropertyChanged(void* propertyPtr)
{
	if (propertyPtr == &Transform ||
		propertyPtr == &Transform.Translation ||
		propertyPtr == &Transform.Scale ||
		propertyPtr == &Transform.Rotation)
	{
		SetTransientTransform(Transform);
		OnPropertyChangedTransform();
	}
	else if (propertyPtr == &Effect)
	{
		const FeAssetRef* effectRef = GetRecursiveEffect();
		GeometryInstance.Effect = effectRef ? effectRef->Path.GetId() : 0;
	}
	else if (Bindings.IndexOf([&](const FeUiDataBinding& o) -> bool { return &o == propertyPtr; }) != FE_INVALID_ID)
	{
		RefreshCurrentWidgetDataBinding();
	}
	else
	{
		FeTraversalPropertyBase* prop = GetTraversalPropertyFromSerializedPtr(propertyPtr);

		if (!prop)
			prop = GetTraversalPropertyFromTransientPtr(propertyPtr);
		else
			prop->PostDeSerialized();

		if (prop)
		{
			if (prop->GetIsPropagated())
			{
				uint32 Id = prop->Id;

				// Propagate property  to children
				Traverse([&, this](FeTraversalState& traversal) -> void
				{
					FeTraversalPropertyBase* propChild = traversal.Current->GetTraversalPropertyFromId(Id);

					if (propChild)
						propChild->OnTransientChanged(true);
				});
			}
			else
			{
				prop->OnTransientChanged(false);
			}
		}
	}
}
bool FeWidget::IsDataDirty() const
{
	return bIsDataDirty;
}
void FeWidget::SetPixelScale(FeVector3 value)
{
	float factors[3];
	ComputePixelConvertFactors(this, factors, 1.0f);

	Transform.Scale[0] = value[0] / factors[0];
	Transform.Scale[1] = value[1] / factors[1];
	Transform.Scale[2] = value[2] / factors[2];
}
FeVector3 FeWidget::GetPixelScale()
{
	float factors[3];
	ComputePixelConvertFactors(this, factors, 1.0f);	

	FeVector3 result(factors[0] * Transform.Scale[0], factors[1] * Transform.Scale[1], factors[2] * Transform.Scale[2]);
	return result;
}
void FeWidget::GetGuid(FeNamedGUID& output) const
{
	output = ID;
	output.Guid = ID.Guid;
}
const FeNamedGUID& FeWidget::GetGuid() const
{
	return ID;
}
void FeUiImage::PostInitialize()
{
	Super::PostInitialize();
	OnPropertyImageChanged();
}
void FeUiImage::OnPropertyChanged(void* propertyPtr)
{
	Super::OnPropertyChanged(propertyPtr);

	if (propertyPtr == &Image.Path)
	{
		OnPropertyImageChanged();
	}
}
void FeUiImage::OnPropertyImageChanged()
{
	const auto pImg = FeModuleUi::Get()->AssetsRegistry.GetAsset<FeImage>(Image);
	
	GeometryInstance.Textures[0].Desired = 0;

	if (pImg)
	{
		GeometryInstance.Textures[0].Desired = FeModuleUi::Get()->LoadImage(pImg);
	}
}

void FeUiEmulatorViewport::PostInitialize()
{
	Super::PostInitialize();
	static const FeString EmulatorTargetName = "[EMULATOR]";
	GeometryInstance.Textures[0].Desired = EmulatorTargetName.Id();
}

void FeUiTextChar::PostInitialize()
{
	auto effect = GeometryInstance.Effect;
	auto texture = GeometryInstance.Textures[0].Desired;

	Super::PostInitialize();

	GeometryInstance.Effect = effect;
	GeometryInstance.Textures[0].Desired = texture;

}
bool FeUiText::IsDataDirty() const
{
	return bIsDataDirty || bIsTextDirty;
}
void FeUiText::PostInitialize()
{
	Super::PostInitialize();
}
uint32 FeUiText::OnDataChanged()
{
	auto result = Super::OnDataChanged();

	if (FE_SUCCEEDED(result))
	{
		if (bIsTextDirty)
		{
			result = GenerateTextRenderingNodes(Text);
			if (FE_SUCCEEDED(result))
				bIsTextDirty = false;

		}
	}
	return result;
}
void FeUiText::OnPropertyChanged(void* propertyPtr)
{
	Super::OnPropertyChanged(propertyPtr);

	if (propertyPtr == &Text ||
		propertyPtr == &Font)
	{
		bIsTextDirty = true;
	}
}
void FeUiText::OnScaleChanged()
{
	bIsTextDirty = true;
}