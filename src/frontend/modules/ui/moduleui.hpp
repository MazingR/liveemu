#pragma once

#include <common/common.hpp>
#include <common/tarray.hpp>
#include <common/application.hpp>
#include <common/filesystem.hpp>

#include <rendering/commonrenderer.hpp>

#include <commonui.hpp>
#include <widget.hpp>
#include <uicontainer.hpp>
#include <uiscriptfile.hpp>
#include <uisequencer.hpp>

#include <uiuserinputhandler.hpp>

struct FeModuleUiInit : public FeModuleInit
{
	FeString ThemeToLoad;
	bool EnableFileWatcher;

	FeModuleUiInit()
	{
		ThemeToLoad = "Default";
	}
};

namespace FeEUiUserInputDeviceType
{
	enum Type
	{
		Mouse,
		GamePad
	};
}

struct FeNavigationUserFocus
{
	FeTPtr<FeWidget> Ptr;
	float				Duration;
	bool				ToggledLongDuration;
	bool				ToggledVeryLongDuration;

	void ResetDuration()
	{
		Duration = 0.0f;
		ToggledLongDuration = false;
		ToggledVeryLongDuration = false;
	}
	void Reset()
	{
		Ptr.Reset();
		ResetDuration();
	}
};
#define FE_UI_MAX_PLAYER_COUNT 8

class FeUiNavigationHandler
{
	friend class FeModuleUi;
public:
	void UpdateUserFocus(uint32 iUserIndex);
	void Update(const FeDt& fDt);
	void AddUser(uint32 userIndex, FeEUiUserInputDeviceType::Type inputType);
	void OnReloadUi();
	void SetIsEnabled(bool bValue);

	FeUiEventParameters& AddPendingEvent(FeEUiEvent::Type type, FeWidget* target = nullptr, uint32 userIndex = FE_INVALID_ID, uint32 button = FE_INVALID_ID);
	FeTPtr<FeUiUserInputHandler> GetUser(int32 index);
	FeNavigationUserFocus& GetUserFocus(int32 index);

	FeModuleUi* ModuleUi;
private:
	bool bIsEnabled;
	FeTPtr<FeUiUserInputHandler> Users[FE_UI_MAX_PLAYER_COUNT];
	FeNavigationUserFocus UsersFocus[FE_UI_MAX_PLAYER_COUNT];
	FeTArray<FeUiEventParameters> PendingEvents;
};

// -----------------------------------------------------------------------------------------------------------------------

struct FeBindingDataResult
{
	FeTArray<FeUiBindingDataField> Fields;
};
struct FeBindingDataResults
{
	FeTArray<FeBindingDataResult> Entries;
};
class FeModuleUi : public FeModule
{
public:
	static FeModuleUi* Get()
	{
		static FeModuleUi* instance = nullptr;

		if (!instance)
			instance = FeApplication::GetStaticInstance()->GetModule<FeModuleUi>();

		return instance;
	}
	friend class FeUiNavigationHandler;

	virtual uint32 Load(const FeModuleInit*) override;
	virtual uint32 Unload() override;
	virtual uint32 Update(const FeDt& fDt) override;
	virtual char* ComputeDebugOutput(const FeDt& fDt);
	virtual const char* GetName()
	{
		static const char* sName = "Ui";
		return sName;
	}

	uint32 Reload(bool bInitialize=false);
	void RefreshWidgets(bool bOnlyVisible = false);
	uint32 LoadScripts();
	uint32 SaveScripts();
	
	uint32 LoadImage(FeImage* image);

	uint32 FetchBindingSourceData(const FeUiBinding& binding, FeBindingDataResults& results);
	FeUiRootWidget& GetRootWidget(uint32 index);
	FeUiRootWidget* GetRootWidget(FeWidget* widget);
	FeTArray<FeUiRootWidget>& GetRootWidgets() { return RootWidgets; }
	FeUiRootWidget& AddRootWidget(FeAssetFile* assetFile, FeWidget* widget);

	FeUiNavigationHandler& GetNavigationHandler() { return NavigationHandler; }
	FeUiSequencer& GetSequencer() { return Sequencer; }
	void SetViewport(const FeTransform& tranform, const FeRect& rect, bool bForceUpdate=false);
	void SetIsRenderEnabled(bool bValue) { bIsRenderEnabled = bValue; }
	FeUiTheme& GetTheme() { return Theme; }
	void OnViewportChanged();
	const FeTransform& GetGlobalTransform() { return GlobalTransform; }
	FeAssetRegistry AssetsRegistry;
	uint32 LoadResources();
private:
	uint32 LoadUnitTest(uint32 iTest);
	uint32 UpdateUnitTest(uint32 iTest, const FeDt& fDt);
private:
	FeModuleUiInit				ModuleInit;
	
	FeUiTheme					Theme;
	FeTArray<FeUiRootWidget>	RootWidgets;

	FeTransform					GlobalTransform;
	FeRect						GlobalRect;
	FeUiNavigationHandler		NavigationHandler;
	FeUiSequencer				Sequencer;
	bool						bIsRenderEnabled;
};