#include <commonui.hpp>
#include <widget.hpp>
#include <moduleui.hpp>
#include <rendering/modulerenderer.hpp>

void ComputePixelConvertFactors(FeWidget* widget, float* factors, float yFactor /*= 1.0f*/)
{
	static const float nativeDim[] = { 1920, 1080 };

	factors[0] = nativeDim[0];
	factors[1] = yFactor*nativeDim[1];
	factors[2] = 100;

	if (!widget->IsRoot())
	{
		const auto& globalT = FeModuleUi::Get()->GetGlobalTransform();
		const auto& parentT = widget->GetParent()->GetTraversalTransform();
		const auto& rootT = widget->GetRootParent()->GetDesignerTransform();

		factors[0] *= FeGeometryHelper::GetMatrixScaleX(parentT);
		factors[1] *= FeGeometryHelper::GetMatrixScaleY(parentT);

		factors[0] *= 1.0f / globalT.Scale[0];
		factors[1] *= 1.0f / globalT.Scale[1];
	}
}

float FeUiSequence::GetDuration()
{
	if (Duration < 0.0f)
	{
		Duration = 0.0f;

		for (auto interpolation : Interpolations)
		{
			uint32 uiKeyFramesCount = interpolation->GetKeyFramesCount();

			for (uint32 uiKey = 0; uiKey < uiKeyFramesCount; ++uiKey)
			{
				if (FeKeyFrame* key = interpolation->GetKeyFrame(uiKey))
				{
					Duration = FeMath::Max(Duration, key->Time);
				}
			}
		}
	}

	return Duration;
}