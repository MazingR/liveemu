#include <uicontainer.hpp>

#include <moduleui.hpp>
#include <rendering/modulerenderer.hpp>
#include <rendering/renderresourceshandler.hpp>

FeUiContainer::FeUiContainer() : Super()
{
	ContainerChildren.SetHeapId(FE_HEAPID_UI);
	WidgetsTemplateRef = nullptr;
	WidgetsTemplate.Type = FeAssetType::Widget();
}
FeUiContainer::~FeUiContainer()
{
	FE_ASSERT(true, "");
}
void FeUiContainer::OnWidgetsTransformChanged(bool bClearChildren)
{
	if (!WidgetsTemplateRef)
		return;

	auto itemScale = WidgetsTemplateRef->GetTransform().Scale;
	auto itemOffset = WidgetsTransform.Translation;

	const auto& matrix = GetTraversalTransform();
	const auto& rootMatrix= GetRootParent()->GetTraversalTransform();

	float Sx = FeGeometryHelper::GetMatrixScaleX(matrix);
	float Sy = FeGeometryHelper::GetMatrixScaleY(matrix);
	
	float rootSx = FeGeometryHelper::GetMatrixScaleX(rootMatrix);
	float rootSy = FeGeometryHelper::GetMatrixScaleY(rootMatrix);

	// re apply root scale
	itemScale[0] *= rootSx;
	itemScale[1] *= rootSy;

	itemOffset[0] *= rootSx;
	itemOffset[1] *= rootSy;
	
	int32 iColumnsCount = 0;
	int32 iRowsCount = 0;
	{
		float ColumnSize = itemScale[0] + itemOffset[0];
		iColumnsCount = FeMath::Floor(Sx / ColumnSize);
		float Remaining = Sx - ColumnSize*iColumnsCount;
		if (Remaining >= itemScale[0])
			iColumnsCount++;
	}
	{
		float RowSize = itemScale[1] - itemOffset[1];
		iRowsCount = FeMath::Floor(Sy / RowSize);
		float Remaining = Sy - RowSize*iRowsCount;
		if (Remaining >= itemScale[1])
			iRowsCount++;
	}

	// make absolute
	itemScale[0] *= 1.f / Sx;
	itemScale[1] *= 1.f / Sy;
	itemOffset[0] *= 1.f / Sx;
	itemOffset[1] *= 1.f / Sy;

	int32 iChildCount = iRowsCount*iColumnsCount;

	iChildCount = FeMath::Min(iChildCount, 256); // limit

	if (bClearChildren)
		ContainerChildren.Free();

	// create new children widgets
	for (int32 i = ContainerChildren.GetSize(); i < iChildCount; ++i)
	{
		FeTPtr<FeWidget>& newChildPtr = ContainerChildren.Add();
		newChildPtr.Reset();
	}
	int32 iChildIndex = 0;
	char strName[128];

	for (auto& childPtr : ContainerChildren)
	{
		if (iChildCount <= iChildIndex)
		{
			if (childPtr.IsValid())
				childPtr->SetVisible(false);
		}
		else
		{
			int32 iColomn = iChildIndex % iColumnsCount;
			int32 iRow = iChildIndex / iColumnsCount;

			bool bIsNewChild = false;
			if (!childPtr.IsValid())
			{
				FeWidget* pNewContainerChild = (FeWidget*)WidgetsTemplateRef->CreateInstance(FE_HEAPID_UI);
				childPtr.Assign(pNewContainerChild, FE_HEAPID_UI, true);
				bIsNewChild = true;
			}

			if (bIsNewChild)
				childPtr->CopyFrom(WidgetsTemplateRef);

			childPtr->SetVisible(true);

			FeTransform& childTransform = childPtr->GetTransform();
			FeVector3& childScale = childTransform.Scale;
			FeVector3& childTranslate = childTransform.Translation;

			childScale = itemScale;

			childTranslate[0] = iColomn*(itemScale[0]);
			childTranslate[0] += iColomn*(itemOffset[0]);

			childTranslate[2] += iColomn*(itemOffset[2]);

			childTranslate[1] = iRow*(-itemScale[1]);
			childTranslate[1] += iRow*(itemOffset[1]);

			if (bIsNewChild)
			{
				sprintf_s(strName, 128, "[%d] _copy_of (%s)", iChildIndex, WidgetsTemplateRef->ID.Name.CstrNotNull());
				
				childPtr->ID.Name = strName;
				childPtr->SetParent(this);
				childPtr->Traverse([&, this](FeTraversalState& traversal) -> void { traversal.Current->PostInitialize(); });
			}
			else
			{
				childPtr->SetTransientTransform(childTransform);
				childPtr->OnPropertyChangedTransform();
			}
		}
		iChildIndex++;
	}
}
void FeUiContainer::PostInitialize()
{
	Super::PostInitialize();
	OnWidgetsTransformChanged(true);
}
void FeUiContainer::OnParentChanged()
{
	Super::OnParentChanged();

	for (auto& child : ContainerChildren)
		child->SetParent(this);
}
bool FeUiContainer::Traverse(std::function<void(FeTraversalState& traversal)> callback, int32 maxDepth /*= FE_MAX_INT32*/)
{
	bool bCanceled = Super::Traverse(callback, maxDepth);

	if (!bCanceled && maxDepth > 0)
	{
		maxDepth--;

		for (auto& child : ContainerChildren)
		{
			bCanceled = child->Traverse(callback, maxDepth);
			if (bCanceled)
				break;

		}
	}

	return bCanceled;
}
uint32 FeUiContainer::OnDataChanged()
{
	//ContainerChildren.Free();

	bIsDataDirty = true;
	{
		{
			bool bApplyVisibilty = GetWidgetsBindings().IndexOf([](const FeUiDataBinding& p) -> bool {return p.GetApplyVisibility(); }) != FE_INVALID_ID;
			if (bApplyVisibilty)
			{
				for (auto& childPtr : ContainerChildren)
					childPtr->SetVisible(false);
			}
		}

		uint32 iRes = RefreshCurrentWidgetDataBinding();

		if (iRes != FeEReturnCode::Success || !WidgetsTemplateRef)
			return iRes; // << === EARLY RETURN (IsDirty=true)

		// Refresh runtime generated data bound container children
		uint32 dataBindChildrenCount = 0;
		auto pModuleUi = FeApplication::GetStaticInstance()->GetModule<FeModuleUi>();

		// Apply none multiple biniding (same value for all children)
		for (auto& dataBind : GetWidgetsBindings())
		{
			if (false == dataBind.GetMultiple())
			{
				FeBindingDataResults results;
				pModuleUi->FetchBindingSourceData(dataBind.GetSource(), results);

				if (results.Entries.GetSize() == 0) // skip empty results
					continue;

				const FeBindingDataResult& result = results.Entries[0];

				if (results.Entries.GetSize()>0)
				{
					for (auto& child : ContainerChildren)
					{
						// Set each result field to widget property
						uint32 iFieldIdx = 0;
						for (auto& field : result.Fields)
						{
							auto& target = dataBind.GetTargets()[iFieldIdx++];

							iRes = ApplyBindingToTargetProperty(child.Get(), field, target);

							if (iRes != FeEReturnCode::Success)
								return iRes; // << === EARLY RETURN (IsDirty=true)
						}
					}
				}
			}
		}
		// Apply multiple biniding (different value for each child)
		for (auto& dataBind : GetWidgetsBindings())
		{
			if (dataBind.GetMultiple())
			{
				FeBindingDataResults results;
				pModuleUi->FetchBindingSourceData(dataBind.GetSource(), results);

				uint32 iDataEntriesCount = results.Entries.GetSize();

				if (iDataEntriesCount)
				{
					// Create children instances from widget template ref if needed
					uint32 iChildIndex = 0;

					for (auto& entry : results.Entries)
					{

						// Set each result field to widget property
						uint32 iFieldIdx = 0;
						for (auto& field : entry.Fields)
						{
							auto& target = dataBind.GetTargets()[iFieldIdx++];

							if (ContainerChildren.GetSize() <= iChildIndex)
								break;

							auto pChildWidget = ContainerChildren[iChildIndex].Get();
							
							if (dataBind.GetApplyVisibility()) // make widgets appear according to current binding results
								pChildWidget->SetVisible(true);

							iRes = ApplyBindingToTargetProperty(pChildWidget, field, target);

							if (iRes != FeEReturnCode::Success)
								return iRes; // << === EARLY RETURN (IsDirty=true)
						}

						iChildIndex++;
					}

					// compute actual needed children
					if (dataBindChildrenCount < iChildIndex)
						dataBindChildrenCount = iChildIndex;
				}
			}
		}

		for (uint32 iChildIndex = 0; iChildIndex < dataBindChildrenCount; ++iChildIndex)
		{
			if (ContainerChildren.GetSize() <= iChildIndex)
				break;

			uint32 iRes = ContainerChildren[iChildIndex].Get()->RefreshCurrentWidgetDataBinding();
			if (iRes != FeEReturnCode::Success)
				return iRes; // << === EARLY RETURN (IsDirty=true)
		}

		for (uint32 iChildIndex = 0; iChildIndex < ContainerChildren.GetSize(); ++iChildIndex)
		{
			if (iChildIndex < dataBindChildrenCount)
				ContainerChildren[iChildIndex]->SetVisible(true);
		}
	}
	bIsDataDirty = false;
	
	return FeEReturnCode::Success;
}
void FeUiContainer::OnTemplateChanged(bool bKeepTransform /*= true*/, bool bKeepName /*= true*/)
{
	Super::OnTemplateChanged(bKeepTransform, bKeepName);
	auto pModuleUi = FeApplication::GetStaticInstance()->GetModule<FeModuleUi>();

	if (!WidgetsTemplate.Path.IsEmpty())
	{
		bIsDataDirty = true;
		WidgetsTemplateRef = pModuleUi->AssetsRegistry.GetAsset<FeWidget>(WidgetsTemplate);

		FE_ASSERT(WidgetsTemplateRef, "template not found !");
		FE_ASSERT(WidgetsTemplateRef->GetTemplate().Path.IsEmpty(), "template of template !");
	}
}
void FeUiContainer::OnPropertyChanged(void* propertyPtr)
{
	Super::OnPropertyChanged(propertyPtr);

	if (propertyPtr == &WidgetsTransform || propertyPtr == &Transform)
		OnWidgetsTransformChanged(false);
	if (propertyPtr == &WidgetsTransform || propertyPtr == &Transform)
		OnWidgetsTransformChanged(false);
}
void FeUiContainer::OnScaleChanged()
{
	OnWidgetsTransformChanged(false);
}