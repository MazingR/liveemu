#include <uisequencer.hpp>
#include <widget.hpp>
#include <geometry.hpp>

void FeUiPropertyInterpolation::ComputeTargetPropertyPtr(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolInstance) const
{
	void* ptr = nullptr;
	const FeSerializable* subProperty = sequence.Target;

	FeWidget* pTargetWidget = FeWidget::GetWidgetFromPath(sequence.Target, TargetPath);
	
	if (pTargetWidget)
	{
		if (interpolInstance.Transient)
			ptr = pTargetWidget->GetTransientPropertyByName(PropertyName);
		else
			ptr = pTargetWidget->GetPropertyByName(PropertyName);
	}

	interpolInstance.TargetWidget = pTargetWidget;
	interpolInstance.TargetProperty = ptr;
	
}

template<typename KeyType, typename ValueType>
bool ComputeInterpolationKeyFrames(float fTime, const FeTArray<KeyType>& KeyFrames, float& fLerp, ValueType const ** previousFrameValue, ValueType const ** nextFrameValue)
{
	bool bEnded = true;
	*previousFrameValue = nullptr;
	*nextFrameValue = nullptr;

	// todo : compute interpolation
	if (KeyFrames.GetSize() > 1)
	{
		bEnded = false;

		uint32 iPreviousFrameIdx = 0;
		uint32 iNextFrameIdx = 1;

		const KeyType& PreviousFrame = KeyFrames[iPreviousFrameIdx];
		const KeyType& NextFrame = KeyFrames[iNextFrameIdx];

		float fRelativeTime0 = fTime - PreviousFrame.Time;
		float fRelativeTime1 = NextFrame.Time - PreviousFrame.Time;

		if (fRelativeTime0 >= 0.0f) // start interpolation after previous frame
		{
			fLerp = fRelativeTime0 / fRelativeTime1;

			if (fLerp > 1.0f)
			{
				bEnded = true;
				fLerp = 1.0f;
			}

			*previousFrameValue = &PreviousFrame.Value;
			*nextFrameValue = &NextFrame.Value;
		}
	}

	return bEnded;
}

template<typename KeyType, typename ValueType>
bool DoUpdatePropertyInterpolation(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolationData, float fTime, const FeTArray<KeyType>& KeyFrames)
{
	ValueType& TargetProperty = *((ValueType*)interpolationData.TargetProperty);

	float fLerp;
	ValueType const * previousFrameValue;
	ValueType const * nextFrameValue;

	interpolationData.bEnded = ComputeInterpolationKeyFrames(fTime, KeyFrames, fLerp, &previousFrameValue, &nextFrameValue);
	
	if (previousFrameValue && nextFrameValue)
		gmtl::lerp(TargetProperty, fLerp, *previousFrameValue, *nextFrameValue);

	interpolationData.TargetWidget->OnPropertyChanged(interpolationData.TargetProperty);

	return interpolationData.bEnded;
}

bool FeUiPropertyInterpolationFloat::UpdatePropertyInterpolation(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolationData, float fTime) const
{
	if (interpolationData.bEnded)
		return true;

	float& TargetProperty = *((float*)interpolationData.TargetProperty);

	float fLerp;
	float const * previousFrameValue;
	float const * nextFrameValue;

	interpolationData.bEnded = ComputeInterpolationKeyFrames(fTime, KeyFrames, fLerp, &previousFrameValue, &nextFrameValue);

	if (previousFrameValue && nextFrameValue)
		TargetProperty = (1.0f-fLerp)*(*previousFrameValue) + fLerp*(*nextFrameValue);

	interpolationData.TargetWidget->OnPropertyChanged(interpolationData.TargetProperty);

	return interpolationData.bEnded;
}
bool FeUiPropertyInterpolationColor::UpdatePropertyInterpolation(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolationData, float fTime) const
{
	return DoUpdatePropertyInterpolation<FeKeyFrameColor, FeColor>(sequence, interpolationData, fTime, KeyFrames);
}
bool FeUiPropertyInterpolationVector2::UpdatePropertyInterpolation(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolationData, float fTime) const
{
	return DoUpdatePropertyInterpolation<FeKeyFrameVector2, FeVector2>(sequence, interpolationData, fTime, KeyFrames);
}
bool FeUiPropertyInterpolationVector3::UpdatePropertyInterpolation(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolationData, float fTime) const
{
	return DoUpdatePropertyInterpolation<FeKeyFrameVector3, FeVector3>(sequence, interpolationData, fTime, KeyFrames);
}
bool FeUiPropertyInterpolationVector4::UpdatePropertyInterpolation(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolationData, float fTime) const
{
	return DoUpdatePropertyInterpolation<FeKeyFrameVector4, FeVector4>(sequence, interpolationData, fTime, KeyFrames);
}
bool FeUiPropertyInterpolationTransformBase::UpdatePropertyInterpolation(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolationData, float fTime) const
{
	return true;
}
bool FeUiPropertyInterpolationTransform::UpdatePropertyInterpolation(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolationData, float fTime) const
{
	if (interpolationData.bEnded)
		return true;

	FeTransform& Property = *((FeTransform*)interpolationData.TargetProperty);

	const FeMatrix4& matTraversal = sequence.Target->GetTraversalTransform();

	float fScaleX = FeGeometryHelper::GetMatrixScaleX(matTraversal);
	float fScaleY = FeGeometryHelper::GetMatrixScaleY(matTraversal);

	float fLerp;
	FeTransform const * previousFrameValue;
	FeTransform const * nextFrameValue;

	interpolationData.bEnded = ComputeInterpolationKeyFrames(fTime, KeyFrames, fLerp, &previousFrameValue, &nextFrameValue);
	if (previousFrameValue && nextFrameValue)
	{
		FeTransform& relative = *((FeTransform*)interpolationData.TargetDefaultProperty);
		#define LERP_PROPERTY(_prop_) gmtl::lerp(Property._prop_, fLerp, (*previousFrameValue)._prop_, (*nextFrameValue)._prop_)

		LERP_PROPERTY(Translation);
		LERP_PROPERTY(Scale);

		if (Additive)
		{
			Property.Translation += relative.Translation;
			INLINE_OP_VEC3(Property.Scale, Property.Scale, relative.Scale, *);
		}
		interpolationData.TargetWidget->OnPropertyChangedTransform();
	}
	return interpolationData.bEnded;
}

void FeUiPropertyInterpolationTransform::ComputeTargetPropertyPtr(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolInstance) const
{
	static FeString PropertyName = "TransientTransform";
	static FeString DefaultPropertyName = "Transform";

	void* ptr = nullptr;
	const FeSerializable* subProperty = sequence.Target;

	FeWidget* pTargetWidget = FeWidget::GetWidgetFromPath(sequence.Target, TargetPath);

	if (pTargetWidget)
	{
		interpolInstance.TargetProperty = pTargetWidget->GetTransientPropertyByName(PropertyName);
		interpolInstance.TargetDefaultProperty = pTargetWidget->GetTransientPropertyByName(DefaultPropertyName);
	}

	interpolInstance.TargetWidget = pTargetWidget;
	
}