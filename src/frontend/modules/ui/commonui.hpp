#pragma once

#include <rendering/effect.hpp>
#include <common/serializable.hpp>
#include <common/maths.hpp>
#include <common/guid.hpp>

class FeWidget;
class FeScriptFile;
class FeAssetFile;

#define FE_HEAPID_UI 4
#define FE_HEAPNAME_UI "Ui"

typedef FeTArray<FeNamedGUID> FeWidgetPath;

void ComputePixelConvertFactors(FeWidget* widget, float* factors, float yFactor = 1.0f);

struct FeAssetType
{
	static const FeString& Theme	() { static FeString name="Theme";		return name; }
	static const FeString& Image	() { static FeString name="Image";		return name; }
	static const FeString& Font		() { static FeString name="Font";		return name; }
	static const FeString& Effect	() { static FeString name="Effect";		return name; }
	static const FeString& Widget	() { static FeString name="Widget";		return name; }
	static const FeString& AssetFile() { static FeString name="AssetFile";	return name; }
	static const FeString& Sequence	() { static FeString name="Sequence";	return name; }
	static const FeString& None		() { static FeString name="None";		return name; }

	static void GetCreatableTypes(FeTArray<const FeString*>& output)
	{
		//output.Add(&Image());  only allowed by import of image
		output.Add(&Font());
		output.Add(&Effect());
		output.Add(&Widget());
		output.Add(&Sequence());
	}
};

namespace FeEWidgetState
{
	enum Type
	{
		Visible = 1 << 0,
		Enabled = 1 << 1,
		Collapsed = 1 << 2,
	};
};

#define FeEUiOrientation_Values(_d)		\
		_d(FeEUiOrientation, Horizontal)	\
		_d(FeEUiOrientation, Vertical)
FE_DECLARE_ENUM(FeEUiOrientation, FeEUiOrientation_Values)

#define FeEUiEvent_Values(_d)				\
	_d(FeEUiEvent, None						)\
	_d(FeEUiEvent, Cursor_Moved				)\
	_d(FeEUiEvent, Cursor_Enter				)\
	_d(FeEUiEvent, Cursor_Hover_Long		)\
	_d(FeEUiEvent, Cursor_Hover_Very_Long	)\
	_d(FeEUiEvent, Cursor_Leave				)\
	_d(FeEUiEvent, Cursor_Leave_Long		)\
	_d(FeEUiEvent, Cursor_Leave_Very_Long	)\
	_d(FeEUiEvent, Button_Pressed			)\
	_d(FeEUiEvent, Button_Released			)\
	_d(FeEUiEvent, Widget_Focus				)\
	_d(FeEUiEvent, Widget_UnFocus			)\
	_d(FeEUiEvent, Widget_Enable			)\
	_d(FeEUiEvent, Widget_Disable			)\
	_d(FeEUiEvent, Widget_Show				)\
	_d(FeEUiEvent, Widget_Hide				)\
	_d(FeEUiEvent, Page_Enter				)\
	_d(FeEUiEvent, Page_Leave				)\

FE_DECLARE_ENUM(FeEUiEvent, FeEUiEvent_Values)

struct FeUiEventParameters
{
	FeEUiEvent::Type Type;
	FeWidget* Target;
	uint32 UserIndex;
	uint32 Button;
	bool bHandled;

	FeUiEventParameters()
	{
		bHandled = false;
		Type = FeEUiEvent::None;
		Target = nullptr;
		UserIndex = FE_INVALID_ID;
		Button = FE_INVALID_ID;
	}
};

#define FeEUiBindingType_Values(_d)		\
		_d(FeEUiBindingType, Variable)	\
		_d(FeEUiBindingType, Database)	\
		_d(FeEUiBindingType, Text)		\
		_d(FeEUiBindingType, Asset_Image)\
		_d(FeEUiBindingType, Unknown)\

FE_DECLARE_ENUM(FeEUiBindingType, FeEUiBindingType_Values)

struct FeUiBindingDataField
{
	FeString Value;

	void* Pointer;
	FeEUiBindingType::Type PointerType;
	FeUiBindingDataField() : Pointer(nullptr), PointerType(FeEUiBindingType::Unknown) {}
};

class FeUiBinding : public FeSerializable
{
public:
#define FeUiBinding_Properties(_d)				\
		_d(FeTArray<FeNamedGUID>,	Path)		\
		_d(FeString,				Property)	\
		_d(int32,					Index)		\
		_d(FeString,				Value)		\
		_d(FeAssetRef,				Asset)		\
		_d(FeEUiBindingType::Type,	Type)		\


	FeUiBinding() : Index(0), Type(FeEUiBindingType::Unknown)
	{}

	FE_DECLARE_CLASS_BODY(FeUiBinding_Properties, FeUiBinding, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeUiBinding)

class FeUiDataBinding : public FeSerializable
{
public:

#define FeUiDataBinding_Properties(_d)		\
		_d(FeUiBinding,				Source)	\
		_d(FeTArray<FeUiBinding>,	Targets)\
		_d(bool,					Multiple)\
		_d(bool,					ApplyVisibility)\


	FeUiDataBinding() : Multiple(false), ApplyVisibility(false) {}

	FE_DECLARE_CLASS_BODY(FeUiDataBinding_Properties, FeUiDataBinding, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeUiDataBinding)

class FeUiPropertyInterpolation;
class FeUiEventSequence;


struct FeInterpolatationInstance
{
	FeWidget*							TargetWidget;
	void*								TargetProperty;
	const void *						TargetDefaultProperty;
	const FeUiPropertyInterpolation*	Interpolation;
	bool								bEnded;
	bool								Transient;

	void Clear()
	{
		Transient = true;
		TargetWidget			= nullptr;
		TargetProperty			= nullptr;
		TargetDefaultProperty	= nullptr;
		Interpolation			= nullptr;
		bEnded					= false;
	}

	FeInterpolatationInstance() : TargetWidget(nullptr), Interpolation(nullptr), TargetDefaultProperty(nullptr), TargetProperty(nullptr), bEnded(false){}
};
struct FeUiSequenceInstance
{
	FeUiEventSequence*					EventSequence;
	FeWidget*							Target;
	float								Time;
	FeTArray<FeInterpolatationInstance>	Interpolations;
	bool								bEnded;
	bool								bCanceled;

	FeUiSequenceInstance() : bCanceled(false), Target(nullptr), EventSequence(nullptr), Time(0.0f), bEnded(false){}
};

#define FeEInterpolation_Values(_d)		\
		_d(FeEInterpolation, None)		\
		_d(FeEInterpolation, Linear)	\

FE_DECLARE_ENUM(FeEInterpolation, FeEInterpolation_Values)


class FeKeyFrame : public FeSerializable
{
public:

#define FeKeyFrame_Properties(_d)			\
	_d(float,					Time)			\
	_d(FeEInterpolation::Type,	Interpolation)	\

	int32 Frame;

	FeKeyFrame() : Frame(-1) {}
	virtual void SetValue(const void* val) {}

	FE_DECLARE_CLASS_BODY(FeKeyFrame_Properties, FeKeyFrame, FeSerializable)
};


class FeUiPropertyInterpolation : public FeSerializable
{
public:

	#define FeUiPropertyInterpolation_Properties(_d)	\
		_d(FeString,					PropertyName)	\
		_d(FeTArray<FeNamedGUID>,		TargetPath)		\


	virtual void ComputeTargetPropertyPtr(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolInstance) const;
	virtual uint32 GetKeyFramesCount() const
	{ 
		return 0; 
	}
	virtual bool UpdatePropertyInterpolation(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolationData, float fTime) const 
	{ 
		return true; 
	}
	virtual FeKeyFrame* AddKeyFrame(float time) 
	{
		return nullptr;
	}
	virtual FeKeyFrame* GetKeyFrame(uint32 index)
	{
		return nullptr;
	}
	virtual void SortKeyFrames() {}

	FE_DECLARE_CLASS_BODY(FeUiPropertyInterpolation_Properties, FeUiPropertyInterpolation, FeSerializable)

};
FE_DECLARE_CLASS_BOTTOM(FeUiPropertyInterpolation)

class FeUiSequence : public FeAsset
{
public:

#define FeUiSequenceInstance_Properties(_d)							\
	_d(FeTArray<FeTPtr<FeUiPropertyInterpolation>>,	Interpolations)	\
	_d(FeAssetRef,									Widget)	\

	FeUiSequence()
	{
		Widget.Type = FeAssetType::Widget();
		Type = FeAssetType::Sequence();
	}

	float GetDuration();

	void OnKeyFramesChanged() { Duration = 0.f;}

	FE_DECLARE_CLASS_BODY(FeUiSequenceInstance_Properties, FeUiSequence, FeAsset)

protected:
	float Duration = -1.0f;
};
FE_DECLARE_CLASS_BOTTOM(FeUiSequence)

class FeUiEventSequence : public FeSerializable
{
public:

#define FeUiEventSequence_Properties(_d)					\
	_d(FeEUiEvent::Type,					EventType)		\
	_d(FeAssetRef,							Sequence)		\
	_d(bool,								bReversed)		\

	FeUiEventSequence()
	{
		bReversed = false;
		Sequence.Type = FeAssetType::Sequence();
	}

	FE_DECLARE_CLASS_BODY(FeUiEventSequence_Properties, FeUiEventSequence, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeUiEventSequence)

struct FeUiRootWidget
{
	FeRenderBatch		RenderBatch;
	FeAssetFile*		AssetFile = nullptr;
	FeWidget*			Root = nullptr;
	bool				IsVisible;

	FeUiRootWidget();
};
