#pragma once

#include <rendering/effect.hpp>
#include <common/serializable.hpp>
#include <common/maths.hpp>
#include <common/asset.hpp>
#include <widget.hpp>

class FeFont : public FeAsset
{
public:
	FeFont()
	{
		Type = FeAssetType::Font();
		Size = 32;
		Space = 16;
		Interval = 1;
		LineSpace = 2;
		Effect.Type = FeAssetType::Effect();
	}
	#define props(_d)				\
	_d(FePath,		TrueTypeFile)	\
	_d(FeAssetRef,	Effect)			\
	_d(int32,		Size)			\
	_d(int32,		Space)			\
	_d(int32,		Interval)		\
	_d(int32,		LineSpace)		\

	FE_DECLARE_CLASS_BODY(props, FeFont, FeAsset)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeFont)


class FeImage : public FeAsset
{
public:
	FeImage()
	{
		Type = FeAssetType::Image();
		ResourceID = 0;
	}
	uint32 GetResourceId();

	#define props(_d)		\
	_d(FePath,		File)	\

	#define propsN(_d)		\
	_d(uint32,	ResourceID)	\

	FE_DECLARE_CLASS_BODY_EX(props, propsN, FeImage, FeAsset)

#undef props
#undef propsN
};
FE_DECLARE_CLASS_BOTTOM(FeImage)

class FeUiThemeRootWidgets : public FeSerializable
{
public:
	FeUiThemeRootWidgets()
	{
		Start.Type		= FeAssetType::Widget();
		Home.Type		= FeAssetType::Widget();
		Start.Type		= FeAssetType::Widget();
		UserLogin.Type	= FeAssetType::Widget();
	}
	#define props(_d)					\
	_d(FeAssetRef,				Start)		\
	_d(FeAssetRef,				Home)		\
	_d(FeAssetRef,				UserLogin)	\
	_d(FeTArray<FeAssetRef>,	Cursors)	\

	FE_DECLARE_CLASS_BODY(props, FeUiThemeRootWidgets, FeSerializable)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeUiThemeRootWidgets)

class FeUiTheme : public FeAsset
{
public:
	FeUiTheme()
	{
		DefaultEffect.Type = FeAssetType::Effect();
	}
	#define props(_d)	\
	_d(FeUiThemeRootWidgets,RootWidgets)\
	_d(FeAssetRef,			DefaultEffect)\

	FePath Directory;
	FePath Path;
	
	
	FE_DECLARE_CLASS_BODY(props, FeUiTheme, FeAsset)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeUiTheme)



class FeScrapperConfigFile : public FeSerializable
{
public:
#define props(_d)								\
	_d(bool,				Enable)				\
	_d(bool,				EnableFileWatch)	\
	_d(bool,				ShowLocalImports)	\
	_d(bool,				UseTwinPlatforms)	\

	FE_DECLARE_CLASS_BODY(props, FeScrapperConfigFile, FeSerializable)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeScrapperConfigFile)

class FeConfigFile : public FeSerializable
{
public:
	#define props(_d)						\
	_d(FeScrapperConfigFile,	Scrapper)	\

	FE_DECLARE_CLASS_BODY(props, FeConfigFile, FeSerializable)
#undef props
};
FE_DECLARE_CLASS_BOTTOM(FeConfigFile)
