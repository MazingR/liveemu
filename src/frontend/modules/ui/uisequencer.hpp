#pragma once

#include <commonui.hpp>
#include <widget.hpp>

struct FeESequencerState
{
	enum Type
	{
		Play,
		Pause,
		Stop
	};
};

#define FE_DECLARE_KEYFRAME_CLASS(_name_, props, _valType_)		\
class FeKeyFrame##_name_ : public FeKeyFrame					\
{																\
public:															\
	virtual void SetValue(const void* val) 						\
	{															\
		Value = *(( _valType_ *)val);							\
	}															\
																\
	FE_DECLARE_CLASS_BODY(props, FeKeyFrame##_name_, FeKeyFrame)\
};																\

#define props(_d)	_d(float,	Value)
FE_DECLARE_KEYFRAME_CLASS(Float, props, float)
#undef props

#define props(_d)	_d(FeColor,	Value)
FE_DECLARE_KEYFRAME_CLASS(Color, props, FeColor)
#undef props

#define props(_d)	_d(FeVector2,	Value)
FE_DECLARE_KEYFRAME_CLASS(Vector2, props, FeVector2)
#undef props

#define props(_d)	_d(FeVector3,	Value)
FE_DECLARE_KEYFRAME_CLASS(Vector3, props, FeVector3)
#undef props

#define props(_d)	_d(FeVector4,	Value)
FE_DECLARE_KEYFRAME_CLASS(Vector4, props, FeVector4)
#undef props

#define props(_d)	_d(FeTransform,	Value)
FE_DECLARE_KEYFRAME_CLASS(Transform, props, FeTransform)
#undef props

template<typename T>
void SortKeyFramesArray(FeTArray<T>& keyFrames)
{
	auto compareKeys = [](const T& a, const T& b) -> int
	{
		if (a.Time < b.Time)		return 1;
		else if (a.Time > b.Time)	return 2;
		else						return 0;
	};

	FeArrayHelper::QuickSort<T>::Execute(keyFrames, compareKeys);
}

#define FE_CREATE_PROPERTY_CLASS(name, properties)																		\
class name : public FeUiPropertyInterpolation																			\
{																														\
public:																													\
	uint32 GetKeyFramesCount() const override { return KeyFrames.GetSize(); }											\
	FeKeyFrame* AddKeyFrame(float time) { auto& key = KeyFrames.Add(); key.Time = time; return &key; }	\
	virtual bool UpdatePropertyInterpolation(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolationData, float fTime) const override;	\
	virtual FeKeyFrame* GetKeyFrame(uint32 index)																		\
	{																													\
		return index<KeyFrames.GetSize() ? &KeyFrames[index] : nullptr;													\
	}																													\
	virtual void SortKeyFrames() { SortKeyFramesArray(KeyFrames); }														\
																														\
	FE_DECLARE_CLASS_BODY(properties, name, FeUiPropertyInterpolation)													\
};																														\
FE_DECLARE_CLASS_BOTTOM(name)																							\

#define props_FeUiPropertyInterpolationFloat(_d)	_d(FeTArray<FeKeyFrameFloat>,		KeyFrames)
FE_CREATE_PROPERTY_CLASS(FeUiPropertyInterpolationFloat, props_FeUiPropertyInterpolationFloat)

#define props_FeUiPropertyInterpolationColor(_d)	_d(FeTArray<FeKeyFrameColor>,		KeyFrames)
FE_CREATE_PROPERTY_CLASS(FeUiPropertyInterpolationColor, props_FeUiPropertyInterpolationColor)

#define props_FeUiPropertyInterpolationVector2(_d)	_d(FeTArray<FeKeyFrameVector2>,	KeyFrames)
FE_CREATE_PROPERTY_CLASS(FeUiPropertyInterpolationVector2, props_FeUiPropertyInterpolationVector2)

#define props_FeUiPropertyInterpolationVector3(_d)	_d(FeTArray<FeKeyFrameVector3>,	KeyFrames)
FE_CREATE_PROPERTY_CLASS(FeUiPropertyInterpolationVector3, props_FeUiPropertyInterpolationVector3)

#define props_FeUiPropertyInterpolationVector4(_d)	_d(FeTArray<FeKeyFrameVector4>,	KeyFrames)
FE_CREATE_PROPERTY_CLASS(FeUiPropertyInterpolationVector4, props_FeUiPropertyInterpolationVector4)

#define props_FeUiPropertyInterpolationTransformBase(_d)_d(FeTArray<FeKeyFrameTransform>,	KeyFrames)
FE_CREATE_PROPERTY_CLASS(FeUiPropertyInterpolationTransformBase, props_FeUiPropertyInterpolationTransformBase)

class FeUiPropertyInterpolationTransform : public FeUiPropertyInterpolationTransformBase
{
public:

	#define FeUiPropertyInterpolationTransform_Properties(_d)\
	_d(bool,	Additive)									\

	bool UpdatePropertyInterpolation(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolationData, float fTime) const override;
	virtual void ComputeTargetPropertyPtr(FeUiSequenceInstance& sequence, FeInterpolatationInstance& interpolInstance) const override;
	
	FeUiPropertyInterpolationTransform()
	{
		Additive = true;
	}
	
	FE_DECLARE_CLASS_BODY(FeUiPropertyInterpolationTransform_Properties, FeUiPropertyInterpolationTransform, FeUiPropertyInterpolationTransformBase)
};
FE_DECLARE_CLASS_BOTTOM(FeUiPropertyInterpolationTransform)

class FeUiSequencer
{
public:
	FeUiSequencer() : Paused(false), Time(0.0f), State(FeESequencerState::Play) {}

	void PlayWidgetEventSequence(FeWidget* target, FeEUiEvent::Type eventType);
	void Update(const FeDt& fDt);
	void OnReloadUi();
	void SetPaused(bool bValue);

	FeESequencerState::Type			State;
	float							Time;
private:
	FeTArray<FeUiSequenceInstance>	Sequences;
	bool							Paused;
};
