#pragma once

#include <common/common.hpp>
#include <common/application.hpp>
#include <rendering/commonrenderer.hpp>

namespace Awesomium
{
	class WebCore;
	class WebView;
};
class FeRenderLoadingResource;

struct FeModuleWebViewerInit : public FeModuleInit
{
public:
};

class FeModuleWebViewer : public FeModule
{
public:
	virtual uint32 Load(const FeModuleInit*) override;
	virtual uint32 Unload() override;
	virtual uint32 Update(const FeDt& fDt) override;
	virtual char* ComputeDebugOutput(const FeDt& fDt) { return ""; }
	virtual const char* GetName()
	{
		static const char* sName = "WebViewer";
		return sName;
	}
private:
	void LoadUnitTest();

	Awesomium::WebCore* Webcore;
	FeTArray<Awesomium::WebView*> Webviews;
	FeRenderLoadingResource WebViewTextureResource;
	FeRenderBatch RenderBatch;
};