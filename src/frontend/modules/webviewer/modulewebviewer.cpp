#include <modulewebviewer.hpp>

#include "filesystem.hpp"
#include <Awesomium/WebCore.h>
#include <Awesomium/BitmapSurface.h>
#include <Awesomium/STLHelpers.h>

#include <d3dx11include.hpp>
#include <rendering/modulerenderer.hpp>
#include <rendering/renderresourceshandler.hpp>

__declspec(thread) FeStaticString<FE_STRING_SIZE_512> WorkString;

//#define FE_HEAPID_UI 4

//#define URL     "http://www.giantbomb.com/garou-mark-of-the-wolves/3030-10674/"
//#define URL     "https://www.youtube.com/watch?v=Lq1ey4-ewyQ"

//#define HTML "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Lq1ey4-ewyQ\" frameborder=\"0\" allowfullscreen></iframe>"

//const unsigned char* html_str = (unsigned char*)"<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Lq1ey4-ewyQ\" frameborder=\"0\" allowfullscreen></iframe>";
//const unsigned char* html_str = (unsigned char*)"<h1>Hello World</h1>";
//
//class MyDataSource : public Awesomium::DataSource
//{
//public:
//	MyDataSource() { }
//	//virtual ~MyDataSource{ }
//
//	virtual void OnRequest(int request_id, const Awesomium::ResourceRequest& request, const Awesomium::WebString& path)
//	{
//		if (path == Awesomium::WSLit("index.html"))
//		{
//			SendResponse(request_id, strlen((char*)html_str), html_str, Awesomium::WSLit("text/html"));
//		}
//	}
//};

void FeModuleWebViewer::LoadUnitTest()
{
	auto ResourcesHandler = FeModuleRenderResourcesHandler::Get();
	
	//Awesomium::WebSession* web_session = Webcore->CreateWebSession(Awesomium::WSLit("."), Awesomium::WebPreferences());
	//Awesomium::DataSource* data_source = new MyDataSource();
	//
	//web_session->AddDataSource(Awesomium::WSLit("MyApplication"), data_source);

	const uint32 width = 640;
	const uint32 height = 400;

	FePath filePath = "test/html/01.html";
	FeFileTools::FormatFullPath(filePath, WorkString);
	Awesomium::WebURL url(Awesomium::WSLit(WorkString.Value));

	Awesomium::WebView*  Webview = Webcore->CreateWebView(width, height, 0, Awesomium::WebViewType::kWebViewType_Offscreen);

	if (Webview)
	{
		Webview->set_parent_window(0);
		Webview->SetTransparent(true);

		Webview->LoadURL(url);
		//Webview->LoadURL(Awesomium::WebURL(Awesomium::WSLit("asset://MyApplication/index.html")));

		WebViewTextureResource.Resource = FeRenderLoadingResource::AllocateResourceT<FeRenderWritableTexture>();
		WebViewTextureResource.Type = FeEResourceType::RenderWritableTexture;
		WebViewTextureResource.Path.Source = "webview_0";

		auto* ResourceData = (FeRenderWritableTexture*)WebViewTextureResource.Resource;

		ResourceData->Width = width;
		ResourceData->Height = height;
		ResourceData->Format = FeEWritableTextureFormat::B8G8R8A8;

		ResourcesHandler->LoadResource(WebViewTextureResource); 

		FeRenderGeometryInstance* geometry = FE_NEW(FeRenderGeometryInstance, FE_HEAPID_RENDERER);
		RenderBatch.GeometryInstances.Add(geometry);

		geometry->Reset();
		geometry->Effect = FeStringTools::GenerateUIntIdFromString("Effect_Default");
		geometry->Geometry = FeGeometryHelper::GetStaticGeometry(FeEGemetryDataType::Quad);
		geometry->Textures[0].Desired = WebViewTextureResource.Id;

		FeVector3	t(0, 0, -1.0f);
		FeRotation	r(0, 0, 0);
		FeVector3	s(1, 1, 1);

		FeGeometryHelper::ComputeMatrix(geometry->Transform, t, r, s);

		Webviews.Add(Webview);
	}
}
uint32 FeModuleWebViewer::Load(const FeModuleInit* initBase)
{
	auto init = (FeModuleWebViewerInit*)initBase;
	Webcore = Awesomium::WebCore::Initialize(Awesomium::WebConfig());


	auto Renderer = FeModuleRendering::Get();
	RenderBatch.Viewport.UseBackBuffer = true;

	//LoadUnitTest();

	return FeEReturnCode::Success;
}
uint32 FeModuleWebViewer::Unload()
{
	for (auto Webview : Webviews)
	{
		Webview->Stop();
		Webview->Destroy();
	}
	Webviews.Clear();

	Awesomium::WebCore::Shutdown();

	return FeEReturnCode::Success;
}
uint32 FeModuleWebViewer::Update(const FeDt& fDt)
{
	auto Renderer = FeModuleRendering::Get();

	if (Webcore)
	{
		for (auto Webview : Webviews)
		{
			if (Webview)// && !Webview->IsLoading())
			{
				Awesomium::Surface* Surface = Webview->surface();
				Awesomium::BitmapSurface * surface = static_cast<Awesomium::BitmapSurface*>(Surface);

				if (surface)
				{
					auto ResourcesHandler = FeModuleRenderResourcesHandler::Get();

					auto Resource = ResourcesHandler->GetResource(WebViewTextureResource.Id);
					if (Resource && Resource->LoadingState == FeEResourceLoadingState::Loaded)
					{
						auto D3DContext = Renderer->GetDevice().GetImmediateContext();
						ID3D11Texture2D* Texture2D;
						auto hr = Resource->GetD3DResource(0)->QueryInterface<ID3D11Texture2D>(&Texture2D);

						D3D11_MAPPED_SUBRESOURCE textureMap;
						hr = D3DContext->Map(Texture2D, 0, D3D11_MAP_WRITE_DISCARD, 0, &textureMap);
						char* pOutput = (char*)textureMap.pData;

						memcpy_s(pOutput, textureMap.RowPitch*surface->height(), surface->buffer(), surface->row_span()*surface->height());
						D3DContext->Unmap(Texture2D, 0);
					}
				}
			}

		}
		Webcore->Update();
	}

	Renderer->RegisterRenderBatch(&RenderBatch);

	return FeEReturnCode::Success;
}
