#pragma once

#include <common/common.hpp>
#include <common/serializable.hpp>
#include <common/network.hpp>
#include <common/application.hpp>

class FeDataGameDump;

#define FeEEmulatorNetMessage_Values(_d)	\
		_d(FeEEmulatorNetMessage, None)		\
		_d(FeEEmulatorNetMessage, Pause)	\
		_d(FeEEmulatorNetMessage, Resume)	\
		_d(FeEEmulatorNetMessage, Stop)		\
		_d(FeEEmulatorNetMessage, HardReset)\
		_d(FeEEmulatorNetMessage, SoftReset)\
		_d(FeEEmulatorNetMessage, LoadGame)	\
		_d(FeEEmulatorNetMessage, SaveState)\
		_d(FeEEmulatorNetMessage, LoadState)\
		_d(FeEEmulatorNetMessage, SetFocus)\

FE_DECLARE_ENUM(FeEEmulatorNetMessage, FeEEmulatorNetMessage_Values)

class FeEmulatorNetMessage : public FeSerializable
{
public:
	#define FeEmulatorNetMessage_Props(_d)		\
		_d(FeEEmulatorNetMessage::Type,	Type)	\


	FeEmulatorNetMessage()
	{
		Type = FeEEmulatorNetMessage::None;
	}

	FE_DECLARE_CLASS_BODY(FeEmulatorNetMessage_Props, FeEmulatorNetMessage, FeSerializable)
};
FE_DECLARE_CLASS_BOTTOM(FeEmulatorNetMessage); // declare factory

class FeEmulatorNetMsgSetFocus : public FeEmulatorNetMessage
{
public:
#define FeEmulatorNetMsgSetFocus_Props(_d)	\
		_d(FeEEmulatorNetMessage::Type,	Type)	\
		_d(bool,	Value)	\


	FeEmulatorNetMsgSetFocus()
	{
		Value = false;
		Type = FeEEmulatorNetMessage::SetFocus;
	}

	FE_DECLARE_CLASS_BODY(FeEmulatorNetMsgSetFocus_Props, FeEmulatorNetMsgSetFocus, FeEmulatorNetMessage)
};
FE_DECLARE_CLASS_BOTTOM(FeEmulatorNetMsgSetFocus); // declare factory

class FeEmulatorNetMsgLoadGame : public FeEmulatorNetMessage
{
public:
	#define FeEmulatorNetMsgLoadGame_Props(_d)	\
		_d(FeEEmulatorNetMessage::Type,	Type)	\
		_d(FeString,	RomPath)	\


	FeEmulatorNetMsgLoadGame()
	{
		Type = FeEEmulatorNetMessage::LoadGame;
	}

	FE_DECLARE_CLASS_BODY(FeEmulatorNetMsgLoadGame_Props, FeEmulatorNetMsgLoadGame, FeEmulatorNetMessage)
};
FE_DECLARE_CLASS_BOTTOM(FeEmulatorNetMsgLoadGame); // declare factory

class FeEmulatorNetMsgLoadSaveState : public FeEmulatorNetMessage
{
public:
	#define FeEmulatorNetMsgLoadSaveState_Props(_d)	\
		_d(uint32,	Slot)							\

	FE_DECLARE_CLASS_BODY(FeEmulatorNetMsgLoadSaveState_Props, FeEmulatorNetMsgLoadSaveState, FeEmulatorNetMessage)
};
FE_DECLARE_CLASS_BOTTOM(FeEmulatorNetMsgLoadSaveState); // declare factory


class FeEmulatorNetMessageContainer : public FeSerializable
{
public:
	#define FeEmulatorNetMessageContainer_Props(_d)		\
		_d(FeTPtr<FeEmulatorNetMessage>, Message)	\

	FE_DECLARE_CLASS_BODY(FeEmulatorNetMessageContainer_Props, FeEmulatorNetMessageContainer, FeSerializable)
};

struct FeEEmulatorProcessState
{
	enum Type
	{
		_0_NotRunning,
		_1_Starting,
		_2_LoadingRenderTargetTexture,
		_3_WaitingTcpConnection,
		_4_Running,
	};
};
class FeNetMessageCommunication
{
public:
	FeNetMessageCommunication();

	uint32 SendOutbox(const FeTcpClientServerConnection& connection);
	uint32 ReadToInbox(const FeTcpClientServerConnection& connection);

	FeTArray<FeEmulatorNetMessageContainer> Inbox;
	FeTArray<FeEmulatorNetMessageContainer> Outbox;

	void* InboxMutex;
	void* OutboxMutex;

private:
	FeStringBuffer NetMsgStringBuffer;

};
class FeIEmulatorCommandHandler
{
public:
	virtual void Pause() = 0;
	virtual void Stop() = 0;
	virtual void LoadGame(const FeDataGameDump& gameDump) = 0;
	virtual void SaveState(int32 iSlot) = 0;
	virtual void LoadState(int32 iSlot) = 0;
};

struct D3dResources
{
	uint64 Device;
	uint64 BackBuffer;
	struct
	{
		uint64 Texture;
		uint64 Surface;
		uint64 Handle;
		int32 Width;
		int32 Height;
	} SharedTarget;

	D3dResources()
	{
		memset(this, 0, sizeof(*this));
	}
};
class FeEmulatorProcessConfig : public FeSerializable
{
public:
#define FeTestEmulatorProcessConfig_Props(_d)	\
	_d(uint64,	SharedTargetHandle)				\
	_d(uint64,	WindowHandle)					\
	_d(uint64,	ModuleHandle)					\
	_d(uint32,	TcpPort)						\
	_d(int32,	SharedTargetWidth)				\
	_d(int32,	SharedTargetHeight)				\
	_d(FeString,FrontEndDirectory)				\

	FE_DECLARE_CLASS_BODY(FeTestEmulatorProcessConfig_Props, FeEmulatorProcessConfig, FeSerializable)
};

class FeEmulatorClient
{
public:
	static FeEmulatorClient* Get();

	void ParseProcessArgs(const FeApplicationInit& appInit);
	void ParseProcessArgs(int32 argc, char *argv[]);

	uint32 Load();
	uint32 Unload();
	uint32 Update(const FeDt& fDt);

	uint32 ConnectToServer();
	void TcpClientFunction(FeEConnectionCallbackType::Type eConnectionType);
	void ReadInboxMessages(std::function<void(const FeEmulatorNetMessageContainer&)> callback);
	void CreateOuboxMessage(std::function<void(FeEmulatorNetMessageContainer&)> callback);
	
	FeEmulatorProcessConfig Config;
	D3dResources D3dResources;
private:

	SDL_mutex* ClientThreadMutex;
	FeTcpClientServerConnection Connection;
	FeNetMessageCommunication Communication;

	bool bStopThread;
	bool bPauseThread;
};
