#pragma once

namespace FeEmulator
{
	void ParseProcessArgs(int argc, char *argv[]);
	unsigned int ConnectToServer();
	unsigned int Startup();
	unsigned long long GetWindowHandle();
	unsigned long long GetFrontEndModuleHandle();
	void RenderTargetGetDimensions(int& width, int& height);
	bool IsFocused();

	namespace D3D9x
	{
		unsigned int RenderTargetUpdate();
		unsigned int RenderTargetHook(unsigned long long device, int width, int height);
	}
}
