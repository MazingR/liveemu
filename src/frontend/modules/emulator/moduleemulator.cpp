#include <moduleemulator.hpp>
#include <emulatorwrapper.hpp>

#include <common/process.hpp>
#include <common/json.hpp>
#include <common/network.hpp>
#include <common/filesystem.hpp>
#include <scraping/commonscraping.hpp>

// Direct3D 9Ex
#include <d3d9.h>
#include <d3dx9.h>
#include <math.h>
#include <DxErr.h>
#pragma comment(lib, "dxerr.lib")

#include <SDL.h>

#define FE_HEAPID_EMULATOR 0

#define CHECK_RETURN_NET(ivalue) { if (FE_FAILED(iResult)) { return;}}

namespace FeEmulator
{
	bool s_bIsFocused = false;

	bool IsFocused()
	{
		return s_bIsFocused;
	}
	unsigned long long GetWindowHandle()
	{
		return (unsigned long long)FeEmulatorClient::Get()->Config.WindowHandle;
	}
	unsigned long long GetFrontEndModuleHandle()
	{
		return (unsigned long long)FeEmulatorClient::Get()->Config.ModuleHandle;
	}
	unsigned int Startup()
	{
		FeStringPool::Initialize();

		return 0;
	}
	void ParseProcessArgs(int argc, char *argv[])
	{
		FeEmulatorClient::Get()->ParseProcessArgs(argc, argv);
	}
	unsigned int ConnectToServer()
	{
		return FeEmulatorClient::Get()->ConnectToServer();
	}
	void RenderTargetGetDimensions(int& width, int& height)
	{
		width = FeEmulatorClient::Get()->Config.SharedTargetWidth;
		height = FeEmulatorClient::Get()->Config.SharedTargetHeight;
	}
	namespace D3D9x
	{
		unsigned int RenderTargetUpdate()
		{
			auto& D3dResources = FeEmulatorClient::Get()->D3dResources;

			if (D3dResources.SharedTarget.Handle == 0 || D3dResources.SharedTarget.Surface == 0)
				return FeEReturnCode::Failed;

			HRESULT hr = S_OK;
			IDirect3DDevice9* d3dDev = (IDirect3DDevice9*)D3dResources.Device;
			IDirect3DSurface9* pSharedSurface = (IDirect3DSurface9*)D3dResources.SharedTarget.Surface;

			hr = IDirect3DDevice9_StretchRect(d3dDev, (IDirect3DSurface9*)D3dResources.BackBuffer, NULL, pSharedSurface, NULL, D3DTEXF_POINT);
			
			if (FAILED(hr))	
				return FeEReturnCode::Failed;	

			return FeEReturnCode::Success;
		}
		unsigned int RenderTargetHook(unsigned long long device, int width, int height)
		{
			auto& D3dResources = FeEmulatorClient::Get()->D3dResources;
			
			D3dResources.Device = (uint64)device;
			D3dResources.SharedTarget.Handle = FeEmulatorClient::Get()->Config.SharedTargetHandle;
			D3dResources.SharedTarget.Width = width;
			D3dResources.SharedTarget.Height = height;

			IDirect3DDevice9* d3dDev = (IDirect3DDevice9 *)device;
			IDirect3DTexture9* pSharedTexture = NULL;
			HRESULT hr = S_OK;

			if (D3dResources.SharedTarget.Handle == 0)
				return FeEReturnCode::Failed;

			
			// Retrieve pointer to Back Buffer texture
			hr = IDirect3DDevice9_GetRenderTarget(d3dDev, 0, (IDirect3DSurface9**)&D3dResources.BackBuffer);
			if (FAILED(hr))
				return FeEReturnCode::Failed;

			// Retrieve shared resource as D3D9 texture interface
			hr = IDirect3DDevice9_CreateTexture(d3dDev,
				D3dResources.SharedTarget.Width,
				D3dResources.SharedTarget.Height,
				1,
				D3DUSAGE_RENDERTARGET,
				D3DFMT_X8R8G8B8,
				//D3DFMT_X8B8G8R8,
				D3DPOOL_DEFAULT,
				(IDirect3DTexture9**)&D3dResources.SharedTarget.Texture,
				(HANDLE*)&D3dResources.SharedTarget.Handle);

			if (FAILED(hr)) 
			{
				FE_ASSERT(false, "Error (%d) : %s\n", DXGetErrorString(hr), DXGetErrorDescription(hr));
				return FeEReturnCode::Failed;
			}

			pSharedTexture = (IDirect3DTexture9*)D3dResources.SharedTarget.Texture;

			hr = IDirect3DTexture9_GetSurfaceLevel(pSharedTexture, 0, (IDirect3DSurface9**)&D3dResources.SharedTarget.Surface);
			if (FAILED(hr))	return FeEReturnCode::Failed;

			return FeEReturnCode::Success;
		}
	}
}


FeNetMessageCommunication::FeNetMessageCommunication()
{
	NetMsgStringBuffer.Heap = FE_HEAPID_FILESYSTEM;
	InboxMutex = SDL_CreateMutex();
	OutboxMutex = SDL_CreateMutex();

	Inbox.Free();
	Inbox.Reserve(2);
}
uint32 FeNetMessageCommunication::SendOutbox(const FeTcpClientServerConnection& connection)
{
	SCOPELOCK((SDL_mutex*)OutboxMutex);
	
	while (Outbox.GetSize())
	{
		auto& msg = Outbox.Back();
		uint32 iResult = FeNetWriteUint(connection.GetComSocket(), 0xFF); // send : magic number
		
		FE_FAILEDRETURN(iResult);

		// Serialize message (json)
		FeStringBuffer sentBuffer;
		FeJson::SerializeToMemory(&msg, sentBuffer, FE_HEAPID_EMULATOR);

		iResult = FeNetWriteString(connection.GetComSocket(), sentBuffer); // send : message as string (for now)
		
		FE_FAILEDRETURN(iResult);

		Outbox.PopBack();
	}

	return FeEReturnCode::Success;
}
uint32 FeNetMessageCommunication::ReadToInbox(const FeTcpClientServerConnection& connection)
{
	// Read messages

	bool bReadMessage = false;

	do
	{
		// Read Magic Number
		uint32 iMagicNumber = 0;

		FE_FAILEDRETURN(FeNetReadUint(connection.GetComSocket(), iMagicNumber));

		if (iMagicNumber == 0xFF)
		{
			if (FeNetReadString(connection.GetComSocket(), NetMsgStringBuffer) != FeEReturnCode::Failed)
			{
				{
					SCOPELOCK((SDL_mutex*)InboxMutex); // <------ Lock Mutex

					FeEmulatorNetMessageContainer& msg = Inbox.Add();
					FeJson::Deserialize(msg, NetMsgStringBuffer.Cstr(), FE_HEAPID_FILESYSTEM);
				} // <------ Unlock Mutex
			}
			NetMsgStringBuffer.Clear();
		}
		else
		{
			FE_ASSERT(false, "Net Server : invalid bytes read");
			break;
		}
	} while (bReadMessage);

	return FeEReturnCode::Success;
}

void TcpClientThreadCallback(FeTcpClientServerConnection* connection, FeEConnectionCallbackType::Type eConnectionType)
{
	auto pThis = (FeEmulatorClient*)connection->UserParam;
	pThis->TcpClientFunction(eConnectionType);
}

FeEmulatorClient* FeEmulatorClient::Get()
{
	static FeEmulatorClient instance;
	return &instance;
}

uint32 FeEmulatorClient::Load()
{
	return FeEReturnCode::Success;
}
uint32 FeEmulatorClient::Unload()
{
	int iReturned = 0;
	return FeEReturnCode::Success;
}
uint32 FeEmulatorClient::Update(const FeDt& fDt)
{
	return FeEReturnCode::Success;
}

void FeEmulatorClient::ParseProcessArgs(const FeApplicationInit& appInit)
{
	FeTArray<FeString> cmdLine;
	FeFetchCommandLineParamsW(appInit.WindowsCmdLine, cmdLine);

	for (auto& param : cmdLine)
	{
		if (param.Cstr()[0] == '{') // detect json object
		{
			FeJson::Deserialize(Config, param.Cstr(), FE_HEAPID_EMULATOR);
			break;
		}
	}
}
void FeEmulatorClient::ParseProcessArgs(int32 argc, char *argv[])
{
	for (int32 i = 0; i < argc; ++i)
	{
		char* arg = argv[i];
		int32 len = strlen(arg);

		if (arg[0] == '{')
		{
			for (int32 i = 0; i < len;++i)
			{
				if (arg[i] == '$')
					arg[i] = '\"';
			}

			FeJson::Deserialize(Config, arg, FE_HEAPID_EMULATOR);
			break;
		}
	}
}

uint32 FeEmulatorClient::ConnectToServer()	
{
	bStopThread = false;
	bPauseThread = false;

	Connection.TcpPort = Config.TcpPort;
	Connection.UserParam = this;
	Connection.ConnectionCallback = TcpClientThreadCallback;
	FeNetCreateClientConnection(Connection);

	return FeEReturnCode::Success;
}
void FeEmulatorClient::TcpClientFunction(FeEConnectionCallbackType::Type eConnectionType)
{
	while (!bStopThread)
	{
		if (eConnectionType == FeEConnectionCallbackType::Read)
		{
			if (FE_FAILED(Communication.ReadToInbox(Connection)))
			{
				break;
			}
		}
		else
		{
			_sleep(16);
			if (FE_FAILED(Communication.SendOutbox(Connection)))
			{
				break;
			}
		}
	}
}
void FeEmulatorClient::ReadInboxMessages(std::function<void(const FeEmulatorNetMessageContainer&)> callback)
{
	{	SCOPELOCK((SDL_mutex*)Communication.InboxMutex); // <------ Lock Mutex

		for (const auto& msg : Communication.Inbox)
		{
			FeEEmulatorNetMessage::Type eMsgType = msg.Message.Get()->Type;

			switch (eMsgType)
			{
				case FeEEmulatorNetMessage::SetFocus:
				{
					auto pMsg = static_cast<FeEmulatorNetMsgSetFocus*>(msg.Message.Get());
					FeEmulator::s_bIsFocused = pMsg->Value;
				}
			}

			callback(msg);
		}
		Communication.Inbox.Clear();
	}  // <------ Unlock Mutex
}
void FeEmulatorClient::CreateOuboxMessage(std::function<void(FeEmulatorNetMessageContainer&)> callback)
{
	{
		SCOPELOCK((SDL_mutex*)Communication.OutboxMutex);
		FeEmulatorNetMessageContainer& container = Communication.Outbox.Add();
		callback(container);
	}  // <------ Unlock Mutex
}