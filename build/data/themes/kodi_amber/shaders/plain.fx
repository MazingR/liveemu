#include "common.fx"

cbuffer cbPerObject : register(b1)
{
	matrix World;
	float4 TextureTransform;
	
	float4 Opacity;
	float4 Luminance;
	float4 BorderColor;
	float4 Color1;
	float4 Color2;
};

#include "simpleVS.fx"

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float4 fColor = Color1;
	
	// fColor.rgb *= Luminance.r;
	// fColor.a *= Opacity.r;
	fColor.a=1.0f-pow(input.Tex.x, 8);
	
	// Draw border
	{
		
		// float2 BorderThickness = 0.005f;
	
		// float2 fBorder = abs(input.Tex-0.5f)*2.0f;
		// BorderThickness *= Viewport.zw*2.0f;
		// BorderThickness.y *= Viewport.x/Viewport.y;
		
		// fBorder = step(1.0-fBorder, BorderThickness);
		
		// float fTime = Time*2.0f;
		// float fBorderAlpha = abs(sin(fTime)+cos(fTime))*0.5f;
		// fBorderAlpha = 1.0f;
		
		// fColor.rgb = lerp(fColor.rgb, BorderColor.rgb, fBorder.x*BorderColor.a*fBorderAlpha);
		// fColor.rgb = lerp(fColor.rgb, BorderColor.rgb, fBorder.y*BorderColor.a*fBorderAlpha);
	}

	return fColor;
}