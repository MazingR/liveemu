#include "common.fx"

cbuffer cbPerObject : register(b1)
{
	matrix World;
	float4 TextureTransform;
	
	float4 Opacity;
	float4 Luminance;
};

#include "simpleVS.fx"
Texture2D		texture0			: register(t0);
SamplerState	samplerTexture0		: register(s0);

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float4 fColor= texture0.Sample(samplerTexture0, input.TexT);
	
	fColor.rgb *= Luminance.r;
	fColor.a = Opacity.r;

	return fColor;
}

