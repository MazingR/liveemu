#include "common.fx"

cbuffer cbPerObject : register(b1)
{
	matrix World;
	float4 TextureTransform;
	
	float4 Opacity;
	float4 FontData;
};

#include "simpleVS.fx"

Texture2D		texture0			: register(t0);

SamplerState	samplerTexture0		: register(s0);

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float2 vCoord = input.Tex;
	vCoord *= FontData.zw;
	vCoord += FontData.xy;
	float4 fColor = texture0.Sample(samplerTexture0, vCoord).r;
	// fColor.a *= Opacity.r;
	// return 1.0f;
	
	// glow test
	// float fTime = Time*0.05f;
	// fColor *= clamp(abs(sin(fTime)),0.8f, 1.0f);
	// fColor = 1.0f;
	
	return fColor;
}