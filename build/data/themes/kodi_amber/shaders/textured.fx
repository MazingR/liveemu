#include "common.fx"

cbuffer cbPerObject : register(b1)
{
	matrix World;
	float4 TextureTransform;
	
	float4 Opacity;
	float4 Luminance;
	float4 BorderColor;
};

#include "simpleVS.fx"
Texture2D		texture0			: register(t0);
SamplerState	samplerTexture0		: register(s0);

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float4 fColor= texture0.Sample(samplerTexture0, input.TexT);
	
	fColor.rgb *= Luminance.r;
	fColor.a *= Opacity.r;
	
	// Draw border
	{
		
		float2 BorderThickness = 0.005f;
		float2 ratio = float2(1.0f / World[0][0], 1.0f / World[1][1]);
		float2 fBorder = abs(input.Tex-0.5f)*2.0f;
		BorderThickness *= ratio*2.0f;
		BorderThickness.y *= Viewport.z/Viewport.w;
		
		fBorder = step(1.0-fBorder, BorderThickness);
		
		float fTime = Time*2.0f;
		float fBorderAlpha = abs(sin(fTime)+cos(fTime))*0.5f;
		fBorderAlpha = 1.0f;
		
		fColor.rgb = lerp(fColor.rgb, BorderColor.rgb, fBorder.x*BorderColor.a*fBorderAlpha);
		fColor.rgb = lerp(fColor.rgb, BorderColor.rgb, fBorder.y*BorderColor.a*fBorderAlpha);
	}

	return fColor;
}

