BEGIN TRANSACTION;
CREATE TABLE "FeDataPlatform" (
	`ID`	INTEGER NOT NULL UNIQUE,
	`Name`	TEXT NOT NULL UNIQUE,
	`Overview`	TEXT,
	PRIMARY KEY(ID)
);
CREATE TABLE "FeDataGamePublisher" (
	`ID`	INTEGER NOT NULL UNIQUE,
	`Name`	TEXT
);
CREATE TABLE "FeDataGameImage" (
	`ID`	INTEGER NOT NULL UNIQUE,
	`Game`	INTEGER,
	`RemotePath`	TEXT,
	`Type`	INTEGER,
	PRIMARY KEY(ID)
);
CREATE TABLE "FeDataGameGenre" (
	`ID`	INTEGER NOT NULL UNIQUE,
	`Name`	TEXT NOT NULL
);
CREATE TABLE `FeDataGameDumpScrap` (
	`ID`	INTEGER NOT NULL UNIQUE,
	`Dump`	INTEGER NOT NULL,
	`Scrapper`	TEXT NOT NULL,
	PRIMARY KEY(ID)
);
CREATE TABLE "FeDataGameDump" (
	`ID`	INTEGER NOT NULL UNIQUE,
	`Name`	TEXT,
	`Checksum`	INTEGER,
	`Game`	INTEGER,
	`CountryCode`	INTEGER,
	`Flags`	INTEGER,
	`LocalPath`	TEXT,
	`IsOnDisk`	REAL,
	PRIMARY KEY(ID)
);
CREATE TABLE "FeDataGameDeveloper" (
	`ID`	INTEGER NOT NULL UNIQUE,
	`Name`	TEXT
);
CREATE TABLE "FeDataGame" (
	`ID`	INTEGER NOT NULL UNIQUE,
	`Name`	TEXT NOT NULL UNIQUE,
	`Platform`	INTEGER,
	`ReleaseDate`	INTEGER,
	`Overview`	TEXT,
	`Adult`	INTEGER,
	`PlayersCount`	INTEGER,
	`PlayersCountSimultaneous`	INTEGER,
	`Rating`	INTEGER,
	PRIMARY KEY(ID,Name,Platform)
);
COMMIT;
