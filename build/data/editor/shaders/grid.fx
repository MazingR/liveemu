
#define BACKGROUND_COLOR float3(0.42f, 0.42f, 0.5f);

#define BACKGROUND_ALPHA 0.25f
#define GRID_A_ALPHA 0.15f
#define GRID_B_ALPHA 0.2f

#define GRID_A_SIZE 128.0f
#define GRID_B_SIZE 32.0f

#include "common.fx"

cbuffer cbPerObject : register(b1)
{
	matrix World;
	float4 TextureTransform;
	
	float4 Color;
	float4 Opacity;
	float4 GridDimensions;
};



#include "simpleVS.fx"

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

float ComputeGrid(float2 fCoord, float fSize)
{
	float2 uvR = fCoord * float2(1920,1080);//Viewport.zw;
	float2 g = step(2.0f,fmod(uvR,fSize));
	return lerp(1,lerp(1,0,g.x),g.y);
}
float4 PS(PS_INPUT input) : SV_Target
{
	float4 fColor = BACKGROUND_ALPHA;
	fColor.rgb = BACKGROUND_COLOR;
	
	fColor.a = lerp(fColor.a, GRID_B_ALPHA, ComputeGrid(input.Tex, GridDimensions.x));
	fColor.a = lerp(fColor.a, GRID_A_ALPHA, ComputeGrid(input.Tex, GridDimensions.x*4.0f));
	

	return fColor;
}