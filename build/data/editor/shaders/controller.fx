#include "common.fx"

cbuffer cbPerObject : register(b1)
{
	matrix World;
	float4 TextureTransform;
	
	float4 Color;
};

#include "simpleVS.fx"

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

float4 PS(PS_INPUT input) : SV_Target
{
	float4 fColor = Color;
	return fColor;
}