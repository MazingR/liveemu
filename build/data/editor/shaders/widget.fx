
#define BORDER_SIZE 0.001f
#define ALPHA 0.8f
#include "common.fx"

cbuffer cbPerObject : register(b1)
{
	matrix World;
	float4 TextureTransform;
	
	float4 BorderColor;
	float4 BackgroundColor;
};


#include "simpleVS.fx"

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float4 fColor = BackgroundColor;
	
	// Draw border
	{
		float2 BorderThickness = BORDER_SIZE;
		
		float2 fBorder = abs(input.Tex-0.5f)*2.0f;
		float2 ratio = float2(1.0f / World[0][0], 1.0f / World[1][1]);
		BorderThickness *= ratio*2.0f;
		BorderThickness.y *= Viewport.z/Viewport.w;
		
		fBorder = step(1.0-fBorder, BorderThickness);
		
		fColor = lerp(fColor, BorderColor, fBorder.x);
		fColor = lerp(fColor, BorderColor, fBorder.y);
		
		fColor.a *= ALPHA;
		
	}

	return fColor;
}