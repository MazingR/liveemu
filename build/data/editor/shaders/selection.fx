#define SPEED 400.0f
#define BORDER_SIZE 0.001f

#include "common.fx"

cbuffer cbPerObject : register(b1)
{
	matrix World;
	float4 TextureTransform;
	
	float4 Color;
	float4 Opacity;
};


#include "simpleVS.fx"

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float4 fColor = float4(0,0,0,0.0f);
	
	// Draw border
	{
		
		float fTime = Time*SPEED;
		float fBlink = clamp(abs(sin(fTime)+cos(fTime)) * 0.5f, 0.2f,1.0f);
		float2 BorderThickness = BORDER_SIZE;
		float4 BorderColor = float4(1.0f, 1.0f, 1.0f, 1.0f);
		
		
		// BorderThickness *=fBlink;
		
		float2 ratio = float2(1.0f / World[0][0], 1.0f / World[1][1]);
		
		float2 fBorder = abs(input.Tex-0.5f)*2.0f;
		BorderThickness *= ratio*2.0f; // xy ratio 
		BorderThickness.y *= Viewport.z/Viewport.w;
		
		fBorder = step(1.0-fBorder, BorderThickness);
		
		fColor = lerp(fColor, BorderColor, fBorder.x);
		fColor = lerp(fColor, BorderColor, fBorder.y);
	}

	return fColor;
}