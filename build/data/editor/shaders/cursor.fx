#define SPEED 400.0f
#define BORDER_SIZE 0.001f

#include "common.fx"

cbuffer cbPerObject : register(b1)
{
	matrix World;
	float4 TextureTransform;
	
	float4 Color;
	float4 Opacity;
};


#include "simpleVS.fx"

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float4 fColor = float4(1,0,0,0.5f);
	return fColor;
}